package org.qnixyz.jbson;

import static org.junit.Assert.assertEquals;
import javax.xml.bind.annotation.XmlEnumValue;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IndexedEnumTest {

  private enum MyEnum {

    @XmlEnumValue("bar")
    BAR("bar"),

    FOO("FOO");

    private final String serializedName;

    private MyEnum(final String serializedName) {
      this.serializedName = serializedName;
    }
  }

  @Test
  public void test() throws Exception {
    final IndexedEnum o = new IndexedEnum(MyEnum.class);
    assertEquals("BAR", MyEnum.BAR.serializedName, o.enumToString(MyEnum.BAR));
    assertEquals("FOO", MyEnum.FOO.serializedName, o.enumToString(MyEnum.FOO));
  }

}
