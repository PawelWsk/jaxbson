package org.qnixyz.jbson;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.junit.Assert.assertEquals;
import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.annotations.cfg.Utils.declaredMethodByName;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.namespace.QName;
import org.bson.Document;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.ReflectiionTestClass;
import org.qnixyz.jbson.annotations.cfg.JaxBsonIgnoreTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonIgnoreTransientImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameTypeMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNumberHintFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNumberHintImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPostImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPostMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPreImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPreMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPostImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPostMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPreImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPreMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonTransientImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingsFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonConfigurationTest {

  @XmlAccessorType(XmlAccessType.FIELD)
  private static class TestClass {

    @SuppressWarnings("unused")
    private Integer numberField;

    @SuppressWarnings("unused")
    private String simpleField;

    private Boolean toBsonPost;

    private Boolean toBsonPostCtx;

    private Boolean toBsonPre;

    private Boolean toBsonPreCtx;

    private Boolean toObjectPost;

    private Boolean toObjectPostCtx;

    private Boolean toObjectPre;

    private Boolean toObjectPreCtx;

    @XmlAnyAttribute
    private Map<QName, String> xmlAnyAttributeField;

    @XmlTransient
    private String xmlTransientField;

    @SuppressWarnings("unused")
    private void testToBsonPost() {
      this.toBsonPost = true;
    }

    @SuppressWarnings("unused")
    private void testToBsonPost(final JaxBsonContext ctx) {
      Objects.requireNonNull(ctx);
      this.toBsonPostCtx = true;
    }

    @SuppressWarnings("unused")
    private void testToBsonPre() {
      this.toBsonPre = true;
    }

    @SuppressWarnings("unused")
    private void testToBsonPre(final JaxBsonContext ctx) {
      Objects.requireNonNull(ctx);
      this.toBsonPreCtx = true;
    }

    @SuppressWarnings("unused")
    private void testToObjectPost() {
      this.toObjectPost = true;
    }

    @SuppressWarnings("unused")
    private void testToObjectPost(final JaxBsonContext ctx) {
      Objects.requireNonNull(ctx);
      this.toObjectPostCtx = true;
    }

    @SuppressWarnings("unused")
    private void testToObjectPre() {
      this.toObjectPre = true;
    }

    @SuppressWarnings("unused")
    private void testToObjectPre(final JaxBsonContext ctx) {
      Objects.requireNonNull(ctx);
      this.toObjectPreCtx = true;
    }
  }

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonConfigurationTest.class);

  @BeforeClass
  public static void setup() {
    XMLUnit.setIgnoreWhitespace(true);
  }

  private JaxBsonIgnoreTransientFieldMap createJaxBsonIgnoreTransientFieldMap() throws Exception {
    final JaxBsonIgnoreTransientFieldMap ret = new JaxBsonIgnoreTransientFieldMap();
    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    ret.put(testField1, new JaxBsonIgnoreTransientImpl());
    return ret;
  }

  private JaxBsonNameFieldMap createJaxBsonNameFieldMap() throws Exception {
    final JaxBsonNameFieldMap ret = new JaxBsonNameFieldMap();
    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    ret.put(testField1, new JaxBsonNameImpl("the-name"));
    return ret;
  }

  private JaxBsonNameTypeMap createJaxBsonNameTypeMap() {
    final JaxBsonNameTypeMap ret = new JaxBsonNameTypeMap();
    final Class<?> testClass1 = ReflectiionTestClass.TEST_CLASS;
    ret.put(testClass1, new JaxBsonNameImpl("the-name"));
    return ret;
  }

  private JaxBsonNumberHintFieldMap createJaxBsonNumberHintFieldMap() throws Exception {
    final JaxBsonNumberHintFieldMap ret = new JaxBsonNumberHintFieldMap();
    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    ret.put(testField1, new JaxBsonNumberHintImpl());
    return ret;
  }

  private JaxBsonToBsonPostMethodMap createJaxBsonToBsonPostMethodMap() throws Exception {
    final JaxBsonToBsonPostMethodMap ret = new JaxBsonToBsonPostMethodMap();
    final Method testMethod1 = ReflectiionTestClass.TEST_CLASS
        .getDeclaredMethod(ReflectiionTestClass.METHOD_NAME_1, JaxBsonContext.class);
    ret.put(testMethod1, new JaxBsonToBsonPostImpl());
    return ret;
  }

  private JaxBsonToBsonPreMethodMap createJaxBsonToBsonPreMethodMap() throws Exception {
    final JaxBsonToBsonPreMethodMap ret = new JaxBsonToBsonPreMethodMap();
    final Method testMethod1 = ReflectiionTestClass.TEST_CLASS
        .getDeclaredMethod(ReflectiionTestClass.METHOD_NAME_1, JaxBsonContext.class);
    ret.put(testMethod1, new JaxBsonToBsonPreImpl());
    return ret;
  }

  private JaxBsonToObjectPostMethodMap createJaxBsonToObjectPostMethodMap() throws Exception {
    final JaxBsonToObjectPostMethodMap ret = new JaxBsonToObjectPostMethodMap();
    final Method testMethod1 = ReflectiionTestClass.TEST_CLASS
        .getDeclaredMethod(ReflectiionTestClass.METHOD_NAME_1, JaxBsonContext.class);
    ret.put(testMethod1, new JaxBsonToObjectPostImpl());
    return ret;
  }

  private JaxBsonToObjectPreMethodMap createJaxBsonToObjectPreMethodMap() throws Exception {
    final JaxBsonToObjectPreMethodMap ret = new JaxBsonToObjectPreMethodMap();
    final Method testMethod1 = ReflectiionTestClass.TEST_CLASS
        .getDeclaredMethod(ReflectiionTestClass.METHOD_NAME_1, JaxBsonContext.class);
    ret.put(testMethod1, new JaxBsonToObjectPreImpl());
    return ret;
  }

  private JaxBsonTransientFieldMap createJaxBsonTransientFieldMap() throws Exception {
    final JaxBsonTransientFieldMap ret = new JaxBsonTransientFieldMap();
    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    ret.put(testField1, new JaxBsonTransientImpl());
    return ret;
  }

  private JaxBsonXmlAnyAttributeMappingFieldMap createJaxBsonXmlAnyAttributeMappingFieldMap()
      throws Exception {
    final JaxBsonXmlAnyAttributeMappingFieldMap ret = new JaxBsonXmlAnyAttributeMappingFieldMap();
    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    ret.put(testField1,
        new JaxBsonXmlAnyAttributeMappingImpl("the-bsonPrefix", "the-namespaceURI"));
    return ret;
  }

  private JaxBsonXmlAnyAttributeMappingsFieldMap createJaxBsonXmlAnyAttributeMappingsFieldMap()
      throws Exception {
    final JaxBsonXmlAnyAttributeMappingsFieldMap ret = new JaxBsonXmlAnyAttributeMappingsFieldMap();
    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    ret.put(testField1, new JaxBsonXmlAnyAttributeMappingsImpl(new JaxBsonXmlAnyAttributeMapping[] { //
        new JaxBsonXmlAnyAttributeMappingImpl("the-bsonPrefix", "the-namespaceURI"), //
        new JaxBsonXmlAnyAttributeMappingImpl("the-other-bsonPrefix", "the-other-namespaceURI"), //
    }));
    return ret;
  }

  @Test
  public void test_01() throws Exception {

    final JaxBsonContext ctx = JaxBsonContext.newInstance(JaxBsonConfiguration.class);

    final JaxBsonConfiguration cfg = new JaxBsonConfiguration();
    cfg.setJaxBsonIgnoreTransientFieldMap(createJaxBsonIgnoreTransientFieldMap());
    cfg.setJaxBsonNameFieldMap(createJaxBsonNameFieldMap());
    cfg.setJaxBsonNameTypeMap(createJaxBsonNameTypeMap());
    cfg.setJaxBsonNumberHintFieldMap(createJaxBsonNumberHintFieldMap());
    cfg.setJaxBsonToBsonPostMethodMap(createJaxBsonToBsonPostMethodMap());
    cfg.setJaxBsonToBsonPreMethodMap(createJaxBsonToBsonPreMethodMap());
    cfg.setJaxBsonToObjectPostMethodMap(createJaxBsonToObjectPostMethodMap());
    cfg.setJaxBsonToObjectPreMethodMap(createJaxBsonToObjectPreMethodMap());
    cfg.setJaxBsonTransientFieldMap(createJaxBsonTransientFieldMap());
    cfg.setJaxBsonXmlAnyAttributeMappingFieldMap(createJaxBsonXmlAnyAttributeMappingFieldMap());
    cfg.setJaxBsonXmlAnyAttributeMappingsFieldMap(createJaxBsonXmlAnyAttributeMappingsFieldMap());

    final Document cfgDocument1 = ctx.toBson(cfg);
    final JaxBsonConfiguration cfgFromBson = (JaxBsonConfiguration) ctx.toObject(cfgDocument1);
    final Document cfgDocument2 = ctx.toBson(cfgFromBson);
    assertThatJson(cfgDocument1.toJson()).isEqualTo(cfgDocument2.toJson());
  }

  @Test
  public void test_02_IgnoreTransientField() throws Exception {

    final TestClass o = new TestClass();
    o.xmlTransientField = "foo";

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(new JaxBsonConfiguration()
        .setJaxBsonIgnoreTransientFieldMap(new JaxBsonIgnoreTransientFieldMap().put(
            declaredFieldByName(TestClass.class, "xmlTransientField"),
            new JaxBsonIgnoreTransientImpl())),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals(null, bson.get("xmlTransientField"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals("foo", bsonCfgAnn.get("xmlTransientField"));
  }

  @Test
  public void test_03_Name_field() throws Exception {

    final TestClass o = new TestClass();
    o.simpleField = "foo";

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(
        new JaxBsonConfiguration().setJaxBsonNameFieldMap(new JaxBsonNameFieldMap().put(
            declaredFieldByName(TestClass.class, "simpleField"), new JaxBsonNameImpl("the-name"))),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals("foo", bson.get("simpleField"));
    assertEquals(null, bson.get("the-name"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals(null, bsonCfgAnn.get("simpleField"));
    assertEquals("foo", bsonCfgAnn.get("the-name"));
  }

  @Test
  public void test_04_Name_type() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(
        new JaxBsonConfiguration().setJaxBsonNameTypeMap(
            new JaxBsonNameTypeMap().put(TestClass.class, new JaxBsonNameImpl("the-name"))),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals("testClass", bson.get("type"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals("the-name", bsonCfgAnn.get("type"));
  }

  @Test
  public void test_05_NumberHint() throws Exception {

    final TestClass o = new TestClass();
    o.numberField = 1;

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn =
        JaxBsonContext.newInstance(new JaxBsonConfiguration().setJaxBsonNumberHintFieldMap(
            new JaxBsonNumberHintFieldMap().put(declaredFieldByName(TestClass.class, "numberField"),
                new JaxBsonNumberHintImpl().setAsString(true))),
            TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals(1, bson.get("numberField"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals("1", bsonCfgAnn.get("numberField"));
  }

  @Test
  public void test_06_ToBsonPost() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(new JaxBsonConfiguration()
        .setJaxBsonToBsonPostMethodMap(new JaxBsonToBsonPostMethodMap().put(
            declaredMethodByName(TestClass.class, "testToBsonPost"), new JaxBsonToBsonPostImpl())),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals(null, o.toBsonPost);
    assertEquals(null, bson.get("toBsonPost"));
    assertEquals(null, o.toBsonPostCtx);
    assertEquals(null, bson.get("toBsonPostCtx"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals(true, o.toBsonPost);
    assertEquals(null, bsonCfgAnn.get("toBsonPost"));
    assertEquals(null, o.toBsonPostCtx);
    assertEquals(null, bsonCfgAnn.get("toBsonPostCtx"));
  }

  @Test
  public void test_07_ToBsonPost_ctx() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn =
        JaxBsonContext
            .newInstance(
                new JaxBsonConfiguration().setJaxBsonToBsonPostMethodMap(
                    new JaxBsonToBsonPostMethodMap().put(declaredMethodByName(TestClass.class,
                        "testToBsonPost", JaxBsonContext.class), new JaxBsonToBsonPostImpl())),
                TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals(null, o.toBsonPost);
    assertEquals(null, bson.get("toBsonPost"));
    assertEquals(null, o.toBsonPostCtx);
    assertEquals(null, bson.get("toBsonPostCtx"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals(null, o.toBsonPost);
    assertEquals(null, bsonCfgAnn.get("toBsonPost"));
    assertEquals(true, o.toBsonPostCtx);
    assertEquals(null, bsonCfgAnn.get("toBsonPostCtx"));
  }

  @Test
  public void test_08_ToBsonPre() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(
        new JaxBsonConfiguration().setJaxBsonToBsonPreMethodMap(new JaxBsonToBsonPreMethodMap().put(
            declaredMethodByName(TestClass.class, "testToBsonPre"), new JaxBsonToBsonPreImpl())),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals(null, o.toBsonPre);
    assertEquals(null, bson.get("toBsonPre"));
    assertEquals(null, o.toBsonPreCtx);
    assertEquals(null, bson.get("toBsonPreCtx"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals(true, o.toBsonPre);
    assertEquals(true, bsonCfgAnn.get("toBsonPre"));
    assertEquals(null, o.toBsonPreCtx);
    assertEquals(null, bsonCfgAnn.get("toBsonPreCtx"));
  }

  @Test
  public void test_09_ToBsonPre_ctx() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn =
        JaxBsonContext
            .newInstance(
                new JaxBsonConfiguration().setJaxBsonToBsonPreMethodMap(
                    new JaxBsonToBsonPreMethodMap().put(declaredMethodByName(TestClass.class,
                        "testToBsonPre", JaxBsonContext.class), new JaxBsonToBsonPreImpl())),
                TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals(null, o.toBsonPre);
    assertEquals(null, bson.get("toBsonPre"));
    assertEquals(null, o.toBsonPreCtx);
    assertEquals(null, bson.get("toBsonPreCtx"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals(null, o.toBsonPre);
    assertEquals(null, bsonCfgAnn.get("toBsonPre"));
    assertEquals(true, o.toBsonPreCtx);
    assertEquals(true, bsonCfgAnn.get("toBsonPreCtx"));
  }

  @Test
  public void test_10_ToObjectPost() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(new JaxBsonConfiguration()
        .setJaxBsonToObjectPostMethodMap(new JaxBsonToObjectPostMethodMap().put(
            declaredMethodByName(TestClass.class, "testToObjectPost"),
            new JaxBsonToObjectPostImpl())),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    final TestClass obj = (TestClass) ctx.toObject(bson);
    assertEquals(null, obj.toObjectPost);
    assertEquals(null, obj.toObjectPostCtx);

    final TestClass objCfgAnn = (TestClass) ctxCfgAnn.toObject(bson);
    assertEquals(true, objCfgAnn.toObjectPost);
    assertEquals(null, objCfgAnn.toObjectPostCtx);
  }

  @Test
  public void test_11_ToObjectPost_ctx() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn =
        JaxBsonContext
            .newInstance(
                new JaxBsonConfiguration().setJaxBsonToObjectPostMethodMap(
                    new JaxBsonToObjectPostMethodMap().put(declaredMethodByName(TestClass.class,
                        "testToObjectPost", JaxBsonContext.class), new JaxBsonToObjectPostImpl())),
                TestClass.class);

    final Document bson = ctx.toBson(o);
    final TestClass obj = (TestClass) ctx.toObject(bson);
    assertEquals(null, obj.toObjectPost);
    assertEquals(null, obj.toObjectPostCtx);

    final TestClass objCfgAnn = (TestClass) ctxCfgAnn.toObject(bson);
    assertEquals(null, objCfgAnn.toObjectPost);
    assertEquals(true, objCfgAnn.toObjectPostCtx);
  }

  @Test
  public void test_12_ToObjectPre() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(
        new JaxBsonConfiguration().setJaxBsonToObjectPreMethodMap(new JaxBsonToObjectPreMethodMap()
            .put(declaredMethodByName(TestClass.class, "testToObjectPre"),
                new JaxBsonToObjectPreImpl())),
        TestClass.class);

    final Document bson = ctx.toBson(o);
    final TestClass obj = (TestClass) ctx.toObject(bson);
    assertEquals(null, obj.toObjectPre);
    assertEquals(null, obj.toObjectPreCtx);

    final TestClass objCfgAnn = (TestClass) ctxCfgAnn.toObject(bson);
    assertEquals(true, objCfgAnn.toObjectPre);
    assertEquals(null, objCfgAnn.toObjectPreCtx);
  }

  @Test
  public void test_13_ToObjectPre_ctx() throws Exception {

    final TestClass o = new TestClass();

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn =
        JaxBsonContext
            .newInstance(
                new JaxBsonConfiguration().setJaxBsonToObjectPreMethodMap(
                    new JaxBsonToObjectPreMethodMap().put(declaredMethodByName(TestClass.class,
                        "testToObjectPre", JaxBsonContext.class), new JaxBsonToObjectPreImpl())),
                TestClass.class);

    final Document bson = ctx.toBson(o);
    final TestClass obj = (TestClass) ctx.toObject(bson);
    assertEquals(null, obj.toObjectPre);
    assertEquals(null, obj.toObjectPreCtx);

    final TestClass objCfgAnn = (TestClass) ctxCfgAnn.toObject(bson);
    assertEquals(null, objCfgAnn.toObjectPre);
    assertEquals(true, objCfgAnn.toObjectPreCtx);
  }

  @Test
  public void test_14_XmlAnyAttributeMappingField() throws Exception {

    final TestClass o = new TestClass();
    o.xmlAnyAttributeField = new HashMap<>();
    o.xmlAnyAttributeField.put(new QName("no-ns-attr1"), "no-ns-attr1");
    o.xmlAnyAttributeField.put(new QName("no-ns-attr2"), "no-ns-attr2");

    final JaxBsonContext ctx = JaxBsonContext.newInstance(TestClass.class);
    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(new JaxBsonConfiguration()
        .setJaxBsonXmlAnyAttributeMappingFieldMap(new JaxBsonXmlAnyAttributeMappingFieldMap() //
            .put(declaredFieldByName(TestClass.class, "xmlAnyAttributeField"),
                new JaxBsonXmlAnyAttributeMappingImpl("no-ns.", "")) //
        ), TestClass.class);

    final Document bson = ctx.toBson(o);
    assertEquals("no-ns-attr1", bson.get("no-ns-attr1"));

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals("no-ns-attr1", bsonCfgAnn.get("no-ns.no-ns-attr1"));
  }

  @Test
  public void test_15_XmlAnyAttributeMappingsField() throws Exception {

    final TestClass o = new TestClass();
    o.xmlAnyAttributeField = new HashMap<>();
    o.xmlAnyAttributeField.put(new QName("no-ns-attr1"), "no-ns-attr1");
    o.xmlAnyAttributeField.put(new QName("no-ns-attr2"), "no-ns-attr2");
    o.xmlAnyAttributeField.put(new QName("ns", "ns-attr1"), "ns-attr1");
    o.xmlAnyAttributeField.put(new QName("ns", "ns-attr2"), "ns-attr2");

    final JaxBsonContext ctxCfgAnn = JaxBsonContext.newInstance(new JaxBsonConfiguration()
        .setJaxBsonXmlAnyAttributeMappingsFieldMap(new JaxBsonXmlAnyAttributeMappingsFieldMap() //
            .put(declaredFieldByName(TestClass.class, "xmlAnyAttributeField"),
                new JaxBsonXmlAnyAttributeMappingsImpl(new JaxBsonXmlAnyAttributeMapping[] { //
                    new JaxBsonXmlAnyAttributeMappingImpl("no-ns.", ""), //
                    new JaxBsonXmlAnyAttributeMappingImpl("with-ns.", "ns"), //
                })) //
        ), TestClass.class);

    final Document bsonCfgAnn = ctxCfgAnn.toBson(o);
    assertEquals("no-ns-attr1", bsonCfgAnn.get("no-ns.no-ns-attr1"));
    assertEquals("no-ns-attr2", bsonCfgAnn.get("no-ns.no-ns-attr2"));
    assertEquals("ns-attr1", bsonCfgAnn.get("with-ns.ns-attr1"));
    assertEquals("ns-attr2", bsonCfgAnn.get("with-ns.ns-attr2"));
  }
}
