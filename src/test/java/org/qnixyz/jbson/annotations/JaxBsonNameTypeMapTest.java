package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameTypeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonNameTypeMapTest {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonNameTypeMapTest.class);

  @Test
  public void test() throws Exception {
    final JaxBsonNameTypeMap o = new JaxBsonNameTypeMap();

    final Class<?> testType1 = ReflectiionTestClass.TEST_CLASS;
    o.put(testType1, new JaxBsonNameImpl("new-name"));
    JaxBsonName annotation1 = o.get(testType1);
    assertNotNull(annotation1);

    o.remove(testType1);
    annotation1 = o.get(testType1);
    assertNull(annotation1);
  }
}
