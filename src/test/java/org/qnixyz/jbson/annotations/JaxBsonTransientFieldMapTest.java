package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.lang.reflect.Field;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.cfg.JaxBsonTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonTransientImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonTransientFieldMapTest {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonTransientFieldMapTest.class);

  @Test
  public void test() throws Exception {
    final JaxBsonTransientFieldMap o = new JaxBsonTransientFieldMap();

    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    o.put(testField1, new JaxBsonTransientImpl());
    JaxBsonTransient annotation1 = o.get(testField1);
    assertNotNull(annotation1);

    o.remove(testField1);
    annotation1 = o.get(testField1);
    assertNull(annotation1);
  }
}
