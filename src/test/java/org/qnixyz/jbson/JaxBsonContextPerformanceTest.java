package org.qnixyz.jbson;

import static org.junit.Assume.assumeTrue;
import static org.qnixyz.jbson.impl.Utils.isBlank;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.bson.Document;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.jaxb.javax.OkJaxbJaxbsonJavaxTest;
import org.qnixyz.jbson.jaxb.javax.OkJaxbJaxbsonJavaxTest.CreateOkRootOptions;
import org.qnixyz.jbson.jaxb.javax.OkJaxbTest;
import org.qnixyz.jbson.jaxb.javax.ok.OkRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonContextPerformanceTest {

  private static class NullOutputStream extends OutputStream {

    @Override
    public void write(final int b) throws IOException {}
  }

  public static final String DO_TEST_PROP = "DO_JAXBSON_CONTEXT_PERFORMANCE_TEST";

  private static final int DURATION_DOCS = 1000000;

  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonContextPerformanceTest.class);

  @BeforeClass
  public static void beforeMethod() {
    assumeTrue((!isBlank(System.getenv(DO_TEST_PROP))
        && System.getenv(DO_TEST_PROP).equalsIgnoreCase("true")) || //
        (!isBlank(System.getProperty(DO_TEST_PROP))
            && System.getProperty(DO_TEST_PROP).equalsIgnoreCase("true")));
  }

  private int jaxbMarshalDuration(final OkRoot objOrig, final Marshaller m) throws Exception {
    try (NullOutputStream os = new NullOutputStream()) {
      final long start = System.currentTimeMillis();
      for (int i = 0; i < DURATION_DOCS; i++) {
        m.marshal(objOrig, os);
      }
      return (int) (System.currentTimeMillis() - start);
    }
  }

  private int jaxBsonToBsonDuration(final OkRoot objOrig, final JaxBsonContext ctx) {
    final long start = System.currentTimeMillis();
    for (int i = 0; i < DURATION_DOCS; i++) {
      ctx.toBson(objOrig);
    }
    return (int) (System.currentTimeMillis() - start);
  }

  private int jaxBsonToObjectDuration(final Document bson, final JaxBsonContext ctx) {
    final long start = System.currentTimeMillis();
    for (int i = 0; i < DURATION_DOCS; i++) {
      ctx.toObject(bson);
    }
    return (int) (System.currentTimeMillis() - start);
  }

  private int jaxbUnmarshalDuration(final ByteArrayInputStream bais, final Unmarshaller u)
      throws Exception {
    final long start = System.currentTimeMillis();
    for (int i = 0; i < DURATION_DOCS; i++) {
      u.unmarshal(bais);
      bais.reset();
    }
    return (int) (System.currentTimeMillis() - start);
  }

  @Test
  public void test() throws Exception {

    final OkRoot objOrig =
        OkJaxbJaxbsonJavaxTest.createOkRoot(new CreateOkRootOptions(true, true, false));

    final Marshaller m = OkJaxbTest.createMarshaller();
    final Unmarshaller u = OkJaxbTest.createUnmarshaller();
    final JaxBsonContext ctx = JaxBsonContext.newInstance(OkRoot.class);

    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    m.marshal(objOrig, baos);
    final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
    final Document bson = ctx.toBson(objOrig);

    final int jaxbMarshalDuration = jaxbMarshalDuration(objOrig, m);
    final int jaxbUnmarshalDuration = jaxbUnmarshalDuration(bais, u);
    final int jaxBsonToBsonDuration = jaxBsonToBsonDuration(objOrig, ctx);
    final int jaxBsonToObjectDuration = jaxBsonToObjectDuration(bson, ctx);

    LOG.info(String.format("jaxbMarshalDuration for %d documents: %dms", DURATION_DOCS,
        jaxbMarshalDuration));
    LOG.info(String.format("jaxbUnmarshalDuration for %d documents: %dms", DURATION_DOCS,
        jaxbUnmarshalDuration));
    LOG.info(String.format("jaxBsonToBsonDuration for %d documents: %dms", DURATION_DOCS,
        jaxBsonToBsonDuration));
    LOG.info(String.format("jaxBsonToObjectDuration for %d documents: %dms", DURATION_DOCS,
        jaxBsonToObjectDuration));

    LOG.info(String.format("jaxbMarshalDuration/jaxBsonToBsonDuration=%.2f",
        (float) jaxbMarshalDuration / (float) jaxBsonToBsonDuration));
    LOG.info(String.format("jaxBsonToBsonDuration/jaxbMarshalDuration=%.2f",
        (float) jaxBsonToBsonDuration / (float) jaxbMarshalDuration));
    LOG.info(String.format("jaxbUnmarshalDuration/jaxBsonToObjectDuration=%.2f",
        (float) jaxbUnmarshalDuration / (float) jaxBsonToObjectDuration));
    LOG.info(String.format("jaxBsonToObjectDuration/jaxbUnmarshalDuration=%.2f",
        (float) jaxBsonToObjectDuration / (float) jaxbUnmarshalDuration));
  }
}
