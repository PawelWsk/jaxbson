package org.qnixyz.jbson.jaxb.javax.ok;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.Animal;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlElementRefTest {

  @XmlElementRef
  private Animal animalElementRef;

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final XmlElementRefTest other = (XmlElementRefTest) obj;
    return Objects.equals(this.animalElementRef, other.animalElementRef);
  }

  public Animal getAnimalElementRef() {
    return this.animalElementRef;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.animalElementRef);
  }

  public void setAnimalElementRef(final Animal animalElementRef) {
    this.animalElementRef = animalElementRef;
  }
}
