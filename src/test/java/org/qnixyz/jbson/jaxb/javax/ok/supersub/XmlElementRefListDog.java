package org.qnixyz.jbson.jaxb.javax.ok.supersub;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlElementRefListDog extends XmlElementRefListAnimal {

  private String barkSound;

  public XmlElementRefListDog() {}

  public XmlElementRefListDog(final String barkSound) {
    this.barkSound = barkSound;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final XmlElementRefListDog other = (XmlElementRefListDog) obj;
    return Objects.equals(this.barkSound, other.barkSound);
  }

  public String getBarkSound() {
    return this.barkSound;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.barkSound);
  }

  public void setBarkSound(final String barkSound) {
    this.barkSound = barkSound;
  }
}
