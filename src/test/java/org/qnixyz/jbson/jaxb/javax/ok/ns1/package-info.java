@XmlSchema(namespace = NS1, elementFormDefault = XmlNsForm.QUALIFIED)
package org.qnixyz.jbson.jaxb.javax.ok.ns1;

import static org.qnixyz.jbson.jaxb.javax.ok.Constants.NS1;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
