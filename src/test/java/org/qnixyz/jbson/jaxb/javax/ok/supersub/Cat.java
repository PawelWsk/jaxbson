package org.qnixyz.jbson.jaxb.javax.ok.supersub;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Cat extends Animal {

  private String meowSound;

  public Cat() {}

  public Cat(final String meowSound) {
    this.meowSound = meowSound;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Cat other = (Cat) obj;
    return Objects.equals(this.meowSound, other.meowSound);
  }

  public String getMeowSound() {
    return this.meowSound;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.meowSound);
  }

  public void setMeowSound(final String meowSound) {
    this.meowSound = meowSound;
  }
}
