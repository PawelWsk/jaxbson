@XmlSchema(namespace = NS2, elementFormDefault = XmlNsForm.QUALIFIED)
package org.qnixyz.jbson.jaxb.javax.ok.ns2;

import static org.qnixyz.jbson.jaxb.javax.ok.Constants.NS2;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
