package org.qnixyz.jbson.jaxb.javax.ok.supersub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class XmlElementListAnimal {

  @Override
  public String toString() {
    final Gson gson =
        new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
    return gson.toJson(this);
  }
}
