package org.qnixyz.jbson.jaxb.javax.ok.ns2;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CrossElementFromNs2 {

  private String cross;

  @XmlAttribute
  private String crossAttribute;

  public CrossElementFromNs2() {}

  public CrossElementFromNs2(final String cross, final String crossAttribute) {
    this.cross = cross;
    this.crossAttribute = crossAttribute;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CrossElementFromNs2 other = (CrossElementFromNs2) obj;
    return Objects.equals(this.cross, other.cross)
        && Objects.equals(this.crossAttribute, other.crossAttribute);
  }

  public String getCross() {
    return this.cross;
  }

  public String getCrossAttribute() {
    return this.crossAttribute;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.cross, this.crossAttribute);
  }

  public void setCross(final String cross) {
    this.cross = cross;
  }

  public void setCrossAttribute(final String crossAttribute) {
    this.crossAttribute = crossAttribute;
  }
}
