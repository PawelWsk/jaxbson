package org.qnixyz.jbson.jaxb.javax;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.qnixyz.jbson.jaxb.javax.OkJaxbJaxbsonJavaxTest.createOkRoot;
import static org.qnixyz.jbson.jaxb.javax.ok.Constants.NS1;
import static org.qnixyz.jbson.jaxb.javax.ok.Constants.NS2;
import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.jaxb.javax.OkJaxbJaxbsonJavaxTest.CreateOkRootOptions;
import org.qnixyz.jbson.jaxb.javax.ok.OkRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.matchers.CompareMatcher;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OkJaxbTest {

  private class MySchemaOutputResolver extends SchemaOutputResolver {

    @Override
    public Result createOutput(final String namespaceUri, final String suggestedFileName)
        throws IOException {
      File file = null;
      if (namespaceUri.equals("")) {
        file = ACTUAL_XSD;
      } else if (namespaceUri.equals(NS1)) {
        file = ACTUAL_XSD_NS1;
      } else if (namespaceUri.equals(NS2)) {
        file = ACTUAL_XSD_NS2;
      } else {
        throw new IllegalStateException("Unsupported namespaceUri: " + namespaceUri);
      }
      final StreamResult result = new StreamResult(file);
      result.setSystemId(file.toURI().toURL().toString());
      return result;
    }
  }

  private static final File _DIR = new File("target/test-classes");

  private static final File _SRC_TEST_RES_DIR = new File("src/test/resources");

  private static File ACTUAL_XML = new File(_DIR, "ok-jaxb-test-actual.xml");

  private static File ACTUAL_XML_2 = new File(_DIR, "ok-jaxb-test-actual-2.xml");

  private static File ACTUAL_XSD = new File(_DIR, "ok-jaxb-test-actual.xsd");

  private static File ACTUAL_XSD_NS1 = new File(_DIR, "ok-jaxb-test-actual-ns1.xsd");

  private static File ACTUAL_XSD_NS2 = new File(_DIR, "ok-jaxb-test-actual-ns2.xsd");

  private static File EXPECTED_XML = new File(_SRC_TEST_RES_DIR, "ok-jaxb-test-expected.xml");

  private static File EXPECTED_XSD = new File(_SRC_TEST_RES_DIR, "ok-jaxb-test-expected.xsd");

  private static File EXPECTED_XSD_NS1 =
      new File(_SRC_TEST_RES_DIR, "ok-jaxb-test-expected-ns1.xsd");

  private static File EXPECTED_XSD_NS2 =
      new File(_SRC_TEST_RES_DIR, "ok-jaxb-test-expected-ns2.xsd");

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(OkJaxbTest.class);

  public static JAXBContext createJAXBContext() throws JAXBException {
    return JAXBContext.newInstance(OkRoot.class);
  }

  public static Marshaller createMarshaller() throws JAXBException {
    final JAXBContext jaxbContext = createJAXBContext();
    return jaxbContext.createMarshaller();
  }

  public static Unmarshaller createUnmarshaller() throws JAXBException {
    final JAXBContext jaxbContext = createJAXBContext();
    return jaxbContext.createUnmarshaller();
  }

  @Test
  public void test_01_xsd() throws Exception {
    final JAXBContext jaxbContext = createJAXBContext();
    final SchemaOutputResolver sor = new MySchemaOutputResolver();
    jaxbContext.generateSchema(sor);
    assertThat(EXPECTED_XSD, CompareMatcher.isSimilarTo(ACTUAL_XSD).ignoreWhitespace());
    assertThat(EXPECTED_XSD_NS1, CompareMatcher.isSimilarTo(ACTUAL_XSD_NS1).ignoreWhitespace());
    assertThat(EXPECTED_XSD_NS2, CompareMatcher.isSimilarTo(ACTUAL_XSD_NS2).ignoreWhitespace());
  }

  @Test
  public void test_02_xml() throws Exception {
    final OkRoot objOrig = createOkRoot(new CreateOkRootOptions(true, true, false));
    final Marshaller m = createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    m.marshal(objOrig, ACTUAL_XML);
    assertThat(EXPECTED_XML, CompareMatcher.isSimilarTo(ACTUAL_XML).ignoreWhitespace());
    final Unmarshaller u = createUnmarshaller();
    final OkRoot objBack = (OkRoot) u.unmarshal(ACTUAL_XML);
    m.marshal(objBack, ACTUAL_XML_2);
    assertThat(ACTUAL_XML, CompareMatcher.isSimilarTo(ACTUAL_XML_2).ignoreWhitespace());
  }
}
