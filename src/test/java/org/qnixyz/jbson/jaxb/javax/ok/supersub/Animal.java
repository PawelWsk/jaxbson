package org.qnixyz.jbson.jaxb.javax.ok.supersub;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Cat.class, Dog.class})
public abstract class Animal {

  @Override
  public String toString() {
    final Gson gson =
        new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
    return gson.toJson(this);
  }
}
