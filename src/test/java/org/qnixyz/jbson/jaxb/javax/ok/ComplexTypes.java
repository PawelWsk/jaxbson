package org.qnixyz.jbson.jaxb.javax.ok;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class ComplexTypes {

  @XmlJavaTypeAdapter(NonXmlComplexType.AdapterToComplexType.class)
  private NonXmlComplexType propXmlJavaTypeAdapterAsComplexType;

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ComplexTypes other = (ComplexTypes) obj;
    return Objects.equals(this.propXmlJavaTypeAdapterAsComplexType,
        other.propXmlJavaTypeAdapterAsComplexType);
  }

  public NonXmlComplexType getPropXmlJavaTypeAdapterAsComplexType() {
    return this.propXmlJavaTypeAdapterAsComplexType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.propXmlJavaTypeAdapterAsComplexType);
  }

  public void setPropXmlJavaTypeAdapterAsComplexType(
      final NonXmlComplexType propXmlJavaTypeAdapterAsComplexType) {
    this.propXmlJavaTypeAdapterAsComplexType = propXmlJavaTypeAdapterAsComplexType;
  }
}
