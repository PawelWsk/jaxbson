/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok.supersub;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class XmlElementRefListAnimal {

  @Override
  public String toString() {
    final Gson gson =
        new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
    return gson.toJson(this);
  }
}
