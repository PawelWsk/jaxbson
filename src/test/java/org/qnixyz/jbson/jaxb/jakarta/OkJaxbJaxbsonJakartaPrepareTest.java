package org.qnixyz.jbson.jaxb.jakarta;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.FileUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OkJaxbJaxbsonJakartaPrepareTest {

  private static final String COMMENT =
      "/* This file is derived from its javax equivalent. Do not modify by hand. */";
  private static final File JAKARTA_SRC = new File("src/test/java/org/qnixyz/jbson/jaxb/jakarta");
  private static final File JAKARTA_TMP =
      new File("target/test-classes/" + OkJaxbJaxbsonJakartaPrepareTest.class.getSimpleName());
  private static final File JAVAX_SRC = new File("src/test/java/org/qnixyz/jbson/jaxb/javax");

  private void check(final File srcDir, final File destDir, final boolean onlyJavax)
      throws IOException {
    for (final File file : srcDir.listFiles()) {
      if (file.isFile()) {
        if (onlyJavax && !file.getName().contains("Javax")) {
          continue;
        }
        check(srcDir, destDir, file.getName(), convertToJakarta(file.getName()));
      } else {
        check(new File(srcDir, file.getName()), new File(destDir, file.getName()), false);
      }
    }
  }

  private void check(final File srcDir, final File destDir, final String srcName,
      final String dstName) throws IOException {
    FileUtils.forceMkdir(destDir);
    final File srcFile = new File(srcDir, srcName);
    final File destFile = new File(destDir, dstName);
    try (
        BufferedReader src = new BufferedReader(new InputStreamReader(
            new BufferedInputStream(new FileInputStream(srcFile)), StandardCharsets.UTF_8));
        BufferedReader dest = new BufferedReader(new InputStreamReader(
            new BufferedInputStream(new FileInputStream(destFile)), StandardCharsets.UTF_8));) {
      int nr = 0;
      for (String destLine = dest.readLine(); destLine != null; destLine = dest.readLine()) {
        nr++;
        if (nr == 1) {
          if (!destLine.equals(COMMENT)) {
            throw new IllegalStateException("On first line of " + destFile + ":\n"
                + "- exxpected:\n" + COMMENT + "\n- but got:\n" + destLine);
          }
          continue;
        }
        final String srcLine = src.readLine();
        if (srcLine == null) {
          throw new IllegalStateException("In " + srcFile + ":\n" + "- expected " + (nr - 1)
              + " lines\n- but got " + nr + " lines");
        }
        final String destLineExpect = convertToJakarta(srcLine);
        if (!destLineExpect.equals(destLine)) {
          throw new IllegalStateException("On line " + nr + " of " + destFile + ":\n"
              + "- exxpected:\n" + destLineExpect + "\n- but got:\n" + destLine);
        }
      }
      for (String srcLine = src.readLine(); srcLine != null; srcLine = src.readLine()) {
        nr++;
        if (!isBlank(srcLine)) {
          continue;
        }
        final String destLineExpect = convertToJakarta(srcLine);
        throw new IllegalStateException("On line " + nr + " of " + destFile + ":\n"
            + "- exxpected:\n" + destLineExpect + "\n- but got no line at all");
      }
    }
  }

  private void convertToJakarta(final File srcDir, final File destDir, final boolean onlyJavax)
      throws IOException {
    for (final File file : srcDir.listFiles()) {
      if (file.isFile()) {
        if (onlyJavax && !file.getName().contains("Javax")) {
          continue;
        }
        convertToJakarta(srcDir, destDir, file.getName(), convertToJakarta(file.getName()));
      } else {
        convertToJakarta(new File(srcDir, file.getName()), new File(destDir, file.getName()),
            false);
      }
    }
  }

  private void convertToJakarta(final File srcDir, final File destDir, final String srcName,
      final String dstName) throws IOException {
    FileUtils.forceMkdir(destDir);
    final File srcFile = new File(srcDir, srcName);
    final File destFile = new File(destDir, dstName);
    try (
        BufferedReader src = new BufferedReader(new InputStreamReader(
            new BufferedInputStream(new FileInputStream(srcFile)), StandardCharsets.UTF_8));
        Writer dest = new OutputStreamWriter(
            new BufferedOutputStream(new FileOutputStream(destFile)), StandardCharsets.UTF_8);) {
      boolean first = true;
      for (String srcLine = src.readLine(); srcLine != null; srcLine = src.readLine()) {
        if (first) {
          first = false;
          dest.write(COMMENT);
          dest.write("\n");
        }
        final String destLine = convertToJakarta(srcLine);
        dest.write(destLine);
        dest.write("\n");
      }
    }
  }

  private String convertToJakarta(String line) {
    line = line.replace("Javax", "Jakarta");
    line = line.replace("org.qnixyz.jbson.jaxb.javax", "org.qnixyz.jbson.jaxb.jakarta");
    line = line.replace("javax.xml.bind", "jakarta.xml.bind");
    return line;
  }

  @Test
  public void test() throws Exception {
    if (JAKARTA_TMP.exists()) {
      FileUtils.forceDelete(JAKARTA_TMP);
    }
    convertToJakarta(JAVAX_SRC, JAKARTA_TMP, true);
    check(JAVAX_SRC, JAKARTA_SRC, true);
  }
}
