/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok;

public class Constants {

  public static final String NS1 = "http://www.qnixyz.org/ns1";

  public static final String NS2 = "http://www.qnixyz.org/ns2";
}
