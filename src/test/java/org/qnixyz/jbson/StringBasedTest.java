package org.qnixyz.jbson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StringBasedTest {

  private static File FILE;

  private static final String FILE_STRING = "/foo/bar";

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(StringBasedTest.class);

  private static final String STRING = "the string";

  private static URI URI;

  private static final String URI_STRING = "uri:xxx";

  private static URL URL;

  private static final String URL_STRING = "http://host";

  static {
    FILE = new File(FILE_STRING);
    try {
      URI = new URI(URI_STRING);
    } catch (final URISyntaxException e) {
      throw new IllegalStateException("This is a bug", e);
    }
    try {
      URL = new URL(URL_STRING);
    } catch (final MalformedURLException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  @Test
  public void test_check() throws Exception {
    final StringBased util = new StringBased();
    assertTrue("File check", util.getChecker().check(File.class));
    assertTrue("String check", util.getChecker().check(String.class));
    assertTrue("URI check", util.getChecker().check(URI.class));
    assertTrue("URL check", util.getChecker().check(URL.class));
    assertFalse("Integer check", util.getChecker().check(Integer.class));
  }

  @Test
  public void test_to_bson() throws Exception {
    final StringBased util = new StringBased();
    assertEquals("File==>Bson", FILE_STRING, util.getToBson().convert(FILE));
    assertEquals("String==>Bson", STRING, util.getToBson().convert(STRING));
    assertEquals("URI==>Bson", URI_STRING, util.getToBson().convert(URI));
    assertEquals("URL==>Bson", URL_STRING, util.getToBson().convert(URL));
  }

  @Test
  public void test_to_obect() throws Exception {
    final StringBased util = new StringBased();
    assertEquals("File==>Object", FILE, util.getToObject().convert(File.class, FILE_STRING));
    assertEquals("String==>Object", STRING, util.getToObject().convert(String.class, STRING));
    assertEquals("URI==>Object", URI, util.getToObject().convert(URI.class, URI_STRING));
    assertEquals("URL==>Object", URL, util.getToObject().convert(URL.class, URL_STRING));
  }
}
