package org.qnixyz.jbson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.bson.types.Decimal128;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;
import org.qnixyz.jbson.helpers.XmlAnyAttributeHelper;
import org.qnixyz.jbson.helpers.XmlAnyElementHelper;
import org.qnixyz.jbson.helpers.XmlElementRefHelper;
import org.qnixyz.jbson.helpers.XmlElementRefsHelper;
import org.qnixyz.jbson.helpers.XmlTransientHelper;
import org.qnixyz.jbson.helpers.XmlValueHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NumberBasedTest {

  private static final Double _DOUBLE_BSON = 1.1D;

  private static final Integer _INTEGER_BSON = 2;

  private static final Long _LONG_BSON = 3L;

  private static final BigDecimal BIGDECIMAL_OBJECT = BigDecimal.valueOf(_DOUBLE_BSON);

  private static final BigInteger BIGINTEGER_OBJECT = BigInteger.valueOf(_LONG_BSON);

  private static final Double DOUBLE_OBJECT = _DOUBLE_BSON;

  private static final double DOUBLE_PRIM_OBJECT = _DOUBLE_BSON;

  private static final JaxBsonFieldContext EMPTY_FIELD_CTX = new JaxBsonFieldContext() {

    @Override
    public JaxBsonConfiguration getConfiguration() {
      return null;
    }

    @Override
    public JaxBsonContext getContext() {
      return null;
    }

    @Override
    public Field getField() {
      return null;
    }

    @Override
    public JaxBsonIgnoreTransient getJaxBsonIgnoreTransient() {
      return null;
    }

    @Override
    public JaxBsonIgnoreTransient getJaxBsonIgnoreTransientCfg() {
      return null;
    }

    @Override
    public JaxBsonName getJaxBsonName() {
      return null;
    }

    @Override
    public JaxBsonName getJaxBsonNameCfg() {
      return null;
    }

    @Override
    public JaxBsonNumberHint getJaxBsonNumberHint() {
      return null;
    }

    @Override
    public JaxBsonNumberHint getJaxBsonNumberHintCfg() {
      return null;
    }

    @Override
    public JaxBsonSeeAlso getJaxBsonSeeAlso() {
      return null;
    }

    @Override
    public JaxBsonTransient getJaxBsonTransient() {
      return null;
    }

    @Override
    public JaxBsonTransient getJaxBsonTransientCfg() {
      return null;
    }

    @Override
    public JaxBsonAdapter<Object, Object> getJaxBsonXmlAdapter() {
      return null;
    }

    @Override
    public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMapping() {
      return null;
    }

    @Override
    public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMappingCfg() {
      return null;
    }

    @Override
    public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappings() {
      return null;
    }

    @Override
    public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappingsCfg() {
      return null;
    }

    @Override
    public XmlAnyAttributeHelper getXmlAnyAttribute() {
      return null;
    }

    @Override
    public XmlAnyElementHelper getXmlAnyElement() {
      return null;
    }

    @Override
    public XmlElementRefHelper getXmlElementRef() {
      return null;
    }

    @Override
    public XmlElementRefsHelper getXmlElementRefs() {
      return null;
    }

    @Override
    public XmlTransientHelper getXmlTransient() {
      return null;
    }

    @Override
    public XmlValueHelper getXmlValue() {
      return null;
    }
  };

  private static final Float FLOAT_OBJECT = _DOUBLE_BSON.floatValue();

  private static final float FLOAT_PRIM_OBJECT = _DOUBLE_BSON.floatValue();

  private static final Integer INTEGER_OBJECT = _INTEGER_BSON;

  private static final int INTEGER_PRIM_OBJECT = _INTEGER_BSON;

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(NumberBasedTest.class);

  private static final Long LONG_OBJECT = _LONG_BSON;

  private static final long LONG_PRIM_OBJECT = _LONG_BSON;

  private static final Short SHORT_OBJECT = _INTEGER_BSON.shortValue();

  private static final short SHORT_PRIM_OBJECT = _INTEGER_BSON.shortValue();

  @Test
  public void test_check() throws Exception {
    final NumberBased util = new NumberBased();
    assertTrue("BigDecimal check", util.getChecker().check(BigDecimal.class));
    assertTrue("BigInteger check", util.getChecker().check(BigInteger.class));
    assertTrue("Double check", util.getChecker().check(Double.class));
    assertTrue("double check", util.getChecker().check(Double.TYPE));
    assertTrue("Float check", util.getChecker().check(Float.class));
    assertTrue("float check", util.getChecker().check(Float.TYPE));
    assertTrue("Integer check", util.getChecker().check(Integer.class));
    assertTrue("integer check", util.getChecker().check(Integer.TYPE));
    assertTrue("Long check", util.getChecker().check(Long.class));
    assertTrue("long check", util.getChecker().check(Long.TYPE));
    assertTrue("Short check", util.getChecker().check(Short.class));
    assertTrue("short check", util.getChecker().check(Short.TYPE));
    assertFalse("String check", util.getChecker().check(String.class));
  }

  @Test
  public void test_to_bson() throws Exception {
    final NumberBased util = new NumberBased();
    assertEquals("BigDecimal==>Bson", _DOUBLE_BSON,
        ((Decimal128) util.getToBson().convert(EMPTY_FIELD_CTX, BIGDECIMAL_OBJECT).getValue())
            .doubleValue(),
        0.001);
    assertEquals("BigInteger==>Bson", _LONG_BSON,
        Long.valueOf(
            ((Decimal128) util.getToBson().convert(EMPTY_FIELD_CTX, BIGINTEGER_OBJECT).getValue())
                .longValue()));
    assertEquals("Double==>Bson", _DOUBLE_BSON,
        (Double) util.getToBson().convert(EMPTY_FIELD_CTX, DOUBLE_OBJECT).getValue(), 0.001);
    assertEquals("double==>Bson", _DOUBLE_BSON,
        (double) util.getToBson().convert(EMPTY_FIELD_CTX, DOUBLE_PRIM_OBJECT).getValue(), 0.001);
    assertEquals("Float==>Bson", _DOUBLE_BSON,
        (double) util.getToBson().convert(EMPTY_FIELD_CTX, FLOAT_OBJECT).getValue(), 0.001);
    assertEquals("float==>Bson", _DOUBLE_BSON,
        (double) util.getToBson().convert(EMPTY_FIELD_CTX, FLOAT_PRIM_OBJECT).getValue(), 0.001);
    assertEquals("Integer==>Bson", _INTEGER_BSON,
        util.getToBson().convert(EMPTY_FIELD_CTX, INTEGER_OBJECT).getValue());
    assertEquals("integer==>Bson", _INTEGER_BSON,
        util.getToBson().convert(EMPTY_FIELD_CTX, INTEGER_PRIM_OBJECT).getValue());
    assertEquals("Long==>Bson", _LONG_BSON,
        util.getToBson().convert(EMPTY_FIELD_CTX, LONG_OBJECT).getValue());
    assertEquals("long==>Bson", _LONG_BSON,
        util.getToBson().convert(EMPTY_FIELD_CTX, LONG_PRIM_OBJECT).getValue());
    assertEquals("Short==>Bson", _INTEGER_BSON,
        util.getToBson().convert(EMPTY_FIELD_CTX, SHORT_OBJECT).getValue());
    assertEquals("short==>Bson", _INTEGER_BSON,
        util.getToBson().convert(EMPTY_FIELD_CTX, SHORT_PRIM_OBJECT).getValue());
  }

  @Test
  public void test_to_obect() throws Exception {
    final NumberBased util = new NumberBased();
    assertEquals("BigDecimal==>Object", BIGDECIMAL_OBJECT.doubleValue(),
        ((BigDecimal) util.getToObject().convert(BigDecimal.class, _DOUBLE_BSON)).doubleValue(),
        0.001);
    assertEquals("BigInteger==>Object", BIGINTEGER_OBJECT.longValue(),
        ((BigInteger) util.getToObject().convert(BigInteger.class, _LONG_BSON)).longValue());
    assertEquals("Double==>Object", DOUBLE_OBJECT,
        (Double) util.getToObject().convert(Double.class, _DOUBLE_BSON), 0.001);
    assertEquals("double==>Object", DOUBLE_PRIM_OBJECT,
        (double) util.getToObject().convert(Double.TYPE, _DOUBLE_BSON), 0.001);
    assertEquals("Float==>Object", FLOAT_OBJECT,
        (Float) util.getToObject().convert(Float.class, _DOUBLE_BSON), 0.001);
    assertEquals("double==>Object", FLOAT_PRIM_OBJECT,
        (float) util.getToObject().convert(Float.TYPE, _DOUBLE_BSON), 0.001);
    assertEquals("Integer==>Object", INTEGER_OBJECT,
        util.getToObject().convert(Integer.class, _INTEGER_BSON));
    assertEquals("int==>Object", INTEGER_PRIM_OBJECT,
        (int) util.getToObject().convert(Integer.TYPE, _INTEGER_BSON));
    assertEquals("Long==>Object", LONG_OBJECT, util.getToObject().convert(Long.class, _LONG_BSON));
    assertEquals("long==>Object", LONG_PRIM_OBJECT,
        (long) util.getToObject().convert(Long.TYPE, _LONG_BSON));
    assertEquals("Short==>Object", SHORT_OBJECT,
        util.getToObject().convert(Short.class, _INTEGER_BSON));
    assertEquals("short==>Object", SHORT_PRIM_OBJECT,
        (short) util.getToObject().convert(Short.TYPE, _INTEGER_BSON));
  }
}
