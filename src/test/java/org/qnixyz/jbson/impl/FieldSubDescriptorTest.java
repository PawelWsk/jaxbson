package org.qnixyz.jbson.impl;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.jaxb.javax.ok.AllSimpleTypes;
import org.qnixyz.jbson.jaxb.javax.ok.ComplexTypes;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldSubDescriptorTest {

  private static final JaxBsonContext CTX = JaxBsonContext.newInstance(FieldDescriptorTest.class);

  private static final JaxBsonContextImpl CTX_IMPL = (JaxBsonContextImpl) CTX;

  @Test
  public void test_field_propDouble() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propDouble"));
  }

  @Test
  public void test_field_propDOUBLE() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propDoublePrim"));
  }

  @Test
  public void test_field_propDoubleArray() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propDoubleArray"));
  }

  @Test
  public void test_field_propDOUBLEArray() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propDoublePrimArray"));
  }

  @Test
  public void test_field_propDoubleList() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propDoubleList"));
  }

  @Test
  public void test_field_propXmlJavaTypeAdapter() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propXmlJavaTypeAdapter"));
  }

  @Test
  public void test_field_propXmlJavaTypeAdapterAsComplexType() throws Exception {
    new FieldDescriptor(CTX_IMPL,
        ComplexTypes.class.getDeclaredField("propXmlJavaTypeAdapterAsComplexType"));
  }
}
