package org.qnixyz.jbson.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlAccessorType(XmlAccessType.FIELD)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SimpleTypesTest {

  private enum MyEnum {
    BAR, FOO;
  }

  private static final long __MS_PER_DAY = 1000 * 60 * 60 * 24;

  private static final BigDecimal _BIG_DECIMAL_ONE = BigDecimal.valueOf(Double.valueOf(1));

  private static final BigDecimal _BIG_DECIMAL_ZERO = BigDecimal.valueOf(Double.valueOf(0));

  private static final Date _DATE1 = new Date(__MS_PER_DAY * 0);

  private static final Date _DATE2 = new Date(__MS_PER_DAY * 1);

  private static final Date _DATE3 = new Date(__MS_PER_DAY * 2);

  private static final ObjectId _OBJECT_ID1 = new ObjectId(_DATE1, 1);

  private static final ObjectId _OBJECT_ID2 = new ObjectId(_DATE2, 2);

  private static final ObjectId _OBJECT_ID3 = new ObjectId(_DATE3, 3);

  private static final BigDecimal BIG_DECIMAL =
      new BigDecimal(1.1).setScale(10, RoundingMode.CEILING);

  private static final BigDecimal[] BIG_DECIMAL_ARRAY =
      {new BigDecimal(1.1).setScale(10, RoundingMode.CEILING),
          new BigDecimal(2.2).setScale(10, RoundingMode.CEILING),
          new BigDecimal(3.3).setScale(10, RoundingMode.CEILING)};

  private static final Collection<BigDecimal> BIG_DECIMAL_COLLECTION =
      Arrays.asList(BIG_DECIMAL_ARRAY);

  private static final Collection<Double> BIG_DECIMAL_TO_BSON_MULTI = new ArrayList<>();

  private static final Double BIG_DECIMAL_TO_BSON_ONE = BIG_DECIMAL.doubleValue();

  private static final BigInteger BIG_INTEGER = BigInteger.valueOf(1);

  private static final BigInteger[] BIG_INTEGER_ARRAY =
      {BigInteger.valueOf(1), BigInteger.valueOf(2), BigInteger.valueOf(3)};

  private static final Collection<BigInteger> BIG_INTEGER_COLLECTION =
      Arrays.asList(BIG_INTEGER_ARRAY);

  private static final SortedSet<BigInteger> BIG_INTEGER_SORTED_SET =
      new TreeSet<>(BIG_INTEGER_COLLECTION);

  private static final Collection<Long> BIG_INTEGER_TO_BSON_MULTI = new ArrayList<>();

  private static final Long BIG_INTEGER_TO_BSON_ONE = BIG_INTEGER.longValue();

  private static final Boolean BOOLEAN = true;

  private static final Boolean[] BOOLEAN_ARRAY = {true, false, true};

  private static final Collection<Boolean> BOOLEAN_COLLECTION = Arrays.asList(BOOLEAN_ARRAY);

  private static final List<Boolean> BOOLEAN_COLLECTION_LIST = Arrays.asList(BOOLEAN_ARRAY);

  private static final Set<Boolean> BOOLEAN_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_ARRAY));

  private static final SortedSet<Boolean> BOOLEAN_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_ARRAY));

  private static final boolean BOOLEAN_PRIM = true;

  private static final boolean[] BOOLEAN_PRIM_ARRAY = {true, false, true};

  private static final Collection<Boolean> BOOLEAN_TO_BSON_MULTI = Arrays.asList(BOOLEAN_ARRAY);

  private static final Boolean BOOLEAN_TO_BSON_ONE = BOOLEAN;

  private static final BigDecimal BOOLEAN_TO_OBJ_BIG_DECIMAL = _BIG_DECIMAL_ONE;

  private static final BigDecimal[] BOOLEAN_TO_OBJ_BIG_DECIMAL_ARRAY =
      {_BIG_DECIMAL_ONE, _BIG_DECIMAL_ZERO, _BIG_DECIMAL_ONE};

  private static final SortedSet<BigDecimal> BOOLEAN_TO_OBJ_BIG_DECIMAL_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_BIG_DECIMAL_ARRAY));

  private static final BigInteger BOOLEAN_TO_OBJ_BIG_INTEGER = BigInteger.ONE;

  private static final BigInteger[] BOOLEAN_TO_OBJ_BIG_INTEGER_ARRAY =
      {BigInteger.ONE, BigInteger.ZERO, BigInteger.ONE};

  private static final Set<BigInteger> BOOLEAN_TO_OBJ_BIG_INTEGER_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_BIG_INTEGER_ARRAY));

  private static final SortedSet<BigInteger> BOOLEAN_TO_OBJ_BIG_INTEGER_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_BIG_INTEGER_ARRAY));

  private static final Byte BOOLEAN_TO_OBJ_BYTE = 0x01;

  private static final Byte[] BOOLEAN_TO_OBJ_BYTE_ARRAY = {0x01, 0x00, 0x01};

  private static final Set<Byte> BOOLEAN_TO_OBJ_BYTE_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_BYTE_ARRAY));

  private static final SortedSet<Byte> BOOLEAN_TO_OBJ_BYTE_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_BYTE_ARRAY));

  private static final byte BOOLEAN_TO_OBJ_BYTE_PRIM = 0x01;

  private static final byte[] BOOLEAN_TO_OBJ_BYTE_PRIM_ARRAY = {0x01, 0x00, 0x01};

  private static final Character BOOLEAN_TO_OBJ_CHAR = '+';

  private static final Character[] BOOLEAN_TO_OBJ_CHAR_ARRAY = {'+', '-', '+'};

  private static final Set<Character> BOOLEAN_TO_OBJ_CHAR_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_CHAR_ARRAY));

  private static final SortedSet<Character> BOOLEAN_TO_OBJ_CHAR_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_CHAR_ARRAY));

  private static final char BOOLEAN_TO_OBJ_CHAR_PRIM = '+';

  private static final char[] BOOLEAN_TO_OBJ_CHAR_PRIM_ARRAY = {'+', '-', '+'};

  private static final Double BOOLEAN_TO_OBJ_DOUBLE = 1D;

  private static final Double[] BOOLEAN_TO_OBJ_DOUBLE_ARRAY = {1D, 0D, 1D};

  private static final Set<Double> BOOLEAN_TO_OBJ_DOUBLE_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_DOUBLE_ARRAY));

  private static final SortedSet<Double> BOOLEAN_TO_OBJ_DOUBLE_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_DOUBLE_ARRAY));

  private static final double BOOLEAN_TO_OBJ_DOUBLE_PRIM = 1D;

  private static final double[] BOOLEAN_TO_OBJ_DOUBLE_PRIM_ARRAY = {1, 0, 1};

  private static final Float BOOLEAN_TO_OBJ_FLOAT = 1F;

  private static final Float[] BOOLEAN_TO_OBJ_FLOAT_ARRAY = {1F, 0F, 1F};

  private static final Set<Float> BOOLEAN_TO_OBJ_FLOAT_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_FLOAT_ARRAY));

  private static final SortedSet<Float> BOOLEAN_TO_OBJ_FLOAT_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_FLOAT_ARRAY));

  private static final float BOOLEAN_TO_OBJ_FLOAT_PRIM = 1F;

  private static final float[] BOOLEAN_TO_OBJ_FLOAT_PRIM_ARRAY = {1, 0, 1};

  private static final Integer BOOLEAN_TO_OBJ_INT = 1;

  private static final Integer[] BOOLEAN_TO_OBJ_INT_ARRAY = {1, 0, 1};

  private static final Set<Integer> BOOLEAN_TO_OBJ_INT_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_INT_ARRAY));

  private static final SortedSet<Integer> BOOLEAN_TO_OBJ_INT_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_INT_ARRAY));

  private static final int BOOLEAN_TO_OBJ_INT_PRIM = 1;

  private static final int[] BOOLEAN_TO_OBJ_INT_PRIM_ARRAY = {1, 0, 1};

  private static final Long BOOLEAN_TO_OBJ_LONG = 1L;

  private static final Long[] BOOLEAN_TO_OBJ_LONG_ARRAY = {1L, 0L, 1L};

  private static final Set<Long> BOOLEAN_TO_OBJ_LONG_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_LONG_ARRAY));

  private static final SortedSet<Long> BOOLEAN_TO_OBJ_LONG_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_LONG_ARRAY));

  private static final long BOOLEAN_TO_OBJ_LONG_PRIM = 1;

  private static final long[] BOOLEAN_TO_OBJ_LONG_PRIM_ARRAY = {1, 0, 1};

  private static final Short BOOLEAN_TO_OBJ_SHORT = 1;

  private static final Short[] BOOLEAN_TO_OBJ_SHORT_ARRAY = {1, 0, 1};

  private static final Set<Short> BOOLEAN_TO_OBJ_SHORT_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_SHORT_ARRAY));

  private static final SortedSet<Short> BOOLEAN_TO_OBJ_SHORT_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_SHORT_ARRAY));

  private static final short BOOLEAN_TO_OBJ_SHORT_PRIM = 1;

  private static final short[] BOOLEAN_TO_OBJ_SHORT_PRIM_ARRAY = {1, 0, 1};

  private static final String BOOLEAN_TO_OBJ_STRING = Boolean.TRUE.toString();

  private static final String[] BOOLEAN_TO_OBJ_STRING_ARRAY =
      {Boolean.TRUE.toString(), Boolean.FALSE.toString(), Boolean.TRUE.toString()};

  private static final Set<String> BOOLEAN_TO_OBJ_STRING_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BOOLEAN_TO_OBJ_STRING_ARRAY));

  private static final SortedSet<String> BOOLEAN_TO_OBJ_STRING_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BOOLEAN_TO_OBJ_STRING_ARRAY));

  private static final Byte BYTE = 0x01;

  private static final Byte[] BYTE_ARRAY = {0x01, 0x00, 0x01};

  private static final Collection<Byte> BYTE_COLLECTION = Arrays.asList(BYTE_ARRAY);

  private static final byte BYTE_PRIM = 0x01;

  private static final byte[] BYTE_PRIM_ARRAY = {0x01, 0x00, 0x01};

  private static final Binary BYTE_TO_BSON_MULTI = new Binary(BYTE_PRIM_ARRAY);

  private static final Binary BYTE_TO_BSON_ONE = new Binary(new byte[] {BYTE});

  private static final Byte BYTE_TO_OBJ_BYTE = 0x01;

  private static final Byte[] BYTE_TO_OBJ_BYTE_ARRAY = {0x01, 0x00, 0x01};

  private static final Set<Byte> BYTE_TO_OBJ_BYTE_COLLECTION_SET =
      new HashSet<>(Arrays.asList(BYTE_TO_OBJ_BYTE_ARRAY));

  private static final SortedSet<Byte> BYTE_TO_OBJ_BYTE_COLLECTION_SORTED_SET =
      new TreeSet<>(Arrays.asList(BYTE_TO_OBJ_BYTE_ARRAY));

  private static final byte BYTE_TO_OBJ_BYTE_PRIM = 0x01;

  private static final byte[] BYTE_TO_OBJ_BYTE_PRIM_ARRAY = {0x01, 0x00, 0x01};

  private static final Calendar CALENDAR = Calendar.getInstance();

  private static final Calendar[] CALENDAR_ARRAY =
      {Calendar.getInstance(), Calendar.getInstance(), Calendar.getInstance()};

  private static final Collection<Calendar> CALENDAR_COLLECTION = new ArrayList<>();

  private static final SortedSet<Calendar> CALENDAR_SORTED_SET = new TreeSet<>();

  private static final Collection<Date> CALENDAR_TO_BSON_MULTI = new ArrayList<>();

  private static final Date CALENDAR_TO_BSON_ONE = _DATE1;

  private static final Character CHARACTER = 'a';

  private static final Character[] CHARACTER_ARRAY = {'a', 'b', 'c'};

  private static final Collection<Character> CHARACTER_COLLECTION = Arrays.asList(CHARACTER_ARRAY);

  private static final char CHARACTER_PRIM = 'a';

  private static final char[] CHARACTER_PRIM_ARRAY = {'a', 'b', 'c'};

  private static final SortedSet<Character> CHARACTER_SORTED_SET =
      new TreeSet<>(CHARACTER_COLLECTION);

  private static final String CHARACTER_TO_BSON_MULTI = new String(CHARACTER_PRIM_ARRAY);

  private static final String CHARACTER_TO_BSON_ONE = new String(new char[] {CHARACTER});

  private static final JaxBsonContext CTX = JaxBsonContext.newInstance(SimpleTypesTest.class);

  private static final JaxBsonContextImpl CTX_IMPL = (JaxBsonContextImpl) CTX;

  private static final SimpleTypes CTX_ST = new SimpleTypes();

  private static final Date DATE = _DATE1;

  private static final Date[] DATE_ARRAY = {_DATE1, _DATE2, _DATE3};

  private static final Collection<Date> DATE_COLLECTION = Arrays.asList(DATE_ARRAY);

  private static final SortedSet<Date> DATE_SORTED_SET = new TreeSet<>(DATE_COLLECTION);

  private static final Collection<Date> DATE_TO_BSON_MULTI = Arrays.asList(DATE_ARRAY);

  private static final Date DATE_TO_BSON_ONE = _DATE1;

  private static final Double DOUBLE = 1.1D;

  private static final Double[] DOUBLE_ARRAY = {1.1D, 2.2D, 3.3D};

  private static final Collection<Double> DOUBLE_COLLECTION = Arrays.asList(DOUBLE_ARRAY);

  private static final double DOUBLE_DELTA = 0.01;

  private static final double DOUBLE_PRIM = 1.1D;

  private static final double[] DOUBLE_PRIM_ARRAY = {1.1D, 2.2D, 3.3D};

  private static final Collection<Double> DOUBLE_TO_BSON_MULTI = Arrays.asList(DOUBLE_ARRAY);

  private static final Double DOUBLE_TO_BSON_ONE = DOUBLE.doubleValue();

  private static final MyEnum ENUM = MyEnum.BAR;

  private static final MyEnum[] ENUM_ARRAY = {MyEnum.BAR, MyEnum.FOO};

  private static final Collection<MyEnum> ENUM_COLLECTION = Arrays.asList(ENUM_ARRAY);

  private static final SortedSet<MyEnum> ENUM_SORTED_SET = new TreeSet<>(ENUM_COLLECTION);

  private static final Collection<String> ENUM_TO_BSON_MULTI =
      Arrays.asList(new String[] {MyEnum.BAR.name(), MyEnum.FOO.name()});

  private static final String ENUM_TO_BSON_ONE = MyEnum.BAR.name();

  private static FieldSubDescriptor FDS_BIG_DECIMAL = null;

  private static FieldSubDescriptor FDS_BIG_DECIMAL_SORTED_SET = null;

  private static FieldSubDescriptor FDS_BIG_INTEGER = null;

  private static FieldSubDescriptor FDS_BIG_INTEGER_ARRAY = null;

  private static FieldSubDescriptor FDS_BIG_INTEGER_LIST = null;

  private static FieldSubDescriptor FDS_BIG_INTEGER_SET = null;

  private static FieldSubDescriptor FDS_BIG_INTEGER_SORTED_SET = null;

  private static FieldSubDescriptor FDS_BOOLEAN = null;

  private static FieldSubDescriptor FDS_BOOLEAN_ARRAY = null;

  private static FieldSubDescriptor FDS_BOOLEAN_PRIM = null;

  private static FieldSubDescriptor FDS_BOOLEAN_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_BOOLEAN_SET = null;

  private static FieldSubDescriptor FDS_BOOLEAN_SORTED_SET = null;

  private static FieldSubDescriptor FDS_BYTE = null;

  private static FieldSubDescriptor FDS_BYTE_ARRAY = null;

  private static FieldSubDescriptor FDS_BYTE_PRIM = null;

  private static FieldSubDescriptor FDS_BYTE_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_BYTE_SET = null;

  private static FieldSubDescriptor FDS_BYTE_SORTED_SET = null;

  private static FieldSubDescriptor FDS_CALENDAR = null;

  private static FieldSubDescriptor FDS_CALENDAR_ARRAY = null;

  private static FieldSubDescriptor FDS_CALENDAR_LIST = null;

  private static FieldSubDescriptor FDS_CALENDAR_SORTED_SET = null;

  private static FieldSubDescriptor FDS_CHARACTER = null;

  private static FieldSubDescriptor FDS_CHARACTER_ARRAY = null;

  private static FieldSubDescriptor FDS_CHARACTER_LIST = null;

  private static FieldSubDescriptor FDS_CHARACTER_PRIM = null;

  private static FieldSubDescriptor FDS_CHARACTER_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_CHARACTER_SET = null;

  private static FieldSubDescriptor FDS_CHARACTER_SORTED_SET = null;

  private static FieldSubDescriptor FDS_DATE = null;

  private static FieldSubDescriptor FDS_DATE_ARRAY = null;

  private static FieldSubDescriptor FDS_DATE_LIST = null;

  private static FieldSubDescriptor FDS_DATE_SORTED_SET = null;

  private static FieldSubDescriptor FDS_DOUBLE = null;

  private static FieldSubDescriptor FDS_DOUBLE_ARRAY = null;

  private static FieldSubDescriptor FDS_DOUBLE_PRIM = null;

  private static FieldSubDescriptor FDS_DOUBLE_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_DOUBLE_SET = null;

  private static FieldSubDescriptor FDS_DOUBLE_SORTED_SET = null;

  private static FieldSubDescriptor FDS_ENUM = null;

  private static FieldSubDescriptor FDS_ENUM_ARRAY = null;

  private static FieldSubDescriptor FDS_ENUM_LIST = null;

  private static FieldSubDescriptor FDS_ENUM_SORTED_SET = null;

  private static FieldSubDescriptor FDS_FILE = null;

  private static FieldSubDescriptor FDS_FILE_ARRAY = null;

  private static FieldSubDescriptor FDS_FILE_LIST = null;

  private static FieldSubDescriptor FDS_FILE_SORTED_SET = null;

  private static FieldSubDescriptor FDS_FLOAT = null;

  private static FieldSubDescriptor FDS_FLOAT_ARRAY = null;

  private static FieldSubDescriptor FDS_FLOAT_PRIM = null;

  private static FieldSubDescriptor FDS_FLOAT_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_FLOAT_SET = null;

  private static FieldSubDescriptor FDS_FLOAT_SORTED_SET = null;

  private static FieldSubDescriptor FDS_GREGORIAN_CALENDAR = null;

  private static FieldSubDescriptor FDS_GREGORIAN_CALENDAR_ARRAY = null;

  private static FieldSubDescriptor FDS_GREGORIAN_CALENDAR_LIST = null;

  private static FieldSubDescriptor FDS_GREGORIAN_CALENDAR_SORTED_SET = null;

  private static FieldSubDescriptor FDS_INTEGER = null;

  private static FieldSubDescriptor FDS_INTEGER_ARRAY = null;

  private static FieldSubDescriptor FDS_INTEGER_LIST = null;

  private static FieldSubDescriptor FDS_INTEGER_PRIM = null;

  private static FieldSubDescriptor FDS_INTEGER_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_INTEGER_SET = null;

  private static FieldSubDescriptor FDS_INTEGER_SORTED_SET = null;

  private static FieldSubDescriptor FDS_LONG = null;

  private static FieldSubDescriptor FDS_LONG_ARRAY = null;

  private static FieldSubDescriptor FDS_LONG_LIST = null;

  private static FieldSubDescriptor FDS_LONG_PRIM = null;

  private static FieldSubDescriptor FDS_LONG_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_LONG_SET = null;

  private static FieldSubDescriptor FDS_LONG_SORTED_SET = null;

  private static FieldSubDescriptor FDS_OBJECT_ID = null;

  private static FieldSubDescriptor FDS_OBJECT_ID_ARRAY = null;

  private static FieldSubDescriptor FDS_OBJECT_ID_LIST = null;

  private static FieldSubDescriptor FDS_OBJECT_ID_SORTED_SET = null;

  private static FieldSubDescriptor FDS_SHORT = null;

  private static FieldSubDescriptor FDS_SHORT_ARRAY = null;

  private static FieldSubDescriptor FDS_SHORT_LIST = null;

  private static FieldSubDescriptor FDS_SHORT_PRIM = null;

  private static FieldSubDescriptor FDS_SHORT_PRIM_ARRAY = null;

  private static FieldSubDescriptor FDS_SHORT_SET = null;

  private static FieldSubDescriptor FDS_SHORT_SORTED_SET = null;

  private static FieldSubDescriptor FDS_STRING = null;

  private static FieldSubDescriptor FDS_STRING_ARRAY = null;

  private static FieldSubDescriptor FDS_STRING_LIST = null;

  private static FieldSubDescriptor FDS_STRING_SET = null;

  private static FieldSubDescriptor FDS_STRING_SORTED_SET = null;

  private static FieldSubDescriptor FDS_URI = null;

  private static FieldSubDescriptor FDS_URI_ARRAY = null;

  private static FieldSubDescriptor FDS_URI_LIST = null;

  private static FieldSubDescriptor FDS_URI_SORTED_SET = null;

  private static FieldSubDescriptor FDS_URL = null;

  private static FieldSubDescriptor FDS_URL_ARRAY = null;

  private static FieldSubDescriptor FDS_URL_LIST = null;

  private static FieldSubDescriptor FDS_XML_GREGORIAN_CALENDAR = null;

  private static FieldSubDescriptor FDS_XML_GREGORIAN_CALENDAR_ARRAY = null;

  private static FieldSubDescriptor FDS_XML_GREGORIAN_CALENDAR_LIST = null;

  private static final File FILE = new File("file1");

  private static final File[] FILE_ARRAY =
      {new File("file2"), new File("file2"), new File("file3")};

  private static final Collection<File> FILE_COLLECTION = Arrays.asList(FILE_ARRAY);

  private static final SortedSet<File> FILE_SORTED_SET = new TreeSet<>(FILE_COLLECTION);

  private static final Collection<String> FILE_TO_BSON_MULTI = new ArrayList<>();

  private static final String FILE_TO_BSON_ONE = FILE.getPath();

  private static final Float FLOAT = 1.1F;

  private static final Float[] FLOAT_ARRAY = {1.1F, 2.2F, 3.3F};

  private static final Collection<Float> FLOAT_COLLECTION = Arrays.asList(FLOAT_ARRAY);

  private static final float FLOAT_PRIM = 1.1F;

  private static final float[] FLOAT_PRIM_ARRAY = {1.1F, 2.2F, 3.3F};

  private static final Collection<Double> FLOAT_TO_BSON_MULTI = new ArrayList<>();

  private static final Double FLOAT_TO_BSON_ONE = FLOAT.doubleValue();

  private static final GregorianCalendar GREGORIAN_CALENDAR = new GregorianCalendar();

  private static final GregorianCalendar[] GREGORIAN_CALENDAR_ARRAY = {null, null, null};

  private static final Collection<GregorianCalendar> GREGORIAN_CALENDAR_COLLECTION =
      new ArrayList<>();

  private static final SortedSet<GregorianCalendar> GREGORIAN_CALENDAR_SORTED_SET = new TreeSet<>();

  private static final Collection<Date> GREGORIAN_CALENDAR_TO_BSON_MULTI =
      Arrays.asList(DATE_ARRAY);

  private static final Date GREGORIAN_CALENDAR_TO_BSON_ONE = _DATE1;

  private static final Integer INTEGER = 1;

  private static final Integer[] INTEGER_ARRAY = {1, 2, 3};

  private static final Collection<Integer> INTEGER_COLLECTION = Arrays.asList(INTEGER_ARRAY);

  private static final int INTEGER_PRIM = 1;

  private static final int[] INTEGER_PRIM_ARRAY = {1, 2, 3};

  private static final SortedSet<Integer> INTEGER_SORTED_SET = new TreeSet<>(INTEGER_COLLECTION);

  private static final Collection<Integer> INTEGER_TO_BSON_MULTI = Arrays.asList(INTEGER_ARRAY);

  private static final Integer INTEGER_TO_BSON_ONE = INTEGER.intValue();

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(SimpleTypesTest.class);

  private static final Long LONG = 1L;

  private static final Long[] LONG_ARRAY = {1L, 2L, 3L};

  private static final Collection<Long> LONG_COLLECTION = Arrays.asList(LONG_ARRAY);

  private static final long LONG_PRIM = 1;

  private static final long[] LONG_PRIM_ARRAY = {1, 2, 3};

  private static final SortedSet<Long> LONG_SORTED_SET = new TreeSet<>(LONG_COLLECTION);

  private static final Collection<Long> LONG_TO_BSON_MULTI = Arrays.asList(LONG_ARRAY);

  private static final Long LONG_TO_BSON_ONE = LONG.longValue();

  private static final ObjectId OBJECT_ID = _OBJECT_ID1;

  private static final ObjectId[] OBJECT_ID_ARRAY = {_OBJECT_ID1, _OBJECT_ID2, _OBJECT_ID3};

  private static final Collection<ObjectId> OBJECT_ID_COLLECTION = Arrays.asList(OBJECT_ID_ARRAY);

  private static final SortedSet<ObjectId> OBJECT_ID_SORTED_SET =
      new TreeSet<>(OBJECT_ID_COLLECTION);

  private static final Collection<ObjectId> OBJECT_ID_TO_BSON_MULTI =
      Arrays.asList(OBJECT_ID_ARRAY);

  private static final ObjectId OBJECT_ID_TO_BSON_ONE = _OBJECT_ID1;

  private static final Short SHORT = 1;

  private static final Short[] SHORT_ARRAY = {1, 2, 3};

  private static final Collection<Short> SHORT_COLLECTION = Arrays.asList(SHORT_ARRAY);

  private static final short SHORT_PRIM = 1;

  private static final short[] SHORT_PRIM_ARRAY = {1, 2, 3};

  private static final SortedSet<Short> SHORT_SORTED_SET = new TreeSet<>(SHORT_COLLECTION);

  private static final Collection<Integer> SHORT_TO_BSON_MULTI = new ArrayList<>();

  private static final Integer SHORT_TO_BSON_ONE = SHORT.intValue();

  private static final String STRING = "1";

  private static final String[] STRING_ARRAY = {"1", "2", "3"};

  private static final Collection<String> STRING_COLLECTION = Arrays.asList(STRING_ARRAY);

  private static final SortedSet<String> STRING_SORTED_SET = new TreeSet<>(STRING_COLLECTION);

  private static final Collection<String> STRING_TO_BSON_MULTI = Arrays.asList(STRING_ARRAY);

  private static final String STRING_TO_BSON_ONE = "1";

  private static URI URI = null;

  private static final URI[] URI_ARRAY = {null, null, null};

  private static final Collection<URI> URI_COLLECTION = new ArrayList<>();

  private static final TreeSet<URI> URI_SORTED_SET = new TreeSet<>();

  private static final Collection<String> URI_TO_BSON_MULTI = new ArrayList<>();

  private static String URI_TO_BSON_ONE = null;

  private static URL URL = null;

  private static final URL[] URL_ARRAY = {null, null, null};

  private static final Collection<URL> URL_COLLECTION = new ArrayList<>();

  private static final Collection<String> URL_TO_BSON_MULTI = new ArrayList<>();

  private static String URL_TO_BSON_ONE = null;

  private static XMLGregorianCalendar XML_GREGORIAN_CALENDAR;

  private static final XMLGregorianCalendar[] XML_GREGORIAN_CALENDAR_ARRAY = {null, null, null};

  private static final Collection<XMLGregorianCalendar> XML_GREGORIAN_CALENDAR_COLLECTION =
      new ArrayList<>();

  private static final Collection<Date> XML_GREGORIAN_CALENDAR_TO_BSON_MULTI =
      Arrays.asList(DATE_ARRAY);

  private static final Date XML_GREGORIAN_CALENDAR_TO_BSON_ONE = _DATE1;

  static {
    for (final BigDecimal e : BIG_DECIMAL_ARRAY) {
      BIG_DECIMAL_TO_BSON_MULTI.add(e.doubleValue());
    }
  }

  static {
    for (final BigInteger e : BIG_INTEGER_ARRAY) {
      BIG_INTEGER_TO_BSON_MULTI.add(e.longValue());
    }
  }

  static {
    for (final File e : FILE_ARRAY) {
      FILE_TO_BSON_MULTI.add(e.getPath());
    }
  }

  static {
    try {
      for (int i = 0; i < DATE_ARRAY.length; i++) {
        final Date e = DATE_ARRAY[i];
        CALENDAR_ARRAY[i].setTime(e);
        CALENDAR_COLLECTION.add(CALENDAR_ARRAY[i]);
        CALENDAR_SORTED_SET.add(CALENDAR_ARRAY[i]);
        CALENDAR_TO_BSON_MULTI.add(e);
        if (i == 0) {
          CALENDAR.setTime(e);
        }
      }
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    for (int i = 0; i < DATE_ARRAY.length; i++) {
      final Date e = DATE_ARRAY[i];
      GREGORIAN_CALENDAR_ARRAY[i] = new GregorianCalendar();
      GREGORIAN_CALENDAR_ARRAY[i].setTime(e);
      GREGORIAN_CALENDAR_COLLECTION.add(GREGORIAN_CALENDAR_ARRAY[i]);
      GREGORIAN_CALENDAR_SORTED_SET.add(GREGORIAN_CALENDAR_ARRAY[i]);
      if (i == 0) {
        GREGORIAN_CALENDAR.setTime(e);
      }
    }
  }

  static {
    try {
      for (int i = 0; i < GREGORIAN_CALENDAR_ARRAY.length; i++) {
        final GregorianCalendar e = GREGORIAN_CALENDAR_ARRAY[i];
        XML_GREGORIAN_CALENDAR_ARRAY[i] = DatatypeFactory.newInstance().newXMLGregorianCalendar(e);
        XML_GREGORIAN_CALENDAR_COLLECTION.add(XML_GREGORIAN_CALENDAR_ARRAY[i]);
        if (i == 0) {
          XML_GREGORIAN_CALENDAR = XML_GREGORIAN_CALENDAR_ARRAY[i];
        }
      }
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    for (final Float e : FLOAT_ARRAY) {
      FLOAT_TO_BSON_MULTI.add(e.doubleValue());
    }
  }

  static {
    for (final Short e : SHORT_ARRAY) {
      SHORT_TO_BSON_MULTI.add(e.intValue());
    }
  }

  static {
    try {
      final URI _URI1 = new URI("urn:" + 1);
      final URI _URI2 = new URI("urn:" + 2);
      final URI _URI3 = new URI("urn:" + 3);
      URI = _URI1;
      URI_ARRAY[0] = _URI1;
      URI_ARRAY[1] = _URI2;
      URI_ARRAY[2] = _URI3;
      URI_TO_BSON_ONE = _URI1.toString();
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug.", e);
    }
    for (int i = 0; i < URI_ARRAY.length; i++) {
      final URI e = URI_ARRAY[i];
      URI_COLLECTION.add(e);
      URI_SORTED_SET.add(e);
      URI_TO_BSON_MULTI.add(e.toString());
    }
  }

  static {
    try {
      final URL _URL1 = new URL("http://host/path/" + 1);
      final URL _URL2 = new URL("http://host/path/" + 2);
      final URL _URL3 = new URL("http://host/path/" + 3);
      URL = _URL1;
      URL_ARRAY[0] = _URL1;
      URL_ARRAY[1] = _URL2;
      URL_ARRAY[2] = _URL3;
      URL_TO_BSON_ONE = _URL1.toString();
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug.", e);
    }
    for (int i = 0; i < URL_ARRAY.length; i++) {
      final URL e = URL_ARRAY[i];
      URL_COLLECTION.add(e);
      URL_TO_BSON_MULTI.add(e.toString());
    }
  }

  static {
    try {
      FDS_BOOLEAN =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBoolean"))
              .getFieldSubDescriptor();
      FDS_BOOLEAN_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBooleanArray"))
              .getFieldSubDescriptor();
      FDS_BOOLEAN_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBooleanSet"))
              .getFieldSubDescriptor();
      FDS_BOOLEAN_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBooleanSortedSet")).getFieldSubDescriptor();
      FDS_BOOLEAN_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBooleanPri"))
              .getFieldSubDescriptor();
      FDS_BOOLEAN_PRIM_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBooleanPriArray")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_BYTE = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldByte"))
          .getFieldSubDescriptor();
      FDS_BYTE_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldByteArray"))
              .getFieldSubDescriptor();
      FDS_BYTE_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldByteSet"))
              .getFieldSubDescriptor();
      FDS_BYTE_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldByteSortedSet")).getFieldSubDescriptor();
      FDS_BYTE_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBytePri"))
              .getFieldSubDescriptor();
      FDS_BYTE_PRIM_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBytePriArray"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_CHARACTER =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldCharacter"))
              .getFieldSubDescriptor();
      FDS_CHARACTER_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldCharacterArray")).getFieldSubDescriptor();
      FDS_CHARACTER_LIST = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldCharacterList")).getFieldSubDescriptor();
      FDS_CHARACTER_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldCharacterSet"))
              .getFieldSubDescriptor();
      FDS_CHARACTER_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldCharacterSortedSet"))
              .getFieldSubDescriptor();
      FDS_CHARACTER_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldCharPri"))
              .getFieldSubDescriptor();
      FDS_CHARACTER_PRIM_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldCharPriArray"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_DOUBLE =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDouble"))
              .getFieldSubDescriptor();
      FDS_DOUBLE_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDoubleArray"))
              .getFieldSubDescriptor();
      FDS_DOUBLE_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDoubleSet"))
              .getFieldSubDescriptor();
      FDS_DOUBLE_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldDoubleSortedSet")).getFieldSubDescriptor();
      FDS_DOUBLE_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDoublePri"))
              .getFieldSubDescriptor();
      FDS_DOUBLE_PRIM_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldDoublePriArray")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_FLOAT =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFloat"))
              .getFieldSubDescriptor();
      FDS_FLOAT_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFloatArray"))
              .getFieldSubDescriptor();
      FDS_FLOAT_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFloatSet"))
              .getFieldSubDescriptor();
      FDS_FLOAT_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldFloatSortedSet")).getFieldSubDescriptor();
      FDS_FLOAT_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFloatPri"))
              .getFieldSubDescriptor();
      FDS_FLOAT_PRIM_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldFloatPriArray")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_INTEGER =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldInteger"))
              .getFieldSubDescriptor();
      FDS_INTEGER_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldIntegerArray"))
              .getFieldSubDescriptor();
      FDS_INTEGER_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldIntegerList"))
              .getFieldSubDescriptor();
      FDS_INTEGER_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldIntegerSet"))
              .getFieldSubDescriptor();
      FDS_INTEGER_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldIntegerSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_INTEGER_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldIntPri"))
              .getFieldSubDescriptor();
      FDS_INTEGER_PRIM_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldIntPriArray"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_LONG = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldLong"))
          .getFieldSubDescriptor();
      FDS_LONG_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldLongArray"))
              .getFieldSubDescriptor();
      FDS_LONG_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldLongList"))
              .getFieldSubDescriptor();
      FDS_LONG_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldLongSet"))
              .getFieldSubDescriptor();
      FDS_LONG_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldLongSortedSet")).getFieldSubDescriptor();
      FDS_LONG_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldLongPri"))
              .getFieldSubDescriptor();
      FDS_LONG_PRIM_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldLongPriArray"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_SHORT =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldShort"))
              .getFieldSubDescriptor();
      FDS_SHORT_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldShortArray"))
              .getFieldSubDescriptor();
      FDS_SHORT_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldShortList"))
              .getFieldSubDescriptor();
      FDS_SHORT_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldShortSet"))
              .getFieldSubDescriptor();
      FDS_SHORT_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldShortSortedSet")).getFieldSubDescriptor();
      FDS_SHORT_PRIM =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldShortPri"))
              .getFieldSubDescriptor();
      FDS_SHORT_PRIM_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldShortPriArray")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_BIG_DECIMAL =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBigDecimal"))
              .getFieldSubDescriptor();
      FDS_BIG_DECIMAL_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBigDecimalSortedSet"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_BIG_INTEGER =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldBigInteger"))
              .getFieldSubDescriptor();
      FDS_BIG_INTEGER_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBigIntegerArray")).getFieldSubDescriptor();
      FDS_BIG_INTEGER_LIST = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBigIntegerList")).getFieldSubDescriptor();
      FDS_BIG_INTEGER_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBigIntegerSet")).getFieldSubDescriptor();
      FDS_BIG_INTEGER_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldBigIntegerSortedSet"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_CALENDAR =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldCalendar"))
              .getFieldSubDescriptor();
      FDS_CALENDAR_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldCalendarArray")).getFieldSubDescriptor();
      FDS_CALENDAR_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldCalendarList"))
              .getFieldSubDescriptor();
      FDS_CALENDAR_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldCalendarSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_DATE = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDate"))
          .getFieldSubDescriptor();
      FDS_DATE_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDateArray"))
              .getFieldSubDescriptor();
      FDS_DATE_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldDateList"))
              .getFieldSubDescriptor();
      FDS_DATE_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldDateSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_ENUM = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldEnum"))
          .getFieldSubDescriptor();
      FDS_ENUM_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldEnumArray"))
              .getFieldSubDescriptor();
      FDS_ENUM_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldEnumList"))
              .getFieldSubDescriptor();
      FDS_ENUM_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldEnumSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_FILE = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFile"))
          .getFieldSubDescriptor();
      FDS_FILE_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFileArray"))
              .getFieldSubDescriptor();
      FDS_FILE_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldFileList"))
              .getFieldSubDescriptor();
      FDS_FILE_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldFileSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_GREGORIAN_CALENDAR = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldGregorianCalendar")).getFieldSubDescriptor();
      FDS_GREGORIAN_CALENDAR_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldGregorianCalendarArray"))
              .getFieldSubDescriptor();
      FDS_GREGORIAN_CALENDAR_LIST = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldGregorianCalendarList"))
              .getFieldSubDescriptor();
      FDS_GREGORIAN_CALENDAR_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldGregorianCalendarSortedSet"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_OBJECT_ID =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldObjectId"))
              .getFieldSubDescriptor();
      FDS_OBJECT_ID_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldObjectIdArray")).getFieldSubDescriptor();
      FDS_OBJECT_ID_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldObjectIdList"))
              .getFieldSubDescriptor();
      FDS_OBJECT_ID_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldObjectIdSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_STRING =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldString"))
              .getFieldSubDescriptor();
      FDS_STRING_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldStringArray"))
              .getFieldSubDescriptor();
      FDS_STRING_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldStringList"))
              .getFieldSubDescriptor();
      FDS_STRING_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldStringSet"))
              .getFieldSubDescriptor();
      FDS_STRING_SORTED_SET = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldStringSortedSet")).getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_URI = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURI"))
          .getFieldSubDescriptor();
      FDS_URI_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURIArray"))
              .getFieldSubDescriptor();
      FDS_URI_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURIList"))
              .getFieldSubDescriptor();
      FDS_URI_SORTED_SET =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURISortedSet"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_URL = new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURL"))
          .getFieldSubDescriptor();
      FDS_URL_ARRAY =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURLArray"))
              .getFieldSubDescriptor();
      FDS_URL_LIST =
          new FieldDescriptor(CTX_IMPL, SimpleTypesTest.class.getDeclaredField("fieldURLList"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  static {
    try {
      FDS_XML_GREGORIAN_CALENDAR = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldXMLGregorianCalendar"))
              .getFieldSubDescriptor();
      FDS_XML_GREGORIAN_CALENDAR_ARRAY = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldXMLGregorianCalendarArray"))
              .getFieldSubDescriptor();
      FDS_XML_GREGORIAN_CALENDAR_LIST = new FieldDescriptor(CTX_IMPL,
          SimpleTypesTest.class.getDeclaredField("fieldXMLGregorianCalendarList"))
              .getFieldSubDescriptor();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug.", e);
    }
  }

  private static void assertEqualsByDouble(final String message, final Collection<?> expected,
      final Collection<?> actual) {
    if (expected == null && actual != null) {
      fail(message + ", expected==null, actual != null");
    }
    if (expected != null && actual == null) {
      fail(message + ", expected!=null, actual == null");
    }
    if (expected.size() != actual.size()) {
      fail(message + ", expected.size() != actual.size(): " + expected.size() + "!="
          + actual.size());
    }
    int i = 0;
    final Iterator<?> expectedIterator = expected.iterator();
    final Iterator<?> actualIterator = actual.iterator();
    while (expectedIterator.hasNext()) {
      final Object expectedNext = expectedIterator.next();
      final Object actualNext = actualIterator.next();
      if (expectedNext == null && actualNext == null) {
        continue;
      }
      if (expectedNext == null && actualNext != null) {
        fail(message + ", expected.get(" + i + ")==null, actual.get(" + i + ")!= null");
      }
      if (expectedNext != null && actualNext == null) {
        fail(message + ", expected.get(" + i + ")!=null, actual.get(" + i + ")==null");
      }
      if (!Number.class.isAssignableFrom(expectedNext.getClass())) {
        throw new IllegalStateException("expected.get(" + i + ") is not a number");
      }
      if (!Number.class.isAssignableFrom(actualNext.getClass())) {
        fail(message + ", actual.get(" + i + ") is not a number");
      }
      assertEquals(message + ", actual.get(" + i + ")", ((Number) expectedNext).doubleValue(),
          ((Number) actualNext).doubleValue(), DOUBLE_DELTA);
      i++;
    }
  }

  @SuppressWarnings("unused")
  private BigDecimal fieldBigDecimal;

  @SuppressWarnings("unused")
  private BigDecimal[] fieldBigDecimalArray;

  @SuppressWarnings("unused")
  private List<BigDecimal> fieldBigDecimalList;

  @SuppressWarnings("unused")
  private Set<BigDecimal> fieldBigDecimalSet;

  @SuppressWarnings("unused")
  private SortedSet<BigDecimal> fieldBigDecimalSortedSet;

  @SuppressWarnings("unused")
  private BigInteger fieldBigInteger;

  @SuppressWarnings("unused")
  private BigInteger[] fieldBigIntegerArray;

  @SuppressWarnings("unused")
  private List<BigInteger> fieldBigIntegerList;

  @SuppressWarnings("unused")
  private Set<BigInteger> fieldBigIntegerSet;

  @SuppressWarnings("unused")
  private SortedSet<BigInteger> fieldBigIntegerSortedSet;

  @SuppressWarnings("unused")
  private Boolean fieldBoolean;

  @SuppressWarnings("unused")
  private Boolean[] fieldBooleanArray;

  @SuppressWarnings("unused")
  private List<Boolean> fieldBooleanList;

  @SuppressWarnings("unused")
  private boolean fieldBooleanPri;

  @SuppressWarnings("unused")
  private boolean[] fieldBooleanPriArray;

  @SuppressWarnings("unused")
  private Set<Boolean> fieldBooleanSet;

  @SuppressWarnings("unused")
  private SortedSet<Boolean> fieldBooleanSortedSet;

  @SuppressWarnings("unused")
  private Byte fieldByte;

  @SuppressWarnings("unused")
  private Byte[] fieldByteArray;

  @SuppressWarnings("unused")
  private List<Byte> fieldByteList;

  @SuppressWarnings("unused")
  private byte fieldBytePri;

  @SuppressWarnings("unused")
  private byte[] fieldBytePriArray;

  @SuppressWarnings("unused")
  private Set<Byte> fieldByteSet;

  @SuppressWarnings("unused")
  private SortedSet<Byte> fieldByteSortedSet;

  @SuppressWarnings("unused")
  private Calendar fieldCalendar;

  @SuppressWarnings("unused")
  private Calendar[] fieldCalendarArray;

  @SuppressWarnings("unused")
  private List<Calendar> fieldCalendarList;

  @SuppressWarnings("unused")
  private Set<Calendar> fieldCalendarSet;

  @SuppressWarnings("unused")
  private SortedSet<Calendar> fieldCalendarSortedSet;

  @SuppressWarnings("unused")
  private Character fieldCharacter;

  @SuppressWarnings("unused")
  private Character[] fieldCharacterArray;

  @SuppressWarnings("unused")
  private List<Character> fieldCharacterList;

  @SuppressWarnings("unused")
  private Set<Character> fieldCharacterSet;

  @SuppressWarnings("unused")
  private SortedSet<Character> fieldCharacterSortedSet;

  @SuppressWarnings("unused")
  private char fieldCharPri;

  @SuppressWarnings("unused")
  private char[] fieldCharPriArray;

  @SuppressWarnings("unused")
  private Date fieldDate;

  @SuppressWarnings("unused")
  private Date[] fieldDateArray;

  @SuppressWarnings("unused")
  private List<Date> fieldDateList;

  @SuppressWarnings("unused")
  private Set<Date> fieldDateSet;

  @SuppressWarnings("unused")
  private SortedSet<Date> fieldDateSortedSet;

  @SuppressWarnings("unused")
  private Double fieldDouble;

  @SuppressWarnings("unused")
  private Double[] fieldDoubleArray;

  @SuppressWarnings("unused")
  private List<Double> fieldDoubleList;

  @SuppressWarnings("unused")
  private double fieldDoublePri;

  @SuppressWarnings("unused")
  private double[] fieldDoublePriArray;

  @SuppressWarnings("unused")
  private Set<Double> fieldDoubleSet;

  @SuppressWarnings("unused")
  private SortedSet<Double> fieldDoubleSortedSet;

  @SuppressWarnings("unused")
  private MyEnum fieldEnum;

  @SuppressWarnings("unused")
  private MyEnum[] fieldEnumArray;

  @SuppressWarnings("unused")
  private List<MyEnum> fieldEnumList;

  @SuppressWarnings("unused")
  private Set<MyEnum> fieldEnumSet;

  @SuppressWarnings("unused")
  private SortedSet<MyEnum> fieldEnumSortedSet;

  @SuppressWarnings("unused")
  private File fieldFile;

  @SuppressWarnings("unused")
  private File[] fieldFileArray;

  @SuppressWarnings("unused")
  private List<File> fieldFileList;

  @SuppressWarnings("unused")
  private Set<File> fieldFileSet;

  @SuppressWarnings("unused")
  private SortedSet<File> fieldFileSortedSet;

  @SuppressWarnings("unused")
  private Float fieldFloat;

  @SuppressWarnings("unused")
  private Float[] fieldFloatArray;

  @SuppressWarnings("unused")
  private List<Float> fieldFloatList;

  @SuppressWarnings("unused")
  private float fieldFloatPri;

  @SuppressWarnings("unused")
  private float[] fieldFloatPriArray;

  @SuppressWarnings("unused")
  private Set<Float> fieldFloatSet;

  @SuppressWarnings("unused")
  private SortedSet<Float> fieldFloatSortedSet;

  @SuppressWarnings("unused")
  private GregorianCalendar fieldGregorianCalendar;

  @SuppressWarnings("unused")
  private GregorianCalendar[] fieldGregorianCalendarArray;

  @SuppressWarnings("unused")
  private List<GregorianCalendar> fieldGregorianCalendarList;

  @SuppressWarnings("unused")
  private Set<GregorianCalendar> fieldGregorianCalendarSet;

  @SuppressWarnings("unused")
  private SortedSet<GregorianCalendar> fieldGregorianCalendarSortedSet;

  @SuppressWarnings("unused")
  private Integer fieldInteger;

  @SuppressWarnings("unused")
  private Integer[] fieldIntegerArray;

  @SuppressWarnings("unused")
  private List<Integer> fieldIntegerList;

  @SuppressWarnings("unused")
  private Set<Integer> fieldIntegerSet;

  @SuppressWarnings("unused")
  private SortedSet<Integer> fieldIntegerSortedSet;

  @SuppressWarnings("unused")
  private int fieldIntPri;

  @SuppressWarnings("unused")
  private int[] fieldIntPriArray;

  @SuppressWarnings("unused")
  private Long fieldLong;

  @SuppressWarnings("unused")
  private Long[] fieldLongArray;

  @SuppressWarnings("unused")
  private List<Long> fieldLongList;

  @SuppressWarnings("unused")
  private long fieldLongPri;

  @SuppressWarnings("unused")
  private long[] fieldLongPriArray;

  @SuppressWarnings("unused")
  private Set<Long> fieldLongSet;

  @SuppressWarnings("unused")
  private SortedSet<Long> fieldLongSortedSet;

  @SuppressWarnings("unused")
  private ObjectId fieldObjectId;

  @SuppressWarnings("unused")
  private ObjectId[] fieldObjectIdArray;

  @SuppressWarnings("unused")
  private List<ObjectId> fieldObjectIdList;

  @SuppressWarnings("unused")
  private Set<ObjectId> fieldObjectIdSet;

  @SuppressWarnings("unused")
  private SortedSet<ObjectId> fieldObjectIdSortedSet;

  @SuppressWarnings("unused")
  private Short fieldShort;

  @SuppressWarnings("unused")
  private Short[] fieldShortArray;

  @SuppressWarnings("unused")
  private List<Short> fieldShortList;

  @SuppressWarnings("unused")
  private short fieldShortPri;

  @SuppressWarnings("unused")
  private short[] fieldShortPriArray;

  @SuppressWarnings("unused")
  private Set<Short> fieldShortSet;

  @SuppressWarnings("unused")
  private SortedSet<Short> fieldShortSortedSet;

  @SuppressWarnings("unused")
  private String fieldString;

  @SuppressWarnings("unused")
  private String[] fieldStringArray;

  @SuppressWarnings("unused")
  private List<String> fieldStringList;

  @SuppressWarnings("unused")
  private Set<String> fieldStringSet;

  @SuppressWarnings("unused")
  private SortedSet<String> fieldStringSortedSet;

  @SuppressWarnings("unused")
  private URI fieldURI;

  @SuppressWarnings("unused")
  private URI[] fieldURIArray;

  @SuppressWarnings("unused")
  private List<URI> fieldURIList;

  @SuppressWarnings("unused")
  private SortedSet<URI> fieldURISortedSet;

  @SuppressWarnings("unused")
  private URL fieldURL;

  @SuppressWarnings("unused")
  private URL[] fieldURLArray;

  @SuppressWarnings("unused")
  private List<URL> fieldURLList;

  @SuppressWarnings("unused")
  private SortedSet<URL> fieldURLSortedSet;

  @SuppressWarnings("unused")
  private XMLGregorianCalendar fieldXMLGregorianCalendar;

  @SuppressWarnings("unused")
  private XMLGregorianCalendar[] fieldXMLGregorianCalendarArray;

  @SuppressWarnings("unused")
  private List<XMLGregorianCalendar> fieldXMLGregorianCalendarList;

  @SuppressWarnings("unused")
  private Set<XMLGregorianCalendar> fieldXMLGregorianCalendarSet;

  @SuppressWarnings("unused")
  private SortedSet<XMLGregorianCalendar> fieldXMLGregorianCalendarSortedSet;

  @Test
  public void test_big_decimal() throws Exception {

    assertEquals("BigDecimal", BIG_DECIMAL_TO_BSON_ONE.doubleValue(),
        ((Number) CTX_ST.toBson(FDS_BIG_DECIMAL.getFd().getFieldCtx(), BIG_DECIMAL)).doubleValue(),
        DOUBLE_DELTA);
    assertEqualsByDouble("BigDecimal[]", BIG_DECIMAL_TO_BSON_MULTI,
        (Collection<?>) CTX_ST.toBson(FDS_BIG_DECIMAL.getFd().getFieldCtx(), BIG_DECIMAL_ARRAY));
    assertEqualsByDouble("Collection<BigDecimal>", BIG_DECIMAL_TO_BSON_MULTI, (Collection<?>) CTX_ST
        .toBson(FDS_BIG_DECIMAL.getFd().getFieldCtx(), BIG_DECIMAL_COLLECTION));

    assertEquals("BigDecimal==>BigDecimal", BIG_DECIMAL.doubleValue(),
        ((BigDecimal) CTX_ST.toObject(FDS_BIG_DECIMAL, BIG_DECIMAL_TO_BSON_ONE)).doubleValue(),
        0.001);
  }

  @Test
  public void test_big_integer() throws Exception {

    assertEquals("BigInteger", BIG_INTEGER_TO_BSON_ONE.doubleValue(),
        ((Number) CTX_ST.toBson(FDS_BIG_INTEGER.getFd().getFieldCtx(), BIG_INTEGER)).doubleValue(),
        DOUBLE_DELTA);
    assertEqualsByDouble("BigInteger[]", BIG_INTEGER_TO_BSON_MULTI,
        (Collection<?>) CTX_ST.toBson(FDS_BIG_INTEGER.getFd().getFieldCtx(), BIG_INTEGER_ARRAY));
    assertEqualsByDouble("Collection<BigInteger>", BIG_INTEGER_TO_BSON_MULTI, (Collection<?>) CTX_ST
        .toBson(FDS_BIG_INTEGER.getFd().getFieldCtx(), BIG_INTEGER_COLLECTION));

    assertEquals("BigInteger==>BigInteger", BIG_INTEGER.longValue(),
        ((BigInteger) CTX_ST.toObject(FDS_BIG_INTEGER, BIG_INTEGER_TO_BSON_ONE)).longValue());
    assertArrayEquals("Collecton<BigInteger>==>BigInteger[]", BIG_INTEGER_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_BIG_INTEGER_ARRAY, BIG_INTEGER_TO_BSON_MULTI));
    assertEquals("Collecton<BigInteger>==>List<BigInteger>", BIG_INTEGER_COLLECTION,
        CTX_ST.toObject(FDS_BIG_INTEGER_LIST, BIG_INTEGER_TO_BSON_MULTI));
    assertEquals("Collecton<BigInteger>==>SortedSet<BigInteger>", BIG_INTEGER_SORTED_SET,
        CTX_ST.toObject(FDS_BIG_INTEGER_SORTED_SET, BIG_INTEGER_TO_BSON_MULTI));
  }

  @Test
  public void test_boolean() throws Exception {

    assertEquals("Boolean=>Bson", BOOLEAN_TO_BSON_ONE,
        CTX_ST.toBson(FDS_BOOLEAN.getFd().getFieldCtx(), BOOLEAN));
    assertEquals("boolean=>Bson", BOOLEAN_TO_BSON_ONE,
        CTX_ST.toBson(FDS_BOOLEAN.getFd().getFieldCtx(), BOOLEAN_PRIM));
    assertEquals("Boolean[]=>Bson", BOOLEAN_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_BOOLEAN.getFd().getFieldCtx(), BOOLEAN_ARRAY));
    assertEquals("boolean[]=>Bson", BOOLEAN_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_BOOLEAN.getFd().getFieldCtx(), BOOLEAN_PRIM_ARRAY));
    assertEquals("Collection<Boolean>=>Bson", BOOLEAN_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_BOOLEAN.getFd().getFieldCtx(), BOOLEAN_COLLECTION));

    assertEquals("Boolean==>Boolean", BOOLEAN, CTX_ST.toObject(FDS_BOOLEAN, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>boolean", BOOLEAN_PRIM,
        CTX_ST.toObject(FDS_BOOLEAN_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Boolean[]", BOOLEAN_ARRAY,
        (Boolean[]) CTX_ST.toObject(FDS_BOOLEAN_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>boolean[]", BOOLEAN_PRIM_ARRAY,
        (boolean[]) CTX_ST.toObject(FDS_BOOLEAN_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<Boolean>", BOOLEAN_COLLECTION_SET,
        CTX_ST.toObject(FDS_BOOLEAN_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Boolean>", BOOLEAN_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_BOOLEAN_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Byte", BOOLEAN_TO_OBJ_BYTE,
        CTX_ST.toObject(FDS_BYTE, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>byte", BOOLEAN_TO_OBJ_BYTE_PRIM,
        CTX_ST.toObject(FDS_BYTE_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Byte[]", BOOLEAN_TO_OBJ_BYTE_ARRAY,
        (Byte[]) CTX_ST.toObject(FDS_BYTE_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>byte[]", BOOLEAN_TO_OBJ_BYTE_PRIM_ARRAY,
        (byte[]) CTX_ST.toObject(FDS_BYTE_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<Byte>", BOOLEAN_TO_OBJ_BYTE_COLLECTION_SET,
        CTX_ST.toObject(FDS_BYTE_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Byte>", BOOLEAN_TO_OBJ_BYTE_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_BYTE_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Character", BOOLEAN_TO_OBJ_CHAR,
        CTX_ST.toObject(FDS_CHARACTER, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>char", BOOLEAN_TO_OBJ_CHAR_PRIM,
        CTX_ST.toObject(FDS_CHARACTER_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Character[]", BOOLEAN_TO_OBJ_CHAR_ARRAY,
        (Character[]) CTX_ST.toObject(FDS_CHARACTER_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>char[]", BOOLEAN_TO_OBJ_CHAR_PRIM_ARRAY,
        (char[]) CTX_ST.toObject(FDS_CHARACTER_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<Character>", BOOLEAN_TO_OBJ_CHAR_COLLECTION_SET,
        CTX_ST.toObject(FDS_CHARACTER_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Character>", BOOLEAN_TO_OBJ_CHAR_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_CHARACTER_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Double", BOOLEAN_TO_OBJ_DOUBLE,
        CTX_ST.toObject(FDS_DOUBLE, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>double", BOOLEAN_TO_OBJ_DOUBLE_PRIM,
        CTX_ST.toObject(FDS_DOUBLE_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Double[]", BOOLEAN_TO_OBJ_DOUBLE_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_DOUBLE_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>double[]", BOOLEAN_TO_OBJ_DOUBLE_PRIM_ARRAY,
        (double[]) CTX_ST.toObject(FDS_DOUBLE_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST), 0.1D);
    assertEquals("List<Boolean>==>Set<Double>", BOOLEAN_TO_OBJ_DOUBLE_COLLECTION_SET,
        CTX_ST.toObject(FDS_DOUBLE_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Double>", BOOLEAN_TO_OBJ_DOUBLE_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_DOUBLE_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Float", BOOLEAN_TO_OBJ_FLOAT,
        CTX_ST.toObject(FDS_FLOAT, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>float", BOOLEAN_TO_OBJ_FLOAT_PRIM,
        CTX_ST.toObject(FDS_FLOAT_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Float[]", BOOLEAN_TO_OBJ_FLOAT_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_FLOAT_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>float[]", BOOLEAN_TO_OBJ_FLOAT_PRIM_ARRAY,
        (float[]) CTX_ST.toObject(FDS_FLOAT_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST), 0.1F);
    assertEquals("List<Boolean>==>Set<Float>", BOOLEAN_TO_OBJ_FLOAT_COLLECTION_SET,
        CTX_ST.toObject(FDS_FLOAT_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Float>", BOOLEAN_TO_OBJ_FLOAT_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_FLOAT_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Integer", BOOLEAN_TO_OBJ_INT,
        CTX_ST.toObject(FDS_INTEGER, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>int", BOOLEAN_TO_OBJ_INT_PRIM,
        CTX_ST.toObject(FDS_INTEGER_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Integer[]", BOOLEAN_TO_OBJ_INT_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_INTEGER_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>int[]", BOOLEAN_TO_OBJ_INT_PRIM_ARRAY,
        (int[]) CTX_ST.toObject(FDS_INTEGER_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<Integer>", BOOLEAN_TO_OBJ_INT_COLLECTION_SET,
        CTX_ST.toObject(FDS_INTEGER_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Integer>", BOOLEAN_TO_OBJ_INT_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_INTEGER_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Long", BOOLEAN_TO_OBJ_LONG,
        CTX_ST.toObject(FDS_LONG, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>long", BOOLEAN_TO_OBJ_LONG_PRIM,
        CTX_ST.toObject(FDS_LONG_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Long[]", BOOLEAN_TO_OBJ_LONG_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_LONG_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>long[]", BOOLEAN_TO_OBJ_LONG_PRIM_ARRAY,
        (long[]) CTX_ST.toObject(FDS_LONG_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<Long>", BOOLEAN_TO_OBJ_LONG_COLLECTION_SET,
        CTX_ST.toObject(FDS_LONG_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Long>", BOOLEAN_TO_OBJ_LONG_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_LONG_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>Short", BOOLEAN_TO_OBJ_SHORT,
        CTX_ST.toObject(FDS_SHORT, BOOLEAN_TO_BSON_ONE));
    assertEquals("boolean==>short", BOOLEAN_TO_OBJ_SHORT_PRIM,
        CTX_ST.toObject(FDS_SHORT_PRIM, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>Short[]", BOOLEAN_TO_OBJ_SHORT_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_SHORT_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertArrayEquals("List<Boolean>==>short[]", BOOLEAN_TO_OBJ_SHORT_PRIM_ARRAY,
        (short[]) CTX_ST.toObject(FDS_SHORT_PRIM_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<Short>", BOOLEAN_TO_OBJ_SHORT_COLLECTION_SET,
        CTX_ST.toObject(FDS_SHORT_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<Short>", BOOLEAN_TO_OBJ_SHORT_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_SHORT_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>BigDecimal", BOOLEAN_TO_OBJ_BIG_DECIMAL.doubleValue(),
        ((BigDecimal) CTX_ST.toObject(FDS_BIG_DECIMAL, BOOLEAN_TO_BSON_ONE)).doubleValue(),
        DOUBLE_DELTA);
    assertEquals("List<Boolean>==>SortedSet<BigDecimal>",
        BOOLEAN_TO_OBJ_BIG_DECIMAL_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_BIG_DECIMAL_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>BigInteger", BOOLEAN_TO_OBJ_BIG_INTEGER,
        CTX_ST.toObject(FDS_BIG_INTEGER, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>BigInteger[]", BOOLEAN_TO_OBJ_BIG_INTEGER_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_BIG_INTEGER_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<BigInteger>", BOOLEAN_TO_OBJ_BIG_INTEGER_COLLECTION_SET,
        CTX_ST.toObject(FDS_BIG_INTEGER_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<BigInteger>",
        BOOLEAN_TO_OBJ_BIG_INTEGER_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_BIG_INTEGER_SORTED_SET, BOOLEAN_COLLECTION_LIST));

    assertEquals("Boolean==>String", BOOLEAN_TO_OBJ_STRING,
        CTX_ST.toObject(FDS_STRING, BOOLEAN_TO_BSON_ONE));
    assertArrayEquals("List<Boolean>==>String[]", BOOLEAN_TO_OBJ_STRING_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_STRING_ARRAY, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>Set<String>", BOOLEAN_TO_OBJ_STRING_COLLECTION_SET,
        CTX_ST.toObject(FDS_STRING_SET, BOOLEAN_COLLECTION_LIST));
    assertEquals("List<Boolean>==>SortedSet<String>", BOOLEAN_TO_OBJ_STRING_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_STRING_SORTED_SET, BOOLEAN_COLLECTION_LIST));
  }

  @Test
  public void test_byte() throws Exception {

    assertEquals("Byte=>Bson", BYTE_TO_BSON_ONE,
        CTX_ST.toBson(FDS_BYTE.getFd().getFieldCtx(), BYTE));
    assertEquals("byte=>Bson", BYTE_TO_BSON_ONE,
        CTX_ST.toBson(FDS_BYTE.getFd().getFieldCtx(), BYTE_PRIM));
    assertArrayEquals("Byte[]=>Bson", BYTE_TO_BSON_MULTI.getData(),
        ((Binary) CTX_ST.toBson(FDS_BYTE.getFd().getFieldCtx(), BYTE_ARRAY)).getData());
    assertArrayEquals("byte[]=>Bson", BYTE_TO_BSON_MULTI.getData(),
        ((Binary) CTX_ST.toBson(FDS_BYTE.getFd().getFieldCtx(), BYTE_PRIM_ARRAY)).getData());
    assertArrayEquals("Collection<Byte>=>Bson", BYTE_TO_BSON_MULTI.getData(),
        ((Binary) CTX_ST.toBson(FDS_BYTE.getFd().getFieldCtx(), BYTE_COLLECTION)).getData());

    assertEquals("Binary==>Byte", BYTE_TO_OBJ_BYTE, CTX_ST.toObject(FDS_BYTE, BYTE_TO_BSON_ONE));
    assertEquals("byte==>byte", BYTE_TO_OBJ_BYTE_PRIM,
        CTX_ST.toObject(FDS_BYTE_PRIM, BYTE_TO_BSON_ONE));
    assertArrayEquals("Binary==>Byte[]", BYTE_TO_OBJ_BYTE_ARRAY,
        (Byte[]) CTX_ST.toObject(FDS_BYTE_ARRAY, BYTE_TO_BSON_MULTI));
    assertArrayEquals("Binary==>byte[]", BYTE_TO_OBJ_BYTE_PRIM_ARRAY,
        (byte[]) CTX_ST.toObject(FDS_BYTE_PRIM_ARRAY, BYTE_TO_BSON_MULTI));
    assertEquals("Binary==>Set<Byte>", BYTE_TO_OBJ_BYTE_COLLECTION_SET,
        CTX_ST.toObject(FDS_BYTE_SET, BYTE_TO_BSON_MULTI));
    assertEquals("Binary==>SortedSet<Byte>", BYTE_TO_OBJ_BYTE_COLLECTION_SORTED_SET,
        CTX_ST.toObject(FDS_BYTE_SORTED_SET, BYTE_TO_BSON_MULTI));
  }

  @Test
  public void test_calendar() throws Exception {

    assertEquals("Calendar=>Bson", CALENDAR_TO_BSON_ONE,
        CTX_ST.toBson(FDS_CALENDAR.getFd().getFieldCtx(), CALENDAR));
    assertEquals("Calendar[]=>Bson", CALENDAR_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_CALENDAR.getFd().getFieldCtx(), CALENDAR_ARRAY));
    assertEquals("Collection<Calendar>=>Bson", CALENDAR_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_CALENDAR.getFd().getFieldCtx(), CALENDAR_COLLECTION));

    assertEquals("Date==>Calendar", CALENDAR, CTX_ST.toObject(FDS_CALENDAR, CALENDAR_TO_BSON_ONE));
    assertArrayEquals("Collecton<Date>==>Calendar[]", CALENDAR_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_CALENDAR_ARRAY, CALENDAR_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>List<Calendar>", CALENDAR_COLLECTION,
        CTX_ST.toObject(FDS_CALENDAR_LIST, CALENDAR_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>SortedSet<Calendar>", CALENDAR_SORTED_SET,
        CTX_ST.toObject(FDS_CALENDAR_SORTED_SET, CALENDAR_TO_BSON_MULTI));
  }

  @Test
  public void test_character() throws Exception {

    assertEquals("Character=>Bson", CHARACTER_TO_BSON_ONE,
        CTX_ST.toBson(FDS_CHARACTER.getFd().getFieldCtx(), CHARACTER));
    assertEquals("char=>Bson", CHARACTER_TO_BSON_ONE,
        CTX_ST.toBson(FDS_CHARACTER.getFd().getFieldCtx(), CHARACTER_PRIM));
    assertEquals("Character[]=>Bson", CHARACTER_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_CHARACTER.getFd().getFieldCtx(), CHARACTER_ARRAY));
    assertEquals("char[]=>Bson", CHARACTER_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_CHARACTER.getFd().getFieldCtx(), CHARACTER_PRIM_ARRAY));
    assertEquals("Collection<Character>=>Bson", CHARACTER_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_CHARACTER.getFd().getFieldCtx(), CHARACTER_COLLECTION));

    assertEquals("String==>Character", CHARACTER,
        CTX_ST.toObject(FDS_CHARACTER, CHARACTER_TO_BSON_ONE));
    assertArrayEquals("String==>Character[]", CHARACTER_ARRAY,
        (Character[]) CTX_ST.toObject(FDS_CHARACTER_ARRAY, CHARACTER_TO_BSON_MULTI));
    assertEquals("String==>List<Character>", CHARACTER_COLLECTION,
        CTX_ST.toObject(FDS_CHARACTER_LIST, CHARACTER_TO_BSON_MULTI));
    assertEquals("String==>SortedSet<Character>", CHARACTER_SORTED_SET,
        CTX_ST.toObject(FDS_CHARACTER_SORTED_SET, CHARACTER_TO_BSON_MULTI));
  }

  @Test
  public void test_date() throws Exception {

    assertEquals("Date=>Bson", DATE_TO_BSON_ONE,
        CTX_ST.toBson(FDS_DATE.getFd().getFieldCtx(), DATE));
    assertEquals("Date[]=>Bson", DATE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_DATE.getFd().getFieldCtx(), DATE_ARRAY));
    assertEquals("Collection<Date>=>Bson", DATE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_DATE.getFd().getFieldCtx(), DATE_COLLECTION));

    assertEquals("Date==>Date", DATE, CTX_ST.toObject(FDS_DATE, DATE_TO_BSON_ONE));
    assertArrayEquals("Collecton<Date>==>Date[]", DATE_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_DATE_ARRAY, DATE_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>List<Date>", DATE_COLLECTION,
        CTX_ST.toObject(FDS_DATE_LIST, DATE_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>SortedSet<Date>", DATE_SORTED_SET,
        CTX_ST.toObject(FDS_DATE_SORTED_SET, DATE_TO_BSON_MULTI));
  }

  @Test
  public void test_double() throws Exception {

    assertEquals("Double=>Bson", DOUBLE_TO_BSON_ONE,
        CTX_ST.toBson(FDS_DOUBLE.getFd().getFieldCtx(), DOUBLE));
    assertEquals("double=>Bson", DOUBLE_TO_BSON_ONE,
        CTX_ST.toBson(FDS_DOUBLE.getFd().getFieldCtx(), DOUBLE_PRIM));
    assertEquals("Double[]=>Bson", DOUBLE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_DOUBLE.getFd().getFieldCtx(), DOUBLE_ARRAY));
    assertEquals("double[]=>Bson", DOUBLE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_DOUBLE.getFd().getFieldCtx(), DOUBLE_PRIM_ARRAY));
    assertEquals("Collection<Double>=>Bson", DOUBLE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_DOUBLE.getFd().getFieldCtx(), DOUBLE_COLLECTION));

    assertEquals("Double==>Double", DOUBLE,
        (Double) CTX_ST.toObject(FDS_DOUBLE, DOUBLE_TO_BSON_ONE), 0.001);
    assertEquals("Double==>double", DOUBLE_PRIM,
        (double) CTX_ST.toObject(FDS_DOUBLE_PRIM, DOUBLE_TO_BSON_ONE), 0.001);
  }

  @Test
  public void test_enum() throws Exception {

    assertEquals("Enum=>Bson", ENUM_TO_BSON_ONE,
        CTX_ST.toBson(FDS_ENUM.getFd().getFieldCtx(), ENUM));
    assertEquals("Enum[]=>Bson", ENUM_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_ENUM.getFd().getFieldCtx(), ENUM_ARRAY));
    assertEquals("Collection<Enum>=>Bson", ENUM_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_ENUM.getFd().getFieldCtx(), ENUM_COLLECTION));

    assertEquals("Enum==>Enum", ENUM, CTX_ST.toObject(FDS_ENUM, ENUM_TO_BSON_ONE));
    assertArrayEquals("Collecton<Enum>==>Enum[]", ENUM_ARRAY,
        (Enum[]) CTX_ST.toObject(FDS_ENUM_ARRAY, ENUM_TO_BSON_MULTI));
    assertEquals("Collecton<Enum>==>List<Enum>", ENUM_COLLECTION,
        CTX_ST.toObject(FDS_ENUM_LIST, ENUM_TO_BSON_MULTI));
    assertEquals("Collecton<Enum>==>SortedSet<Enum>", ENUM_SORTED_SET,
        CTX_ST.toObject(FDS_ENUM_SORTED_SET, ENUM_TO_BSON_MULTI));
  }

  @Test
  public void test_file() throws Exception {

    assertEquals("File=>Bson", FILE_TO_BSON_ONE,
        CTX_ST.toBson(FDS_FILE.getFd().getFieldCtx(), FILE));
    assertEquals("File[]=>Bson", FILE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_FILE.getFd().getFieldCtx(), FILE_ARRAY));
    assertEquals("Collection<File>=>Bson", FILE_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_FILE.getFd().getFieldCtx(), FILE_COLLECTION));

    assertEquals("String==>File", FILE, CTX_ST.toObject(FDS_FILE, FILE_TO_BSON_ONE));
    assertArrayEquals("String==>File[]", FILE_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_FILE_ARRAY, FILE_TO_BSON_MULTI));
    assertEquals("String==>List<File>", FILE_COLLECTION,
        CTX_ST.toObject(FDS_FILE_LIST, FILE_TO_BSON_MULTI));
    assertEquals("String==>SortedSet<File>", FILE_SORTED_SET,
        CTX_ST.toObject(FDS_FILE_SORTED_SET, FILE_TO_BSON_MULTI));
  }

  @Test
  public void test_float() throws Exception {

    assertEquals("Float=>Bson", FLOAT_TO_BSON_ONE,
        CTX_ST.toBson(FDS_FLOAT.getFd().getFieldCtx(), FLOAT));
    assertEquals("float=>Bson", FLOAT_TO_BSON_ONE,
        CTX_ST.toBson(FDS_FLOAT.getFd().getFieldCtx(), FLOAT_PRIM));
    assertEquals("Float[]=>Bson", FLOAT_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_FLOAT.getFd().getFieldCtx(), FLOAT_ARRAY));
    assertEquals("float[]=>Bson", FLOAT_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_FLOAT.getFd().getFieldCtx(), FLOAT_PRIM_ARRAY));
    assertEquals("Collection<Float>=>Bson", FLOAT_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_FLOAT.getFd().getFieldCtx(), FLOAT_COLLECTION));

    assertEquals("Float==>Float", FLOAT, (Float) CTX_ST.toObject(FDS_FLOAT, FLOAT_TO_BSON_ONE),
        0.001);
    assertEquals("Float==>double", FLOAT_PRIM,
        (float) CTX_ST.toObject(FDS_FLOAT_PRIM, FLOAT_TO_BSON_ONE), 0.001);
  }

  @Test
  public void test_gregorian_calendar() throws Exception {

    assertEquals("GregorianCalendar=>Bson", GREGORIAN_CALENDAR_TO_BSON_ONE,
        CTX_ST.toBson(FDS_GREGORIAN_CALENDAR.getFd().getFieldCtx(), GREGORIAN_CALENDAR));
    assertEquals("GregorianCalendar[]=>Bson", GREGORIAN_CALENDAR_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_GREGORIAN_CALENDAR.getFd().getFieldCtx(), GREGORIAN_CALENDAR_ARRAY));
    assertEquals("Collection<GregorianCalendar>=>Bson", GREGORIAN_CALENDAR_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_GREGORIAN_CALENDAR.getFd().getFieldCtx(), GREGORIAN_CALENDAR_COLLECTION));

    assertEquals("Date==>GregorianCalendar", GREGORIAN_CALENDAR,
        CTX_ST.toObject(FDS_GREGORIAN_CALENDAR, GREGORIAN_CALENDAR_TO_BSON_ONE));
    assertArrayEquals("Collecton<Date>==>GregorianCalendar[]", GREGORIAN_CALENDAR_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_GREGORIAN_CALENDAR_ARRAY, GREGORIAN_CALENDAR_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>List<GregorianCalendar>", GREGORIAN_CALENDAR_COLLECTION,
        CTX_ST.toObject(FDS_GREGORIAN_CALENDAR_LIST, GREGORIAN_CALENDAR_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>SortedSet<GregorianCalendar>", GREGORIAN_CALENDAR_SORTED_SET,
        CTX_ST.toObject(FDS_GREGORIAN_CALENDAR_SORTED_SET, GREGORIAN_CALENDAR_TO_BSON_MULTI));
  }

  @Test
  public void test_integer() throws Exception {

    assertEquals("Integer=>Bson", INTEGER_TO_BSON_ONE,
        CTX_ST.toBson(FDS_INTEGER.getFd().getFieldCtx(), INTEGER));
    assertEquals("int=>Bson", INTEGER_TO_BSON_ONE,
        CTX_ST.toBson(FDS_INTEGER.getFd().getFieldCtx(), INTEGER_PRIM));
    assertEquals("Integer[]=>Bson", INTEGER_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_INTEGER.getFd().getFieldCtx(), INTEGER_ARRAY));
    assertEquals("int[]=>Bson", INTEGER_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_INTEGER.getFd().getFieldCtx(), INTEGER_PRIM_ARRAY));
    assertEquals("Collection<Integer>=>Bson", INTEGER_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_INTEGER.getFd().getFieldCtx(), INTEGER_COLLECTION));

    assertEquals("Integer==>Integer", INTEGER, CTX_ST.toObject(FDS_INTEGER, INTEGER_TO_BSON_ONE));
    assertEquals("Integer==>int", INTEGER_PRIM,
        CTX_ST.toObject(FDS_INTEGER_PRIM, INTEGER_TO_BSON_ONE));
    assertArrayEquals("Collecton<Integer>==>Integer[]", INTEGER_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_INTEGER_ARRAY, INTEGER_TO_BSON_MULTI));
    assertArrayEquals("Collecton<Integer>==>int[]", INTEGER_PRIM_ARRAY,
        (int[]) CTX_ST.toObject(FDS_INTEGER_PRIM_ARRAY, INTEGER_TO_BSON_MULTI));
    assertEquals("Collecton<Integer>==>List<Integer>", INTEGER_COLLECTION,
        CTX_ST.toObject(FDS_INTEGER_LIST, INTEGER_TO_BSON_MULTI));
    assertEquals("Collecton<Integer>==>SortedSet<Integer>", INTEGER_SORTED_SET,
        CTX_ST.toObject(FDS_INTEGER_SORTED_SET, INTEGER_TO_BSON_MULTI));
  }

  @Test
  public void test_long() throws Exception {

    assertEquals("Long=>Bson", LONG_TO_BSON_ONE,
        CTX_ST.toBson(FDS_LONG.getFd().getFieldCtx(), LONG));
    assertEquals("long=>Bson", LONG_TO_BSON_ONE,
        CTX_ST.toBson(FDS_LONG.getFd().getFieldCtx(), LONG_PRIM));
    assertEquals("Long[]=>Bson", LONG_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_LONG.getFd().getFieldCtx(), LONG_ARRAY));
    assertEquals("long[]=>Bson", LONG_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_LONG.getFd().getFieldCtx(), LONG_PRIM_ARRAY));
    assertEquals("Collection<Long>=>Bson", LONG_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_LONG.getFd().getFieldCtx(), LONG_COLLECTION));

    assertEquals("Long==>Long", LONG, CTX_ST.toObject(FDS_LONG, LONG_TO_BSON_ONE));
    assertEquals("Long==>long", LONG_PRIM, CTX_ST.toObject(FDS_LONG_PRIM, LONG_TO_BSON_ONE));
    assertArrayEquals("Collecton<Long>==>Long[]", LONG_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_LONG_ARRAY, LONG_TO_BSON_MULTI));
    assertArrayEquals("Collecton<Long>==>long[]", LONG_PRIM_ARRAY,
        (long[]) CTX_ST.toObject(FDS_LONG_PRIM_ARRAY, LONG_TO_BSON_MULTI));
    assertEquals("Collecton<Long>==>List<Long>", LONG_COLLECTION,
        CTX_ST.toObject(FDS_LONG_LIST, LONG_TO_BSON_MULTI));
    assertEquals("Collecton<Long>==>SortedSet<Long>", LONG_SORTED_SET,
        CTX_ST.toObject(FDS_LONG_SORTED_SET, LONG_TO_BSON_MULTI));
  }

  @Test
  public void test_object_id() throws Exception {

    assertEquals("ObjectId=>Bson", OBJECT_ID_TO_BSON_ONE,
        CTX_ST.toBson(FDS_OBJECT_ID.getFd().getFieldCtx(), OBJECT_ID));
    assertEquals("ObjectId[]=>Bson", OBJECT_ID_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_OBJECT_ID.getFd().getFieldCtx(), OBJECT_ID_ARRAY));
    assertEquals("Collection<ObjectId>=>Bson", OBJECT_ID_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_OBJECT_ID.getFd().getFieldCtx(), OBJECT_ID_COLLECTION));

    assertEquals("ObjectId==>ObjectId", OBJECT_ID,
        CTX_ST.toObject(FDS_OBJECT_ID, OBJECT_ID_TO_BSON_ONE));
    assertArrayEquals("Collecton<ObjectId>==>ObjectId[]", OBJECT_ID_ARRAY,
        (ObjectId[]) CTX_ST.toObject(FDS_OBJECT_ID_ARRAY, OBJECT_ID_TO_BSON_MULTI));
    assertEquals("Collecton<ObjectId>==>List<ObjectId>", OBJECT_ID_COLLECTION,
        CTX_ST.toObject(FDS_OBJECT_ID_LIST, OBJECT_ID_TO_BSON_MULTI));
    assertEquals("Collecton<ObjectId>==>SortedSet<ObjectId>", OBJECT_ID_SORTED_SET,
        CTX_ST.toObject(FDS_OBJECT_ID_SORTED_SET, OBJECT_ID_TO_BSON_MULTI));
  }

  @Test
  public void test_short() throws Exception {

    assertEquals("Short=>Bson", SHORT_TO_BSON_ONE,
        CTX_ST.toBson(FDS_SHORT.getFd().getFieldCtx(), SHORT));
    assertEquals("short=>Bson", SHORT_TO_BSON_ONE,
        CTX_ST.toBson(FDS_SHORT.getFd().getFieldCtx(), SHORT_PRIM));
    assertEquals("Short[]=>Bson", SHORT_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_SHORT.getFd().getFieldCtx(), SHORT_ARRAY));
    assertEquals("short[]=>Bson", SHORT_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_SHORT.getFd().getFieldCtx(), SHORT_PRIM_ARRAY));
    assertEquals("Collection<Short>=>Bson", SHORT_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_SHORT.getFd().getFieldCtx(), SHORT_COLLECTION));

    assertEquals("Short==>Short", SHORT, CTX_ST.toObject(FDS_SHORT, SHORT_TO_BSON_ONE));
    assertEquals("Short==>short", SHORT_PRIM, CTX_ST.toObject(FDS_SHORT_PRIM, SHORT_TO_BSON_ONE));
    assertArrayEquals("Collecton<Short>==>Short[]", SHORT_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_SHORT_ARRAY, SHORT_TO_BSON_MULTI));
    assertArrayEquals("Collecton<Short>==>short[]", SHORT_PRIM_ARRAY,
        (short[]) CTX_ST.toObject(FDS_SHORT_PRIM_ARRAY, SHORT_TO_BSON_MULTI));
    assertEquals("Collecton<Short>==>List<Short>", SHORT_COLLECTION,
        CTX_ST.toObject(FDS_SHORT_LIST, SHORT_TO_BSON_MULTI));
    assertEquals("Collecton<Short>==>SortedSet<Short>", SHORT_SORTED_SET,
        CTX_ST.toObject(FDS_SHORT_SORTED_SET, SHORT_TO_BSON_MULTI));
  }

  @Test
  public void test_string() throws Exception {

    assertEquals("String=>Bson", STRING_TO_BSON_ONE,
        CTX_ST.toBson(FDS_STRING.getFd().getFieldCtx(), STRING));
    assertEquals("String[]=>Bson", STRING_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_STRING.getFd().getFieldCtx(), STRING_ARRAY));
    assertEquals("Collection<String>=>Bson", STRING_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_STRING.getFd().getFieldCtx(), STRING_COLLECTION));

    assertEquals("String==>String", STRING, CTX_ST.toObject(FDS_STRING, STRING_TO_BSON_ONE));
    assertArrayEquals("Collecton<String>==>String[]", STRING_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_STRING_ARRAY, STRING_TO_BSON_MULTI));
    assertEquals("Collecton<String>==>List<String>", STRING_COLLECTION,
        CTX_ST.toObject(FDS_STRING_LIST, STRING_TO_BSON_MULTI));
    assertEquals("Collecton<String>==>SortedSet<String>", STRING_SORTED_SET,
        CTX_ST.toObject(FDS_STRING_SORTED_SET, STRING_TO_BSON_MULTI));
  }

  @Test
  public void test_uri() throws Exception {

    assertEquals("URI=>Bson", URI_TO_BSON_ONE, CTX_ST.toBson(FDS_URI.getFd().getFieldCtx(), URI));
    assertEquals("URI[]=>Bson", URI_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_URI.getFd().getFieldCtx(), URI_ARRAY));
    assertEquals("Collection<URI>=>Bson", URI_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_URI.getFd().getFieldCtx(), URI_COLLECTION));

    assertEquals("String==>URI", URI, CTX_ST.toObject(FDS_URI, URI_TO_BSON_ONE));
    assertArrayEquals("Collecton<String>==>URI[]", URI_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_URI_ARRAY, URI_TO_BSON_MULTI));
    assertEquals("Collecton<String>==>List<URI>", URI_COLLECTION,
        CTX_ST.toObject(FDS_URI_LIST, URI_TO_BSON_MULTI));
    assertEquals("Collecton<String>==>SortedSet<URI>", URI_SORTED_SET,
        CTX_ST.toObject(FDS_URI_SORTED_SET, URI_TO_BSON_MULTI));
  }

  @Test
  public void test_url() throws Exception {

    assertEquals("URL=>Bson", URL_TO_BSON_ONE, CTX_ST.toBson(FDS_URL.getFd().getFieldCtx(), URL));
    assertEquals("URL[]=>Bson", URL_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_URL.getFd().getFieldCtx(), URL_ARRAY));
    assertEquals("Collection<URL>=>Bson", URL_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_URL.getFd().getFieldCtx(), URL_COLLECTION));

    assertEquals("String==>URL", URL, CTX_ST.toObject(FDS_URL, URL_TO_BSON_ONE));
    assertArrayEquals("Collecton<String>==>URL[]", URL_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_URL_ARRAY, URL_TO_BSON_MULTI));
    assertEquals("Collecton<String>==>List<URL>", URL_COLLECTION,
        CTX_ST.toObject(FDS_URL_LIST, URL_TO_BSON_MULTI));
  }

  @Test
  public void test_xml_gregorian_calendar() throws Exception {

    assertEquals("XMLGregorianCalendar=>Bson", XML_GREGORIAN_CALENDAR_TO_BSON_ONE,
        CTX_ST.toBson(FDS_XML_GREGORIAN_CALENDAR.getFd().getFieldCtx(), XML_GREGORIAN_CALENDAR));
    assertEquals("XMLGregorianCalendar[]=>Bson", XML_GREGORIAN_CALENDAR_TO_BSON_MULTI, CTX_ST
        .toBson(FDS_XML_GREGORIAN_CALENDAR.getFd().getFieldCtx(), XML_GREGORIAN_CALENDAR_ARRAY));
    assertEquals("Collection<XMLGregorianCalendar>=>Bson", XML_GREGORIAN_CALENDAR_TO_BSON_MULTI,
        CTX_ST.toBson(FDS_XML_GREGORIAN_CALENDAR.getFd().getFieldCtx(),
            XML_GREGORIAN_CALENDAR_COLLECTION));

    assertEquals("Date==>XMLGregorianCalendar", XML_GREGORIAN_CALENDAR,
        CTX_ST.toObject(FDS_XML_GREGORIAN_CALENDAR, XML_GREGORIAN_CALENDAR_TO_BSON_ONE));
    assertArrayEquals("Collecton<Date>==>XMLGregorianCalendar[]", XML_GREGORIAN_CALENDAR_ARRAY,
        (Object[]) CTX_ST.toObject(FDS_XML_GREGORIAN_CALENDAR_ARRAY,
            XML_GREGORIAN_CALENDAR_TO_BSON_MULTI));
    assertEquals("Collecton<Date>==>List<XMLGregorianCalendar>", XML_GREGORIAN_CALENDAR_COLLECTION,
        CTX_ST.toObject(FDS_XML_GREGORIAN_CALENDAR_LIST, XML_GREGORIAN_CALENDAR_TO_BSON_MULTI));
  }
}
