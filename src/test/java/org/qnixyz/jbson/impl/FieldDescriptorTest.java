package org.qnixyz.jbson.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import java.lang.reflect.Field;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.jaxb.javax.ok.AllSimpleTypes;
import org.qnixyz.jbson.jaxb.javax.ok.NonXmlComplexType;
import org.qnixyz.jbson.jaxb.javax.ok.XmlComplexType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlAccessorType(XmlAccessType.FIELD)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FieldDescriptorTest {

  private static final JaxBsonContext CTX = JaxBsonContext.newInstance(FieldDescriptorTest.class);

  private static final JaxBsonContextImpl CTX_IMPL = (JaxBsonContextImpl) CTX;

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(FieldDescriptorTest.class);

  @SuppressWarnings("unused")
  private Integer _fieldInteger;

  @SuppressWarnings("unused")
  private int _fieldINTEGER;

  @SuppressWarnings("unused")
  private Integer[] _fieldIntegerArray;

  @SuppressWarnings("unused")
  private int[] _fieldINTEGERArray;

  @SuppressWarnings("unused")
  private List<Integer> _fieldIntegerList;

  @SuppressWarnings("unused")
  private XmlComplexType _fieldXmlComplexType;

  @SuppressWarnings("unused")
  private XmlComplexType[] _fieldXmlComplexTypeArray;

  @SuppressWarnings("unused")
  private List<XmlComplexType> _fieldXmlComplexTypeList;

  @XmlJavaTypeAdapter(NonXmlComplexType.AdapterToSimpleType.class)
  private NonXmlComplexType _fieldXmlJavaTypeAdapterSimple;

  @Test
  public void test_fieldInteger() throws Exception {
    final Field field = FieldDescriptorTest.class.getDeclaredField("_fieldInteger");
    final FieldDescriptor fd = new FieldDescriptor(CTX_IMPL, field);
    assertFalse("Ignore", fd.isIgnore());
    final SimpleTypes simpleTypes = new SimpleTypes();
    final Object bsonValue = simpleTypes.toBson(fd.getFieldCtx(), 1);
    assertEquals("_fieldInteger BSON value", 1, bsonValue);
  }

  @Test
  public void test_fieldINTEGER() throws Exception {
    final Field field = FieldDescriptorTest.class.getDeclaredField("_fieldINTEGER");
    final FieldDescriptor fd = new FieldDescriptor(CTX_IMPL, field);
    assertFalse("Ignore", fd.isIgnore());
    final SimpleTypes simpleTypes = new SimpleTypes();
    final Object bsonValue = simpleTypes.toBson(fd.getFieldCtx(), 1);
    assertEquals("_fieldINTEGER BSON value", 1, bsonValue);
  }

  @Test
  public void test_fieldXmlComplexType() throws Exception {
    new FieldDescriptor(CTX_IMPL, AllSimpleTypes.class.getDeclaredField("propXmlJavaTypeAdapter"));
    new FieldDescriptor(CTX_IMPL,
        FieldDescriptorTest.class.getDeclaredField("_fieldXmlJavaTypeAdapterSimple"));
  }

  @Test
  public void test_fieldXmlJavaTypeAdapterSimple() throws Exception {
    final Field field = AllSimpleTypes.class.getDeclaredField("propXmlJavaTypeAdapter");
    final FieldDescriptor fd = new FieldDescriptor(CTX_IMPL, field);
    assertFalse("Ignore", fd.isIgnore());
  }
}
