package org.qnixyz.jbson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DateBasedTest {

  private static final long __MS_PER_DAY = 1000 * 60 * 60 * 24;

  private static final Date _DATE = new Date(__MS_PER_DAY * 1);

  private static final Calendar CALENDAR = Calendar.getInstance();

  private static final GregorianCalendar GREGORIAN_CALENDAR = new GregorianCalendar();

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(DateBasedTest.class);

  private static XMLGregorianCalendar XML_GREGORIAN_CALENDAR;

  static {
    CALENDAR.setTime(_DATE);
    GREGORIAN_CALENDAR.setTime(_DATE);
    try {
      XML_GREGORIAN_CALENDAR =
          DatatypeFactory.newInstance().newXMLGregorianCalendar(GREGORIAN_CALENDAR);
    } catch (final DatatypeConfigurationException e) {
      throw new IllegalStateException("This is a bug");
    }
  }

  @Test
  public void test_check() throws Exception {
    final DateBased util = new DateBased();
    assertTrue("Calendar check", util.getChecker().check(Calendar.class));
    assertTrue("Date check", util.getChecker().check(Date.class));
    assertTrue("GregorianCalendar check", util.getChecker().check(GregorianCalendar.class));
    assertTrue("XMLGregorianCalendar check", util.getChecker().check(XMLGregorianCalendar.class));
    assertFalse("String check", util.getChecker().check(String.class));
  }

  @Test
  public void test_to_bson() throws Exception {
    final DateBased util = new DateBased();
    assertEquals("Calendar==>Bson", _DATE, util.getToBson().convert(CALENDAR));
    assertEquals("Date==>Bson", _DATE, util.getToBson().convert(_DATE));
    assertEquals("GregorianCalendar==>Bson", _DATE, util.getToBson().convert(GREGORIAN_CALENDAR));
    assertEquals("XMLGregorianCalendar==>Bson", _DATE,
        util.getToBson().convert(XML_GREGORIAN_CALENDAR));
  }

  @Test
  public void test_to_obect() throws Exception {
    final DateBased util = new DateBased();
    assertEquals("Calendar==>Object", CALENDAR, util.getToObject().convert(Calendar.class, _DATE));
    assertEquals("Date==>Object", _DATE, util.getToObject().convert(Date.class, _DATE));
    assertEquals("GregorianCalendar==>Object", GREGORIAN_CALENDAR,
        util.getToObject().convert(GregorianCalendar.class, _DATE));
    assertEquals("XMLGregorianCalendar==>Object", XML_GREGORIAN_CALENDAR,
        util.getToObject().convert(XMLGregorianCalendar.class, _DATE));
  }
}
