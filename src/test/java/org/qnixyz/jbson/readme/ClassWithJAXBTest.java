package org.qnixyz.jbson.readme;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import static org.junit.Assume.assumeTrue;
import java.util.Date;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.bson.Document;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonContext;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClassWithJAXBTest {

  public static final String DO_TEST_PROP = "DO_JAXBSON_README_TEST";

  @BeforeClass
  public static void beforeMethod() {
    assumeTrue(!isBlank(System.getenv(DO_TEST_PROP))
        && System.getenv(DO_TEST_PROP).equalsIgnoreCase("true") || //
        !isBlank(System.getProperty(DO_TEST_PROP))
            && System.getProperty(DO_TEST_PROP).equalsIgnoreCase("true"));
  }

  @Test
  public void test_bson() throws Exception {

    final ClassWithJAXB o = new ClassWithJAXB();
    o.id = "my-id";
    o.foo = "my-foo";
    o.date = new Date(0L);

    final JaxBsonContext jaxBsonContext = JaxBsonContext.newInstance(ClassWithJAXB.class);
    final Document bson = jaxBsonContext.toBson(o);
    System.out.println(bson.toJson());
  }

  @Test
  public void test_json() throws Exception {

    final JAXBContext jaxbContext = JAXBContext.newInstance(ClassWithJAXB.class);
    final Marshaller m = jaxbContext.createMarshaller();
    m.setProperty(JAXBContextProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
    m.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, false);

    final ClassWithJAXB o = new ClassWithJAXB();
    o.id = "my-id";
    o.date = new Date(0L);
    o.foo = "my-foo";
    m.marshal(o, System.out);
    System.out.println();
  }

  @Test
  public void test_xml() throws Exception {

    final JAXBContext jaxbContext = JAXBContext.newInstance(ClassWithJAXB.class);
    final Marshaller m = jaxbContext.createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    final ClassWithJAXB o = new ClassWithJAXB();
    o.id = "my-id";
    o.foo = "my-foo";
    o.date = new Date(0L);
    m.marshal(o, System.out);

    final JaxBsonContext jaxBsonContext = JaxBsonContext.newInstance(ClassWithJAXB.class);
    final Document bson = jaxBsonContext.toBson(o);
    System.out.println(bson.toJson());

    final ClassWithJAXB oBack = (ClassWithJAXB) jaxBsonContext.toObject(bson);
    m.marshal(oBack, System.out);
  }
}
