package org.qnixyz.jbson.readme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JaxbMapExample {

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapElement {

    @XmlAttribute
    public String key;

    @XmlAttribute
    public String value;

    public MapElement() {}

    public MapElement(final String key, final String value) {
      this.key = key;
      this.value = value;
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapList {
    public List<MapElement> element = new ArrayList<>();
  }

  public static class MapXmlAdapter extends XmlAdapter<MapList, Map<String, String>> {

    @Override
    public MapList marshal(final Map<String, String> v) throws Exception {
      if (v == null) {
        return null;
      }
      final MapList ret = new MapList();
      v.forEach((key, value) -> ret.element.add(new MapElement(key, value)));
      return ret;
    }

    @Override
    public Map<String, String> unmarshal(final MapList v) throws Exception {
      if (v == null || v.element == null) {
        return null;
      }
      final Map<String, String> ret = new HashMap<>();
      v.element.forEach(e -> ret.put(e.key, e.value));
      return ret;
    }
  }

  @XmlJavaTypeAdapter(MapXmlAdapter.class)
  public Map<String, String> map = new HashMap<>();
}
