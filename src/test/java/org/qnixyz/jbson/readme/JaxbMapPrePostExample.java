package org.qnixyz.jbson.readme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPost;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.annotations.JaxBsonTransient;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JaxbMapPrePostExample {

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapElement {

    @XmlAttribute
    public String key;

    @XmlAttribute
    public String value;

    public MapElement() {}

    public MapElement(final String key, final String value) {
      this.key = key;
      this.value = value;
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapList {
    public List<MapElement> element = new ArrayList<>();
  }

  public static class MapXmlAdapter extends XmlAdapter<MapList, Map<String, String>> {

    @Override
    public MapList marshal(final Map<String, String> v) throws Exception {
      if (v == null) {
        return null;
      }
      final MapList ret = new MapList();
      v.forEach((key, value) -> ret.element.add(new MapElement(key, value)));
      return ret;
    }

    @Override
    public Map<String, String> unmarshal(final MapList v) throws Exception {
      if (v == null || v.element == null) {
        return null;
      }
      final Map<String, String> ret = new HashMap<>();
      v.element.forEach(e -> ret.put(e.key, e.value));
      return ret;
    }
  }

  @JaxBsonTransient
  @XmlJavaTypeAdapter(MapXmlAdapter.class)
  public Map<String, String> map = new HashMap<>();

  @JaxBsonName(name = "map")
  @JaxBsonIgnoreTransient
  @XmlTransient
  private List<MapElement> mapList = new ArrayList<>();

  @JaxBsonToBsonPost
  private void myJaxBsonToBsonPost() {
    mapList = null;
  }

  @JaxBsonToBsonPre
  private void myJaxBsonToBsonPre() {
    if (map != null) {
      mapList = new ArrayList<>();
      map.forEach((key, value) -> mapList.add(new MapElement(key, value)));
    }
  }

  @JaxBsonToObjectPost
  private void myJaxBsonToObjectPost() {
    if (mapList != null) {
      map = new HashMap<>();
      mapList.forEach(e -> map.put(e.key, e.value));
    }
  }

  @JaxBsonToObjectPre
  private void myJaxBsonToObjectPre() {
    mapList = null;
  }
}
