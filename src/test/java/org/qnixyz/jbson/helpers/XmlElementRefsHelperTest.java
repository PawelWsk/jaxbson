package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlElementRefsHelperTest {

  private jakarta.xml.bind.annotation.XmlElementRefs createJakarta() {
    return new jakarta.xml.bind.annotation.XmlElementRefs() {

      private final jakarta.xml.bind.annotation.XmlElementRef[] value = createValue();

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlElementRefs.class;
      }

      private jakarta.xml.bind.annotation.XmlElementRef[] createValue() {
        final jakarta.xml.bind.annotation.XmlElementRef[] ret =
            new jakarta.xml.bind.annotation.XmlElementRef[1];
        ret[0] = XmlElementRefHelperTest.createJakarta();
        return ret;
      }

      @Override
      public jakarta.xml.bind.annotation.XmlElementRef[] value() {
        return this.value;
      }
    };
  }

  private javax.xml.bind.annotation.XmlElementRefs createJavax() {
    return new javax.xml.bind.annotation.XmlElementRefs() {

      private final javax.xml.bind.annotation.XmlElementRef[] value = createValue();

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlElementRefs.class;
      }

      private javax.xml.bind.annotation.XmlElementRef[] createValue() {
        final javax.xml.bind.annotation.XmlElementRef[] ret =
            new javax.xml.bind.annotation.XmlElementRef[1];
        ret[0] = XmlElementRefHelperTest.createJavax();
        return ret;
      }

      @Override
      public javax.xml.bind.annotation.XmlElementRef[] value() {
        return this.value;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementRefsHelperJakarta));
    final XmlElementRefsHelperJakarta cast = (XmlElementRefsHelperJakarta) inst;
    assertEquals(1, cast.value().length);
    assertNotNull(cast.value()[0]);
    assertEquals("nameJakarta", cast.value()[0].name());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementRefsHelperJavax));
    final XmlElementRefsHelperJavax cast = (XmlElementRefsHelperJavax) inst;
    assertEquals(1, cast.value().length);
    assertNotNull(cast.value()[0]);
    assertEquals("nameJavax", cast.value()[0].name());
  }
}
