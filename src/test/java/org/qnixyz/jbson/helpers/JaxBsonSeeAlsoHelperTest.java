package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonSeeAlsoHelperTest {

  private static final class JakartaClassSeeAlso1 {
  }

  private static final class JakartaClassSeeAlso2 {
  }

  private static final class JavaxClassSeeAlso1 {
  }

  private static final class JavaxClassSeeAlso2 {
  }

  private static final class JaxBsonClassSeeAlso1 {
  }

  private static final class JaxBsonClassSeeAlso2 {
  }

  private static final class NoSeeAlso1 {
  }

  @JaxBsonSeeAlso(value = {})
  @jakarta.xml.bind.annotation.XmlSeeAlso(value = {})
  @javax.xml.bind.annotation.XmlSeeAlso(value = {})
  private static final class NoSeeAlso2 {
  }

  @JaxBsonSeeAlso(value = {JaxBsonClassSeeAlso1.class, JaxBsonClassSeeAlso2.class})
  @jakarta.xml.bind.annotation.XmlSeeAlso(
      value = {JakartaClassSeeAlso1.class, JakartaClassSeeAlso2.class})
  @javax.xml.bind.annotation.XmlSeeAlso(
      value = {JavaxClassSeeAlso1.class, JavaxClassSeeAlso2.class})
  private static final class SeeAlsoFull {
  }

  @Test
  public void testNoSeeAlso1() throws Exception {
    assertNull(JaxBsonSeeAlsoHelper.instance(NoSeeAlso1.class));
  }

  @Test
  public void testNoSeeAlso2() throws Exception {
    assertNull(JaxBsonSeeAlsoHelper.instance(NoSeeAlso2.class));
  }

  @Test
  public void testSeeAlsoFull() throws Exception {
    final JaxBsonSeeAlso a = JaxBsonSeeAlsoHelper.instance(SeeAlsoFull.class);
    assertEquals(6, a.value().length);
    final Set<Class<?>> set = new HashSet<>(Arrays.asList(a.value()));
    assertTrue(set.contains(JaxBsonClassSeeAlso1.class));
    assertTrue(set.contains(JaxBsonClassSeeAlso2.class));
    assertTrue(set.contains(JakartaClassSeeAlso1.class));
    assertTrue(set.contains(JakartaClassSeeAlso2.class));
    assertTrue(set.contains(JavaxClassSeeAlso1.class));
    assertTrue(set.contains(JavaxClassSeeAlso2.class));
  }
}
