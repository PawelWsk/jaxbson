package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlAnyElementHelperTest {

  private jakarta.xml.bind.annotation.XmlAnyElement createJakarta() {
    return new jakarta.xml.bind.annotation.XmlAnyElement() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlAnyElement.class;
      }

      @Override
      public boolean lax() {
        return false;
      }

      @Override
      public Class<? extends jakarta.xml.bind.annotation.DomHandler<?, ?>> value() {
        return null;
      }
    };
  }

  private javax.xml.bind.annotation.XmlAnyElement createJavax() {
    return new javax.xml.bind.annotation.XmlAnyElement() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlAnyElement.class;
      }

      @Override
      public boolean lax() {
        return false;
      }

      @SuppressWarnings("rawtypes")
      @Override
      public Class<? extends javax.xml.bind.annotation.DomHandler> value() {
        return null;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlAnyElementHelperJakarta));
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlAnyElementHelperJavax));
  }
}
