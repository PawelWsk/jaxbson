package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlTransientHelperTest {

  private jakarta.xml.bind.annotation.XmlTransient createJakarta() {
    return new jakarta.xml.bind.annotation.XmlTransient() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlTransient.class;
      }
    };
  }

  private javax.xml.bind.annotation.XmlTransient createJavax() {
    return new javax.xml.bind.annotation.XmlTransient() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlTransient.class;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlTransientHelperJakarta));
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlTransientHelperJavax));
  }
}
