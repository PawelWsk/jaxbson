package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlElementHelperTest {

  static jakarta.xml.bind.annotation.XmlElement createJakarta() {
    return new jakarta.xml.bind.annotation.XmlElement() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlElement.class;
      }

      @Override
      public String defaultValue() {
        return "defaultValueJakarta";
      }

      @Override
      public String name() {
        return "nameJakarta";
      }

      @Override
      public String namespace() {
        return "namespaceJakarta";
      }

      @Override
      public boolean nillable() {
        return false;
      }

      @Override
      public boolean required() {
        return false;
      }

      @Override
      public Class<?> type() {
        return null;
      }
    };
  }

  static javax.xml.bind.annotation.XmlElement createJavax() {
    return new javax.xml.bind.annotation.XmlElement() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlElement.class;
      }

      @Override
      public String defaultValue() {
        return "defaultValueJavax";
      }

      @Override
      public String name() {
        return "nameJavax";
      }

      @Override
      public String namespace() {
        return "namespaceJavax";
      }

      @Override
      public boolean nillable() {
        return false;
      }

      @Override
      public boolean required() {
        return false;
      }

      @Override
      public Class<?> type() {
        return null;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementHelperJakarta));
    final XmlElementHelperJakarta cast = (XmlElementHelperJakarta) inst;
    assertEquals("defaultValueJakarta", cast.defaultValue());
    assertEquals("nameJakarta", cast.name());
    assertEquals("namespaceJakarta", cast.namespace());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementHelperJavax));
    final XmlElementHelperJavax cast = (XmlElementHelperJavax) inst;
    assertEquals("defaultValueJavax", cast.defaultValue());
    assertEquals("nameJavax", cast.name());
    assertEquals("namespaceJavax", cast.namespace());
  }
}
