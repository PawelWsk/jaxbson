package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlElementsHelperTest {

  private jakarta.xml.bind.annotation.XmlElements createJakarta() {
    return new jakarta.xml.bind.annotation.XmlElements() {

      private final jakarta.xml.bind.annotation.XmlElement[] value = createValue();

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlElements.class;
      }

      private jakarta.xml.bind.annotation.XmlElement[] createValue() {
        final jakarta.xml.bind.annotation.XmlElement[] ret =
            new jakarta.xml.bind.annotation.XmlElement[1];
        ret[0] = XmlElementHelperTest.createJakarta();
        return ret;
      }

      @Override
      public jakarta.xml.bind.annotation.XmlElement[] value() {
        return this.value;
      }
    };
  }

  private javax.xml.bind.annotation.XmlElements createJavax() {
    return new javax.xml.bind.annotation.XmlElements() {

      private final javax.xml.bind.annotation.XmlElement[] value = createValue();

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlElements.class;
      }

      private javax.xml.bind.annotation.XmlElement[] createValue() {
        final javax.xml.bind.annotation.XmlElement[] ret =
            new javax.xml.bind.annotation.XmlElement[1];
        ret[0] = XmlElementHelperTest.createJavax();
        return ret;
      }

      @Override
      public javax.xml.bind.annotation.XmlElement[] value() {
        return this.value;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementsHelperJakarta));
    final XmlElementsHelperJakarta cast = (XmlElementsHelperJakarta) inst;
    assertEquals(1, cast.value().length);
    assertNotNull(cast.value()[0]);
    assertEquals("nameJakarta", cast.value()[0].name());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementsHelperJavax));
    final XmlElementsHelperJavax cast = (XmlElementsHelperJavax) inst;
    assertEquals(1, cast.value().length);
    assertNotNull(cast.value()[0]);
    assertEquals("nameJavax", cast.value()[0].name());
  }
}
