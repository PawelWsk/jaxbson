package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonAdapterHelperTest {

  private static class TestAdapterJakarta extends
      jakarta.xml.bind.annotation.adapters.XmlAdapter<TestValueTypeJakarta, TestBoundTypeJakarta> {

    @Override
    public TestValueTypeJakarta marshal(final TestBoundTypeJakarta v) throws Exception {
      return null;
    }

    @Override
    public TestBoundTypeJakarta unmarshal(final TestValueTypeJakarta v) throws Exception {
      return null;
    }
  }

  private static class TestAdapterJavax extends
      javax.xml.bind.annotation.adapters.XmlAdapter<TestValueTypeJavax, TestBoundTypeJavax> {

    @Override
    public TestValueTypeJavax marshal(final TestBoundTypeJavax v) throws Exception {
      return null;
    }

    @Override
    public TestBoundTypeJavax unmarshal(final TestValueTypeJavax v) throws Exception {
      return null;
    }
  }

  private static class TestAdapterJaxBson
      extends JaxBsonAdapter<TestValueTypeJaxBson, TestBoundTypeJaxBson> {

    @Override
    public TestValueTypeJaxBson marshal(final TestBoundTypeJaxBson v) throws Exception {
      return null;
    }

    @Override
    public TestBoundTypeJaxBson unmarshal(final TestValueTypeJaxBson v) throws Exception {
      return null;
    }
  }

  private static class TestBoundTypeJakarta {
  }

  private static class TestBoundTypeJavax {
  }

  private static class TestBoundTypeJaxBson {
  }

  private static class TestValueTypeJakarta {
  }

  private static class TestValueTypeJavax {
  }

  private static class TestValueTypeJaxBson {
  }

  private jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter createJakarta() {
    return new jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter.class;
      }

      @Override
      public Class<?> type() {
        return null;
      }

      @SuppressWarnings("rawtypes")
      @Override
      public Class<? extends jakarta.xml.bind.annotation.adapters.XmlAdapter> value() {
        return TestAdapterJakarta.class;
      }
    };
  }

  private javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter createJavax() {
    return new javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.class;
      }

      @Override
      public Class<?> type() {
        return null;
      }

      @SuppressWarnings("rawtypes")
      @Override
      public Class<? extends javax.xml.bind.annotation.adapters.XmlAdapter> value() {
        return TestAdapterJavax.class;
      }
    };
  }

  @Test
  public void test_jakarta() {
    final JaxBsonAdapter<?, ?> o = JaxBsonAdapterHelper.instance(TestAdapterJakarta.class);
    assertNotNull(o);
    assertEquals(TestValueTypeJakarta.class,
        o.valueType(TestBoundTypeJakarta.class, "JaxBsonAdapterHelperTest TestAdapterJakarta"));
    assertEquals(TestBoundTypeJakarta.class,
        o.boundType(TestValueTypeJakarta.class, "JaxBsonAdapterHelperTest TestAdapterJakarta"));
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof JaxBsonJavaTypeAdapter));
    final JaxBsonJavaTypeAdapter cast = (JaxBsonJavaTypeAdapter) inst;
    final Class<?> xmlAdapterClass = cast.value();
    assertTrue(TestAdapterJakarta.class.isAssignableFrom(xmlAdapterClass));
    final JaxBsonAdapter<Object, Object> jaxBsonXmlAdapter =
        JaxBsonAdapterHelper.instance(xmlAdapterClass);
    assertNotNull(jaxBsonXmlAdapter);
    assertEquals(TestValueTypeJakarta.class, jaxBsonXmlAdapter.valueType(TestBoundTypeJakarta.class,
        "JaxBsonAdapterHelperTest TestAdapterJakarta"));
  }

  @Test
  public void test_javax() {
    final JaxBsonAdapter<?, ?> o = JaxBsonAdapterHelper.instance(TestAdapterJavax.class);
    assertNotNull(o);
    assertEquals(TestValueTypeJavax.class,
        o.valueType(TestBoundTypeJavax.class, "JaxBsonAdapterHelperTest TestAdapterJavax"));
    assertEquals(TestBoundTypeJavax.class,
        o.boundType(TestValueTypeJavax.class, "JaxBsonAdapterHelperTest TestAdapterJavax"));
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof JaxBsonJavaTypeAdapter));
    final JaxBsonJavaTypeAdapter cast = (JaxBsonJavaTypeAdapter) inst;
    final Class<?> xmlAdapterClass = cast.value();
    assertTrue(TestAdapterJavax.class.isAssignableFrom(xmlAdapterClass));
    final JaxBsonAdapter<Object, Object> jaxBsonXmlAdapter =
        JaxBsonAdapterHelper.instance(xmlAdapterClass);
    assertNotNull(jaxBsonXmlAdapter);
    assertEquals(TestValueTypeJavax.class, jaxBsonXmlAdapter.valueType(TestBoundTypeJavax.class,
        "JaxBsonAdapterHelperTest TestAdapterJavax"));
  }

  @Test
  public void test_jaxbson() {
    final JaxBsonAdapter<?, ?> o = JaxBsonAdapterHelper.instance(TestAdapterJaxBson.class);
    assertNotNull(o);
    assertEquals(TestValueTypeJaxBson.class,
        o.valueType(TestBoundTypeJaxBson.class, "JaxBsonAdapterHelperTest TestAdapterJaxBson"));
    assertEquals(TestBoundTypeJaxBson.class,
        o.boundType(TestValueTypeJaxBson.class, "JaxBsonAdapterHelperTest TestAdapterJaxBson"));
  }
}
