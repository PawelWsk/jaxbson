package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlEnumValueHelperTest {

  private jakarta.xml.bind.annotation.XmlEnumValue createJakarta() {
    return new jakarta.xml.bind.annotation.XmlEnumValue() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlEnumValue.class;
      }

      @Override
      public String value() {
        return "valueJakarta";
      }
    };
  }

  private javax.xml.bind.annotation.XmlEnumValue createJavax() {
    return new javax.xml.bind.annotation.XmlEnumValue() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlEnumValue.class;
      }

      @Override
      public String value() {
        return "valueJavax";
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlEnumValueHelperJakarta));
    final XmlEnumValueHelperJakarta cast = (XmlEnumValueHelperJakarta) inst;
    assertEquals("valueJakarta", cast.value());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlEnumValueHelperJavax));
    final XmlEnumValueHelperJavax cast = (XmlEnumValueHelperJavax) inst;
    assertEquals("valueJavax", cast.value());
  }
}
