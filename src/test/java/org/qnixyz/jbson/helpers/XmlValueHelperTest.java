package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlValueHelperTest {

  private jakarta.xml.bind.annotation.XmlValue createJakarta() {
    return new jakarta.xml.bind.annotation.XmlValue() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlValue.class;
      }
    };
  }

  private javax.xml.bind.annotation.XmlValue createJavax() {
    return new javax.xml.bind.annotation.XmlValue() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlValue.class;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlValueHelperJakarta));
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlValueHelperJavax));
  }
}
