package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlTypeHelperTest {

  private jakarta.xml.bind.annotation.XmlType createJakarta() {
    return new jakarta.xml.bind.annotation.XmlType() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlType.class;
      }

      @Override
      public Class<?> factoryClass() {
        return null;
      }

      @Override
      public String factoryMethod() {
        return null;
      }

      @Override
      public String name() {
        return "nameJakarta";
      }

      @Override
      public String namespace() {
        return null;
      }

      @Override
      public String[] propOrder() {
        return null;
      }
    };
  }

  private javax.xml.bind.annotation.XmlType createJavax() {
    return new javax.xml.bind.annotation.XmlType() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlType.class;
      }

      @Override
      public Class<?> factoryClass() {
        return null;
      }

      @Override
      public String factoryMethod() {
        return null;
      }

      @Override
      public String name() {
        return "nameJavax";
      }

      @Override
      public String namespace() {
        return null;
      }

      @Override
      public String[] propOrder() {
        return null;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlTypeHelperJakarta));
    final XmlTypeHelperJakarta cast = (XmlTypeHelperJakarta) inst;
    assertEquals("nameJakarta", cast.name());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlTypeHelperJavax));
    final XmlTypeHelperJavax cast = (XmlTypeHelperJavax) inst;
    assertEquals("nameJavax", cast.name());
  }
}
