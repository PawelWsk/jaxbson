package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlRootElementHelperTest {

  private jakarta.xml.bind.annotation.XmlRootElement createJakarta() {
    return new jakarta.xml.bind.annotation.XmlRootElement() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlRootElement.class;
      }

      @Override
      public String name() {
        return "nameJakarta";
      }

      @Override
      public String namespace() {
        return "namespaceJakarta";
      }
    };
  }

  private javax.xml.bind.annotation.XmlRootElement createJavax() {
    return new javax.xml.bind.annotation.XmlRootElement() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlRootElement.class;
      }

      @Override
      public String name() {
        return "nameJavax";
      }

      @Override
      public String namespace() {
        return "namespaceJavax";
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlRootElementHelperJakarta));
    final XmlRootElementHelperJakarta cast = (XmlRootElementHelperJakarta) inst;
    assertEquals("nameJakarta", cast.name());
    assertEquals("namespaceJakarta", cast.namespace());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlRootElementHelperJavax));
    final XmlRootElementHelperJavax cast = (XmlRootElementHelperJavax) inst;
    assertEquals("nameJavax", cast.name());
    assertEquals("namespaceJavax", cast.namespace());
  }
}
