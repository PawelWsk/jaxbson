package org.qnixyz.jbson;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.bson.types.Decimal128;

public class StringBased {

  @FunctionalInterface
  public interface Checker {
    boolean check(Class<?> type);
  }

  private static class FuncMap {

    private final Map<Class<?>, ToBson> toBsonMap = new HashMap<>();

    private final Map<Class<?>, ToObject> toObjectMap = new HashMap<>();

    private FuncMap() {
      putToObjectMap();
      putToBsonMap();
    }

    private ToBson getToBson(final Class<?> type) {
      return toBsonMap.get(type);
    }

    private ToObject getToObject(final Class<?> type) {
      return toObjectMap.get(type);
    }

    private void putToBsonMap() {
      toBsonMap.put(String.class, (value) -> (String) value);
      toBsonMap.put(File.class, (value) -> value.toString());
      toBsonMap.put(URI.class, (value) -> value.toString());
      toBsonMap.put(URL.class, (value) -> value.toString());
    }

    private void putToObjectMap() {
      toObjectMap.put(BigDecimal.class, (__, value) -> new BigDecimal(value));
      toObjectMap.put(BigInteger.class, (__, value) -> new BigInteger(value));
      toObjectMap.put(Decimal128.class, (__, value) -> Decimal128.parse(value));
      toObjectMap.put(Double.class, (__, value) -> Double.valueOf(value));
      toObjectMap.put(Double.TYPE, (__, value) -> Double.valueOf(value));
      toObjectMap.put(File.class, (__, value) -> new File(value));
      toObjectMap.put(Float.class, (__, value) -> Float.valueOf(value));
      toObjectMap.put(Float.TYPE, (__, value) -> Float.valueOf(value));
      toObjectMap.put(Integer.class, (__, value) -> Integer.valueOf(value));
      toObjectMap.put(Integer.TYPE, (__, value) -> Integer.valueOf(value));
      toObjectMap.put(Long.class, (__, value) -> Long.valueOf(value));
      toObjectMap.put(Long.TYPE, (__, value) -> Long.valueOf(value));
      toObjectMap.put(Short.class, (__, value) -> Short.valueOf(value));
      toObjectMap.put(Short.TYPE, (__, value) -> Short.valueOf(value));
      toObjectMap.put(String.class, (__, value) -> value);
      toObjectMap.put(URI.class, (__, value) -> {
        try {
          return new URI(value);
        } catch (final URISyntaxException e) {
          throw new IllegalStateException("Failed to convert value to Java URI object: " + value,
              e);
        }
      });
      toObjectMap.put(URL.class, (__, value) -> {
        try {
          return new URL(value);
        } catch (final MalformedURLException e) {
          throw new IllegalStateException("Failed to convert value to Java URL object: " + value,
              e);
        }
      });
    }
  }

  @FunctionalInterface
  public interface ToBson {
    String convert(Object value);
  }

  @FunctionalInterface
  public interface ToObject {
    Object convert(Class<?> type, String value);
  }

  private static final FuncMap FUNC_MAP = new FuncMap();

  private static Class<?>[] TYPES = {//
      File.class, //
      String.class, //
      URI.class, //
      URL.class, //
  };

  public static final Set<Class<?>> TYPES_SET =
      Collections.unmodifiableSet(new HashSet<>(Arrays.asList(TYPES)));

  private final Set<Class<?>> _checkerFalse = new HashSet<>();

  private final Set<Class<?>> _checkerTrue = new HashSet<>();

  private Checker checker = (type) -> {
    if (_checkerTrue.contains(type)) {
      return true;
    }
    if (_checkerFalse.contains(type)) {
      return false;
    }
    for (final Class<?> e : TYPES_SET) {
      if (e.isAssignableFrom(type)) {
        _checkerTrue.add(type);
        return true;
      }
    }
    _checkerFalse.add(type);
    return false;
  };

  private ToBson toBson = (value) -> {
    if (value == null) {
      return null;
    }
    final Class<?> type = value.getClass();
    final ToBson func = FUNC_MAP.getToBson(type);
    if (func != null) {
      return func.convert(value);
    }
    throw new IllegalStateException("Failed to convert value to Bson: " + value);
  };

  private ToObject toObject = (type, value) -> {
    if (value == null) {
      return null;
    }
    final ToObject func = FUNC_MAP.getToObject(type);
    if (func != null) {
      return func.convert(null, value);
    }
    throw new IllegalStateException("Failed to convert value to Java object: " + value);
  };

  public Checker getChecker() {
    return checker;
  }

  public ToBson getToBson() {
    return toBson;
  }

  public ToObject getToObject() {
    return toObject;
  }

  public void setChecker(final Checker checker) {
    this.checker = Objects.requireNonNull(checker);
  }

  public void setToBson(final ToBson toBson) {
    this.toBson = Objects.requireNonNull(toBson);
  }

  public void setToObject(final ToObject toObject) {
    this.toObject = Objects.requireNonNull(toObject);
  }
}
