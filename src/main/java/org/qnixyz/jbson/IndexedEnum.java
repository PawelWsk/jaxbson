package org.qnixyz.jbson;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.qnixyz.jbson.helpers.XmlEnumValueHelper;

public class IndexedEnum {

  private final Map<Object, String> enumToString = new HashMap<>();

  private final Map<String, Object> stringToEnum = new HashMap<>();

  private final Class<?> type;

  public IndexedEnum(final Class<?> type) {
    this.type = Objects.requireNonNull(type);
    if (!Enum.class.isAssignableFrom(type)) {
      throw new IllegalArgumentException("Supplied class isn't enum: " + type);
    }
    index();
  }

  public String enumToString(final Object v) {
    if (v == null) {
      return null;
    }
    if (!enumToString.containsKey(v)) {
      throw new IllegalArgumentException(
          "Supplied parameter 'v' (" + v + ") not in value map for class " + type
              + ". Contained enumerations: " + enumToString.keySet());
    }
    return enumToString.get(v);
  }

  public Class<?> getType() {
    return type;
  }

  public Object stringToEnum(final Object v) {
    if (v == null) {
      return null;
    }
    if (!stringToEnum.containsKey(v)) {
      throw new IllegalArgumentException("Supplied parameter 'v' not in this map: " + v
          + ". Contained enumerations: " + stringToEnum.keySet());
    }
    return stringToEnum.get(v);
  }

  private String getEnumString(final Object enumConstant) {
    try {
      final XmlEnumValueHelper xmlEnumValue = XmlEnumValueHelper
          .instance(enumConstant.getClass().getField(((Enum<?>) enumConstant).name()));
      if (xmlEnumValue == null) {
        return enumConstant.toString();
      }
      return xmlEnumValue.value();
    } catch (NoSuchFieldException | SecurityException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private void index() {
    final Object[] enumConstants = type.getEnumConstants();
    if (enumConstants != null) {
      for (final Object enumConstant : enumConstants) {
        final String enumString = getEnumString(enumConstant);
        if (stringToEnum.containsKey(enumString)) {
          throw new IllegalStateException(
              "Multiple string values '" + enumString + "' in enum " + type);
        }
        stringToEnum.put(enumString, enumConstant);
        enumToString.put(enumConstant, enumString);
      }
    }
  }
}
