package org.qnixyz.jbson;

import static org.qnixyz.jbson.impl.Utils.toObjectArray;
import static org.qnixyz.jbson.impl.Utils.toPrimitiveArray;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import org.bson.types.Binary;
import org.bson.types.ObjectId;

public class JaxBsonToObject {

  @FunctionalInterface
  public interface BinaryCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Binary> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToLongPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Binary src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BinaryToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Binary src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BinaryToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Binary src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BinaryToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Binary src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BinaryToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Binary src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BinaryToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Binary src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BinaryToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Boolean> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Boolean src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BooleanToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Boolean src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BooleanToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Boolean src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BooleanToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Boolean src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BooleanToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Boolean src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BooleanToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Boolean src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BooleanToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Date> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface DateToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface DateToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface DateToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Date src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface DateToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Date src);
  }

  @FunctionalInterface
  public interface DateToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Date src);
  }

  @FunctionalInterface
  public interface DateToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Date src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface DateToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface DateToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Number> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Number src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface NumberToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Number src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface NumberToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Number src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface NumberToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Number src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface NumberToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Number src);
  }

  @FunctionalInterface
  public interface NumberToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Number src);
  }

  @FunctionalInterface
  public interface NumberToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Number src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface NumberToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Number src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface NumberToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type,
        Collection<ObjectId> src, Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<ObjectId> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type,
        Collection<ObjectId> src, Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type,
        Collection<ObjectId> src, Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, ObjectId src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, ObjectId src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, ObjectId src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, ObjectId src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, ObjectId src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, ObjectId src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<String> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, String src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface StringToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, String src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface StringToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, String src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface StringToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, String src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface StringToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, String src);
  }

  @FunctionalInterface
  public interface StringToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, String src);
  }

  @FunctionalInterface
  public interface StringToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, String src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface StringToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, String src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface StringToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src,
        Collection<Object> dst);
  }

  public static void setArrayComponent(final Class<?> type, final Object array, final int i,
      final Object value) {
    if (Boolean.TYPE.isAssignableFrom(type)) {
      Array.setBoolean(array, i, (boolean) value);
    } else if (Byte.TYPE.isAssignableFrom(type)) {
      Array.setByte(array, i, (byte) value);
    } else if (Character.TYPE.isAssignableFrom(type)) {
      Array.setChar(array, i, (char) value);
    } else if (Double.TYPE.isAssignableFrom(type)) {
      Array.setDouble(array, i, (double) value);
    } else if (Float.TYPE.isAssignableFrom(type)) {
      Array.setFloat(array, i, (float) value);
    } else if (Integer.TYPE.isAssignableFrom(type)) {
      Array.setInt(array, i, (int) value);
    } else if (Long.TYPE.isAssignableFrom(type)) {
      Array.setLong(array, i, (long) value);
    } else if (Short.TYPE.isAssignableFrom(type)) {
      Array.setShort(array, i, (short) value);
    } else {
      Array.set(array, i, value);
    }
  }

  private BinaryCollectionToBinary binaryCollectionToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBinary");
  };

  private BinaryCollectionToBinaryArray binaryCollectionToBinaryArray = (fieldCtx, src) -> {
    final Binary[] ret = new Binary[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToBinary().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToBinaryCollection binaryCollectionToBinaryCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBinaryToBinary().convert(fieldCtx, e)));
        return dst;
      };

  private BinaryCollectionToBoolean binaryCollectionToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBoolean");
  };

  private BinaryCollectionToBooleanArray binaryCollectionToBooleanArray = (fieldCtx, src) -> {
    final Boolean[] ret = new Boolean[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToBoolean().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToBooleanCollection binaryCollectionToBooleanCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBinaryToBoolean().convert(fieldCtx, e)));
        return dst;
      };

  private BinaryCollectionToBooleanPrim binaryCollectionToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBooleanPrim");
  };

  private BinaryCollectionToBooleanPrimArray binaryCollectionToBooleanPrimArray =
      (fieldCtx, src) -> {
        final boolean[] ret = new boolean[src.size()];
        int i = 0;
        for (final Binary e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToBooleanPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BinaryCollectionToByte binaryCollectionToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToByte");
  };

  private BinaryCollectionToByteArray binaryCollectionToByteArray = (fieldCtx, src) -> {
    final Byte[] ret = new Byte[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToByte().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToByteCollection binaryCollectionToByteCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst
            .add(fieldCtx.getConfiguration().getToObject().getBinaryToByte().convert(fieldCtx, e)));
        return dst;
      };

  private BinaryCollectionToBytePrim binaryCollectionToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBytePrim");
  };

  private BinaryCollectionToBytePrimArray binaryCollectionToBytePrimArray = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToBytePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToCharacter binaryCollectionToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToCharacter");
  };

  private BinaryCollectionToCharacterArray binaryCollectionToCharacterArray = (fieldCtx, src) -> {
    final Character[] ret = new Character[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBinaryToCharacter().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToCharacterCollection binaryCollectionToCharacterCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBinaryToCharacter().convert(fieldCtx, e)));
        return dst;
      };

  private BinaryCollectionToCharacterPrim binaryCollectionToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BinaryCollectionToCharacterPrim");
  };

  private BinaryCollectionToCharacterPrimArray binaryCollectionToCharacterPrimArray =
      (fieldCtx, src) -> {
        final char[] ret = new char[src.size()];
        int i = 0;
        for (final Binary e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToCharacterPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BinaryCollectionToDateBased binaryCollectionToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToDateBased");
  };

  private BinaryCollectionToDateBasedArray binaryCollectionToDateBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Binary e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToDateBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private BinaryCollectionToDateBasedCollection binaryCollectionToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToDateBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private BinaryCollectionToDoublePrim binaryCollectionToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToDoublePrim");
  };

  private BinaryCollectionToDoublePrimArray binaryCollectionToDoublePrimArray = (fieldCtx, src) -> {
    final double[] ret = new double[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBinaryToDoublePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToEnum binaryCollectionToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToEnum");
  };

  private BinaryCollectionToEnumArray binaryCollectionToEnumArray = (fieldCtx, enumType, src) -> {
    final Enum<?>[] ret = new Enum[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToEnum().convert(fieldCtx,
          enumType, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToEnumCollection binaryCollectionToEnumCollection =
      (fieldCtx, enumType, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToEnum()
            .convert(fieldCtx, enumType, e)));
        return dst;
      };

  private BinaryCollectionToFloatPrim binaryCollectionToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToFloatPrim");
  };

  private BinaryCollectionToFloatPrimArray binaryCollectionToFloatPrimArray = (fieldCtx, src) -> {
    final float[] ret = new float[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBinaryToFloatPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToIntegerPrim binaryCollectionToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToIntegerPrim");
  };

  private BinaryCollectionToIntegerPrimArray binaryCollectionToIntegerPrimArray =
      (fieldCtx, src) -> {
        final int[] ret = new int[src.size()];
        int i = 0;
        for (final Binary e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToIntegerPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BinaryCollectionToLongPrim binaryCollectionToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToLongPrim");
  };

  private BinaryCollectionToLongPrimArray binaryCollectionToLongPrimArray = (fieldCtx, src) -> {
    final long[] ret = new long[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToLongPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToNumberBased binaryCollectionToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToNumberBased");
  };

  private BinaryCollectionToNumberBasedArray binaryCollectionToNumberBasedArray =
      (fieldCtx, type, src) -> {
        final Object ret = Array.newInstance(type, src.size());
        int i = 0;
        for (final Binary e : src) {
          final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased()
              .convert(fieldCtx, type, e);
          setArrayComponent(type, ret, i, value);
          i++;
        }
        return (Object[]) ret;
      };

  private BinaryCollectionToNumberBasedCollection binaryCollectionToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private BinaryCollectionToObjectId binaryCollectionToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToObjectId");
  };

  private BinaryCollectionToObjectIdArray binaryCollectionToObjectIdArray = (fieldCtx, src) -> {
    final ObjectId[] ret = new ObjectId[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToObjectId().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToObjectIdCollection binaryCollectionToObjectIdCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBinaryToObjectId().convert(fieldCtx, e)));
        return dst;
      };

  private BinaryCollectionToShortPrim binaryCollectionToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToShortPrim");
  };

  private BinaryCollectionToShortPrimArray binaryCollectionToShortPrimArray = (fieldCtx, src) -> {
    final short[] ret = new short[src.size()];
    int i = 0;
    for (final Binary e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBinaryToShortPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BinaryCollectionToStringBased binaryCollectionToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToStringBased");
  };

  private BinaryCollectionToStringBasedArray binaryCollectionToStringBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Binary e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToStringBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private BinaryCollectionToStringBasedCollection binaryCollectionToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToStringBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private BinaryToBinary binaryToBinary = (fieldCtx, src) -> src;

  private BinaryToBinaryArray binaryToBinaryArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBinaryArray");
  };

  private BinaryToBinaryCollection binaryToBinaryCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToBinary().convert(fieldCtx, src));
    return dst;
  };

  private BinaryToBoolean binaryToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBoolean");
  };

  private BinaryToBooleanArray binaryToBooleanArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBooleanArray");
  };

  private BinaryToBooleanCollection binaryToBooleanCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToBoolean().convert(fieldCtx, src));
    return dst;
  };

  private BinaryToBooleanPrim binaryToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBooleanPrim");
  };

  private BinaryToBooleanPrimArray binaryToBooleanPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBooleanPrimArray");
  };

  private BinaryToByte binaryToByte = (fieldCtx, src) -> {
    if ((src == null) || (src.getData() == null) || (src.getData().length < 1)) {
      return null;
    }
    if (src.getData().length > 1) {
      throw new IllegalStateException("Size of data too long: " + src);
    }
    return src.getData()[0];
  };

  private BinaryToByteArray binaryToByteArray = (fieldCtx, src) -> {
    if (src == null) {
      return null;
    }
    return toObjectArray(src.getData());
  };

  private BinaryToByteCollection binaryToByteCollection = (fieldCtx, src, dst) -> {
    final Byte[] ret =
        fieldCtx.getConfiguration().getToObject().getBinaryToByteArray().convert(fieldCtx, src);
    dst.addAll(Arrays.asList(ret));
    return dst;
  };

  private BinaryToBytePrim binaryToBytePrim = (fieldCtx, src) -> {
    final Byte ret =
        fieldCtx.getConfiguration().getToObject().getBinaryToByte().convert(fieldCtx, src);
    if (ret == null) {
      return fieldCtx.getConfiguration().getValueBytePrimNull();
    }
    return ret;
  };

  private BinaryToBytePrimArray binaryToBytePrimArray = (fieldCtx, src) -> {
    final Byte[] ret =
        fieldCtx.getConfiguration().getToObject().getBinaryToByteArray().convert(fieldCtx, src);
    return toPrimitiveArray(ret);
  };

  private BinaryToCharacter binaryToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacter");
  };

  private BinaryToCharacterArray binaryToCharacterArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacterArray");
  };

  private BinaryToCharacterCollection binaryToCharacterCollection = (fieldCtx, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getBinaryToCharacter().convert(fieldCtx, src));
    return dst;
  };

  private BinaryToCharacterPrim binaryToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacterPrim");
  };

  private BinaryToCharacterPrimArray binaryToCharacterPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacterPrimArray");
  };

  private BinaryToDateBased binaryToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToDateBased");
  };

  private BinaryToDateBasedArray binaryToDateBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToDateBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private BinaryToDateBasedCollection binaryToDateBasedCollection = (fieldCtx, type, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToDateBased().convert(fieldCtx, type,
        src));
    return dst;
  };

  private BinaryToDoublePrim binaryToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToDoublePrim");
  };

  private BinaryToDoublePrimArray binaryToDoublePrimArray = (fieldCtx, src) -> new double[] {
      fieldCtx.getConfiguration().getToObject().getBinaryToDoublePrim().convert(fieldCtx, src)};

  private BinaryToEnum binaryToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToEnum");
  };

  private BinaryToEnumArray binaryToEnumArray = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToEnumArray");
  };

  private BinaryToEnumCollection binaryToEnumCollection = (fieldCtx, enumType, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToEnum().convert(fieldCtx, enumType,
        src));
    return dst;
  };

  private BinaryToFloatPrim binaryToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToFloatPrim");
  };

  private BinaryToFloatPrimArray binaryToFloatPrimArray = (fieldCtx, src) -> new float[] {
      fieldCtx.getConfiguration().getToObject().getBinaryToFloatPrim().convert(fieldCtx, src)};

  private BinaryToIntegerPrim binaryToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToIntegerPrim");
  };

  private BinaryToIntegerPrimArray binaryToIntegerPrimArray = (fieldCtx, src) -> new int[] {
      fieldCtx.getConfiguration().getToObject().getBinaryToIntegerPrim().convert(fieldCtx, src)};

  private BinaryToLongPrim binaryToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToLongPrim");
  };

  private BinaryToLongPrimArray binaryToLongPrimArray = (fieldCtx, src) -> new long[] {
      fieldCtx.getConfiguration().getToObject().getBinaryToLongPrim().convert(fieldCtx, src)};

  private BinaryToNumberBased binaryToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToNumberBased");
  };

  private BinaryToNumberBasedArray binaryToNumberBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private BinaryToNumberBasedCollection binaryToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  private BinaryToObjectId binaryToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToObjectId");
  };

  private BinaryToObjectIdArray binaryToObjectIdArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToObjectIdArray");
  };

  private BinaryToObjectIdCollection binaryToObjectIdCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToObjectId().convert(fieldCtx, src));
    return dst;
  };

  private BinaryToShortPrim binaryToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToShortPrim");
  };

  private BinaryToShortPrimArray binaryToShortPrimArray = (fieldCtx, src) -> new short[] {
      fieldCtx.getConfiguration().getToObject().getBinaryToShortPrim().convert(fieldCtx, src)};

  private BinaryToStringBased binaryToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BinaryToStringBased");
  };

  private BinaryToStringBasedArray binaryToStringBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToStringBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private BinaryToStringBasedCollection binaryToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToStringBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  private BooleanCollectionToBinary booleanCollectionToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToBinary");
  };

  private BooleanCollectionToBinaryArray booleanCollectionToBinaryArray = (fieldCtx, src) -> {
    final Binary[] ret = new Binary[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToBinaryCollection booleanCollectionToBinaryCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, e)));
        return dst;
      };

  private BooleanCollectionToBoolean booleanCollectionToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToBoolean");
  };

  private BooleanCollectionToBooleanArray booleanCollectionToBooleanArray = (fieldCtx, src) -> {
    final Boolean[] ret = new Boolean[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToBooleanCollection booleanCollectionToBooleanCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, e)));
        return dst;
      };

  private BooleanCollectionToBooleanPrim booleanCollectionToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BooleanCollectionToBooleanPrim");
  };

  private BooleanCollectionToBooleanPrimArray booleanCollectionToBooleanPrimArray =
      (fieldCtx, src) -> {
        final boolean[] ret = new boolean[src.size()];
        int i = 0;
        for (final Boolean e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToBooleanPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BooleanCollectionToByte booleanCollectionToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToByte");
  };

  private BooleanCollectionToByteArray booleanCollectionToByteArray = (fieldCtx, src) -> {
    final Byte[] ret = new Byte[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToByteCollection booleanCollectionToByteCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, e)));
        return dst;
      };

  private BooleanCollectionToBytePrim booleanCollectionToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToBytePrim");
  };

  private BooleanCollectionToBytePrimArray booleanCollectionToBytePrimArray = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBooleanToBytePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToCharacter booleanCollectionToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToCharacter");
  };

  private BooleanCollectionToCharacterArray booleanCollectionToCharacterArray = (fieldCtx, src) -> {
    final Character[] ret = new Character[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToCharacterCollection booleanCollectionToCharacterCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToCharacter()
            .convert(fieldCtx, e)));
        return dst;
      };

  private BooleanCollectionToCharacterPrim booleanCollectionToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BooleanCollectionToCharacterPrim");
  };

  private BooleanCollectionToCharacterPrimArray booleanCollectionToCharacterPrimArray =
      (fieldCtx, src) -> {
        final char[] ret = new char[src.size()];
        int i = 0;
        for (final Boolean e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToCharacterPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BooleanCollectionToDateBased booleanCollectionToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToDateBased");
  };

  private BooleanCollectionToDateBasedArray booleanCollectionToDateBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Boolean e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private BooleanCollectionToDateBasedCollection booleanCollectionToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private BooleanCollectionToDoublePrim booleanCollectionToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToDoublePrim");
  };

  private BooleanCollectionToDoublePrimArray booleanCollectionToDoublePrimArray =
      (fieldCtx, src) -> {
        final double[] ret = new double[src.size()];
        int i = 0;
        for (final Boolean e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToDoublePrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BooleanCollectionToEnum booleanCollectionToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToEnum");
  };

  private BooleanCollectionToEnumArray booleanCollectionToEnumArray = (fieldCtx, enumType, src) -> {
    final Enum<?>[] ret = new Enum[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToEnum().convert(fieldCtx,
          enumType, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToEnumCollection booleanCollectionToEnumCollection =
      (fieldCtx, enumType, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToEnum()
            .convert(fieldCtx, enumType, e)));
        return dst;
      };

  private BooleanCollectionToFloatPrim booleanCollectionToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToFloatPrim");
  };

  private BooleanCollectionToFloatPrimArray booleanCollectionToFloatPrimArray = (fieldCtx, src) -> {
    final float[] ret = new float[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBooleanToFloatPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToIntegerPrim booleanCollectionToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BooleanCollectionToIntegerPrim");
  };

  private BooleanCollectionToIntegerPrimArray booleanCollectionToIntegerPrimArray =
      (fieldCtx, src) -> {
        final int[] ret = new int[src.size()];
        int i = 0;
        for (final Boolean e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToIntegerPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private BooleanCollectionToLongPrim booleanCollectionToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToLongPrim");
  };

  private BooleanCollectionToLongPrimArray booleanCollectionToLongPrimArray = (fieldCtx, src) -> {
    final long[] ret = new long[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBooleanToLongPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToNumberBased booleanCollectionToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BooleanCollectionToNumberBased");
  };

  private BooleanCollectionToNumberBasedArray booleanCollectionToNumberBasedArray =
      (fieldCtx, type, src) -> {
        final Object ret = Array.newInstance(type, src.size());
        int i = 0;
        for (final Boolean e : src) {
          final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
              .convert(fieldCtx, type, e);
          setArrayComponent(type, ret, i, value);
          i++;
        }
        return (Object[]) ret;
      };

  private BooleanCollectionToNumberBasedCollection booleanCollectionToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private BooleanCollectionToObjectId booleanCollectionToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToObjectId");
  };

  private BooleanCollectionToObjectIdArray booleanCollectionToObjectIdArray = (fieldCtx, src) -> {
    final ObjectId[] ret = new ObjectId[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBooleanToObjectId().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToObjectIdCollection booleanCollectionToObjectIdCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getBooleanToObjectId().convert(fieldCtx, e)));
        return dst;
      };

  private BooleanCollectionToShortPrim booleanCollectionToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToShortPrim");
  };

  private BooleanCollectionToShortPrimArray booleanCollectionToShortPrimArray = (fieldCtx, src) -> {
    final short[] ret = new short[src.size()];
    int i = 0;
    for (final Boolean e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getBooleanToShortPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private BooleanCollectionToStringBased booleanCollectionToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BooleanCollectionToStringBased");
  };

  private BooleanCollectionToStringBasedArray booleanCollectionToStringBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Boolean e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private BooleanCollectionToStringBasedCollection booleanCollectionToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private BooleanToBinary booleanToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanToBinary");
  };

  private BooleanToBinaryArray booleanToBinaryArray = (fieldCtx, src) -> {
    return new Binary[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, src)};
  };

  private BooleanToBinaryCollection booleanToBinaryCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, src));
    return dst;
  };

  private BooleanToBoolean booleanToBoolean = (fieldCtx, src) -> src;

  private BooleanToBooleanArray booleanToBooleanArray = (fieldCtx, src) -> {
    return new Boolean[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, src)};
  };

  private BooleanToBooleanCollection booleanToBooleanCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, src));
    return dst;
  };

  private BooleanToBooleanPrim booleanToBooleanPrim = (fieldCtx, src) -> {
    src = fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, src);
    if (src == null) {
      return fieldCtx.getConfiguration().isValueBooleanPrimBooleanNull();
    }
    return src;
  };

  private BooleanToBooleanPrimArray booleanToBooleanPrimArray = (fieldCtx, src) -> {
    return new boolean[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToBooleanPrim().convert(fieldCtx, src)};
  };

  private BooleanToByte booleanToByte = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueByteBooleanNull();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueByteBooleanTrue();
    }
    return fieldCtx.getConfiguration().getValueByteBooleanFalse();
  };

  private BooleanToByteArray booleanToByteArray = (fieldCtx, src) -> {
    return new Byte[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, src)};
  };

  private BooleanToByteCollection booleanToByteCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, src));
    return dst;
  };

  private BooleanToBytePrim booleanToBytePrim = (fieldCtx, src) -> {
    final Byte ret =
        fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, src);
    if (ret == null) {
      return fieldCtx.getConfiguration().getValueBytePrimBooleanNull();
    }
    return ret;
  };

  private BooleanToBytePrimArray booleanToBytePrimArray = (fieldCtx, src) -> {
    return new byte[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToBytePrim().convert(fieldCtx, src)};
  };

  private BooleanToCharacter booleanToCharacter = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueCharacterBooleanNull();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueCharacterBooleanTrue();
    }
    return fieldCtx.getConfiguration().getValueCharacterBooleanFalse();
  };

  private BooleanToCharacterArray booleanToCharacterArray = (fieldCtx, src) -> {
    return new Character[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, src)};
  };

  private BooleanToCharacterCollection booleanToCharacterCollection = (fieldCtx, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, src));
    return dst;
  };

  private BooleanToCharacterPrim booleanToCharacterPrim = (fieldCtx, src) -> {
    final Character ret =
        fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, src);
    if (ret == null) {
      return fieldCtx.getConfiguration().getValueCharacterPrimBooleanNull();
    }
    return ret;
  };

  private BooleanToCharacterPrimArray booleanToCharacterPrimArray = (fieldCtx, src) -> {
    return new char[] {fieldCtx.getConfiguration().getToObject().getBooleanToCharacterPrim()
        .convert(fieldCtx, src)};
  };

  private BooleanToDateBased booleanToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanToDateBased");
  };

  private BooleanToDateBasedArray booleanToDateBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private BooleanToDateBasedCollection booleanToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToDateBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  private BooleanToDoublePrim booleanToDoublePrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().doubleValue();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueNumberBooleanTrue().doubleValue();
    }
    return fieldCtx.getConfiguration().getValueNumberBooleanFalse().doubleValue();
  };

  private BooleanToDoublePrimArray booleanToDoublePrimArray = (fieldCtx, src) -> new double[] {
      fieldCtx.getConfiguration().getToObject().getBooleanToDoublePrim().convert(fieldCtx, src)};

  private BooleanToEnum booleanToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanToEnum");
  };

  private BooleanToEnumArray booleanToEnumArray = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanToEnumArray");
  };

  private BooleanToEnumCollection booleanToEnumCollection = (fieldCtx, enumType, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToEnum().convert(fieldCtx, enumType,
        src));
    return dst;
  };

  private BooleanToFloatPrim booleanToFloatPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().floatValue();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueNumberBooleanTrue().floatValue();
    }
    return fieldCtx.getConfiguration().getValueNumberBooleanFalse().floatValue();
  };

  private BooleanToFloatPrimArray booleanToFloatPrimArray = (fieldCtx, src) -> new float[] {
      fieldCtx.getConfiguration().getToObject().getBooleanToFloatPrim().convert(fieldCtx, src)};

  private BooleanToIntegerPrim booleanToIntegerPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().intValue();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueNumberBooleanTrue().intValue();
    }
    return fieldCtx.getConfiguration().getValueNumberBooleanFalse().intValue();
  };

  private BooleanToIntegerPrimArray booleanToIntegerPrimArray = (fieldCtx, src) -> new int[] {
      fieldCtx.getConfiguration().getToObject().getBooleanToIntegerPrim().convert(fieldCtx, src)};

  private BooleanToLongPrim booleanToLongPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().longValue();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueNumberBooleanTrue().longValue();
    }
    return fieldCtx.getConfiguration().getValueNumberBooleanFalse().longValue();
  };

  private BooleanToLongPrimArray booleanToLongPrimArray = (fieldCtx, src) -> new long[] {
      fieldCtx.getConfiguration().getToObject().getBooleanToLongPrim().convert(fieldCtx, src)};

  private BooleanToNumberBased booleanToNumberBased = (fieldCtx, type, src) -> {
    if (type.isPrimitive() && (src == null)) {
      return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
          fieldCtx.getConfiguration().getValueNumberPrimBooleanNull());
    }
    if (src == null) {
      return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
          fieldCtx.getConfiguration().getValueNumberBooleanNull());
    }
    if (src) {
      return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
          fieldCtx.getConfiguration().getValueNumberBooleanTrue());
    }
    return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
        fieldCtx.getConfiguration().getValueNumberBooleanFalse());
  };

  private BooleanToNumberBasedArray booleanToNumberBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private BooleanToNumberBasedCollection booleanToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
            .convert(fieldCtx, type, src));
        return dst;
      };

  private BooleanToObjectId booleanToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanToObjectId");
  };

  private BooleanToObjectIdArray booleanToObjectIdArray = (fieldCtx, src) -> {
    return new ObjectId[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToObjectId().convert(fieldCtx, src)};
  };

  private BooleanToObjectIdCollection booleanToObjectIdCollection = (fieldCtx, src, dst) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: BooleanToObjectIdCollection");
  };

  private BooleanToShortPrim booleanToShortPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().shortValue();
    }
    if (src) {
      return fieldCtx.getConfiguration().getValueNumberBooleanTrue().shortValue();
    }
    return fieldCtx.getConfiguration().getValueNumberBooleanFalse().shortValue();
  };

  private BooleanToShortPrimArray booleanToShortPrimArray = (fieldCtx, src) -> new short[] {
      fieldCtx.getConfiguration().getToObject().getBooleanToShortPrim().convert(fieldCtx, src)};

  private BooleanToStringBased booleanToStringBased = (fieldCtx, type, src) -> {
    if (String.class.isAssignableFrom(type)) {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueStringBooleanNull();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueStringBooleanTrue();
      }
      return fieldCtx.getConfiguration().getValueStringBooleanFalse();
    }
    throw new UnsupportedOperationException(
        "Not (yet) implemented: BooleanToStringBased for type " + type);
  };

  private BooleanToStringBasedArray booleanToStringBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private BooleanToStringBasedCollection booleanToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
            .convert(fieldCtx, type, src));
        return dst;
      };

  private DateCollectionToBinary dateCollectionToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBinary");
  };

  private DateCollectionToBinaryArray dateCollectionToBinaryArray = (fieldCtx, src) -> {
    final Binary[] ret = new Binary[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToBinary().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToBinaryCollection dateCollectionToBinaryCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst
            .add(fieldCtx.getConfiguration().getToObject().getDateToBinary().convert(fieldCtx, e)));
        return dst;
      };

  private DateCollectionToBoolean dateCollectionToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBoolean");
  };

  private DateCollectionToBooleanArray dateCollectionToBooleanArray = (fieldCtx, src) -> {
    final Boolean[] ret = new Boolean[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToBoolean().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToBooleanCollection dateCollectionToBooleanCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getDateToBoolean().convert(fieldCtx, e)));
        return dst;
      };

  private DateCollectionToBooleanPrim dateCollectionToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBooleanPrim");
  };

  private DateCollectionToBooleanPrimArray dateCollectionToBooleanPrimArray = (fieldCtx, src) -> {
    final boolean[] ret = new boolean[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getDateToBooleanPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToByte dateCollectionToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToByte");
  };

  private DateCollectionToByteArray dateCollectionToByteArray = (fieldCtx, src) -> {
    final Byte[] ret = new Byte[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToByte().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToByteCollection dateCollectionToByteCollection = (fieldCtx, src, dst) -> {
    src.forEach(e -> dst
        .add(fieldCtx.getConfiguration().getToObject().getDateToByte().convert(fieldCtx, e)));
    return dst;
  };

  private DateCollectionToBytePrim dateCollectionToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBytePrim");
  };

  private DateCollectionToBytePrimArray dateCollectionToBytePrimArray = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToBytePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToCharacter dateCollectionToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToCharacter");
  };

  private DateCollectionToCharacterArray dateCollectionToCharacterArray = (fieldCtx, src) -> {
    final Character[] ret = new Character[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToCharacter().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToCharacterCollection dateCollectionToCharacterCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getDateToCharacter().convert(fieldCtx, e)));
        return dst;
      };

  private DateCollectionToCharacterPrim dateCollectionToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToCharacterPrim");
  };

  private DateCollectionToCharacterPrimArray dateCollectionToCharacterPrimArray =
      (fieldCtx, src) -> {
        final char[] ret = new char[src.size()];
        int i = 0;
        for (final Date e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getDateToCharacterPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private DateCollectionToDateBased dateCollectionToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToDateBased");
  };

  private DateCollectionToDateBasedArray dateCollectionToDateBasedArray = (fieldCtx, type, src) -> {
    final Object[] ret = (Object[]) Array.newInstance(type, src.size());
    int i = 0;
    for (final Date e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getDateToDateBased().convert(fieldCtx, type, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToDateBasedCollection dateCollectionToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToDateBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private DateCollectionToDoublePrim dateCollectionToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToDoublePrim");
  };

  private DateCollectionToDoublePrimArray dateCollectionToDoublePrimArray = (fieldCtx, src) -> {
    final double[] ret = new double[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToDoublePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToEnum dateCollectionToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToEnum");
  };

  private DateCollectionToEnumArray dateCollectionToEnumArray = (fieldCtx, enumType, src) -> {
    final Enum<?>[] ret = new Enum[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getDateToEnum().convert(fieldCtx, enumType, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToEnumCollection dateCollectionToEnumCollection =
      (fieldCtx, enumType, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToEnum()
            .convert(fieldCtx, enumType, e)));
        return dst;
      };

  private DateCollectionToFloatPrim dateCollectionToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToFloatPrim");
  };

  private DateCollectionToFloatPrimArray dateCollectionToFloatPrimArray = (fieldCtx, src) -> {
    final float[] ret = new float[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToFloatPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToIntegerPrim dateCollectionToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToIntegerPrim");
  };

  private DateCollectionToIntegerPrimArray dateCollectionToIntegerPrimArray = (fieldCtx, src) -> {
    final int[] ret = new int[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getDateToIntegerPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToLongPrim dateCollectionToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToLongPrim");
  };

  private DateCollectionToLongPrimArray dateCollectionToLongPrimArray = (fieldCtx, src) -> {
    final long[] ret = new long[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToLongPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToNumberBased dateCollectionToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToNumberBased");
  };

  private DateCollectionToNumberBasedArray dateCollectionToNumberBasedArray =
      (fieldCtx, type, src) -> {
        final Object ret = Array.newInstance(type, src.size());
        int i = 0;
        for (final Date e : src) {
          final Object value = fieldCtx.getConfiguration().getToObject().getDateToNumberBased()
              .convert(fieldCtx, type, e);
          setArrayComponent(type, ret, i, value);
          i++;
        }
        return (Object[]) ret;
      };

  private DateCollectionToNumberBasedCollection dateCollectionToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToNumberBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private DateCollectionToObjectId dateCollectionToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToObjectId");
  };

  private DateCollectionToObjectIdArray dateCollectionToObjectIdArray = (fieldCtx, src) -> {
    final ObjectId[] ret = new ObjectId[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToObjectId().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToObjectIdCollection dateCollectionToObjectIdCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getDateToObjectId().convert(fieldCtx, e)));
        return dst;
      };

  private DateCollectionToShortPrim dateCollectionToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToShortPrim");
  };

  private DateCollectionToShortPrimArray dateCollectionToShortPrimArray = (fieldCtx, src) -> {
    final short[] ret = new short[src.size()];
    int i = 0;
    for (final Date e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getDateToShortPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private DateCollectionToStringBased dateCollectionToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToStringBased");
  };

  private DateCollectionToStringBasedArray dateCollectionToStringBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Date e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getDateToStringBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private DateCollectionToStringBasedCollection dateCollectionToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToStringBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private DateToBinary dateToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBinary");
  };

  private DateToBinaryArray dateToBinaryArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBinaryArray");
  };

  private DateToBinaryCollection dateToBinaryCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToBinary().convert(fieldCtx, src));
    return dst;
  };

  private DateToBoolean dateToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBoolean");
  };

  private DateToBooleanArray dateToBooleanArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBooleanArray");
  };

  private DateToBooleanCollection dateToBooleanCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToBoolean().convert(fieldCtx, src));
    return dst;
  };

  private DateToBooleanPrim dateToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBooleanPrim");
  };

  private DateToBooleanPrimArray dateToBooleanPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBooleanPrimArray");
  };

  private DateToByte dateToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToByte");
  };

  private DateToByteArray dateToByteArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToByteArray");
  };

  private DateToByteCollection dateToByteCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToByte().convert(fieldCtx, src));
    return dst;
  };

  private DateToBytePrim dateToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBytePrim");
  };

  private DateToBytePrimArray dateToBytePrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToBytePrimArray");
  };

  private DateToCharacter dateToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacter");
  };

  private DateToCharacterArray dateToCharacterArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacterArray");
  };

  private DateToCharacterCollection dateToCharacterCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToCharacter().convert(fieldCtx, src));
    return dst;
  };

  private DateToCharacterPrim dateToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacterPrim");
  };

  private DateToCharacterPrimArray dateToCharacterPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacterPrimArray");
  };

  private DateToDateBased dateToDateBased = (fieldCtx, type, src) -> fieldCtx.getConfiguration()
      .getDateBased().getToObject().convert(type, src);

  private DateToDateBasedArray dateToDateBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value =
        fieldCtx.getConfiguration().getToObject().getDateToDateBased().convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private DateToDateBasedCollection dateToDateBasedCollection = (fieldCtx, type, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToDateBased().convert(fieldCtx, type,
        src));
    return dst;
  };

  private DateToDoublePrim dateToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToDoublePrim");
  };

  private DateToDoublePrimArray dateToDoublePrimArray = (fieldCtx, src) -> new double[] {
      fieldCtx.getConfiguration().getToObject().getDateToDoublePrim().convert(fieldCtx, src)};

  private DateToEnum dateToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToEnum");
  };

  private DateToEnumArray dateToEnumArray = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToEnumArray");
  };

  private DateToEnumCollection dateToEnumCollection = (fieldCtx, enumType, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getDateToEnum().convert(fieldCtx, enumType, src));
    return dst;
  };

  private DateToFloatPrim dateToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToFloatPrim");
  };

  private DateToFloatPrimArray dateToFloatPrimArray = (fieldCtx, src) -> new float[] {
      fieldCtx.getConfiguration().getToObject().getDateToFloatPrim().convert(fieldCtx, src)};

  private DateToIntegerPrim dateToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToIntegerPrim");
  };

  private DateToIntegerPrimArray dateToIntegerPrimArray = (fieldCtx, src) -> new int[] {
      fieldCtx.getConfiguration().getToObject().getDateToIntegerPrim().convert(fieldCtx, src)};

  private DateToLongPrim dateToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToLongPrim");
  };

  private DateToLongPrimArray dateToLongPrimArray = (fieldCtx, src) -> new long[] {
      fieldCtx.getConfiguration().getToObject().getDateToLongPrim().convert(fieldCtx, src)};

  private DateToNumberBased dateToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToNumberBased");
  };

  private DateToNumberBasedArray dateToNumberBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getDateToNumberBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private DateToNumberBasedCollection dateToNumberBasedCollection = (fieldCtx, type, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToNumberBased().convert(fieldCtx, type,
        src));
    return dst;
  };

  private DateToObjectId dateToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToObjectId");
  };

  private DateToObjectIdArray dateToObjectIdArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToObjectIdArray");
  };

  private DateToObjectIdCollection dateToObjectIdCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToObjectId().convert(fieldCtx, src));
    return dst;
  };

  private DateToShortPrim dateToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToShortPrim");
  };

  private DateToShortPrimArray dateToShortPrimArray = (fieldCtx, src) -> new short[] {
      fieldCtx.getConfiguration().getToObject().getDateToShortPrim().convert(fieldCtx, src)};

  private DateToStringBased dateToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: DateToStringBased");
  };

  private DateToStringBasedArray dateToStringBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getDateToStringBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private DateToStringBasedCollection dateToStringBasedCollection = (fieldCtx, type, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getDateToStringBased().convert(fieldCtx, type,
        src));
    return dst;
  };

  private NumberCollectionToBinary numberCollectionToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBinary");
  };

  private NumberCollectionToBinaryArray numberCollectionToBinaryArray = (fieldCtx, src) -> {
    final Binary[] ret = new Binary[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToBinaryCollection numberCollectionToBinaryCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, e)));
        return dst;
      };

  private NumberCollectionToBoolean numberCollectionToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBoolean");
  };

  private NumberCollectionToBooleanArray numberCollectionToBooleanArray = (fieldCtx, src) -> {
    final Boolean[] ret = new Boolean[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToBooleanCollection numberCollectionToBooleanCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, e)));
        return dst;
      };

  private NumberCollectionToBooleanPrim numberCollectionToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBooleanPrim");
  };

  private NumberCollectionToBooleanPrimArray numberCollectionToBooleanPrimArray =
      (fieldCtx, src) -> {
        final boolean[] ret = new boolean[src.size()];
        int i = 0;
        for (final Number e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToBooleanPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private NumberCollectionToByte numberCollectionToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToByte");
  };

  private NumberCollectionToByteArray numberCollectionToByteArray = (fieldCtx, src) -> {
    final Byte[] ret = new Byte[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToByteCollection numberCollectionToByteCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst
            .add(fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, e)));
        return dst;
      };

  private NumberCollectionToBytePrim numberCollectionToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBytePrim");
  };

  private NumberCollectionToBytePrimArray numberCollectionToBytePrimArray = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToBytePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToCharacter numberCollectionToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToCharacter");
  };

  private NumberCollectionToCharacterArray numberCollectionToCharacterArray = (fieldCtx, src) -> {
    final Character[] ret = new Character[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getNumberToCharacter().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToCharacterCollection numberCollectionToCharacterCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getNumberToCharacter().convert(fieldCtx, e)));
        return dst;
      };

  private NumberCollectionToCharacterPrim numberCollectionToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: NumberCollectionToCharacterPrim");
  };

  private NumberCollectionToCharacterPrimArray numberCollectionToCharacterPrimArray =
      (fieldCtx, src) -> {
        final char[] ret = new char[src.size()];
        int i = 0;
        for (final Number e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToCharacterPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private NumberCollectionToDateBased numberCollectionToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToDateBased");
  };

  private NumberCollectionToDateBasedArray numberCollectionToDateBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Number e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToDateBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private NumberCollectionToDateBasedCollection numberCollectionToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToDateBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private NumberCollectionToDoublePrim numberCollectionToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToDoublePrim");
  };

  private NumberCollectionToDoublePrimArray numberCollectionToDoublePrimArray = (fieldCtx, src) -> {
    final double[] ret = new double[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getNumberToDoublePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToEnum numberCollectionToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToEnum");
  };

  private NumberCollectionToEnumArray numberCollectionToEnumArray = (fieldCtx, enumType, src) -> {
    final Enum<?>[] ret = new Enum[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToEnum().convert(fieldCtx,
          enumType, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToEnumCollection numberCollectionToEnumCollection =
      (fieldCtx, enumType, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToEnum()
            .convert(fieldCtx, enumType, e)));
        return dst;
      };

  private NumberCollectionToFloatPrim numberCollectionToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToFloatPrim");
  };

  private NumberCollectionToFloatPrimArray numberCollectionToFloatPrimArray = (fieldCtx, src) -> {
    final float[] ret = new float[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getNumberToFloatPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToIntegerPrim numberCollectionToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToIntegerPrim");
  };

  private NumberCollectionToIntegerPrimArray numberCollectionToIntegerPrimArray =
      (fieldCtx, src) -> {
        final int[] ret = new int[src.size()];
        int i = 0;
        for (final Number e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToIntegerPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private NumberCollectionToLongPrim numberCollectionToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToLongPrim");
  };

  private NumberCollectionToLongPrimArray numberCollectionToLongPrimArray = (fieldCtx, src) -> {
    final long[] ret = new long[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToLongPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToNumberBased numberCollectionToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToNumberBased");
  };

  private NumberCollectionToNumberBasedArray numberCollectionToNumberBasedArray =
      (fieldCtx, type, src) -> {
        final Object ret = Array.newInstance(type, src.size());
        int i = 0;
        for (final Number e : src) {
          final Object value =
              fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, e);
          setArrayComponent(type, ret, i, value);
          i++;
        }
        return (Object[]) ret;
      };

  private NumberCollectionToNumberBasedCollection numberCollectionToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst
            .add(fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, e)));
        return dst;
      };

  private NumberCollectionToObjectId numberCollectionToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToObjectId");
  };

  private NumberCollectionToObjectIdArray numberCollectionToObjectIdArray = (fieldCtx, src) -> {
    final ObjectId[] ret = new ObjectId[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToObjectId().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToObjectIdCollection numberCollectionToObjectIdCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getNumberToObjectId().convert(fieldCtx, e)));
        return dst;
      };

  private NumberCollectionToShortPrim numberCollectionToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToShortPrim");
  };

  private NumberCollectionToShortPrimArray numberCollectionToShortPrimArray = (fieldCtx, src) -> {
    final short[] ret = new short[src.size()];
    int i = 0;
    for (final Number e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getNumberToShortPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private NumberCollectionToStringBased numberCollectionToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToStringBased");
  };

  private NumberCollectionToStringBasedArray numberCollectionToStringBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final Number e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToStringBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private NumberCollectionToStringBasedCollection numberCollectionToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToStringBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private NumberToBinary numberToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToBinary");
  };

  private NumberToBinaryArray numberToBinaryArray = (fieldCtx, src) -> src == null ? null
      : new Binary[] {
          fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, src)};

  private NumberToBinaryCollection numberToBinaryCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, src));
    return dst;
  };

  private NumberToBoolean numberToBoolean = (fieldCtx, src) -> {
    if (src == null) {
      return null;
    }
    if (src.longValue() == 0) {
      return Boolean.FALSE;
    }
    return Boolean.TRUE;
  };

  private NumberToBooleanArray numberToBooleanArray = (fieldCtx, src) -> src == null ? null
      : new Boolean[] {
          fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, src)};

  private NumberToBooleanCollection numberToBooleanCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, src));
    return dst;
  };

  private NumberToBooleanPrim numberToBooleanPrim = (fieldCtx, src) -> {
    final Boolean ret =
        fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, src);
    if (ret == null) {
      return fieldCtx.getConfiguration().isValueBooleanPrimBooleanNull();
    }
    return ret;
  };

  private NumberToBooleanPrimArray numberToBooleanPrimArray = (fieldCtx, src) -> src == null ? null
      : new boolean[] {fieldCtx.getConfiguration().getToObject().getNumberToBooleanPrim()
          .convert(fieldCtx, src)};

  private NumberToByte numberToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToByte");
  };

  private NumberToByteArray numberToByteArray = (fieldCtx, src) -> src == null ? null
      : new Byte[] {
          fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, src)};

  private NumberToByteCollection numberToByteCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, src));
    return dst;
  };

  private NumberToBytePrim numberToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToBytePrim");
  };

  private NumberToBytePrimArray numberToBytePrimArray = (fieldCtx, src) -> src == null ? null
      : new byte[] {
          fieldCtx.getConfiguration().getToObject().getNumberToBytePrim().convert(fieldCtx, src)};

  private NumberToCharacter numberToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToCharacter");
  };

  private NumberToCharacterArray numberToCharacterArray = (fieldCtx, src) -> src == null ? null
      : new Character[] {
          fieldCtx.getConfiguration().getToObject().getNumberToCharacter().convert(fieldCtx, src)};

  private NumberToCharacterCollection numberToCharacterCollection = (fieldCtx, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getNumberToCharacter().convert(fieldCtx, src));
    return dst;
  };

  private NumberToCharacterPrim numberToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToCharacterPrim");
  };

  private NumberToCharacterPrimArray numberToCharacterPrimArray =
      (fieldCtx, src) -> src == null ? null
          : new char[] {fieldCtx.getConfiguration().getToObject().getNumberToCharacterPrim()
              .convert(fieldCtx, src)};

  private NumberToDateBased numberToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToDateBased");
  };

  private NumberToDateBasedArray numberToDateBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getNumberToDateBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private NumberToDateBasedCollection numberToDateBasedCollection = (fieldCtx, type, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getNumberToDateBased().convert(fieldCtx, type,
        src));
    return dst;
  };

  private NumberToDoublePrim numberToDoublePrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().doubleValue();
    }
    return src.doubleValue();
  };

  private NumberToDoublePrimArray numberToDoublePrimArray = (fieldCtx, src) -> new double[] {
      fieldCtx.getConfiguration().getToObject().getNumberToDoublePrim().convert(fieldCtx, src)};

  private NumberToEnum numberToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToEnum");
  };

  private NumberToEnumArray numberToEnumArray = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToEnumArray");
  };

  private NumberToEnumCollection numberToEnumCollection = (fieldCtx, enumType, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getNumberToEnum().convert(fieldCtx, enumType,
        src));
    return dst;
  };

  private NumberToFloatPrim numberToFloatPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().floatValue();
    }
    return src.floatValue();
  };

  private NumberToFloatPrimArray numberToFloatPrimArray = (fieldCtx, src) -> new float[] {
      fieldCtx.getConfiguration().getToObject().getNumberToFloatPrim().convert(fieldCtx, src)};

  private NumberToIntegerPrim numberToIntegerPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().intValue();
    }
    return src.intValue();
  };

  private NumberToIntegerPrimArray numberToIntegerPrimArray = (fieldCtx, src) -> new int[] {
      fieldCtx.getConfiguration().getToObject().getNumberToIntegerPrim().convert(fieldCtx, src)};

  private NumberToLongPrim numberToLongPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().longValue();
    }
    return src.longValue();
  };

  private NumberToLongPrimArray numberToLongPrimArray = (fieldCtx, src) -> new long[] {
      fieldCtx.getConfiguration().getToObject().getNumberToLongPrim().convert(fieldCtx, src)};

  private NumberToNumberBased numberToNumberBased = (fieldCtx, type, src) -> fieldCtx
      .getConfiguration().getNumberBased().getToObject().convert(type, src);

  private NumberToNumberBasedArray numberToNumberBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value =
        fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private NumberToNumberBasedCollection numberToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, src));
        return dst;

      };

  private NumberToObjectId numberToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToObjectId");
  };

  private NumberToObjectIdArray numberToObjectIdArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToObjectIdArray");
  };

  private NumberToObjectIdCollection numberToObjectIdCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getNumberToObjectId().convert(fieldCtx, src));
    return dst;
  };

  private NumberToShortPrim numberToShortPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().shortValue();
    }
    return src.shortValue();
  };

  private NumberToShortPrimArray numberToShortPrimArray = (fieldCtx, src) -> new short[] {
      fieldCtx.getConfiguration().getToObject().getNumberToShortPrim().convert(fieldCtx, src)};

  private NumberToStringBased numberToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: NumberToStringBased");
  };

  private NumberToStringBasedArray numberToStringBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getNumberToStringBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private NumberToStringBasedCollection numberToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getNumberToStringBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  private ObjectIdCollectionToBinary objectIdCollectionToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToBinary");
  };

  private ObjectIdCollectionToBinaryArray objectIdCollectionToBinaryArray = (fieldCtx, src) -> {
    final Binary[] ret = new Binary[src.size()];
    int i = 0;
    for (final ObjectId e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToBinary().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private ObjectIdCollectionToBinaryCollection objectIdCollectionToBinaryCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getObjectIdToBinary().convert(fieldCtx, e)));
        return dst;
      };

  private ObjectIdCollectionToBoolean objectIdCollectionToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToBoolean");
  };

  private ObjectIdCollectionToBooleanArray objectIdCollectionToBooleanArray = (fieldCtx, src) -> {
    final Boolean[] ret = new Boolean[src.size()];
    int i = 0;
    for (final ObjectId e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getObjectIdToBoolean().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private ObjectIdCollectionToBooleanCollection objectIdCollectionToBooleanCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getObjectIdToBoolean().convert(fieldCtx, e)));
        return dst;
      };

  private ObjectIdCollectionToBooleanPrim objectIdCollectionToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: ObjectIdCollectionToBooleanPrim");
  };

  private ObjectIdCollectionToBooleanPrimArray objectIdCollectionToBooleanPrimArray =
      (fieldCtx, src) -> {
        final boolean[] ret = new boolean[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToBooleanPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToByte objectIdCollectionToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToByte");
  };

  private ObjectIdCollectionToByteArray objectIdCollectionToByteArray = (fieldCtx, src) -> {
    final Byte[] ret = new Byte[src.size()];
    int i = 0;
    for (final ObjectId e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToByte().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private ObjectIdCollectionToByteCollection objectIdCollectionToByteCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getObjectIdToByte().convert(fieldCtx, e)));
        return dst;
      };

  private ObjectIdCollectionToBytePrim objectIdCollectionToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToBytePrim");
  };

  private ObjectIdCollectionToBytePrimArray objectIdCollectionToBytePrimArray = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final ObjectId e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getObjectIdToBytePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private ObjectIdCollectionToCharacter objectIdCollectionToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToCharacter");
  };

  private ObjectIdCollectionToCharacterArray objectIdCollectionToCharacterArray =
      (fieldCtx, src) -> {
        final Character[] ret = new Character[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToCharacter()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToCharacterCollection objectIdCollectionToCharacterCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToCharacter()
            .convert(fieldCtx, e)));
        return dst;
      };

  private ObjectIdCollectionToCharacterPrim objectIdCollectionToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: ObjectIdCollectionToCharacterPrim");
  };

  private ObjectIdCollectionToCharacterPrimArray objectIdCollectionToCharacterPrimArray =
      (fieldCtx, src) -> {
        final char[] ret = new char[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToCharacterPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToDateBased objectIdCollectionToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToDateBased");
  };

  private ObjectIdCollectionToDateBasedArray objectIdCollectionToDateBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToDateBasedCollection objectIdCollectionToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private ObjectIdCollectionToDoublePrim objectIdCollectionToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: ObjectIdCollectionToDoublePrim");
  };

  private ObjectIdCollectionToDoublePrimArray objectIdCollectionToDoublePrimArray =
      (fieldCtx, src) -> {
        final double[] ret = new double[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToDoublePrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToEnum objectIdCollectionToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToEnum");
  };

  private ObjectIdCollectionToEnumArray objectIdCollectionToEnumArray =
      (fieldCtx, enumType, src) -> {
        final Enum<?>[] ret = new Enum[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToEnum().convert(fieldCtx,
              enumType, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToEnumCollection objectIdCollectionToEnumCollection =
      (fieldCtx, enumType, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToEnum()
            .convert(fieldCtx, enumType, e)));
        return dst;
      };

  private ObjectIdCollectionToFloatPrim objectIdCollectionToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToFloatPrim");
  };

  private ObjectIdCollectionToFloatPrimArray objectIdCollectionToFloatPrimArray =
      (fieldCtx, src) -> {
        final float[] ret = new float[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToFloatPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToIntegerPrim objectIdCollectionToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: ObjectIdCollectionToIntegerPrim");
  };

  private ObjectIdCollectionToIntegerPrimArray objectIdCollectionToIntegerPrimArray =
      (fieldCtx, src) -> {
        final int[] ret = new int[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToIntegerPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToLongPrim objectIdCollectionToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToLongPrim");
  };

  private ObjectIdCollectionToLongPrimArray objectIdCollectionToLongPrimArray = (fieldCtx, src) -> {
    final long[] ret = new long[src.size()];
    int i = 0;
    for (final ObjectId e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getObjectIdToLongPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private ObjectIdCollectionToNumberBased objectIdCollectionToNumberBased =
      (fieldCtx, type, src) -> {
        throw new UnsupportedOperationException(
            "Not (yet) implemented: ObjectIdCollectionToNumberBased");
      };

  private ObjectIdCollectionToNumberBasedArray objectIdCollectionToNumberBasedArray =
      (fieldCtx, type, src) -> {
        final Object ret = Array.newInstance(type, src.size());
        int i = 0;
        for (final ObjectId e : src) {
          final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToNumberBased()
              .convert(fieldCtx, type, e);
          setArrayComponent(type, ret, i, value);
          i++;
        }
        return (Object[]) ret;
      };

  private ObjectIdCollectionToNumberBasedCollection objectIdCollectionToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
            .getObjectIdToNumberBased().convert(fieldCtx, type, e)));
        return dst;
      };

  private ObjectIdCollectionToObjectId objectIdCollectionToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToObjectId");
  };

  private ObjectIdCollectionToObjectIdArray objectIdCollectionToObjectIdArray = (fieldCtx, src) -> {
    final ObjectId[] ret = new ObjectId[src.size()];
    int i = 0;
    for (final ObjectId e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getObjectIdToObjectId().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private ObjectIdCollectionToObjectIdCollection objectIdCollectionToObjectIdCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToObjectId()
            .convert(fieldCtx, e)));
        return dst;
      };

  private ObjectIdCollectionToShortPrim objectIdCollectionToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToShortPrim");
  };

  private ObjectIdCollectionToShortPrimArray objectIdCollectionToShortPrimArray =
      (fieldCtx, src) -> {
        final short[] ret = new short[src.size()];
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToShortPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToStringBased objectIdCollectionToStringBased =
      (fieldCtx, type, src) -> {
        throw new UnsupportedOperationException(
            "Not (yet) implemented: ObjectIdCollectionToStringBased");
      };

  private ObjectIdCollectionToStringBasedArray objectIdCollectionToStringBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final ObjectId e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToStringBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private ObjectIdCollectionToStringBasedCollection objectIdCollectionToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
            .getObjectIdToStringBased().convert(fieldCtx, type, e)));
        return dst;
      };

  private ObjectIdToBinary objectIdToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBinary");
  };

  private ObjectIdToBinaryArray objectIdToBinaryArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBinaryArray");
  };

  private ObjectIdToBinaryCollection objectIdToBinaryCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToBinary().convert(fieldCtx, src));
    return dst;
  };

  private ObjectIdToBoolean objectIdToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBoolean");
  };

  private ObjectIdToBooleanArray objectIdToBooleanArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBooleanArray");
  };

  private ObjectIdToBooleanCollection objectIdToBooleanCollection = (fieldCtx, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getObjectIdToBoolean().convert(fieldCtx, src));
    return dst;
  };

  private ObjectIdToBooleanPrim objectIdToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBooleanPrim");
  };

  private ObjectIdToBooleanPrimArray objectIdToBooleanPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBooleanPrimArray");
  };

  private ObjectIdToByte objectIdToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToByte");
  };

  private ObjectIdToByteArray objectIdToByteArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToByteArray");
  };

  private ObjectIdToByteCollection objectIdToByteCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToByte().convert(fieldCtx, src));
    return dst;
  };

  private ObjectIdToBytePrim objectIdToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBytePrim");
  };

  private ObjectIdToBytePrimArray objectIdToBytePrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBytePrimArray");
  };
  private ObjectIdToCharacter objectIdToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacter");
  };

  private ObjectIdToCharacterArray objectIdToCharacterArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacterArray");
  };

  private ObjectIdToCharacterCollection objectIdToCharacterCollection = (fieldCtx, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getObjectIdToCharacter().convert(fieldCtx, src));
    return dst;
  };

  private ObjectIdToCharacterPrim objectIdToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacterPrim");
  };

  private ObjectIdToCharacterPrimArray objectIdToCharacterPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacterPrimArray");
  };

  private ObjectIdToDateBased objectIdToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToDateBased");
  };

  private ObjectIdToDateBasedArray objectIdToDateBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private ObjectIdToDateBasedCollection objectIdToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  private ObjectIdToDoublePrim objectIdToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToDoublePrim");
  };

  private ObjectIdToDoublePrimArray objectIdToDoublePrimArray = (fieldCtx, src) -> new double[] {
      fieldCtx.getConfiguration().getToObject().getObjectIdToDoublePrim().convert(fieldCtx, src)};

  private ObjectIdToEnum objectIdToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToEnum");
  };

  private ObjectIdToEnumArray objectIdToEnumArray = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToEnumArray");
  };

  private ObjectIdToEnumCollection objectIdToEnumCollection = (fieldCtx, enumType, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToEnum().convert(fieldCtx,
        enumType, src));
    return dst;
  };

  private ObjectIdToFloatPrim objectIdToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToFloatPrim");
  };

  private ObjectIdToFloatPrimArray objectIdToFloatPrimArray = (fieldCtx, src) -> new float[] {
      fieldCtx.getConfiguration().getToObject().getObjectIdToFloatPrim().convert(fieldCtx, src)};

  private ObjectIdToIntegerPrim objectIdToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToIntegerPrim");
  };

  private ObjectIdToIntegerPrimArray objectIdToIntegerPrimArray = (fieldCtx, src) -> new int[] {
      fieldCtx.getConfiguration().getToObject().getObjectIdToIntegerPrim().convert(fieldCtx, src)};

  private ObjectIdToLongPrim objectIdToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToLongPrim");
  };

  private ObjectIdToLongPrimArray objectIdToLongPrimArray = (fieldCtx, src) -> new long[] {
      fieldCtx.getConfiguration().getToObject().getObjectIdToLongPrim().convert(fieldCtx, src)};

  private ObjectIdToNumberBased objectIdToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToNumberBased");
  };

  private ObjectIdToNumberBasedArray objectIdToNumberBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToNumberBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private ObjectIdToNumberBasedCollection objectIdToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToNumberBased()
            .convert(fieldCtx, type, src));
        return dst;
      };

  private ObjectIdToObjectId objectIdToObjectId = (fieldCtx, src) -> src;

  private ObjectIdToObjectIdArray objectIdToObjectIdArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToObjectIdArray");
  };

  private ObjectIdToObjectIdCollection objectIdToObjectIdCollection = (fieldCtx, src, dst) -> {
    dst.add(
        fieldCtx.getConfiguration().getToObject().getObjectIdToObjectId().convert(fieldCtx, src));
    return dst;
  };

  private ObjectIdToShortPrim objectIdToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToShortPrim");
  };

  private ObjectIdToShortPrimArray objectIdToShortPrimArray = (fieldCtx, src) -> new short[] {
      fieldCtx.getConfiguration().getToObject().getObjectIdToShortPrim().convert(fieldCtx, src)};

  private ObjectIdToStringBased objectIdToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToStringBased");
  };

  private ObjectIdToStringBasedArray objectIdToStringBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToStringBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private ObjectIdToStringBasedCollection objectIdToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToStringBased()
            .convert(fieldCtx, type, src));
        return dst;
      };

  private StringCollectionToBinary stringCollectionToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBinary");
  };

  private StringCollectionToBinaryArray stringCollectionToBinaryArray = (fieldCtx, src) -> {
    final Binary[] ret = new Binary[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToBinary().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToBinaryCollection stringCollectionToBinaryCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getStringToBinary().convert(fieldCtx, e)));
        return dst;
      };

  private StringCollectionToBoolean stringCollectionToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBoolean");
  };

  private StringCollectionToBooleanArray stringCollectionToBooleanArray = (fieldCtx, src) -> {
    final Boolean[] ret = new Boolean[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToBoolean().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToBooleanCollection stringCollectionToBooleanCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getStringToBoolean().convert(fieldCtx, e)));
        return dst;
      };

  private StringCollectionToBooleanPrim stringCollectionToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBooleanPrim");
  };

  private StringCollectionToBooleanPrimArray stringCollectionToBooleanPrimArray =
      (fieldCtx, src) -> {
        final boolean[] ret = new boolean[src.size()];
        int i = 0;
        for (final String e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getStringToBooleanPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private StringCollectionToByte stringCollectionToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToByte");
  };

  private StringCollectionToByteArray stringCollectionToByteArray = (fieldCtx, src) -> {
    final Byte[] ret = new Byte[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToByte().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToByteCollection stringCollectionToByteCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst
            .add(fieldCtx.getConfiguration().getToObject().getStringToByte().convert(fieldCtx, e)));
        return dst;
      };

  private StringCollectionToBytePrim stringCollectionToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBytePrim");
  };

  private StringCollectionToBytePrimArray stringCollectionToBytePrimArray = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToBytePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToCharacter stringCollectionToCharacter = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToCharacter");
  };

  private StringCollectionToCharacterArray stringCollectionToCharacterArray = (fieldCtx, src) -> {
    final Character[] ret = new Character[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getStringToCharacter().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToCharacterCollection stringCollectionToCharacterCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getStringToCharacter().convert(fieldCtx, e)));
        return dst;
      };

  private StringCollectionToCharacterPrim stringCollectionToCharacterPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException(
        "Not (yet) implemented: StringCollectionToCharacterPrim");
  };

  private StringCollectionToCharacterPrimArray stringCollectionToCharacterPrimArray =
      (fieldCtx, src) -> {
        final char[] ret = new char[src.size()];
        int i = 0;
        for (final String e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getStringToCharacterPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private StringCollectionToDateBased stringCollectionToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToDateBased");
  };

  private StringCollectionToDateBasedArray stringCollectionToDateBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final String e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getStringToDateBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private StringCollectionToDateBasedCollection stringCollectionToDateBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToDateBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private StringCollectionToDoublePrim stringCollectionToDoublePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToDoublePrim");
  };

  private StringCollectionToDoublePrimArray stringCollectionToDoublePrimArray = (fieldCtx, src) -> {
    final double[] ret = new double[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getStringToDoublePrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToEnum stringCollectionToEnum = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToEnum");
  };

  private StringCollectionToEnumArray stringCollectionToEnumArray = (fieldCtx, enumType, src) -> {
    final Enum<?>[] ret = new Enum[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToEnum().convert(fieldCtx,
          enumType, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToEnumCollection stringCollectionToEnumCollection =
      (fieldCtx, enumType, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToEnum()
            .convert(fieldCtx, enumType, e)));
        return dst;
      };

  private StringCollectionToFloatPrim stringCollectionToFloatPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToFloatPrim");
  };

  private StringCollectionToFloatPrimArray stringCollectionToFloatPrimArray = (fieldCtx, src) -> {
    final float[] ret = new float[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getStringToFloatPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToIntegerPrim stringCollectionToIntegerPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToIntegerPrim");
  };

  private StringCollectionToIntegerPrimArray stringCollectionToIntegerPrimArray =
      (fieldCtx, src) -> {
        final int[] ret = new int[src.size()];
        int i = 0;
        for (final String e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getStringToIntegerPrim()
              .convert(fieldCtx, e);
          i++;
        }
        return ret;
      };

  private StringCollectionToLongPrim stringCollectionToLongPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToLongPrim");
  };

  private StringCollectionToLongPrimArray stringCollectionToLongPrimArray = (fieldCtx, src) -> {
    final long[] ret = new long[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToLongPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToNumberBased stringCollectionToNumberBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToNumberBased");
  };

  private StringCollectionToNumberBasedArray stringCollectionToNumberBasedArray =
      (fieldCtx, type, src) -> {
        final Object ret = Array.newInstance(type, src.size());
        int i = 0;
        for (final String e : src) {
          final Object value = fieldCtx.getConfiguration().getToObject().getStringToNumberBased()
              .convert(fieldCtx, type, e);
          setArrayComponent(type, ret, i, value);
          i++;
        }
        return (Object[]) ret;
      };

  private StringCollectionToNumberBasedCollection stringCollectionToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToNumberBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private StringCollectionToObjectId stringCollectionToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToObjectId");
  };

  private StringCollectionToObjectIdArray stringCollectionToObjectIdArray = (fieldCtx, src) -> {
    final ObjectId[] ret = new ObjectId[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] = fieldCtx.getConfiguration().getToObject().getStringToObjectId().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToObjectIdCollection stringCollectionToObjectIdCollection =
      (fieldCtx, src, dst) -> {
        src.forEach(e -> dst.add(
            fieldCtx.getConfiguration().getToObject().getStringToObjectId().convert(fieldCtx, e)));
        return dst;
      };

  private StringCollectionToShortPrim stringCollectionToShortPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToShortPrim");
  };

  private StringCollectionToShortPrimArray stringCollectionToShortPrimArray = (fieldCtx, src) -> {
    final short[] ret = new short[src.size()];
    int i = 0;
    for (final String e : src) {
      ret[i] =
          fieldCtx.getConfiguration().getToObject().getStringToShortPrim().convert(fieldCtx, e);
      i++;
    }
    return ret;
  };

  private StringCollectionToStringBased stringCollectionToStringBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToStringBased");
  };

  private StringCollectionToStringBasedArray stringCollectionToStringBasedArray =
      (fieldCtx, type, src) -> {
        final Object[] ret = (Object[]) Array.newInstance(type, src.size());
        int i = 0;
        for (final String e : src) {
          ret[i] = fieldCtx.getConfiguration().getToObject().getStringToStringBased()
              .convert(fieldCtx, type, e);
          i++;
        }
        return ret;
      };

  private StringCollectionToStringBasedCollection stringCollectionToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToStringBased()
            .convert(fieldCtx, type, e)));
        return dst;
      };

  private StringToBinary stringToBinary = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBinary");
  };

  private StringToBinaryArray stringToBinaryArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBinaryArray");
  };

  private StringToBinaryCollection stringToBinaryCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getStringToBinary().convert(fieldCtx, src));
    return dst;
  };

  private StringToBoolean stringToBoolean = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBoolean");
  };

  private StringToBooleanArray stringToBooleanArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBooleanArray");
  };

  private StringToBooleanCollection stringToBooleanCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getStringToBoolean().convert(fieldCtx, src));
    return dst;
  };

  private StringToBooleanPrim stringToBooleanPrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBooleanPrim");
  };

  private StringToBooleanPrimArray stringToBooleanPrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBooleanPrimArray");
  };

  private StringToByte stringToByte = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToByte");
  };

  private StringToByteArray stringToByteArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToByteArray");
  };

  private StringToByteCollection stringToByteCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getStringToByte().convert(fieldCtx, src));
    return dst;
  };

  private StringToBytePrim stringToBytePrim = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBytePrim");
  };

  private StringToBytePrimArray stringToBytePrimArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToBytePrimArray");
  };

  private StringToCharacter stringToCharacter = (fieldCtx, src) -> {
    if ((src == null) || (src.length() < 1)) {
      return null;
    }
    if (src.length() > 1) {
      throw new IllegalStateException("Size of data too long: " + src);
    }
    return src.charAt(0);
  };

  private StringToCharacterArray stringToCharacterArray = (fieldCtx, src) -> {
    if (src == null) {
      return null;
    }
    return toObjectArray(src.toCharArray());
  };

  private StringToCharacterCollection stringToCharacterCollection = (fieldCtx, src, dst) -> {
    dst.addAll(Arrays.asList(toObjectArray(src.toCharArray())));
    return dst;
  };

  private StringToCharacterPrim stringToCharacterPrim = (fieldCtx, src) -> {
    final Character ret =
        fieldCtx.getConfiguration().getToObject().getStringToCharacter().convert(fieldCtx, src);
    if (ret == null) {
      return fieldCtx.getConfiguration().getValueCharacterPrimNull();
    }
    return ret;
  };

  private StringToCharacterPrimArray stringToCharacterPrimArray = (fieldCtx, src) -> {
    if (src == null) {
      return null;
    }
    return src.toCharArray();
  };

  private StringToDateBased stringToDateBased = (fieldCtx, type, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToDateBased");
  };

  private StringToDateBasedArray stringToDateBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getStringToDateBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private StringToDateBasedCollection stringToDateBasedCollection = (fieldCtx, type, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getStringToDateBased().convert(fieldCtx, type,
        src));
    return dst;
  };

  private StringToDoublePrim stringToDoublePrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().doubleValue();
    }
    return Double.parseDouble(src);
  };

  private StringToDoublePrimArray stringToDoublePrimArray = (fieldCtx, src) -> new double[] {
      fieldCtx.getConfiguration().getToObject().getStringToDoublePrim().convert(fieldCtx, src)};

  private StringToEnum stringToEnum = (fieldCtx, enumType,
      src) -> (Enum<?>) fieldCtx.getConfiguration().getIndexedEnum(enumType).stringToEnum(src);

  private StringToEnumArray stringToEnumArray = (fieldCtx, enumType, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToEnumArray");
  };

  private StringToEnumCollection stringToEnumCollection = (fieldCtx, enumType, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getStringToEnum().convert(fieldCtx, enumType,
        src));
    return dst;
  };

  private StringToFloatPrim stringToFloatPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().floatValue();
    }
    return Float.parseFloat(src);
  };

  private StringToFloatPrimArray stringToFloatPrimArray = (fieldCtx, src) -> new float[] {
      fieldCtx.getConfiguration().getToObject().getStringToFloatPrim().convert(fieldCtx, src)};

  private StringToIntegerPrim stringToIntegerPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().intValue();
    }
    return Integer.parseInt(src);
  };

  private StringToIntegerPrimArray stringToIntegerPrimArray = (fieldCtx, src) -> new int[] {
      fieldCtx.getConfiguration().getToObject().getStringToIntegerPrim().convert(fieldCtx, src)};

  private StringToLongPrim stringToLongPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().longValue();
    }
    return Long.parseLong(src);
  };

  private StringToLongPrimArray stringToLongPrimArray = (fieldCtx, src) -> new long[] {
      fieldCtx.getConfiguration().getToObject().getStringToLongPrim().convert(fieldCtx, src)};

  private StringToNumberBased stringToNumberBased = (fieldCtx, type, src) -> fieldCtx
      .getConfiguration().getStringBased().getToObject().convert(type, src);

  private StringToNumberBasedArray stringToNumberBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getStringToNumberBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private StringToNumberBasedCollection stringToNumberBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getStringToNumberBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  private StringToObjectId stringToObjectId = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToObjectId");
  };

  private StringToObjectIdArray stringToObjectIdArray = (fieldCtx, src) -> {
    throw new UnsupportedOperationException("Not (yet) implemented: StringToObjectIdArray");
  };

  private StringToObjectIdCollection stringToObjectIdCollection = (fieldCtx, src, dst) -> {
    dst.add(fieldCtx.getConfiguration().getToObject().getStringToObjectId().convert(fieldCtx, src));
    return dst;
  };

  private StringToShortPrim stringToShortPrim = (fieldCtx, src) -> {
    if (src == null) {
      return fieldCtx.getConfiguration().getValueNumberPrimNull().shortValue();
    }
    return Short.parseShort(src);
  };

  private StringToShortPrimArray stringToShortPrimArray = (fieldCtx, src) -> new short[] {
      fieldCtx.getConfiguration().getToObject().getStringToShortPrim().convert(fieldCtx, src)};

  private StringToStringBased stringToStringBased = (fieldCtx, type, src) -> fieldCtx
      .getConfiguration().getStringBased().getToObject().convert(type, src);

  private StringToStringBasedArray stringToStringBasedArray = (fieldCtx, type, src) -> {
    final Object ret = Array.newInstance(type, 1);
    final Object value = fieldCtx.getConfiguration().getToObject().getStringToStringBased()
        .convert(fieldCtx, type, src);
    setArrayComponent(type, ret, 0, value);
    return (Object[]) ret;
  };

  private StringToStringBasedCollection stringToStringBasedCollection =
      (fieldCtx, type, src, dst) -> {
        dst.add(fieldCtx.getConfiguration().getToObject().getStringToStringBased().convert(fieldCtx,
            type, src));
        return dst;
      };

  public BinaryCollectionToBinary getBinaryCollectionToBinary() {
    return this.binaryCollectionToBinary;
  }

  public BinaryCollectionToBinaryArray getBinaryCollectionToBinaryArray() {
    return this.binaryCollectionToBinaryArray;
  }

  public BinaryCollectionToBinaryCollection getBinaryCollectionToBinaryCollection() {
    return this.binaryCollectionToBinaryCollection;
  }

  public BinaryCollectionToBoolean getBinaryCollectionToBoolean() {
    return this.binaryCollectionToBoolean;
  }

  public BinaryCollectionToBooleanArray getBinaryCollectionToBooleanArray() {
    return this.binaryCollectionToBooleanArray;
  }

  public BinaryCollectionToBooleanCollection getBinaryCollectionToBooleanCollection() {
    return this.binaryCollectionToBooleanCollection;
  }

  public BinaryCollectionToBooleanPrim getBinaryCollectionToBooleanPrim() {
    return this.binaryCollectionToBooleanPrim;
  }

  public BinaryCollectionToBooleanPrimArray getBinaryCollectionToBooleanPrimArray() {
    return this.binaryCollectionToBooleanPrimArray;
  }

  public BinaryCollectionToByte getBinaryCollectionToByte() {
    return this.binaryCollectionToByte;
  }

  public BinaryCollectionToByteArray getBinaryCollectionToByteArray() {
    return this.binaryCollectionToByteArray;
  }

  public BinaryCollectionToByteCollection getBinaryCollectionToByteCollection() {
    return this.binaryCollectionToByteCollection;
  }

  public BinaryCollectionToBytePrim getBinaryCollectionToBytePrim() {
    return this.binaryCollectionToBytePrim;
  }

  public BinaryCollectionToBytePrimArray getBinaryCollectionToBytePrimArray() {
    return this.binaryCollectionToBytePrimArray;
  }

  public BinaryCollectionToCharacter getBinaryCollectionToCharacter() {
    return this.binaryCollectionToCharacter;
  }

  public BinaryCollectionToCharacterArray getBinaryCollectionToCharacterArray() {
    return this.binaryCollectionToCharacterArray;
  }

  public BinaryCollectionToCharacterCollection getBinaryCollectionToCharacterCollection() {
    return this.binaryCollectionToCharacterCollection;
  }

  public BinaryCollectionToCharacterPrim getBinaryCollectionToCharacterPrim() {
    return this.binaryCollectionToCharacterPrim;
  }

  public BinaryCollectionToCharacterPrimArray getBinaryCollectionToCharacterPrimArray() {
    return this.binaryCollectionToCharacterPrimArray;
  }

  public BinaryCollectionToDateBased getBinaryCollectionToDateBased() {
    return this.binaryCollectionToDateBased;
  }

  public BinaryCollectionToDateBasedArray getBinaryCollectionToDateBasedArray() {
    return this.binaryCollectionToDateBasedArray;
  }

  public BinaryCollectionToDateBasedCollection getBinaryCollectionToDateBasedCollection() {
    return this.binaryCollectionToDateBasedCollection;
  }

  public BinaryCollectionToDoublePrim getBinaryCollectionToDoublePrim() {
    return this.binaryCollectionToDoublePrim;
  }

  public BinaryCollectionToDoublePrimArray getBinaryCollectionToDoublePrimArray() {
    return this.binaryCollectionToDoublePrimArray;
  }

  public BinaryCollectionToEnum getBinaryCollectionToEnum() {
    return this.binaryCollectionToEnum;
  }

  public BinaryCollectionToEnumArray getBinaryCollectionToEnumArray() {
    return this.binaryCollectionToEnumArray;
  }

  public BinaryCollectionToEnumCollection getBinaryCollectionToEnumCollection() {
    return this.binaryCollectionToEnumCollection;
  }

  public BinaryCollectionToFloatPrim getBinaryCollectionToFloatPrim() {
    return this.binaryCollectionToFloatPrim;
  }

  public BinaryCollectionToFloatPrimArray getBinaryCollectionToFloatPrimArray() {
    return this.binaryCollectionToFloatPrimArray;
  }

  public BinaryCollectionToIntegerPrim getBinaryCollectionToIntegerPrim() {
    return this.binaryCollectionToIntegerPrim;
  }

  public BinaryCollectionToIntegerPrimArray getBinaryCollectionToIntegerPrimArray() {
    return this.binaryCollectionToIntegerPrimArray;
  }

  public BinaryCollectionToLongPrim getBinaryCollectionToLongPrim() {
    return this.binaryCollectionToLongPrim;
  }

  public BinaryCollectionToLongPrimArray getBinaryCollectionToLongPrimArray() {
    return this.binaryCollectionToLongPrimArray;
  }

  public BinaryCollectionToNumberBased getBinaryCollectionToNumberBased() {
    return this.binaryCollectionToNumberBased;
  }

  public BinaryCollectionToNumberBasedArray getBinaryCollectionToNumberBasedArray() {
    return this.binaryCollectionToNumberBasedArray;
  }

  public BinaryCollectionToNumberBasedCollection getBinaryCollectionToNumberBasedCollection() {
    return this.binaryCollectionToNumberBasedCollection;
  }

  public BinaryCollectionToObjectId getBinaryCollectionToObjectId() {
    return this.binaryCollectionToObjectId;
  }

  public BinaryCollectionToObjectIdArray getBinaryCollectionToObjectIdArray() {
    return this.binaryCollectionToObjectIdArray;
  }

  public BinaryCollectionToObjectIdCollection getBinaryCollectionToObjectIdCollection() {
    return this.binaryCollectionToObjectIdCollection;
  }

  public BinaryCollectionToShortPrim getBinaryCollectionToShortPrim() {
    return this.binaryCollectionToShortPrim;
  }

  public BinaryCollectionToShortPrimArray getBinaryCollectionToShortPrimArray() {
    return this.binaryCollectionToShortPrimArray;
  }

  public BinaryCollectionToStringBased getBinaryCollectionToStringBased() {
    return this.binaryCollectionToStringBased;
  }

  public BinaryCollectionToStringBasedArray getBinaryCollectionToStringBasedArray() {
    return this.binaryCollectionToStringBasedArray;
  }

  public BinaryCollectionToStringBasedCollection getBinaryCollectionToStringBasedCollection() {
    return this.binaryCollectionToStringBasedCollection;
  }

  public BinaryToBinary getBinaryToBinary() {
    return this.binaryToBinary;
  }

  public BinaryToBinaryArray getBinaryToBinaryArray() {
    return this.binaryToBinaryArray;
  }

  public BinaryToBinaryCollection getBinaryToBinaryCollection() {
    return this.binaryToBinaryCollection;
  }

  public BinaryToBoolean getBinaryToBoolean() {
    return this.binaryToBoolean;
  }

  public BinaryToBooleanArray getBinaryToBooleanArray() {
    return this.binaryToBooleanArray;
  }

  public BinaryToBooleanCollection getBinaryToBooleanCollection() {
    return this.binaryToBooleanCollection;
  }

  public BinaryToBooleanPrim getBinaryToBooleanPrim() {
    return this.binaryToBooleanPrim;
  }

  public BinaryToBooleanPrimArray getBinaryToBooleanPrimArray() {
    return this.binaryToBooleanPrimArray;
  }

  public BinaryToByte getBinaryToByte() {
    return this.binaryToByte;
  }

  public BinaryToByteArray getBinaryToByteArray() {
    return this.binaryToByteArray;
  }

  public BinaryToByteCollection getBinaryToByteCollection() {
    return this.binaryToByteCollection;
  }

  public BinaryToBytePrim getBinaryToBytePrim() {
    return this.binaryToBytePrim;
  }

  public BinaryToBytePrimArray getBinaryToBytePrimArray() {
    return this.binaryToBytePrimArray;
  }

  public BinaryToCharacter getBinaryToCharacter() {
    return this.binaryToCharacter;
  }

  public BinaryToCharacterArray getBinaryToCharacterArray() {
    return this.binaryToCharacterArray;
  }

  public BinaryToCharacterCollection getBinaryToCharacterCollection() {
    return this.binaryToCharacterCollection;
  }

  public BinaryToCharacterPrim getBinaryToCharacterPrim() {
    return this.binaryToCharacterPrim;
  }

  public BinaryToCharacterPrimArray getBinaryToCharacterPrimArray() {
    return this.binaryToCharacterPrimArray;
  }

  public BinaryToDateBased getBinaryToDateBased() {
    return this.binaryToDateBased;
  }

  public BinaryToDateBasedArray getBinaryToDateBasedArray() {
    return this.binaryToDateBasedArray;
  }

  public BinaryToDateBasedCollection getBinaryToDateBasedCollection() {
    return this.binaryToDateBasedCollection;
  }

  public BinaryToDoublePrim getBinaryToDoublePrim() {
    return this.binaryToDoublePrim;
  }

  public BinaryToDoublePrimArray getBinaryToDoublePrimArray() {
    return this.binaryToDoublePrimArray;
  }

  public BinaryToEnum getBinaryToEnum() {
    return this.binaryToEnum;
  }

  public BinaryToEnumArray getBinaryToEnumArray() {
    return this.binaryToEnumArray;
  }

  public BinaryToEnumCollection getBinaryToEnumCollection() {
    return this.binaryToEnumCollection;
  }

  public BinaryToFloatPrim getBinaryToFloatPrim() {
    return this.binaryToFloatPrim;
  }

  public BinaryToFloatPrimArray getBinaryToFloatPrimArray() {
    return this.binaryToFloatPrimArray;
  }

  public BinaryToIntegerPrim getBinaryToIntegerPrim() {
    return this.binaryToIntegerPrim;
  }

  public BinaryToIntegerPrimArray getBinaryToIntegerPrimArray() {
    return this.binaryToIntegerPrimArray;
  }

  public BinaryToLongPrim getBinaryToLongPrim() {
    return this.binaryToLongPrim;
  }

  public BinaryToLongPrimArray getBinaryToLongPrimArray() {
    return this.binaryToLongPrimArray;
  }

  public BinaryToNumberBased getBinaryToNumberBased() {
    return this.binaryToNumberBased;
  }

  public BinaryToNumberBasedArray getBinaryToNumberBasedArray() {
    return this.binaryToNumberBasedArray;
  }

  public BinaryToNumberBasedCollection getBinaryToNumberBasedCollection() {
    return this.binaryToNumberBasedCollection;
  }

  public BinaryToObjectId getBinaryToObjectId() {
    return this.binaryToObjectId;
  }

  public BinaryToObjectIdArray getBinaryToObjectIdArray() {
    return this.binaryToObjectIdArray;
  }

  public BinaryToObjectIdCollection getBinaryToObjectIdCollection() {
    return this.binaryToObjectIdCollection;
  }

  public BinaryToShortPrim getBinaryToShortPrim() {
    return this.binaryToShortPrim;
  }

  public BinaryToShortPrimArray getBinaryToShortPrimArray() {
    return this.binaryToShortPrimArray;
  }

  public BinaryToStringBased getBinaryToStringBased() {
    return this.binaryToStringBased;
  }

  public BinaryToStringBasedArray getBinaryToStringBasedArray() {
    return this.binaryToStringBasedArray;
  }

  public BinaryToStringBasedCollection getBinaryToStringBasedCollection() {
    return this.binaryToStringBasedCollection;
  }

  public BooleanCollectionToBinary getBooleanCollectionToBinary() {
    return this.booleanCollectionToBinary;
  }

  public BooleanCollectionToBinaryArray getBooleanCollectionToBinaryArray() {
    return this.booleanCollectionToBinaryArray;
  }

  public BooleanCollectionToBinaryCollection getBooleanCollectionToBinaryCollection() {
    return this.booleanCollectionToBinaryCollection;
  }

  public BooleanCollectionToBoolean getBooleanCollectionToBoolean() {
    return this.booleanCollectionToBoolean;
  }

  public BooleanCollectionToBooleanArray getBooleanCollectionToBooleanArray() {
    return this.booleanCollectionToBooleanArray;
  }

  public BooleanCollectionToBooleanCollection getBooleanCollectionToBooleanCollection() {
    return this.booleanCollectionToBooleanCollection;
  }

  public BooleanCollectionToBooleanPrim getBooleanCollectionToBooleanPrim() {
    return this.booleanCollectionToBooleanPrim;
  }

  public BooleanCollectionToBooleanPrimArray getBooleanCollectionToBooleanPrimArray() {
    return this.booleanCollectionToBooleanPrimArray;
  }

  public BooleanCollectionToByte getBooleanCollectionToByte() {
    return this.booleanCollectionToByte;
  }

  public BooleanCollectionToByteArray getBooleanCollectionToByteArray() {
    return this.booleanCollectionToByteArray;
  }

  public BooleanCollectionToByteCollection getBooleanCollectionToByteCollection() {
    return this.booleanCollectionToByteCollection;
  }

  public BooleanCollectionToBytePrim getBooleanCollectionToBytePrim() {
    return this.booleanCollectionToBytePrim;
  }

  public BooleanCollectionToBytePrimArray getBooleanCollectionToBytePrimArray() {
    return this.booleanCollectionToBytePrimArray;
  }

  public BooleanCollectionToCharacter getBooleanCollectionToCharacter() {
    return this.booleanCollectionToCharacter;
  }

  public BooleanCollectionToCharacterArray getBooleanCollectionToCharacterArray() {
    return this.booleanCollectionToCharacterArray;
  }

  public BooleanCollectionToCharacterCollection getBooleanCollectionToCharacterCollection() {
    return this.booleanCollectionToCharacterCollection;
  }

  public BooleanCollectionToCharacterPrim getBooleanCollectionToCharacterPrim() {
    return this.booleanCollectionToCharacterPrim;
  }

  public BooleanCollectionToCharacterPrimArray getBooleanCollectionToCharacterPrimArray() {
    return this.booleanCollectionToCharacterPrimArray;
  }

  public BooleanCollectionToDateBased getBooleanCollectionToDateBased() {
    return this.booleanCollectionToDateBased;
  }

  public BooleanCollectionToDateBasedArray getBooleanCollectionToDateBasedArray() {
    return this.booleanCollectionToDateBasedArray;
  }

  public BooleanCollectionToDateBasedCollection getBooleanCollectionToDateBasedCollection() {
    return this.booleanCollectionToDateBasedCollection;
  }

  public BooleanCollectionToDoublePrim getBooleanCollectionToDoublePrim() {
    return this.booleanCollectionToDoublePrim;
  }

  public BooleanCollectionToDoublePrimArray getBooleanCollectionToDoublePrimArray() {
    return this.booleanCollectionToDoublePrimArray;
  }

  public BooleanCollectionToEnum getBooleanCollectionToEnum() {
    return this.booleanCollectionToEnum;
  }

  public BooleanCollectionToEnumArray getBooleanCollectionToEnumArray() {
    return this.booleanCollectionToEnumArray;
  }

  public BooleanCollectionToEnumCollection getBooleanCollectionToEnumCollection() {
    return this.booleanCollectionToEnumCollection;
  }

  public BooleanCollectionToFloatPrim getBooleanCollectionToFloatPrim() {
    return this.booleanCollectionToFloatPrim;
  }

  public BooleanCollectionToFloatPrimArray getBooleanCollectionToFloatPrimArray() {
    return this.booleanCollectionToFloatPrimArray;
  }

  public BooleanCollectionToIntegerPrim getBooleanCollectionToIntegerPrim() {
    return this.booleanCollectionToIntegerPrim;
  }

  public BooleanCollectionToIntegerPrimArray getBooleanCollectionToIntegerPrimArray() {
    return this.booleanCollectionToIntegerPrimArray;
  }

  public BooleanCollectionToLongPrim getBooleanCollectionToLongPrim() {
    return this.booleanCollectionToLongPrim;
  }

  public BooleanCollectionToLongPrimArray getBooleanCollectionToLongPrimArray() {
    return this.booleanCollectionToLongPrimArray;
  }

  public BooleanCollectionToNumberBased getBooleanCollectionToNumberBased() {
    return this.booleanCollectionToNumberBased;
  }

  public BooleanCollectionToNumberBasedArray getBooleanCollectionToNumberBasedArray() {
    return this.booleanCollectionToNumberBasedArray;
  }

  public BooleanCollectionToNumberBasedCollection getBooleanCollectionToNumberBasedCollection() {
    return this.booleanCollectionToNumberBasedCollection;
  }

  public BooleanCollectionToObjectId getBooleanCollectionToObjectId() {
    return this.booleanCollectionToObjectId;
  }

  public BooleanCollectionToObjectIdArray getBooleanCollectionToObjectIdArray() {
    return this.booleanCollectionToObjectIdArray;
  }

  public BooleanCollectionToObjectIdCollection getBooleanCollectionToObjectIdCollection() {
    return this.booleanCollectionToObjectIdCollection;
  }

  public BooleanCollectionToShortPrim getBooleanCollectionToShortPrim() {
    return this.booleanCollectionToShortPrim;
  }

  public BooleanCollectionToShortPrimArray getBooleanCollectionToShortPrimArray() {
    return this.booleanCollectionToShortPrimArray;
  }

  public BooleanCollectionToStringBased getBooleanCollectionToStringBased() {
    return this.booleanCollectionToStringBased;
  }

  public BooleanCollectionToStringBasedArray getBooleanCollectionToStringBasedArray() {
    return this.booleanCollectionToStringBasedArray;
  }

  public BooleanCollectionToStringBasedCollection getBooleanCollectionToStringBasedCollection() {
    return this.booleanCollectionToStringBasedCollection;
  }

  public BooleanToBinary getBooleanToBinary() {
    return this.booleanToBinary;
  }

  public BooleanToBinaryArray getBooleanToBinaryArray() {
    return this.booleanToBinaryArray;
  }

  public BooleanToBinaryCollection getBooleanToBinaryCollection() {
    return this.booleanToBinaryCollection;
  }

  public BooleanToBoolean getBooleanToBoolean() {
    return this.booleanToBoolean;
  }

  public BooleanToBooleanArray getBooleanToBooleanArray() {
    return this.booleanToBooleanArray;
  }

  public BooleanToBooleanCollection getBooleanToBooleanCollection() {
    return this.booleanToBooleanCollection;
  }

  public BooleanToBooleanPrim getBooleanToBooleanPrim() {
    return this.booleanToBooleanPrim;
  }

  public BooleanToBooleanPrimArray getBooleanToBooleanPrimArray() {
    return this.booleanToBooleanPrimArray;
  }

  public BooleanToByte getBooleanToByte() {
    return this.booleanToByte;
  }

  public BooleanToByteArray getBooleanToByteArray() {
    return this.booleanToByteArray;
  }

  public BooleanToByteCollection getBooleanToByteCollection() {
    return this.booleanToByteCollection;
  }

  public BooleanToBytePrim getBooleanToBytePrim() {
    return this.booleanToBytePrim;
  }

  public BooleanToBytePrimArray getBooleanToBytePrimArray() {
    return this.booleanToBytePrimArray;
  }

  public BooleanToCharacter getBooleanToCharacter() {
    return this.booleanToCharacter;
  }

  public BooleanToCharacterArray getBooleanToCharacterArray() {
    return this.booleanToCharacterArray;
  }

  public BooleanToCharacterCollection getBooleanToCharacterCollection() {
    return this.booleanToCharacterCollection;
  }

  public BooleanToCharacterPrim getBooleanToCharacterPrim() {
    return this.booleanToCharacterPrim;
  }

  public BooleanToCharacterPrimArray getBooleanToCharacterPrimArray() {
    return this.booleanToCharacterPrimArray;
  }

  public BooleanToDateBased getBooleanToDateBased() {
    return this.booleanToDateBased;
  }

  public BooleanToDateBasedArray getBooleanToDateBasedArray() {
    return this.booleanToDateBasedArray;
  }

  public BooleanToDateBasedCollection getBooleanToDateBasedCollection() {
    return this.booleanToDateBasedCollection;
  }

  public BooleanToDoublePrim getBooleanToDoublePrim() {
    return this.booleanToDoublePrim;
  }

  public BooleanToDoublePrimArray getBooleanToDoublePrimArray() {
    return this.booleanToDoublePrimArray;
  }

  public BooleanToEnum getBooleanToEnum() {
    return this.booleanToEnum;
  }

  public BooleanToEnumArray getBooleanToEnumArray() {
    return this.booleanToEnumArray;
  }

  public BooleanToEnumCollection getBooleanToEnumCollection() {
    return this.booleanToEnumCollection;
  }

  public BooleanToFloatPrim getBooleanToFloatPrim() {
    return this.booleanToFloatPrim;
  }

  public BooleanToFloatPrimArray getBooleanToFloatPrimArray() {
    return this.booleanToFloatPrimArray;
  }

  public BooleanToIntegerPrim getBooleanToIntegerPrim() {
    return this.booleanToIntegerPrim;
  }

  public BooleanToIntegerPrimArray getBooleanToIntegerPrimArray() {
    return this.booleanToIntegerPrimArray;
  }

  public BooleanToLongPrim getBooleanToLongPrim() {
    return this.booleanToLongPrim;
  }

  public BooleanToLongPrimArray getBooleanToLongPrimArray() {
    return this.booleanToLongPrimArray;
  }

  public BooleanToNumberBased getBooleanToNumberBased() {
    return this.booleanToNumberBased;
  }

  public BooleanToNumberBasedArray getBooleanToNumberBasedArray() {
    return this.booleanToNumberBasedArray;
  }

  public BooleanToNumberBasedCollection getBooleanToNumberBasedCollection() {
    return this.booleanToNumberBasedCollection;
  }

  public BooleanToObjectId getBooleanToObjectId() {
    return this.booleanToObjectId;
  }

  public BooleanToObjectIdArray getBooleanToObjectIdArray() {
    return this.booleanToObjectIdArray;
  }

  public BooleanToObjectIdCollection getBooleanToObjectIdCollection() {
    return this.booleanToObjectIdCollection;
  }

  public BooleanToShortPrim getBooleanToShortPrim() {
    return this.booleanToShortPrim;
  }

  public BooleanToShortPrimArray getBooleanToShortPrimArray() {
    return this.booleanToShortPrimArray;
  }

  public BooleanToStringBased getBooleanToStringBased() {
    return this.booleanToStringBased;
  }

  public BooleanToStringBasedArray getBooleanToStringBasedArray() {
    return this.booleanToStringBasedArray;
  }

  public BooleanToStringBasedCollection getBooleanToStringBasedCollection() {
    return this.booleanToStringBasedCollection;
  }

  public DateCollectionToBinary getDateCollectionToBinary() {
    return this.dateCollectionToBinary;
  }

  public DateCollectionToBinaryArray getDateCollectionToBinaryArray() {
    return this.dateCollectionToBinaryArray;
  }

  public DateCollectionToBinaryCollection getDateCollectionToBinaryCollection() {
    return this.dateCollectionToBinaryCollection;
  }

  public DateCollectionToBoolean getDateCollectionToBoolean() {
    return this.dateCollectionToBoolean;
  }

  public DateCollectionToBooleanArray getDateCollectionToBooleanArray() {
    return this.dateCollectionToBooleanArray;
  }

  public DateCollectionToBooleanCollection getDateCollectionToBooleanCollection() {
    return this.dateCollectionToBooleanCollection;
  }

  public DateCollectionToBooleanPrim getDateCollectionToBooleanPrim() {
    return this.dateCollectionToBooleanPrim;
  }

  public DateCollectionToBooleanPrimArray getDateCollectionToBooleanPrimArray() {
    return this.dateCollectionToBooleanPrimArray;
  }

  public DateCollectionToByte getDateCollectionToByte() {
    return this.dateCollectionToByte;
  }

  public DateCollectionToByteArray getDateCollectionToByteArray() {
    return this.dateCollectionToByteArray;
  }

  public DateCollectionToByteCollection getDateCollectionToByteCollection() {
    return this.dateCollectionToByteCollection;
  }

  public DateCollectionToBytePrim getDateCollectionToBytePrim() {
    return this.dateCollectionToBytePrim;
  }

  public DateCollectionToBytePrimArray getDateCollectionToBytePrimArray() {
    return this.dateCollectionToBytePrimArray;
  }

  public DateCollectionToCharacter getDateCollectionToCharacter() {
    return this.dateCollectionToCharacter;
  }

  public DateCollectionToCharacterArray getDateCollectionToCharacterArray() {
    return this.dateCollectionToCharacterArray;
  }

  public DateCollectionToCharacterCollection getDateCollectionToCharacterCollection() {
    return this.dateCollectionToCharacterCollection;
  }

  public DateCollectionToCharacterPrim getDateCollectionToCharacterPrim() {
    return this.dateCollectionToCharacterPrim;
  }

  public DateCollectionToCharacterPrimArray getDateCollectionToCharacterPrimArray() {
    return this.dateCollectionToCharacterPrimArray;
  }

  public DateCollectionToDateBased getDateCollectionToDateBased() {
    return this.dateCollectionToDateBased;
  }

  public DateCollectionToDateBasedArray getDateCollectionToDateBasedArray() {
    return this.dateCollectionToDateBasedArray;
  }

  public DateCollectionToDateBasedCollection getDateCollectionToDateBasedCollection() {
    return this.dateCollectionToDateBasedCollection;
  }

  public DateCollectionToDoublePrim getDateCollectionToDoublePrim() {
    return this.dateCollectionToDoublePrim;
  }

  public DateCollectionToDoublePrimArray getDateCollectionToDoublePrimArray() {
    return this.dateCollectionToDoublePrimArray;
  }

  public DateCollectionToEnum getDateCollectionToEnum() {
    return this.dateCollectionToEnum;
  }

  public DateCollectionToEnumArray getDateCollectionToEnumArray() {
    return this.dateCollectionToEnumArray;
  }

  public DateCollectionToEnumCollection getDateCollectionToEnumCollection() {
    return this.dateCollectionToEnumCollection;
  }

  public DateCollectionToFloatPrim getDateCollectionToFloatPrim() {
    return this.dateCollectionToFloatPrim;
  }

  public DateCollectionToFloatPrimArray getDateCollectionToFloatPrimArray() {
    return this.dateCollectionToFloatPrimArray;
  }

  public DateCollectionToIntegerPrim getDateCollectionToIntegerPrim() {
    return this.dateCollectionToIntegerPrim;
  }

  public DateCollectionToIntegerPrimArray getDateCollectionToIntegerPrimArray() {
    return this.dateCollectionToIntegerPrimArray;
  }

  public DateCollectionToLongPrim getDateCollectionToLongPrim() {
    return this.dateCollectionToLongPrim;
  }

  public DateCollectionToLongPrimArray getDateCollectionToLongPrimArray() {
    return this.dateCollectionToLongPrimArray;
  }

  public DateCollectionToNumberBased getDateCollectionToNumberBased() {
    return this.dateCollectionToNumberBased;
  }

  public DateCollectionToNumberBasedArray getDateCollectionToNumberBasedArray() {
    return this.dateCollectionToNumberBasedArray;
  }

  public DateCollectionToNumberBasedCollection getDateCollectionToNumberBasedCollection() {
    return this.dateCollectionToNumberBasedCollection;
  }

  public DateCollectionToObjectId getDateCollectionToObjectId() {
    return this.dateCollectionToObjectId;
  }

  public DateCollectionToObjectIdArray getDateCollectionToObjectIdArray() {
    return this.dateCollectionToObjectIdArray;
  }

  public DateCollectionToObjectIdCollection getDateCollectionToObjectIdCollection() {
    return this.dateCollectionToObjectIdCollection;
  }

  public DateCollectionToShortPrim getDateCollectionToShortPrim() {
    return this.dateCollectionToShortPrim;
  }

  public DateCollectionToShortPrimArray getDateCollectionToShortPrimArray() {
    return this.dateCollectionToShortPrimArray;
  }

  public DateCollectionToStringBased getDateCollectionToStringBased() {
    return this.dateCollectionToStringBased;
  }

  public DateCollectionToStringBasedArray getDateCollectionToStringBasedArray() {
    return this.dateCollectionToStringBasedArray;
  }

  public DateCollectionToStringBasedCollection getDateCollectionToStringBasedCollection() {
    return this.dateCollectionToStringBasedCollection;
  }

  public DateToBinary getDateToBinary() {
    return this.dateToBinary;
  }

  public DateToBinaryArray getDateToBinaryArray() {
    return this.dateToBinaryArray;
  }

  public DateToBinaryCollection getDateToBinaryCollection() {
    return this.dateToBinaryCollection;
  }

  public DateToBoolean getDateToBoolean() {
    return this.dateToBoolean;
  }

  public DateToBooleanArray getDateToBooleanArray() {
    return this.dateToBooleanArray;
  }

  public DateToBooleanCollection getDateToBooleanCollection() {
    return this.dateToBooleanCollection;
  }

  public DateToBooleanPrim getDateToBooleanPrim() {
    return this.dateToBooleanPrim;
  }

  public DateToBooleanPrimArray getDateToBooleanPrimArray() {
    return this.dateToBooleanPrimArray;
  }

  public DateToByte getDateToByte() {
    return this.dateToByte;
  }

  public DateToByteArray getDateToByteArray() {
    return this.dateToByteArray;
  }

  public DateToByteCollection getDateToByteCollection() {
    return this.dateToByteCollection;
  }

  public DateToBytePrim getDateToBytePrim() {
    return this.dateToBytePrim;
  }

  public DateToBytePrimArray getDateToBytePrimArray() {
    return this.dateToBytePrimArray;
  }

  public DateToCharacter getDateToCharacter() {
    return this.dateToCharacter;
  }

  public DateToCharacterArray getDateToCharacterArray() {
    return this.dateToCharacterArray;
  }

  public DateToCharacterCollection getDateToCharacterCollection() {
    return this.dateToCharacterCollection;
  }

  public DateToCharacterPrim getDateToCharacterPrim() {
    return this.dateToCharacterPrim;
  }

  public DateToCharacterPrimArray getDateToCharacterPrimArray() {
    return this.dateToCharacterPrimArray;
  }

  public DateToDateBased getDateToDateBased() {
    return this.dateToDateBased;
  }

  public DateToDateBasedArray getDateToDateBasedArray() {
    return this.dateToDateBasedArray;
  }

  public DateToDateBasedCollection getDateToDateBasedCollection() {
    return this.dateToDateBasedCollection;
  }

  public DateToDoublePrim getDateToDoublePrim() {
    return this.dateToDoublePrim;
  }

  public DateToDoublePrimArray getDateToDoublePrimArray() {
    return this.dateToDoublePrimArray;
  }

  public DateToEnum getDateToEnum() {
    return this.dateToEnum;
  }

  public DateToEnumArray getDateToEnumArray() {
    return this.dateToEnumArray;
  }

  public DateToEnumCollection getDateToEnumCollection() {
    return this.dateToEnumCollection;
  }

  public DateToFloatPrim getDateToFloatPrim() {
    return this.dateToFloatPrim;
  }

  public DateToFloatPrimArray getDateToFloatPrimArray() {
    return this.dateToFloatPrimArray;
  }

  public DateToIntegerPrim getDateToIntegerPrim() {
    return this.dateToIntegerPrim;
  }

  public DateToIntegerPrimArray getDateToIntegerPrimArray() {
    return this.dateToIntegerPrimArray;
  }

  public DateToLongPrim getDateToLongPrim() {
    return this.dateToLongPrim;
  }

  public DateToLongPrimArray getDateToLongPrimArray() {
    return this.dateToLongPrimArray;
  }

  public DateToNumberBased getDateToNumberBased() {
    return this.dateToNumberBased;
  }

  public DateToNumberBasedArray getDateToNumberBasedArray() {
    return this.dateToNumberBasedArray;
  }

  public DateToNumberBasedCollection getDateToNumberBasedCollection() {
    return this.dateToNumberBasedCollection;
  }

  public DateToObjectId getDateToObjectId() {
    return this.dateToObjectId;
  }

  public DateToObjectIdArray getDateToObjectIdArray() {
    return this.dateToObjectIdArray;
  }

  public DateToObjectIdCollection getDateToObjectIdCollection() {
    return this.dateToObjectIdCollection;
  }

  public DateToShortPrim getDateToShortPrim() {
    return this.dateToShortPrim;
  }

  public DateToShortPrimArray getDateToShortPrimArray() {
    return this.dateToShortPrimArray;
  }

  public DateToStringBased getDateToStringBased() {
    return this.dateToStringBased;
  }

  public DateToStringBasedArray getDateToStringBasedArray() {
    return this.dateToStringBasedArray;
  }

  public DateToStringBasedCollection getDateToStringBasedCollection() {
    return this.dateToStringBasedCollection;
  }

  public NumberCollectionToBinary getNumberCollectionToBinary() {
    return this.numberCollectionToBinary;
  }

  public NumberCollectionToBinaryArray getNumberCollectionToBinaryArray() {
    return this.numberCollectionToBinaryArray;
  }

  public NumberCollectionToBinaryCollection getNumberCollectionToBinaryCollection() {
    return this.numberCollectionToBinaryCollection;
  }

  public NumberCollectionToBoolean getNumberCollectionToBoolean() {
    return this.numberCollectionToBoolean;
  }

  public NumberCollectionToBooleanArray getNumberCollectionToBooleanArray() {
    return this.numberCollectionToBooleanArray;
  }

  public NumberCollectionToBooleanCollection getNumberCollectionToBooleanCollection() {
    return this.numberCollectionToBooleanCollection;
  }

  public NumberCollectionToBooleanPrim getNumberCollectionToBooleanPrim() {
    return this.numberCollectionToBooleanPrim;
  }

  public NumberCollectionToBooleanPrimArray getNumberCollectionToBooleanPrimArray() {
    return this.numberCollectionToBooleanPrimArray;
  }

  public NumberCollectionToByte getNumberCollectionToByte() {
    return this.numberCollectionToByte;
  }

  public NumberCollectionToByteArray getNumberCollectionToByteArray() {
    return this.numberCollectionToByteArray;
  }

  public NumberCollectionToByteCollection getNumberCollectionToByteCollection() {
    return this.numberCollectionToByteCollection;
  }

  public NumberCollectionToBytePrim getNumberCollectionToBytePrim() {
    return this.numberCollectionToBytePrim;
  }

  public NumberCollectionToBytePrimArray getNumberCollectionToBytePrimArray() {
    return this.numberCollectionToBytePrimArray;
  }

  public NumberCollectionToCharacter getNumberCollectionToCharacter() {
    return this.numberCollectionToCharacter;
  }

  public NumberCollectionToCharacterArray getNumberCollectionToCharacterArray() {
    return this.numberCollectionToCharacterArray;
  }

  public NumberCollectionToCharacterCollection getNumberCollectionToCharacterCollection() {
    return this.numberCollectionToCharacterCollection;
  }

  public NumberCollectionToCharacterPrim getNumberCollectionToCharacterPrim() {
    return this.numberCollectionToCharacterPrim;
  }

  public NumberCollectionToCharacterPrimArray getNumberCollectionToCharacterPrimArray() {
    return this.numberCollectionToCharacterPrimArray;
  }

  public NumberCollectionToDateBased getNumberCollectionToDateBased() {
    return this.numberCollectionToDateBased;
  }

  public NumberCollectionToDateBasedArray getNumberCollectionToDateBasedArray() {
    return this.numberCollectionToDateBasedArray;
  }

  public NumberCollectionToDateBasedCollection getNumberCollectionToDateBasedCollection() {
    return this.numberCollectionToDateBasedCollection;
  }

  public NumberCollectionToDoublePrim getNumberCollectionToDoublePrim() {
    return this.numberCollectionToDoublePrim;
  }

  public NumberCollectionToDoublePrimArray getNumberCollectionToDoublePrimArray() {
    return this.numberCollectionToDoublePrimArray;
  }

  public NumberCollectionToEnum getNumberCollectionToEnum() {
    return this.numberCollectionToEnum;
  }

  public NumberCollectionToEnumArray getNumberCollectionToEnumArray() {
    return this.numberCollectionToEnumArray;
  }

  public NumberCollectionToEnumCollection getNumberCollectionToEnumCollection() {
    return this.numberCollectionToEnumCollection;
  }

  public NumberCollectionToFloatPrim getNumberCollectionToFloatPrim() {
    return this.numberCollectionToFloatPrim;
  }

  public NumberCollectionToFloatPrimArray getNumberCollectionToFloatPrimArray() {
    return this.numberCollectionToFloatPrimArray;
  }

  public NumberCollectionToIntegerPrim getNumberCollectionToIntegerPrim() {
    return this.numberCollectionToIntegerPrim;
  }

  public NumberCollectionToIntegerPrimArray getNumberCollectionToIntegerPrimArray() {
    return this.numberCollectionToIntegerPrimArray;
  }

  public NumberCollectionToLongPrim getNumberCollectionToLongPrim() {
    return this.numberCollectionToLongPrim;
  }

  public NumberCollectionToLongPrimArray getNumberCollectionToLongPrimArray() {
    return this.numberCollectionToLongPrimArray;
  }

  public NumberCollectionToNumberBased getNumberCollectionToNumberBased() {
    return this.numberCollectionToNumberBased;
  }

  public NumberCollectionToNumberBasedArray getNumberCollectionToNumberBasedArray() {
    return this.numberCollectionToNumberBasedArray;
  }

  public NumberCollectionToNumberBasedCollection getNumberCollectionToNumberBasedCollection() {
    return this.numberCollectionToNumberBasedCollection;
  }

  public NumberCollectionToObjectId getNumberCollectionToObjectId() {
    return this.numberCollectionToObjectId;
  }

  public NumberCollectionToObjectIdArray getNumberCollectionToObjectIdArray() {
    return this.numberCollectionToObjectIdArray;
  }

  public NumberCollectionToObjectIdCollection getNumberCollectionToObjectIdCollection() {
    return this.numberCollectionToObjectIdCollection;
  }

  public NumberCollectionToShortPrim getNumberCollectionToShortPrim() {
    return this.numberCollectionToShortPrim;
  }

  public NumberCollectionToShortPrimArray getNumberCollectionToShortPrimArray() {
    return this.numberCollectionToShortPrimArray;
  }

  public NumberCollectionToStringBased getNumberCollectionToStringBased() {
    return this.numberCollectionToStringBased;
  }

  public NumberCollectionToStringBasedArray getNumberCollectionToStringBasedArray() {
    return this.numberCollectionToStringBasedArray;
  }

  public NumberCollectionToStringBasedCollection getNumberCollectionToStringBasedCollection() {
    return this.numberCollectionToStringBasedCollection;
  }

  public NumberToBinary getNumberToBinary() {
    return this.numberToBinary;
  }

  public NumberToBinaryArray getNumberToBinaryArray() {
    return this.numberToBinaryArray;
  }

  public NumberToBinaryCollection getNumberToBinaryCollection() {
    return this.numberToBinaryCollection;
  }

  public NumberToBoolean getNumberToBoolean() {
    return this.numberToBoolean;
  }

  public NumberToBooleanArray getNumberToBooleanArray() {
    return this.numberToBooleanArray;
  }

  public NumberToBooleanCollection getNumberToBooleanCollection() {
    return this.numberToBooleanCollection;
  }

  public NumberToBooleanPrim getNumberToBooleanPrim() {
    return this.numberToBooleanPrim;
  }

  public NumberToBooleanPrimArray getNumberToBooleanPrimArray() {
    return this.numberToBooleanPrimArray;
  }

  public NumberToByte getNumberToByte() {
    return this.numberToByte;
  }

  public NumberToByteArray getNumberToByteArray() {
    return this.numberToByteArray;
  }

  public NumberToByteCollection getNumberToByteCollection() {
    return this.numberToByteCollection;
  }

  public NumberToBytePrim getNumberToBytePrim() {
    return this.numberToBytePrim;
  }

  public NumberToBytePrimArray getNumberToBytePrimArray() {
    return this.numberToBytePrimArray;
  }

  public NumberToCharacter getNumberToCharacter() {
    return this.numberToCharacter;
  }

  public NumberToCharacterArray getNumberToCharacterArray() {
    return this.numberToCharacterArray;
  }

  public NumberToCharacterCollection getNumberToCharacterCollection() {
    return this.numberToCharacterCollection;
  }

  public NumberToCharacterPrim getNumberToCharacterPrim() {
    return this.numberToCharacterPrim;
  }

  public NumberToCharacterPrimArray getNumberToCharacterPrimArray() {
    return this.numberToCharacterPrimArray;
  }

  public NumberToDateBased getNumberToDateBased() {
    return this.numberToDateBased;
  }

  public NumberToDateBasedArray getNumberToDateBasedArray() {
    return this.numberToDateBasedArray;
  }

  public NumberToDateBasedCollection getNumberToDateBasedCollection() {
    return this.numberToDateBasedCollection;
  }

  public NumberToDoublePrim getNumberToDoublePrim() {
    return this.numberToDoublePrim;
  }

  public NumberToDoublePrimArray getNumberToDoublePrimArray() {
    return this.numberToDoublePrimArray;
  }

  public NumberToEnum getNumberToEnum() {
    return this.numberToEnum;
  }

  public NumberToEnumArray getNumberToEnumArray() {
    return this.numberToEnumArray;
  }

  public NumberToEnumCollection getNumberToEnumCollection() {
    return this.numberToEnumCollection;
  }

  public NumberToFloatPrim getNumberToFloatPrim() {
    return this.numberToFloatPrim;
  }

  public NumberToFloatPrimArray getNumberToFloatPrimArray() {
    return this.numberToFloatPrimArray;
  }

  public NumberToIntegerPrim getNumberToIntegerPrim() {
    return this.numberToIntegerPrim;
  }

  public NumberToIntegerPrimArray getNumberToIntegerPrimArray() {
    return this.numberToIntegerPrimArray;
  }

  public NumberToLongPrim getNumberToLongPrim() {
    return this.numberToLongPrim;
  }

  public NumberToLongPrimArray getNumberToLongPrimArray() {
    return this.numberToLongPrimArray;
  }

  public NumberToNumberBased getNumberToNumberBased() {
    return this.numberToNumberBased;
  }

  public NumberToNumberBasedArray getNumberToNumberBasedArray() {
    return this.numberToNumberBasedArray;
  }

  public NumberToNumberBasedCollection getNumberToNumberBasedCollection() {
    return this.numberToNumberBasedCollection;
  }

  public NumberToObjectId getNumberToObjectId() {
    return this.numberToObjectId;
  }

  public NumberToObjectIdArray getNumberToObjectIdArray() {
    return this.numberToObjectIdArray;
  }

  public NumberToObjectIdCollection getNumberToObjectIdCollection() {
    return this.numberToObjectIdCollection;
  }

  public NumberToShortPrim getNumberToShortPrim() {
    return this.numberToShortPrim;
  }

  public NumberToShortPrimArray getNumberToShortPrimArray() {
    return this.numberToShortPrimArray;
  }

  public NumberToStringBased getNumberToStringBased() {
    return this.numberToStringBased;
  }

  public NumberToStringBasedArray getNumberToStringBasedArray() {
    return this.numberToStringBasedArray;
  }

  public NumberToStringBasedCollection getNumberToStringBasedCollection() {
    return this.numberToStringBasedCollection;
  }

  public ObjectIdCollectionToBinary getObjectIdCollectionToBinary() {
    return this.objectIdCollectionToBinary;
  }

  public ObjectIdCollectionToBinaryArray getObjectIdCollectionToBinaryArray() {
    return this.objectIdCollectionToBinaryArray;
  }

  public ObjectIdCollectionToBinaryCollection getObjectIdCollectionToBinaryCollection() {
    return this.objectIdCollectionToBinaryCollection;
  }

  public ObjectIdCollectionToBoolean getObjectIdCollectionToBoolean() {
    return this.objectIdCollectionToBoolean;
  }

  public ObjectIdCollectionToBooleanArray getObjectIdCollectionToBooleanArray() {
    return this.objectIdCollectionToBooleanArray;
  }

  public ObjectIdCollectionToBooleanCollection getObjectIdCollectionToBooleanCollection() {
    return this.objectIdCollectionToBooleanCollection;
  }

  public ObjectIdCollectionToBooleanPrim getObjectIdCollectionToBooleanPrim() {
    return this.objectIdCollectionToBooleanPrim;
  }

  public ObjectIdCollectionToBooleanPrimArray getObjectIdCollectionToBooleanPrimArray() {
    return this.objectIdCollectionToBooleanPrimArray;
  }

  public ObjectIdCollectionToByte getObjectIdCollectionToByte() {
    return this.objectIdCollectionToByte;
  }

  public ObjectIdCollectionToByteArray getObjectIdCollectionToByteArray() {
    return this.objectIdCollectionToByteArray;
  }

  public ObjectIdCollectionToByteCollection getObjectIdCollectionToByteCollection() {
    return this.objectIdCollectionToByteCollection;
  }

  public ObjectIdCollectionToBytePrim getObjectIdCollectionToBytePrim() {
    return this.objectIdCollectionToBytePrim;
  }

  public ObjectIdCollectionToBytePrimArray getObjectIdCollectionToBytePrimArray() {
    return this.objectIdCollectionToBytePrimArray;
  }

  public ObjectIdCollectionToCharacter getObjectIdCollectionToCharacter() {
    return this.objectIdCollectionToCharacter;
  }

  public ObjectIdCollectionToCharacterArray getObjectIdCollectionToCharacterArray() {
    return this.objectIdCollectionToCharacterArray;
  }

  public ObjectIdCollectionToCharacterCollection getObjectIdCollectionToCharacterCollection() {
    return this.objectIdCollectionToCharacterCollection;
  }

  public ObjectIdCollectionToCharacterPrim getObjectIdCollectionToCharacterPrim() {
    return this.objectIdCollectionToCharacterPrim;
  }

  public ObjectIdCollectionToCharacterPrimArray getObjectIdCollectionToCharacterPrimArray() {
    return this.objectIdCollectionToCharacterPrimArray;
  }

  public ObjectIdCollectionToDateBased getObjectIdCollectionToDateBased() {
    return this.objectIdCollectionToDateBased;
  }

  public ObjectIdCollectionToDateBasedArray getObjectIdCollectionToDateBasedArray() {
    return this.objectIdCollectionToDateBasedArray;
  }

  public ObjectIdCollectionToDateBasedCollection getObjectIdCollectionToDateBasedCollection() {
    return this.objectIdCollectionToDateBasedCollection;
  }

  public ObjectIdCollectionToDoublePrim getObjectIdCollectionToDoublePrim() {
    return this.objectIdCollectionToDoublePrim;
  }

  public ObjectIdCollectionToDoublePrimArray getObjectIdCollectionToDoublePrimArray() {
    return this.objectIdCollectionToDoublePrimArray;
  }

  public ObjectIdCollectionToEnum getObjectIdCollectionToEnum() {
    return this.objectIdCollectionToEnum;
  }

  public ObjectIdCollectionToEnumArray getObjectIdCollectionToEnumArray() {
    return this.objectIdCollectionToEnumArray;
  }

  public ObjectIdCollectionToEnumCollection getObjectIdCollectionToEnumCollection() {
    return this.objectIdCollectionToEnumCollection;
  }

  public ObjectIdCollectionToFloatPrim getObjectIdCollectionToFloatPrim() {
    return this.objectIdCollectionToFloatPrim;
  }

  public ObjectIdCollectionToFloatPrimArray getObjectIdCollectionToFloatPrimArray() {
    return this.objectIdCollectionToFloatPrimArray;
  }

  public ObjectIdCollectionToIntegerPrim getObjectIdCollectionToIntegerPrim() {
    return this.objectIdCollectionToIntegerPrim;
  }

  public ObjectIdCollectionToIntegerPrimArray getObjectIdCollectionToIntegerPrimArray() {
    return this.objectIdCollectionToIntegerPrimArray;
  }

  public ObjectIdCollectionToLongPrim getObjectIdCollectionToLongPrim() {
    return this.objectIdCollectionToLongPrim;
  }

  public ObjectIdCollectionToLongPrimArray getObjectIdCollectionToLongPrimArray() {
    return this.objectIdCollectionToLongPrimArray;
  }

  public ObjectIdCollectionToNumberBased getObjectIdCollectionToNumberBased() {
    return this.objectIdCollectionToNumberBased;
  }

  public ObjectIdCollectionToNumberBasedArray getObjectIdCollectionToNumberBasedArray() {
    return this.objectIdCollectionToNumberBasedArray;
  }

  public ObjectIdCollectionToNumberBasedCollection getObjectIdCollectionToNumberBasedCollection() {
    return this.objectIdCollectionToNumberBasedCollection;
  }

  public ObjectIdCollectionToObjectId getObjectIdCollectionToObjectId() {
    return this.objectIdCollectionToObjectId;
  }

  public ObjectIdCollectionToObjectIdArray getObjectIdCollectionToObjectIdArray() {
    return this.objectIdCollectionToObjectIdArray;
  }

  public ObjectIdCollectionToObjectIdCollection getObjectIdCollectionToObjectIdCollection() {
    return this.objectIdCollectionToObjectIdCollection;
  }

  public ObjectIdCollectionToShortPrim getObjectIdCollectionToShortPrim() {
    return this.objectIdCollectionToShortPrim;
  }

  public ObjectIdCollectionToShortPrimArray getObjectIdCollectionToShortPrimArray() {
    return this.objectIdCollectionToShortPrimArray;
  }

  public ObjectIdCollectionToStringBased getObjectIdCollectionToStringBased() {
    return this.objectIdCollectionToStringBased;
  }

  public ObjectIdCollectionToStringBasedArray getObjectIdCollectionToStringBasedArray() {
    return this.objectIdCollectionToStringBasedArray;
  }

  public ObjectIdCollectionToStringBasedCollection getObjectIdCollectionToStringBasedCollection() {
    return this.objectIdCollectionToStringBasedCollection;
  }

  public ObjectIdToBinary getObjectIdToBinary() {
    return this.objectIdToBinary;
  }

  public ObjectIdToBinaryArray getObjectIdToBinaryArray() {
    return this.objectIdToBinaryArray;
  }

  public ObjectIdToBinaryCollection getObjectIdToBinaryCollection() {
    return this.objectIdToBinaryCollection;
  }

  public ObjectIdToBoolean getObjectIdToBoolean() {
    return this.objectIdToBoolean;
  }

  public ObjectIdToBooleanArray getObjectIdToBooleanArray() {
    return this.objectIdToBooleanArray;
  }

  public ObjectIdToBooleanCollection getObjectIdToBooleanCollection() {
    return this.objectIdToBooleanCollection;
  }

  public ObjectIdToBooleanPrim getObjectIdToBooleanPrim() {
    return this.objectIdToBooleanPrim;
  }

  public ObjectIdToBooleanPrimArray getObjectIdToBooleanPrimArray() {
    return this.objectIdToBooleanPrimArray;
  }

  public ObjectIdToByte getObjectIdToByte() {
    return this.objectIdToByte;
  }

  public ObjectIdToByteArray getObjectIdToByteArray() {
    return this.objectIdToByteArray;
  }

  public ObjectIdToByteCollection getObjectIdToByteCollection() {
    return this.objectIdToByteCollection;
  }

  public ObjectIdToBytePrim getObjectIdToBytePrim() {
    return this.objectIdToBytePrim;
  }

  public ObjectIdToBytePrimArray getObjectIdToBytePrimArray() {
    return this.objectIdToBytePrimArray;
  }

  public ObjectIdToCharacter getObjectIdToCharacter() {
    return this.objectIdToCharacter;
  }

  public ObjectIdToCharacterArray getObjectIdToCharacterArray() {
    return this.objectIdToCharacterArray;
  }

  public ObjectIdToCharacterCollection getObjectIdToCharacterCollection() {
    return this.objectIdToCharacterCollection;
  }

  public ObjectIdToCharacterPrim getObjectIdToCharacterPrim() {
    return this.objectIdToCharacterPrim;
  }

  public ObjectIdToCharacterPrimArray getObjectIdToCharacterPrimArray() {
    return this.objectIdToCharacterPrimArray;
  }

  public ObjectIdToDateBased getObjectIdToDateBased() {
    return this.objectIdToDateBased;
  }

  public ObjectIdToDateBasedArray getObjectIdToDateBasedArray() {
    return this.objectIdToDateBasedArray;
  }

  public ObjectIdToDateBasedCollection getObjectIdToDateBasedCollection() {
    return this.objectIdToDateBasedCollection;
  }

  public ObjectIdToDoublePrim getObjectIdToDoublePrim() {
    return this.objectIdToDoublePrim;
  }

  public ObjectIdToDoublePrimArray getObjectIdToDoublePrimArray() {
    return this.objectIdToDoublePrimArray;
  }

  public ObjectIdToEnum getObjectIdToEnum() {
    return this.objectIdToEnum;
  }

  public ObjectIdToEnumArray getObjectIdToEnumArray() {
    return this.objectIdToEnumArray;
  }

  public ObjectIdToEnumCollection getObjectIdToEnumCollection() {
    return this.objectIdToEnumCollection;
  }

  public ObjectIdToFloatPrim getObjectIdToFloatPrim() {
    return this.objectIdToFloatPrim;
  }

  public ObjectIdToFloatPrimArray getObjectIdToFloatPrimArray() {
    return this.objectIdToFloatPrimArray;
  }

  public ObjectIdToIntegerPrim getObjectIdToIntegerPrim() {
    return this.objectIdToIntegerPrim;
  }

  public ObjectIdToIntegerPrimArray getObjectIdToIntegerPrimArray() {
    return this.objectIdToIntegerPrimArray;
  }

  public ObjectIdToLongPrim getObjectIdToLongPrim() {
    return this.objectIdToLongPrim;
  }

  public ObjectIdToLongPrimArray getObjectIdToLongPrimArray() {
    return this.objectIdToLongPrimArray;
  }

  public ObjectIdToNumberBased getObjectIdToNumberBased() {
    return this.objectIdToNumberBased;
  }

  public ObjectIdToNumberBasedArray getObjectIdToNumberBasedArray() {
    return this.objectIdToNumberBasedArray;
  }

  public ObjectIdToNumberBasedCollection getObjectIdToNumberBasedCollection() {
    return this.objectIdToNumberBasedCollection;
  }

  public ObjectIdToObjectId getObjectIdToObjectId() {
    return this.objectIdToObjectId;
  }

  public ObjectIdToObjectIdArray getObjectIdToObjectIdArray() {
    return this.objectIdToObjectIdArray;
  }

  public ObjectIdToObjectIdCollection getObjectIdToObjectIdCollection() {
    return this.objectIdToObjectIdCollection;
  }

  public ObjectIdToShortPrim getObjectIdToShortPrim() {
    return this.objectIdToShortPrim;
  }

  public ObjectIdToShortPrimArray getObjectIdToShortPrimArray() {
    return this.objectIdToShortPrimArray;
  }

  public ObjectIdToStringBased getObjectIdToStringBased() {
    return this.objectIdToStringBased;
  }

  public ObjectIdToStringBasedArray getObjectIdToStringBasedArray() {
    return this.objectIdToStringBasedArray;
  }

  public ObjectIdToStringBasedCollection getObjectIdToStringBasedCollection() {
    return this.objectIdToStringBasedCollection;
  }

  public StringCollectionToBinary getStringCollectionToBinary() {
    return this.stringCollectionToBinary;
  }

  public StringCollectionToBinaryArray getStringCollectionToBinaryArray() {
    return this.stringCollectionToBinaryArray;
  }

  public StringCollectionToBinaryCollection getStringCollectionToBinaryCollection() {
    return this.stringCollectionToBinaryCollection;
  }

  public StringCollectionToBoolean getStringCollectionToBoolean() {
    return this.stringCollectionToBoolean;
  }

  public StringCollectionToBooleanArray getStringCollectionToBooleanArray() {
    return this.stringCollectionToBooleanArray;
  }

  public StringCollectionToBooleanCollection getStringCollectionToBooleanCollection() {
    return this.stringCollectionToBooleanCollection;
  }

  public StringCollectionToBooleanPrim getStringCollectionToBooleanPrim() {
    return this.stringCollectionToBooleanPrim;
  }

  public StringCollectionToBooleanPrimArray getStringCollectionToBooleanPrimArray() {
    return this.stringCollectionToBooleanPrimArray;
  }

  public StringCollectionToByte getStringCollectionToByte() {
    return this.stringCollectionToByte;
  }

  public StringCollectionToByteArray getStringCollectionToByteArray() {
    return this.stringCollectionToByteArray;
  }

  public StringCollectionToByteCollection getStringCollectionToByteCollection() {
    return this.stringCollectionToByteCollection;
  }

  public StringCollectionToBytePrim getStringCollectionToBytePrim() {
    return this.stringCollectionToBytePrim;
  }

  public StringCollectionToBytePrimArray getStringCollectionToBytePrimArray() {
    return this.stringCollectionToBytePrimArray;
  }

  public StringCollectionToCharacter getStringCollectionToCharacter() {
    return this.stringCollectionToCharacter;
  }

  public StringCollectionToCharacterArray getStringCollectionToCharacterArray() {
    return this.stringCollectionToCharacterArray;
  }

  public StringCollectionToCharacterCollection getStringCollectionToCharacterCollection() {
    return this.stringCollectionToCharacterCollection;
  }

  public StringCollectionToCharacterPrim getStringCollectionToCharacterPrim() {
    return this.stringCollectionToCharacterPrim;
  }

  public StringCollectionToCharacterPrimArray getStringCollectionToCharacterPrimArray() {
    return this.stringCollectionToCharacterPrimArray;
  }

  public StringCollectionToDateBased getStringCollectionToDateBased() {
    return this.stringCollectionToDateBased;
  }

  public StringCollectionToDateBasedArray getStringCollectionToDateBasedArray() {
    return this.stringCollectionToDateBasedArray;
  }

  public StringCollectionToDateBasedCollection getStringCollectionToDateBasedCollection() {
    return this.stringCollectionToDateBasedCollection;
  }

  public StringCollectionToDoublePrim getStringCollectionToDoublePrim() {
    return this.stringCollectionToDoublePrim;
  }

  public StringCollectionToDoublePrimArray getStringCollectionToDoublePrimArray() {
    return this.stringCollectionToDoublePrimArray;
  }

  public StringCollectionToEnum getStringCollectionToEnum() {
    return this.stringCollectionToEnum;
  }

  public StringCollectionToEnumArray getStringCollectionToEnumArray() {
    return this.stringCollectionToEnumArray;
  }

  public StringCollectionToEnumCollection getStringCollectionToEnumCollection() {
    return this.stringCollectionToEnumCollection;
  }

  public StringCollectionToFloatPrim getStringCollectionToFloatPrim() {
    return this.stringCollectionToFloatPrim;
  }

  public StringCollectionToFloatPrimArray getStringCollectionToFloatPrimArray() {
    return this.stringCollectionToFloatPrimArray;
  }

  public StringCollectionToIntegerPrim getStringCollectionToIntegerPrim() {
    return this.stringCollectionToIntegerPrim;
  }

  public StringCollectionToIntegerPrimArray getStringCollectionToIntegerPrimArray() {
    return this.stringCollectionToIntegerPrimArray;
  }

  public StringCollectionToLongPrim getStringCollectionToLongPrim() {
    return this.stringCollectionToLongPrim;
  }

  public StringCollectionToLongPrimArray getStringCollectionToLongPrimArray() {
    return this.stringCollectionToLongPrimArray;
  }

  public StringCollectionToNumberBased getStringCollectionToNumberBased() {
    return this.stringCollectionToNumberBased;
  }

  public StringCollectionToNumberBasedArray getStringCollectionToNumberBasedArray() {
    return this.stringCollectionToNumberBasedArray;
  }

  public StringCollectionToNumberBasedCollection getStringCollectionToNumberBasedCollection() {
    return this.stringCollectionToNumberBasedCollection;
  }

  public StringCollectionToObjectId getStringCollectionToObjectId() {
    return this.stringCollectionToObjectId;
  }

  public StringCollectionToObjectIdArray getStringCollectionToObjectIdArray() {
    return this.stringCollectionToObjectIdArray;
  }

  public StringCollectionToObjectIdCollection getStringCollectionToObjectIdCollection() {
    return this.stringCollectionToObjectIdCollection;
  }

  public StringCollectionToShortPrim getStringCollectionToShortPrim() {
    return this.stringCollectionToShortPrim;
  }

  public StringCollectionToShortPrimArray getStringCollectionToShortPrimArray() {
    return this.stringCollectionToShortPrimArray;
  }

  public StringCollectionToStringBased getStringCollectionToStringBased() {
    return this.stringCollectionToStringBased;
  }

  public StringCollectionToStringBasedArray getStringCollectionToStringBasedArray() {
    return this.stringCollectionToStringBasedArray;
  }

  public StringCollectionToStringBasedCollection getStringCollectionToStringBasedCollection() {
    return this.stringCollectionToStringBasedCollection;
  }

  public StringToBinary getStringToBinary() {
    return this.stringToBinary;
  }

  public StringToBinaryArray getStringToBinaryArray() {
    return this.stringToBinaryArray;
  }

  public StringToBinaryCollection getStringToBinaryCollection() {
    return this.stringToBinaryCollection;
  }

  public StringToBoolean getStringToBoolean() {
    return this.stringToBoolean;
  }

  public StringToBooleanArray getStringToBooleanArray() {
    return this.stringToBooleanArray;
  }

  public StringToBooleanCollection getStringToBooleanCollection() {
    return this.stringToBooleanCollection;
  }

  public StringToBooleanPrim getStringToBooleanPrim() {
    return this.stringToBooleanPrim;
  }

  public StringToBooleanPrimArray getStringToBooleanPrimArray() {
    return this.stringToBooleanPrimArray;
  }

  public StringToByte getStringToByte() {
    return this.stringToByte;
  }

  public StringToByteArray getStringToByteArray() {
    return this.stringToByteArray;
  }

  public StringToByteCollection getStringToByteCollection() {
    return this.stringToByteCollection;
  }

  public StringToBytePrim getStringToBytePrim() {
    return this.stringToBytePrim;
  }

  public StringToBytePrimArray getStringToBytePrimArray() {
    return this.stringToBytePrimArray;
  }

  public StringToCharacter getStringToCharacter() {
    return this.stringToCharacter;
  }

  public StringToCharacterArray getStringToCharacterArray() {
    return this.stringToCharacterArray;
  }

  public StringToCharacterCollection getStringToCharacterCollection() {
    return this.stringToCharacterCollection;
  }

  public StringToCharacterPrim getStringToCharacterPrim() {
    return this.stringToCharacterPrim;
  }

  public StringToCharacterPrimArray getStringToCharacterPrimArray() {
    return this.stringToCharacterPrimArray;
  }

  public StringToDateBased getStringToDateBased() {
    return this.stringToDateBased;
  }

  public StringToDateBasedArray getStringToDateBasedArray() {
    return this.stringToDateBasedArray;
  }

  public StringToDateBasedCollection getStringToDateBasedCollection() {
    return this.stringToDateBasedCollection;
  }

  public StringToDoublePrim getStringToDoublePrim() {
    return this.stringToDoublePrim;
  }

  public StringToDoublePrimArray getStringToDoublePrimArray() {
    return this.stringToDoublePrimArray;
  }

  public StringToEnum getStringToEnum() {
    return this.stringToEnum;
  }

  public StringToEnumArray getStringToEnumArray() {
    return this.stringToEnumArray;
  }

  public StringToEnumCollection getStringToEnumCollection() {
    return this.stringToEnumCollection;
  }

  public StringToFloatPrim getStringToFloatPrim() {
    return this.stringToFloatPrim;
  }

  public StringToFloatPrimArray getStringToFloatPrimArray() {
    return this.stringToFloatPrimArray;
  }

  public StringToIntegerPrim getStringToIntegerPrim() {
    return this.stringToIntegerPrim;
  }

  public StringToIntegerPrimArray getStringToIntegerPrimArray() {
    return this.stringToIntegerPrimArray;
  }

  public StringToLongPrim getStringToLongPrim() {
    return this.stringToLongPrim;
  }

  public StringToLongPrimArray getStringToLongPrimArray() {
    return this.stringToLongPrimArray;
  }

  public StringToNumberBased getStringToNumberBased() {
    return this.stringToNumberBased;
  }

  public StringToNumberBasedArray getStringToNumberBasedArray() {
    return this.stringToNumberBasedArray;
  }

  public StringToNumberBasedCollection getStringToNumberBasedCollection() {
    return this.stringToNumberBasedCollection;
  }

  public StringToObjectId getStringToObjectId() {
    return this.stringToObjectId;
  }

  public StringToObjectIdArray getStringToObjectIdArray() {
    return this.stringToObjectIdArray;
  }

  public StringToObjectIdCollection getStringToObjectIdCollection() {
    return this.stringToObjectIdCollection;
  }

  public StringToShortPrim getStringToShortPrim() {
    return this.stringToShortPrim;
  }

  public StringToShortPrimArray getStringToShortPrimArray() {
    return this.stringToShortPrimArray;
  }

  public StringToStringBased getStringToStringBased() {
    return this.stringToStringBased;
  }

  public StringToStringBasedArray getStringToStringBasedArray() {
    return this.stringToStringBasedArray;
  }

  public StringToStringBasedCollection getStringToStringBasedCollection() {
    return this.stringToStringBasedCollection;
  }

  public void setBinaryCollectionToBinary(final BinaryCollectionToBinary binaryCollectionToBinary) {
    this.binaryCollectionToBinary = Objects.requireNonNull(binaryCollectionToBinary);
  }

  public void setBinaryCollectionToBinaryArray(
      final BinaryCollectionToBinaryArray binaryCollectionToBinaryArray) {
    this.binaryCollectionToBinaryArray = Objects.requireNonNull(binaryCollectionToBinaryArray);
  }

  public void setBinaryCollectionToBinaryCollection(
      final BinaryCollectionToBinaryCollection binaryCollectionToBinaryCollection) {
    this.binaryCollectionToBinaryCollection =
        Objects.requireNonNull(binaryCollectionToBinaryCollection);
  }

  public void setBinaryCollectionToBoolean(
      final BinaryCollectionToBoolean binaryCollectionToBoolean) {
    this.binaryCollectionToBoolean = Objects.requireNonNull(binaryCollectionToBoolean);
  }

  public void setBinaryCollectionToBooleanArray(
      final BinaryCollectionToBooleanArray binaryCollectionToBooleanArray) {
    this.binaryCollectionToBooleanArray = Objects.requireNonNull(binaryCollectionToBooleanArray);
  }

  public void setBinaryCollectionToBooleanCollection(
      final BinaryCollectionToBooleanCollection binaryCollectionToBooleanCollection) {
    this.binaryCollectionToBooleanCollection =
        Objects.requireNonNull(binaryCollectionToBooleanCollection);
  }

  public void setBinaryCollectionToBooleanPrim(
      final BinaryCollectionToBooleanPrim binaryCollectionToBooleanPrim) {
    this.binaryCollectionToBooleanPrim = Objects.requireNonNull(binaryCollectionToBooleanPrim);
  }

  public void setBinaryCollectionToBooleanPrimArray(
      final BinaryCollectionToBooleanPrimArray binaryCollectionToBooleanPrimArray) {
    this.binaryCollectionToBooleanPrimArray =
        Objects.requireNonNull(binaryCollectionToBooleanPrimArray);
  }

  public void setBinaryCollectionToByte(final BinaryCollectionToByte binaryCollectionToByte) {
    this.binaryCollectionToByte = Objects.requireNonNull(binaryCollectionToByte);
  }

  public void setBinaryCollectionToByteArray(
      final BinaryCollectionToByteArray binaryCollectionToByteArray) {
    this.binaryCollectionToByteArray = Objects.requireNonNull(binaryCollectionToByteArray);
  }

  public void setBinaryCollectionToByteCollection(
      final BinaryCollectionToByteCollection binaryCollectionToByteCollection) {
    this.binaryCollectionToByteCollection =
        Objects.requireNonNull(binaryCollectionToByteCollection);
  }

  public void setBinaryCollectionToBytePrim(
      final BinaryCollectionToBytePrim binaryCollectionToBytePrim) {
    this.binaryCollectionToBytePrim = Objects.requireNonNull(binaryCollectionToBytePrim);
  }

  public void setBinaryCollectionToBytePrimArray(
      final BinaryCollectionToBytePrimArray binaryCollectionToBytePrimArray) {
    this.binaryCollectionToBytePrimArray = Objects.requireNonNull(binaryCollectionToBytePrimArray);
  }

  public void setBinaryCollectionToCharacter(
      final BinaryCollectionToCharacter binaryCollectionToCharacter) {
    this.binaryCollectionToCharacter = Objects.requireNonNull(binaryCollectionToCharacter);
  }

  public void setBinaryCollectionToCharacterArray(
      final BinaryCollectionToCharacterArray binaryCollectionToCharacterArray) {
    this.binaryCollectionToCharacterArray =
        Objects.requireNonNull(binaryCollectionToCharacterArray);
  }

  public void setBinaryCollectionToCharacterCollection(
      final BinaryCollectionToCharacterCollection binaryCollectionToCharacterCollection) {
    this.binaryCollectionToCharacterCollection =
        Objects.requireNonNull(binaryCollectionToCharacterCollection);
  }

  public void setBinaryCollectionToCharacterPrim(
      final BinaryCollectionToCharacterPrim binaryCollectionToCharacterPrim) {
    this.binaryCollectionToCharacterPrim = Objects.requireNonNull(binaryCollectionToCharacterPrim);
  }

  public void setBinaryCollectionToCharacterPrimArray(
      final BinaryCollectionToCharacterPrimArray binaryCollectionToCharacterPrimArray) {
    this.binaryCollectionToCharacterPrimArray =
        Objects.requireNonNull(binaryCollectionToCharacterPrimArray);
  }

  public void setBinaryCollectionToDateBased(
      final BinaryCollectionToDateBased binaryCollectionToDateBased) {
    this.binaryCollectionToDateBased = Objects.requireNonNull(binaryCollectionToDateBased);
  }

  public void setBinaryCollectionToDateBasedArray(
      final BinaryCollectionToDateBasedArray binaryCollectionToDateBasedArray) {
    this.binaryCollectionToDateBasedArray =
        Objects.requireNonNull(binaryCollectionToDateBasedArray);
  }

  public void setBinaryCollectionToDateBasedCollection(
      final BinaryCollectionToDateBasedCollection binaryCollectionToDateBasedCollection) {
    this.binaryCollectionToDateBasedCollection =
        Objects.requireNonNull(binaryCollectionToDateBasedCollection);
  }

  public void setBinaryCollectionToDoublePrim(
      final BinaryCollectionToDoublePrim binaryCollectionToDoublePrim) {
    this.binaryCollectionToDoublePrim = Objects.requireNonNull(binaryCollectionToDoublePrim);
  }

  public void setBinaryCollectionToDoublePrimArray(
      final BinaryCollectionToDoublePrimArray binaryCollectionToDoublePrimArray) {
    this.binaryCollectionToDoublePrimArray =
        Objects.requireNonNull(binaryCollectionToDoublePrimArray);
  }

  public void setBinaryCollectionToEnum(final BinaryCollectionToEnum binaryCollectionToEnum) {
    this.binaryCollectionToEnum = Objects.requireNonNull(binaryCollectionToEnum);
  }

  public void setBinaryCollectionToEnumArray(
      final BinaryCollectionToEnumArray binaryCollectionToEnumArray) {
    this.binaryCollectionToEnumArray = Objects.requireNonNull(binaryCollectionToEnumArray);
  }

  public void setBinaryCollectionToEnumCollection(
      final BinaryCollectionToEnumCollection binaryCollectionToEnumCollection) {
    this.binaryCollectionToEnumCollection =
        Objects.requireNonNull(binaryCollectionToEnumCollection);
  }

  public void setBinaryCollectionToFloatPrim(
      final BinaryCollectionToFloatPrim binaryCollectionToFloatPrim) {
    this.binaryCollectionToFloatPrim = Objects.requireNonNull(binaryCollectionToFloatPrim);
  }

  public void setBinaryCollectionToFloatPrimArray(
      final BinaryCollectionToFloatPrimArray binaryCollectionToFloatPrimArray) {
    this.binaryCollectionToFloatPrimArray =
        Objects.requireNonNull(binaryCollectionToFloatPrimArray);
  }

  public void setBinaryCollectionToIntegerPrim(
      final BinaryCollectionToIntegerPrim binaryCollectionToIntegerPrim) {
    this.binaryCollectionToIntegerPrim = Objects.requireNonNull(binaryCollectionToIntegerPrim);
  }

  public void setBinaryCollectionToIntegerPrimArray(
      final BinaryCollectionToIntegerPrimArray binaryCollectionToIntegerPrimArray) {
    this.binaryCollectionToIntegerPrimArray =
        Objects.requireNonNull(binaryCollectionToIntegerPrimArray);
  }

  public void setBinaryCollectionToLongPrim(
      final BinaryCollectionToLongPrim binaryCollectionToLongPrim) {
    this.binaryCollectionToLongPrim = Objects.requireNonNull(binaryCollectionToLongPrim);
  }

  public void setBinaryCollectionToLongPrimArray(
      final BinaryCollectionToLongPrimArray binaryCollectionToLongPrimArray) {
    this.binaryCollectionToLongPrimArray = Objects.requireNonNull(binaryCollectionToLongPrimArray);
  }

  public void setBinaryCollectionToNumberBased(
      final BinaryCollectionToNumberBased binaryCollectionToNumberBased) {
    this.binaryCollectionToNumberBased = Objects.requireNonNull(binaryCollectionToNumberBased);
  }

  public void setBinaryCollectionToNumberBasedArray(
      final BinaryCollectionToNumberBasedArray binaryCollectionToNumberBasedArray) {
    this.binaryCollectionToNumberBasedArray =
        Objects.requireNonNull(binaryCollectionToNumberBasedArray);
  }

  public void setBinaryCollectionToNumberBasedCollection(
      final BinaryCollectionToNumberBasedCollection binaryCollectionToNumberBasedCollection) {
    this.binaryCollectionToNumberBasedCollection =
        Objects.requireNonNull(binaryCollectionToNumberBasedCollection);
  }

  public void setBinaryCollectionToObjectId(
      final BinaryCollectionToObjectId binaryCollectionToObjectId) {
    this.binaryCollectionToObjectId = Objects.requireNonNull(binaryCollectionToObjectId);
  }

  public void setBinaryCollectionToObjectIdArray(
      final BinaryCollectionToObjectIdArray binaryCollectionToObjectIdArray) {
    this.binaryCollectionToObjectIdArray = Objects.requireNonNull(binaryCollectionToObjectIdArray);
  }

  public void setBinaryCollectionToObjectIdCollection(
      final BinaryCollectionToObjectIdCollection binaryCollectionToObjectIdCollection) {
    this.binaryCollectionToObjectIdCollection =
        Objects.requireNonNull(binaryCollectionToObjectIdCollection);
  }

  public void setBinaryCollectionToShortPrim(
      final BinaryCollectionToShortPrim binaryCollectionToShortPrim) {
    this.binaryCollectionToShortPrim = Objects.requireNonNull(binaryCollectionToShortPrim);
  }

  public void setBinaryCollectionToShortPrimArray(
      final BinaryCollectionToShortPrimArray binaryCollectionToShortPrimArray) {
    this.binaryCollectionToShortPrimArray =
        Objects.requireNonNull(binaryCollectionToShortPrimArray);
  }

  public void setBinaryCollectionToStringBased(
      final BinaryCollectionToStringBased binaryCollectionToStringBased) {
    this.binaryCollectionToStringBased = Objects.requireNonNull(binaryCollectionToStringBased);
  }

  public void setBinaryCollectionToStringBasedArray(
      final BinaryCollectionToStringBasedArray binaryCollectionToStringBasedArray) {
    this.binaryCollectionToStringBasedArray =
        Objects.requireNonNull(binaryCollectionToStringBasedArray);
  }

  public void setBinaryCollectionToStringBasedCollection(
      final BinaryCollectionToStringBasedCollection binaryCollectionToStringBasedCollection) {
    this.binaryCollectionToStringBasedCollection =
        Objects.requireNonNull(binaryCollectionToStringBasedCollection);
  }

  public void setBinaryToBinary(final BinaryToBinary binaryToBinary) {
    this.binaryToBinary = Objects.requireNonNull(binaryToBinary);
  }

  public void setBinaryToBinaryArray(final BinaryToBinaryArray binaryToBinaryArray) {
    this.binaryToBinaryArray = Objects.requireNonNull(binaryToBinaryArray);
  }

  public void setBinaryToBinaryCollection(final BinaryToBinaryCollection binaryToBinaryCollection) {
    this.binaryToBinaryCollection = Objects.requireNonNull(binaryToBinaryCollection);
  }

  public void setBinaryToBoolean(final BinaryToBoolean binaryToBoolean) {
    this.binaryToBoolean = Objects.requireNonNull(binaryToBoolean);
  }

  public void setBinaryToBooleanArray(final BinaryToBooleanArray binaryToBooleanArray) {
    this.binaryToBooleanArray = Objects.requireNonNull(binaryToBooleanArray);
  }

  public void setBinaryToBooleanCollection(
      final BinaryToBooleanCollection binaryToBooleanCollection) {
    this.binaryToBooleanCollection = Objects.requireNonNull(binaryToBooleanCollection);
  }

  public void setBinaryToBooleanPrim(final BinaryToBooleanPrim binaryToBooleanPrim) {
    this.binaryToBooleanPrim = Objects.requireNonNull(binaryToBooleanPrim);
  }

  public void setBinaryToBooleanPrimArray(final BinaryToBooleanPrimArray binaryToBooleanPrimArray) {
    this.binaryToBooleanPrimArray = Objects.requireNonNull(binaryToBooleanPrimArray);
  }

  public void setBinaryToByte(final BinaryToByte binaryToByte) {
    this.binaryToByte = Objects.requireNonNull(binaryToByte);
  }

  public void setBinaryToByteArray(final BinaryToByteArray binaryToByteArray) {
    this.binaryToByteArray = Objects.requireNonNull(binaryToByteArray);
  }

  public void setBinaryToByteCollection(final BinaryToByteCollection binaryToByteCollection) {
    this.binaryToByteCollection = Objects.requireNonNull(binaryToByteCollection);
  }

  public void setBinaryToBytePrim(final BinaryToBytePrim binaryToBytePrim) {
    this.binaryToBytePrim = Objects.requireNonNull(binaryToBytePrim);
  }

  public void setBinaryToBytePrimArray(final BinaryToBytePrimArray binaryToBytePrimArray) {
    this.binaryToBytePrimArray = Objects.requireNonNull(binaryToBytePrimArray);
  }

  public void setBinaryToCharacter(final BinaryToCharacter binaryToCharacter) {
    this.binaryToCharacter = Objects.requireNonNull(binaryToCharacter);
  }

  public void setBinaryToCharacterArray(final BinaryToCharacterArray binaryToCharacterArray) {
    this.binaryToCharacterArray = Objects.requireNonNull(binaryToCharacterArray);
  }

  public void setBinaryToCharacterCollection(
      final BinaryToCharacterCollection binaryToCharacterCollection) {
    this.binaryToCharacterCollection = Objects.requireNonNull(binaryToCharacterCollection);
  }

  public void setBinaryToCharacterPrim(final BinaryToCharacterPrim binaryToCharacterPrim) {
    this.binaryToCharacterPrim = Objects.requireNonNull(binaryToCharacterPrim);
  }

  public void setBinaryToCharacterPrimArray(
      final BinaryToCharacterPrimArray binaryToCharacterPrimArray) {
    this.binaryToCharacterPrimArray = Objects.requireNonNull(binaryToCharacterPrimArray);
  }

  public void setBinaryToDateBased(final BinaryToDateBased binaryToDateBased) {
    this.binaryToDateBased = Objects.requireNonNull(binaryToDateBased);
  }

  public void setBinaryToDateBasedArray(final BinaryToDateBasedArray binaryToDateBasedArray) {
    this.binaryToDateBasedArray = Objects.requireNonNull(binaryToDateBasedArray);
  }

  public void setBinaryToDateBasedCollection(
      final BinaryToDateBasedCollection binaryToDateBasedCollection) {
    this.binaryToDateBasedCollection = Objects.requireNonNull(binaryToDateBasedCollection);
  }

  public void setBinaryToDoublePrim(final BinaryToDoublePrim binaryToDoublePrim) {
    this.binaryToDoublePrim = Objects.requireNonNull(binaryToDoublePrim);
  }

  public void setBinaryToDoublePrimArray(final BinaryToDoublePrimArray binaryToDoublePrimArray) {
    this.binaryToDoublePrimArray = Objects.requireNonNull(binaryToDoublePrimArray);
  }

  public void setBinaryToEnum(final BinaryToEnum binaryToEnum) {
    this.binaryToEnum = Objects.requireNonNull(binaryToEnum);
  }

  public void setBinaryToEnumArray(final BinaryToEnumArray binaryToEnumArray) {
    this.binaryToEnumArray = Objects.requireNonNull(binaryToEnumArray);
  }

  public void setBinaryToEnumCollection(final BinaryToEnumCollection binaryToEnumCollection) {
    this.binaryToEnumCollection = Objects.requireNonNull(binaryToEnumCollection);
  }

  public void setBinaryToFloatPrim(final BinaryToFloatPrim binaryToFloatPrim) {
    this.binaryToFloatPrim = Objects.requireNonNull(binaryToFloatPrim);
  }

  public void setBinaryToFloatPrimArray(final BinaryToFloatPrimArray binaryToFloatPrimArray) {
    this.binaryToFloatPrimArray = Objects.requireNonNull(binaryToFloatPrimArray);
  }

  public void setBinaryToIntegerPrim(final BinaryToIntegerPrim binaryToIntegerPrim) {
    this.binaryToIntegerPrim = Objects.requireNonNull(binaryToIntegerPrim);
  }

  public void setBinaryToIntegerPrimArray(final BinaryToIntegerPrimArray binaryToIntegerPrimArray) {
    this.binaryToIntegerPrimArray = Objects.requireNonNull(binaryToIntegerPrimArray);
  }

  public void setBinaryToLongPrim(final BinaryToLongPrim binaryToLongPrim) {
    this.binaryToLongPrim = Objects.requireNonNull(binaryToLongPrim);
  }

  public void setBinaryToLongPrimArray(final BinaryToLongPrimArray binaryToLongPrimArray) {
    this.binaryToLongPrimArray = Objects.requireNonNull(binaryToLongPrimArray);
  }

  public void setBinaryToNumberBased(final BinaryToNumberBased binaryToNumberBased) {
    this.binaryToNumberBased = Objects.requireNonNull(binaryToNumberBased);
  }

  public void setBinaryToNumberBasedArray(final BinaryToNumberBasedArray binaryToNumberBasedArray) {
    this.binaryToNumberBasedArray = Objects.requireNonNull(binaryToNumberBasedArray);
  }

  public void setBinaryToNumberBasedCollection(
      final BinaryToNumberBasedCollection binaryToNumberBasedCollection) {
    this.binaryToNumberBasedCollection = Objects.requireNonNull(binaryToNumberBasedCollection);
  }

  public void setBinaryToObjectId(final BinaryToObjectId binaryToObjectId) {
    this.binaryToObjectId = Objects.requireNonNull(binaryToObjectId);
  }

  public void setBinaryToObjectIdArray(final BinaryToObjectIdArray binaryToObjectIdArray) {
    this.binaryToObjectIdArray = Objects.requireNonNull(binaryToObjectIdArray);
  }

  public void setBinaryToObjectIdCollection(
      final BinaryToObjectIdCollection binaryToObjectIdCollection) {
    this.binaryToObjectIdCollection = Objects.requireNonNull(binaryToObjectIdCollection);
  }

  public void setBinaryToShortPrim(final BinaryToShortPrim binaryToShortPrim) {
    this.binaryToShortPrim = Objects.requireNonNull(binaryToShortPrim);
  }

  public void setBinaryToShortPrimArray(final BinaryToShortPrimArray binaryToShortPrimArray) {
    this.binaryToShortPrimArray = Objects.requireNonNull(binaryToShortPrimArray);
  }

  public void setBinaryToStringBased(final BinaryToStringBased binaryToStringBased) {
    this.binaryToStringBased = Objects.requireNonNull(binaryToStringBased);
  }

  public void setBinaryToStringBasedArray(final BinaryToStringBasedArray binaryToStringBasedArray) {
    this.binaryToStringBasedArray = Objects.requireNonNull(binaryToStringBasedArray);
  }

  public void setBinaryToStringBasedCollection(
      final BinaryToStringBasedCollection binaryToStringBasedCollection) {
    this.binaryToStringBasedCollection = Objects.requireNonNull(binaryToStringBasedCollection);
  }

  public void setBooleanCollectionToBinary(
      final BooleanCollectionToBinary booleanCollectionToBinary) {
    this.booleanCollectionToBinary = Objects.requireNonNull(booleanCollectionToBinary);
  }

  public void setBooleanCollectionToBinaryArray(
      final BooleanCollectionToBinaryArray booleanCollectionToBinaryArray) {
    this.booleanCollectionToBinaryArray = Objects.requireNonNull(booleanCollectionToBinaryArray);
  }

  public void setBooleanCollectionToBinaryCollection(
      final BooleanCollectionToBinaryCollection booleanCollectionToBinaryCollection) {
    this.booleanCollectionToBinaryCollection =
        Objects.requireNonNull(booleanCollectionToBinaryCollection);
  }

  public void setBooleanCollectionToBoolean(
      final BooleanCollectionToBoolean booleanCollectionToBoolean) {
    this.booleanCollectionToBoolean = Objects.requireNonNull(booleanCollectionToBoolean);
  }

  public void setBooleanCollectionToBooleanArray(
      final BooleanCollectionToBooleanArray booleanCollectionToBooleanArray) {
    this.booleanCollectionToBooleanArray = Objects.requireNonNull(booleanCollectionToBooleanArray);
  }

  public void setBooleanCollectionToBooleanCollection(
      final BooleanCollectionToBooleanCollection booleanCollectionToBooleanCollection) {
    this.booleanCollectionToBooleanCollection =
        Objects.requireNonNull(booleanCollectionToBooleanCollection);
  }

  public void setBooleanCollectionToBooleanPrim(
      final BooleanCollectionToBooleanPrim booleanCollectionToBooleanPrim) {
    this.booleanCollectionToBooleanPrim = Objects.requireNonNull(booleanCollectionToBooleanPrim);
  }

  public void setBooleanCollectionToBooleanPrimArray(
      final BooleanCollectionToBooleanPrimArray booleanCollectionToBooleanPrimArray) {
    this.booleanCollectionToBooleanPrimArray =
        Objects.requireNonNull(booleanCollectionToBooleanPrimArray);
  }

  public void setBooleanCollectionToByte(final BooleanCollectionToByte booleanCollectionToByte) {
    this.booleanCollectionToByte = Objects.requireNonNull(booleanCollectionToByte);
  }

  public void setBooleanCollectionToByteArray(
      final BooleanCollectionToByteArray booleanCollectionToByteArray) {
    this.booleanCollectionToByteArray = Objects.requireNonNull(booleanCollectionToByteArray);
  }

  public void setBooleanCollectionToByteCollection(
      final BooleanCollectionToByteCollection booleanCollectionToByteCollection) {
    this.booleanCollectionToByteCollection =
        Objects.requireNonNull(booleanCollectionToByteCollection);
  }

  public void setBooleanCollectionToBytePrim(
      final BooleanCollectionToBytePrim booleanCollectionToBytePrim) {
    this.booleanCollectionToBytePrim = Objects.requireNonNull(booleanCollectionToBytePrim);
  }

  public void setBooleanCollectionToBytePrimArray(
      final BooleanCollectionToBytePrimArray booleanCollectionToBytePrimArray) {
    this.booleanCollectionToBytePrimArray =
        Objects.requireNonNull(booleanCollectionToBytePrimArray);
  }

  public void setBooleanCollectionToCharacter(
      final BooleanCollectionToCharacter booleanCollectionToCharacter) {
    this.booleanCollectionToCharacter = Objects.requireNonNull(booleanCollectionToCharacter);
  }

  public void setBooleanCollectionToCharacterArray(
      final BooleanCollectionToCharacterArray booleanCollectionToCharacterArray) {
    this.booleanCollectionToCharacterArray =
        Objects.requireNonNull(booleanCollectionToCharacterArray);
  }

  public void setBooleanCollectionToCharacterCollection(
      final BooleanCollectionToCharacterCollection booleanCollectionToCharacterCollection) {
    this.booleanCollectionToCharacterCollection =
        Objects.requireNonNull(booleanCollectionToCharacterCollection);
  }

  public void setBooleanCollectionToCharacterPrim(
      final BooleanCollectionToCharacterPrim booleanCollectionToCharacterPrim) {
    this.booleanCollectionToCharacterPrim =
        Objects.requireNonNull(booleanCollectionToCharacterPrim);
  }

  public void setBooleanCollectionToCharacterPrimArray(
      final BooleanCollectionToCharacterPrimArray booleanCollectionToCharacterPrimArray) {
    this.booleanCollectionToCharacterPrimArray =
        Objects.requireNonNull(booleanCollectionToCharacterPrimArray);
  }

  public void setBooleanCollectionToDateBased(
      final BooleanCollectionToDateBased booleanCollectionToDateBased) {
    this.booleanCollectionToDateBased = Objects.requireNonNull(booleanCollectionToDateBased);
  }

  public void setBooleanCollectionToDateBasedArray(
      final BooleanCollectionToDateBasedArray booleanCollectionToDateBasedArray) {
    this.booleanCollectionToDateBasedArray =
        Objects.requireNonNull(booleanCollectionToDateBasedArray);
  }

  public void setBooleanCollectionToDateBasedCollection(
      final BooleanCollectionToDateBasedCollection booleanCollectionToDateBasedCollection) {
    this.booleanCollectionToDateBasedCollection =
        Objects.requireNonNull(booleanCollectionToDateBasedCollection);
  }

  public void setBooleanCollectionToDoublePrim(
      final BooleanCollectionToDoublePrim booleanCollectionToDoublePrim) {
    this.booleanCollectionToDoublePrim = Objects.requireNonNull(booleanCollectionToDoublePrim);
  }

  public void setBooleanCollectionToDoublePrimArray(
      final BooleanCollectionToDoublePrimArray booleanCollectionToDoublePrimArray) {
    this.booleanCollectionToDoublePrimArray =
        Objects.requireNonNull(booleanCollectionToDoublePrimArray);
  }

  public void setBooleanCollectionToEnum(final BooleanCollectionToEnum booleanCollectionToEnum) {
    this.booleanCollectionToEnum = Objects.requireNonNull(booleanCollectionToEnum);
  }

  public void setBooleanCollectionToEnumArray(
      final BooleanCollectionToEnumArray booleanCollectionToEnumArray) {
    this.booleanCollectionToEnumArray = Objects.requireNonNull(booleanCollectionToEnumArray);
  }

  public void setBooleanCollectionToEnumCollection(
      final BooleanCollectionToEnumCollection booleanCollectionToEnumCollection) {
    this.booleanCollectionToEnumCollection =
        Objects.requireNonNull(booleanCollectionToEnumCollection);
  }

  public void setBooleanCollectionToFloatPrim(
      final BooleanCollectionToFloatPrim booleanCollectionToFloatPrim) {
    this.booleanCollectionToFloatPrim = Objects.requireNonNull(booleanCollectionToFloatPrim);
  }

  public void setBooleanCollectionToFloatPrimArray(
      final BooleanCollectionToFloatPrimArray booleanCollectionToFloatPrimArray) {
    this.booleanCollectionToFloatPrimArray =
        Objects.requireNonNull(booleanCollectionToFloatPrimArray);
  }

  public void setBooleanCollectionToIntegerPrim(
      final BooleanCollectionToIntegerPrim booleanCollectionToIntegerPrim) {
    this.booleanCollectionToIntegerPrim = Objects.requireNonNull(booleanCollectionToIntegerPrim);
  }

  public void setBooleanCollectionToIntegerPrimArray(
      final BooleanCollectionToIntegerPrimArray booleanCollectionToIntegerPrimArray) {
    this.booleanCollectionToIntegerPrimArray =
        Objects.requireNonNull(booleanCollectionToIntegerPrimArray);
  }

  public void setBooleanCollectionToLongPrim(
      final BooleanCollectionToLongPrim booleanCollectionToLongPrim) {
    this.booleanCollectionToLongPrim = Objects.requireNonNull(booleanCollectionToLongPrim);
  }

  public void setBooleanCollectionToLongPrimArray(
      final BooleanCollectionToLongPrimArray booleanCollectionToLongPrimArray) {
    this.booleanCollectionToLongPrimArray =
        Objects.requireNonNull(booleanCollectionToLongPrimArray);
  }

  public void setBooleanCollectionToNumberBased(
      final BooleanCollectionToNumberBased booleanCollectionToNumberBased) {
    this.booleanCollectionToNumberBased = Objects.requireNonNull(booleanCollectionToNumberBased);
  }

  public void setBooleanCollectionToNumberBasedArray(
      final BooleanCollectionToNumberBasedArray booleanCollectionToNumberBasedArray) {
    this.booleanCollectionToNumberBasedArray =
        Objects.requireNonNull(booleanCollectionToNumberBasedArray);
  }

  public void setBooleanCollectionToNumberBasedCollection(
      final BooleanCollectionToNumberBasedCollection booleanCollectionToNumberBasedCollection) {
    this.booleanCollectionToNumberBasedCollection =
        Objects.requireNonNull(booleanCollectionToNumberBasedCollection);
  }

  public void setBooleanCollectionToObjectId(
      final BooleanCollectionToObjectId booleanCollectionToObjectId) {
    this.booleanCollectionToObjectId = Objects.requireNonNull(booleanCollectionToObjectId);
  }

  public void setBooleanCollectionToObjectIdArray(
      final BooleanCollectionToObjectIdArray booleanCollectionToObjectIdArray) {
    this.booleanCollectionToObjectIdArray =
        Objects.requireNonNull(booleanCollectionToObjectIdArray);
  }

  public void setBooleanCollectionToObjectIdCollection(
      final BooleanCollectionToObjectIdCollection booleanCollectionToObjectIdCollection) {
    this.booleanCollectionToObjectIdCollection =
        Objects.requireNonNull(booleanCollectionToObjectIdCollection);
  }

  public void setBooleanCollectionToShortPrim(
      final BooleanCollectionToShortPrim booleanCollectionToShortPrim) {
    this.booleanCollectionToShortPrim = Objects.requireNonNull(booleanCollectionToShortPrim);
  }

  public void setBooleanCollectionToShortPrimArray(
      final BooleanCollectionToShortPrimArray booleanCollectionToShortPrimArray) {
    this.booleanCollectionToShortPrimArray =
        Objects.requireNonNull(booleanCollectionToShortPrimArray);
  }

  public void setBooleanCollectionToStringBased(
      final BooleanCollectionToStringBased booleanCollectionToStringBased) {
    this.booleanCollectionToStringBased = Objects.requireNonNull(booleanCollectionToStringBased);
  }

  public void setBooleanCollectionToStringBasedArray(
      final BooleanCollectionToStringBasedArray booleanCollectionToStringBasedArray) {
    this.booleanCollectionToStringBasedArray =
        Objects.requireNonNull(booleanCollectionToStringBasedArray);
  }

  public void setBooleanCollectionToStringBasedCollection(
      final BooleanCollectionToStringBasedCollection booleanCollectionToStringBasedCollection) {
    this.booleanCollectionToStringBasedCollection =
        Objects.requireNonNull(booleanCollectionToStringBasedCollection);
  }

  public void setBooleanToBinary(final BooleanToBinary booleanToBinary) {
    this.booleanToBinary = Objects.requireNonNull(booleanToBinary);
  }

  public void setBooleanToBinaryArray(final BooleanToBinaryArray booleanToBinaryArray) {
    this.booleanToBinaryArray = Objects.requireNonNull(booleanToBinaryArray);
  }

  public void setBooleanToBinaryCollection(
      final BooleanToBinaryCollection booleanToBinaryCollection) {
    this.booleanToBinaryCollection = Objects.requireNonNull(booleanToBinaryCollection);
  }

  public void setBooleanToBoolean(final BooleanToBoolean booleanToBoolean) {
    this.booleanToBoolean = Objects.requireNonNull(booleanToBoolean);
  }

  public void setBooleanToBooleanArray(final BooleanToBooleanArray booleanToBooleanArray) {
    this.booleanToBooleanArray = Objects.requireNonNull(booleanToBooleanArray);
  }

  public void setBooleanToBooleanCollection(
      final BooleanToBooleanCollection booleanToBooleanCollection) {
    this.booleanToBooleanCollection = Objects.requireNonNull(booleanToBooleanCollection);
  }

  public void setBooleanToBooleanPrim(final BooleanToBooleanPrim booleanToBooleanPrim) {
    this.booleanToBooleanPrim = Objects.requireNonNull(booleanToBooleanPrim);
  }

  public void setBooleanToBooleanPrimArray(
      final BooleanToBooleanPrimArray booleanToBooleanPrimArray) {
    this.booleanToBooleanPrimArray = Objects.requireNonNull(booleanToBooleanPrimArray);
  }

  public void setBooleanToByte(final BooleanToByte booleanToByte) {
    this.booleanToByte = Objects.requireNonNull(booleanToByte);
  }

  public void setBooleanToByteArray(final BooleanToByteArray booleanToByteArray) {
    this.booleanToByteArray = Objects.requireNonNull(booleanToByteArray);
  }

  public void setBooleanToByteCollection(final BooleanToByteCollection booleanToByteCollection) {
    this.booleanToByteCollection = Objects.requireNonNull(booleanToByteCollection);
  }

  public void setBooleanToBytePrim(final BooleanToBytePrim booleanToBytePrim) {
    this.booleanToBytePrim = Objects.requireNonNull(booleanToBytePrim);
  }

  public void setBooleanToBytePrimArray(final BooleanToBytePrimArray booleanToBytePrimArray) {
    this.booleanToBytePrimArray = Objects.requireNonNull(booleanToBytePrimArray);
  }

  public void setBooleanToCharacter(final BooleanToCharacter booleanToCharacter) {
    this.booleanToCharacter = Objects.requireNonNull(booleanToCharacter);
  }

  public void setBooleanToCharacterArray(final BooleanToCharacterArray booleanToCharacterArray) {
    this.booleanToCharacterArray = Objects.requireNonNull(booleanToCharacterArray);
  }

  public void setBooleanToCharacterCollection(
      final BooleanToCharacterCollection booleanToCharacterCollection) {
    this.booleanToCharacterCollection = Objects.requireNonNull(booleanToCharacterCollection);
  }

  public void setBooleanToCharacterPrim(final BooleanToCharacterPrim booleanToCharacterPrim) {
    this.booleanToCharacterPrim = Objects.requireNonNull(booleanToCharacterPrim);
  }

  public void setBooleanToCharacterPrimArray(
      final BooleanToCharacterPrimArray booleanToCharacterPrimArray) {
    this.booleanToCharacterPrimArray = Objects.requireNonNull(booleanToCharacterPrimArray);
  }

  public void setBooleanToDateBased(final BooleanToDateBased booleanToDateBased) {
    this.booleanToDateBased = Objects.requireNonNull(booleanToDateBased);
  }

  public void setBooleanToDateBasedArray(final BooleanToDateBasedArray booleanToDateBasedArray) {
    this.booleanToDateBasedArray = Objects.requireNonNull(booleanToDateBasedArray);
  }

  public void setBooleanToDateBasedCollection(
      final BooleanToDateBasedCollection booleanToDateBasedCollection) {
    this.booleanToDateBasedCollection = Objects.requireNonNull(booleanToDateBasedCollection);
  }

  public void setBooleanToDoublePrim(final BooleanToDoublePrim booleanToDoublePrim) {
    this.booleanToDoublePrim = Objects.requireNonNull(booleanToDoublePrim);
  }

  public void setBooleanToDoublePrimArray(final BooleanToDoublePrimArray booleanToDoublePrimArray) {
    this.booleanToDoublePrimArray = Objects.requireNonNull(booleanToDoublePrimArray);
  }

  public void setBooleanToEnum(final BooleanToEnum booleanToEnum) {
    this.booleanToEnum = Objects.requireNonNull(booleanToEnum);
  }

  public void setBooleanToEnumArray(final BooleanToEnumArray booleanToEnumArray) {
    this.booleanToEnumArray = Objects.requireNonNull(booleanToEnumArray);
  }

  public void setBooleanToEnumCollection(final BooleanToEnumCollection booleanToEnumCollection) {
    this.booleanToEnumCollection = Objects.requireNonNull(booleanToEnumCollection);
  }

  public void setBooleanToFloatPrim(final BooleanToFloatPrim booleanToFloatPrim) {
    this.booleanToFloatPrim = Objects.requireNonNull(booleanToFloatPrim);
  }

  public void setBooleanToFloatPrimArray(final BooleanToFloatPrimArray booleanToFloatPrimArray) {
    this.booleanToFloatPrimArray = Objects.requireNonNull(booleanToFloatPrimArray);
  }

  public void setBooleanToIntegerPrim(final BooleanToIntegerPrim booleanToIntegerPrim) {
    this.booleanToIntegerPrim = Objects.requireNonNull(booleanToIntegerPrim);
  }

  public void setBooleanToIntegerPrimArray(
      final BooleanToIntegerPrimArray booleanToIntegerPrimArray) {
    this.booleanToIntegerPrimArray = Objects.requireNonNull(booleanToIntegerPrimArray);
  }

  public void setBooleanToLongPrim(final BooleanToLongPrim booleanToLongPrim) {
    this.booleanToLongPrim = Objects.requireNonNull(booleanToLongPrim);
  }

  public void setBooleanToLongPrimArray(final BooleanToLongPrimArray booleanToLongPrimArray) {
    this.booleanToLongPrimArray = Objects.requireNonNull(booleanToLongPrimArray);
  }

  public void setBooleanToNumberBased(final BooleanToNumberBased booleanToNumberBased) {
    this.booleanToNumberBased = Objects.requireNonNull(booleanToNumberBased);
  }

  public void setBooleanToNumberBasedArray(
      final BooleanToNumberBasedArray booleanToNumberBasedArray) {
    this.booleanToNumberBasedArray = Objects.requireNonNull(booleanToNumberBasedArray);
  }

  public void setBooleanToNumberBasedCollection(
      final BooleanToNumberBasedCollection booleanToNumberBasedCollection) {
    this.booleanToNumberBasedCollection = Objects.requireNonNull(booleanToNumberBasedCollection);
  }

  public void setBooleanToObjectId(final BooleanToObjectId booleanToObjectId) {
    this.booleanToObjectId = Objects.requireNonNull(booleanToObjectId);
  }

  public void setBooleanToObjectIdArray(final BooleanToObjectIdArray booleanToObjectIdArray) {
    this.booleanToObjectIdArray = Objects.requireNonNull(booleanToObjectIdArray);
  }

  public void setBooleanToObjectIdCollection(
      final BooleanToObjectIdCollection booleanToObjectIdCollection) {
    this.booleanToObjectIdCollection = Objects.requireNonNull(booleanToObjectIdCollection);
  }

  public void setBooleanToShortPrim(final BooleanToShortPrim booleanToShortPrim) {
    this.booleanToShortPrim = Objects.requireNonNull(booleanToShortPrim);
  }

  public void setBooleanToShortPrimArray(final BooleanToShortPrimArray booleanToShortPrimArray) {
    this.booleanToShortPrimArray = Objects.requireNonNull(booleanToShortPrimArray);
  }

  public void setBooleanToStringBased(final BooleanToStringBased booleanToStringBased) {
    this.booleanToStringBased = Objects.requireNonNull(booleanToStringBased);
  }

  public void setBooleanToStringBasedArray(
      final BooleanToStringBasedArray booleanToStringBasedArray) {
    this.booleanToStringBasedArray = Objects.requireNonNull(booleanToStringBasedArray);
  }

  public void setBooleanToStringBasedCollection(
      final BooleanToStringBasedCollection booleanToStringBasedCollection) {
    this.booleanToStringBasedCollection = Objects.requireNonNull(booleanToStringBasedCollection);
  }

  public void setDateCollectionToBinary(final DateCollectionToBinary dateCollectionToBinary) {
    this.dateCollectionToBinary = Objects.requireNonNull(dateCollectionToBinary);
  }

  public void setDateCollectionToBinaryArray(
      final DateCollectionToBinaryArray dateCollectionToBinaryArray) {
    this.dateCollectionToBinaryArray = Objects.requireNonNull(dateCollectionToBinaryArray);
  }

  public void setDateCollectionToBinaryCollection(
      final DateCollectionToBinaryCollection dateCollectionToBinaryCollection) {
    this.dateCollectionToBinaryCollection =
        Objects.requireNonNull(dateCollectionToBinaryCollection);
  }

  public void setDateCollectionToBoolean(final DateCollectionToBoolean dateCollectionToBoolean) {
    this.dateCollectionToBoolean = Objects.requireNonNull(dateCollectionToBoolean);
  }

  public void setDateCollectionToBooleanArray(
      final DateCollectionToBooleanArray dateCollectionToBooleanArray) {
    this.dateCollectionToBooleanArray = Objects.requireNonNull(dateCollectionToBooleanArray);
  }

  public void setDateCollectionToBooleanCollection(
      final DateCollectionToBooleanCollection dateCollectionToBooleanCollection) {
    this.dateCollectionToBooleanCollection =
        Objects.requireNonNull(dateCollectionToBooleanCollection);
  }

  public void setDateCollectionToBooleanPrim(
      final DateCollectionToBooleanPrim dateCollectionToBooleanPrim) {
    this.dateCollectionToBooleanPrim = Objects.requireNonNull(dateCollectionToBooleanPrim);
  }

  public void setDateCollectionToBooleanPrimArray(
      final DateCollectionToBooleanPrimArray dateCollectionToBooleanPrimArray) {
    this.dateCollectionToBooleanPrimArray =
        Objects.requireNonNull(dateCollectionToBooleanPrimArray);
  }

  public void setDateCollectionToByte(final DateCollectionToByte dateCollectionToByte) {
    this.dateCollectionToByte = Objects.requireNonNull(dateCollectionToByte);
  }

  public void setDateCollectionToByteArray(
      final DateCollectionToByteArray dateCollectionToByteArray) {
    this.dateCollectionToByteArray = Objects.requireNonNull(dateCollectionToByteArray);
  }

  public void setDateCollectionToByteCollection(
      final DateCollectionToByteCollection dateCollectionToByteCollection) {
    this.dateCollectionToByteCollection = Objects.requireNonNull(dateCollectionToByteCollection);
  }

  public void setDateCollectionToBytePrim(final DateCollectionToBytePrim dateCollectionToBytePrim) {
    this.dateCollectionToBytePrim = Objects.requireNonNull(dateCollectionToBytePrim);
  }

  public void setDateCollectionToBytePrimArray(
      final DateCollectionToBytePrimArray dateCollectionToBytePrimArray) {
    this.dateCollectionToBytePrimArray = Objects.requireNonNull(dateCollectionToBytePrimArray);
  }

  public void setDateCollectionToCharacter(
      final DateCollectionToCharacter dateCollectionToCharacter) {
    this.dateCollectionToCharacter = Objects.requireNonNull(dateCollectionToCharacter);
  }

  public void setDateCollectionToCharacterArray(
      final DateCollectionToCharacterArray dateCollectionToCharacterArray) {
    this.dateCollectionToCharacterArray = Objects.requireNonNull(dateCollectionToCharacterArray);
  }

  public void setDateCollectionToCharacterCollection(
      final DateCollectionToCharacterCollection dateCollectionToCharacterCollection) {
    this.dateCollectionToCharacterCollection =
        Objects.requireNonNull(dateCollectionToCharacterCollection);
  }

  public void setDateCollectionToCharacterPrim(
      final DateCollectionToCharacterPrim dateCollectionToCharacterPrim) {
    this.dateCollectionToCharacterPrim = Objects.requireNonNull(dateCollectionToCharacterPrim);
  }

  public void setDateCollectionToCharacterPrimArray(
      final DateCollectionToCharacterPrimArray dateCollectionToCharacterPrimArray) {
    this.dateCollectionToCharacterPrimArray =
        Objects.requireNonNull(dateCollectionToCharacterPrimArray);
  }

  public void setDateCollectionToDateBased(
      final DateCollectionToDateBased dateCollectionToDateBased) {
    this.dateCollectionToDateBased = Objects.requireNonNull(dateCollectionToDateBased);
  }

  public void setDateCollectionToDateBasedArray(
      final DateCollectionToDateBasedArray dateCollectionToDateBasedArray) {
    this.dateCollectionToDateBasedArray = Objects.requireNonNull(dateCollectionToDateBasedArray);
  }

  public void setDateCollectionToDateBasedCollection(
      final DateCollectionToDateBasedCollection dateCollectionToDateBasedCollection) {
    this.dateCollectionToDateBasedCollection =
        Objects.requireNonNull(dateCollectionToDateBasedCollection);
  }

  public void setDateCollectionToDoublePrim(
      final DateCollectionToDoublePrim dateCollectionToDoublePrim) {
    this.dateCollectionToDoublePrim = Objects.requireNonNull(dateCollectionToDoublePrim);
  }

  public void setDateCollectionToDoublePrimArray(
      final DateCollectionToDoublePrimArray dateCollectionToDoublePrimArray) {
    this.dateCollectionToDoublePrimArray = Objects.requireNonNull(dateCollectionToDoublePrimArray);
  }

  public void setDateCollectionToEnum(final DateCollectionToEnum dateCollectionToEnum) {
    this.dateCollectionToEnum = Objects.requireNonNull(dateCollectionToEnum);
  }

  public void setDateCollectionToEnumArray(
      final DateCollectionToEnumArray dateCollectionToEnumArray) {
    this.dateCollectionToEnumArray = Objects.requireNonNull(dateCollectionToEnumArray);
  }

  public void setDateCollectionToEnumCollection(
      final DateCollectionToEnumCollection dateCollectionToEnumCollection) {
    this.dateCollectionToEnumCollection = Objects.requireNonNull(dateCollectionToEnumCollection);
  }

  public void setDateCollectionToFloatPrim(
      final DateCollectionToFloatPrim dateCollectionToFloatPrim) {
    this.dateCollectionToFloatPrim = Objects.requireNonNull(dateCollectionToFloatPrim);
  }

  public void setDateCollectionToFloatPrimArray(
      final DateCollectionToFloatPrimArray dateCollectionToFloatPrimArray) {
    this.dateCollectionToFloatPrimArray = Objects.requireNonNull(dateCollectionToFloatPrimArray);
  }

  public void setDateCollectionToIntegerPrim(
      final DateCollectionToIntegerPrim dateCollectionToIntegerPrim) {
    this.dateCollectionToIntegerPrim = Objects.requireNonNull(dateCollectionToIntegerPrim);
  }

  public void setDateCollectionToIntegerPrimArray(
      final DateCollectionToIntegerPrimArray dateCollectionToIntegerPrimArray) {
    this.dateCollectionToIntegerPrimArray =
        Objects.requireNonNull(dateCollectionToIntegerPrimArray);
  }

  public void setDateCollectionToLongPrim(final DateCollectionToLongPrim dateCollectionToLongPrim) {
    this.dateCollectionToLongPrim = Objects.requireNonNull(dateCollectionToLongPrim);
  }

  public void setDateCollectionToLongPrimArray(
      final DateCollectionToLongPrimArray dateCollectionToLongPrimArray) {
    this.dateCollectionToLongPrimArray = Objects.requireNonNull(dateCollectionToLongPrimArray);
  }

  public void setDateCollectionToNumberBased(
      final DateCollectionToNumberBased dateCollectionToNumberBased) {
    this.dateCollectionToNumberBased = Objects.requireNonNull(dateCollectionToNumberBased);
  }

  public void setDateCollectionToNumberBasedArray(
      final DateCollectionToNumberBasedArray dateCollectionToNumberBasedArray) {
    this.dateCollectionToNumberBasedArray =
        Objects.requireNonNull(dateCollectionToNumberBasedArray);
  }

  public void setDateCollectionToNumberBasedCollection(
      final DateCollectionToNumberBasedCollection dateCollectionToNumberBasedCollection) {
    this.dateCollectionToNumberBasedCollection =
        Objects.requireNonNull(dateCollectionToNumberBasedCollection);
  }

  public void setDateCollectionToObjectId(final DateCollectionToObjectId dateCollectionToObjectId) {
    this.dateCollectionToObjectId = Objects.requireNonNull(dateCollectionToObjectId);
  }

  public void setDateCollectionToObjectIdArray(
      final DateCollectionToObjectIdArray dateCollectionToObjectIdArray) {
    this.dateCollectionToObjectIdArray = Objects.requireNonNull(dateCollectionToObjectIdArray);
  }

  public void setDateCollectionToObjectIdCollection(
      final DateCollectionToObjectIdCollection dateCollectionToObjectIdCollection) {
    this.dateCollectionToObjectIdCollection =
        Objects.requireNonNull(dateCollectionToObjectIdCollection);
  }

  public void setDateCollectionToShortPrim(
      final DateCollectionToShortPrim dateCollectionToShortPrim) {
    this.dateCollectionToShortPrim = Objects.requireNonNull(dateCollectionToShortPrim);
  }

  public void setDateCollectionToShortPrimArray(
      final DateCollectionToShortPrimArray dateCollectionToShortPrimArray) {
    this.dateCollectionToShortPrimArray = Objects.requireNonNull(dateCollectionToShortPrimArray);
  }

  public void setDateCollectionToStringBased(
      final DateCollectionToStringBased dateCollectionToStringBased) {
    this.dateCollectionToStringBased = Objects.requireNonNull(dateCollectionToStringBased);
  }

  public void setDateCollectionToStringBasedArray(
      final DateCollectionToStringBasedArray dateCollectionToStringBasedArray) {
    this.dateCollectionToStringBasedArray =
        Objects.requireNonNull(dateCollectionToStringBasedArray);
  }

  public void setDateCollectionToStringBasedCollection(
      final DateCollectionToStringBasedCollection dateCollectionToStringBasedCollection) {
    this.dateCollectionToStringBasedCollection =
        Objects.requireNonNull(dateCollectionToStringBasedCollection);
  }

  public void setDateToBinary(final DateToBinary dateToBinary) {
    this.dateToBinary = Objects.requireNonNull(dateToBinary);
  }

  public void setDateToBinaryArray(final DateToBinaryArray dateToBinaryArray) {
    this.dateToBinaryArray = Objects.requireNonNull(dateToBinaryArray);
  }

  public void setDateToBinaryCollection(final DateToBinaryCollection dateToBinaryCollection) {
    this.dateToBinaryCollection = Objects.requireNonNull(dateToBinaryCollection);
  }

  public void setDateToBoolean(final DateToBoolean dateToBoolean) {
    this.dateToBoolean = Objects.requireNonNull(dateToBoolean);
  }

  public void setDateToBooleanArray(final DateToBooleanArray dateToBooleanArray) {
    this.dateToBooleanArray = Objects.requireNonNull(dateToBooleanArray);
  }

  public void setDateToBooleanCollection(final DateToBooleanCollection dateToBooleanCollection) {
    this.dateToBooleanCollection = Objects.requireNonNull(dateToBooleanCollection);
  }

  public void setDateToBooleanPrim(final DateToBooleanPrim dateToBooleanPrim) {
    this.dateToBooleanPrim = Objects.requireNonNull(dateToBooleanPrim);
  }

  public void setDateToBooleanPrimArray(final DateToBooleanPrimArray dateToBooleanPrimArray) {
    this.dateToBooleanPrimArray = Objects.requireNonNull(dateToBooleanPrimArray);
  }

  public void setDateToByte(final DateToByte dateToByte) {
    this.dateToByte = Objects.requireNonNull(dateToByte);
  }

  public void setDateToByteArray(final DateToByteArray dateToByteArray) {
    this.dateToByteArray = Objects.requireNonNull(dateToByteArray);
  }

  public void setDateToByteCollection(final DateToByteCollection dateToByteCollection) {
    this.dateToByteCollection = Objects.requireNonNull(dateToByteCollection);
  }

  public void setDateToBytePrim(final DateToBytePrim dateToBytePrim) {
    this.dateToBytePrim = Objects.requireNonNull(dateToBytePrim);
  }

  public void setDateToBytePrimArray(final DateToBytePrimArray dateToBytePrimArray) {
    this.dateToBytePrimArray = Objects.requireNonNull(dateToBytePrimArray);
  }

  public void setDateToCharacter(final DateToCharacter dateToCharacter) {
    this.dateToCharacter = Objects.requireNonNull(dateToCharacter);
  }

  public void setDateToCharacterArray(final DateToCharacterArray dateToCharacterArray) {
    this.dateToCharacterArray = Objects.requireNonNull(dateToCharacterArray);
  }

  public void setDateToCharacterCollection(
      final DateToCharacterCollection dateToCharacterCollection) {
    this.dateToCharacterCollection = Objects.requireNonNull(dateToCharacterCollection);
  }

  public void setDateToCharacterPrim(final DateToCharacterPrim dateToCharacterPrim) {
    this.dateToCharacterPrim = Objects.requireNonNull(dateToCharacterPrim);
  }

  public void setDateToCharacterPrimArray(final DateToCharacterPrimArray dateToCharacterPrimArray) {
    this.dateToCharacterPrimArray = Objects.requireNonNull(dateToCharacterPrimArray);
  }

  public void setDateToDateBased(final DateToDateBased dateToDateBased) {
    this.dateToDateBased = Objects.requireNonNull(dateToDateBased);
  }

  public void setDateToDateBasedArray(final DateToDateBasedArray dateToDateBasedArray) {
    this.dateToDateBasedArray = Objects.requireNonNull(dateToDateBasedArray);
  }

  public void setDateToDateBasedCollection(
      final DateToDateBasedCollection dateToDateBasedCollection) {
    this.dateToDateBasedCollection = Objects.requireNonNull(dateToDateBasedCollection);
  }

  public void setDateToDoublePrim(final DateToDoublePrim dateToDoublePrim) {
    this.dateToDoublePrim = Objects.requireNonNull(dateToDoublePrim);
  }

  public void setDateToDoublePrimArray(final DateToDoublePrimArray dateToDoublePrimArray) {
    this.dateToDoublePrimArray = Objects.requireNonNull(dateToDoublePrimArray);
  }

  public void setDateToEnum(final DateToEnum dateToEnum) {
    this.dateToEnum = Objects.requireNonNull(dateToEnum);
  }

  public void setDateToEnumArray(final DateToEnumArray dateToEnumArray) {
    this.dateToEnumArray = Objects.requireNonNull(dateToEnumArray);
  }

  public void setDateToEnumCollection(final DateToEnumCollection dateToEnumCollection) {
    this.dateToEnumCollection = Objects.requireNonNull(dateToEnumCollection);
  }

  public void setDateToFloatPrim(final DateToFloatPrim dateToFloatPrim) {
    this.dateToFloatPrim = Objects.requireNonNull(dateToFloatPrim);
  }

  public void setDateToFloatPrimArray(final DateToFloatPrimArray dateToFloatPrimArray) {
    this.dateToFloatPrimArray = Objects.requireNonNull(dateToFloatPrimArray);
  }

  public void setDateToIntegerPrim(final DateToIntegerPrim dateToIntegerPrim) {
    this.dateToIntegerPrim = Objects.requireNonNull(dateToIntegerPrim);
  }

  public void setDateToIntegerPrimArray(final DateToIntegerPrimArray dateToIntegerPrimArray) {
    this.dateToIntegerPrimArray = Objects.requireNonNull(dateToIntegerPrimArray);
  }

  public void setDateToLongPrim(final DateToLongPrim dateToLongPrim) {
    this.dateToLongPrim = Objects.requireNonNull(dateToLongPrim);
  }

  public void setDateToLongPrimArray(final DateToLongPrimArray dateToLongPrimArray) {
    this.dateToLongPrimArray = Objects.requireNonNull(dateToLongPrimArray);
  }

  public void setDateToNumberBased(final DateToNumberBased dateToNumberBased) {
    this.dateToNumberBased = Objects.requireNonNull(dateToNumberBased);
  }

  public void setDateToNumberBasedArray(final DateToNumberBasedArray dateToNumberBasedArray) {
    this.dateToNumberBasedArray = Objects.requireNonNull(dateToNumberBasedArray);
  }

  public void setDateToNumberBasedCollection(
      final DateToNumberBasedCollection dateToNumberBasedCollection) {
    this.dateToNumberBasedCollection = Objects.requireNonNull(dateToNumberBasedCollection);
  }

  public void setDateToObjectId(final DateToObjectId dateToObjectId) {
    this.dateToObjectId = Objects.requireNonNull(dateToObjectId);
  }

  public void setDateToObjectIdArray(final DateToObjectIdArray dateToObjectIdArray) {
    this.dateToObjectIdArray = Objects.requireNonNull(dateToObjectIdArray);
  }

  public void setDateToObjectIdCollection(final DateToObjectIdCollection dateToObjectIdCollection) {
    this.dateToObjectIdCollection = Objects.requireNonNull(dateToObjectIdCollection);
  }

  public void setDateToShortPrim(final DateToShortPrim dateToShortPrim) {
    this.dateToShortPrim = Objects.requireNonNull(dateToShortPrim);
  }

  public void setDateToShortPrimArray(final DateToShortPrimArray dateToShortPrimArray) {
    this.dateToShortPrimArray = Objects.requireNonNull(dateToShortPrimArray);
  }

  public void setDateToStringBased(final DateToStringBased dateToStringBased) {
    this.dateToStringBased = Objects.requireNonNull(dateToStringBased);
  }

  public void setDateToStringBasedArray(final DateToStringBasedArray dateToStringBasedArray) {
    this.dateToStringBasedArray = Objects.requireNonNull(dateToStringBasedArray);
  }

  public void setDateToStringBasedCollection(
      final DateToStringBasedCollection dateToStringBasedCollection) {
    this.dateToStringBasedCollection = Objects.requireNonNull(dateToStringBasedCollection);
  }

  public void setNumberCollectionToBinary(final NumberCollectionToBinary numberCollectionToBinary) {
    this.numberCollectionToBinary = Objects.requireNonNull(numberCollectionToBinary);
  }

  public void setNumberCollectionToBinaryArray(
      final NumberCollectionToBinaryArray numberCollectionToBinaryArray) {
    this.numberCollectionToBinaryArray = Objects.requireNonNull(numberCollectionToBinaryArray);
  }

  public void setNumberCollectionToBinaryCollection(
      final NumberCollectionToBinaryCollection numberCollectionToBinaryCollection) {
    this.numberCollectionToBinaryCollection =
        Objects.requireNonNull(numberCollectionToBinaryCollection);
  }

  public void setNumberCollectionToBoolean(
      final NumberCollectionToBoolean numberCollectionToBoolean) {
    this.numberCollectionToBoolean = Objects.requireNonNull(numberCollectionToBoolean);
  }

  public void setNumberCollectionToBooleanArray(
      final NumberCollectionToBooleanArray numberCollectionToBooleanArray) {
    this.numberCollectionToBooleanArray = Objects.requireNonNull(numberCollectionToBooleanArray);
  }

  public void setNumberCollectionToBooleanCollection(
      final NumberCollectionToBooleanCollection numberCollectionToBooleanCollection) {
    this.numberCollectionToBooleanCollection =
        Objects.requireNonNull(numberCollectionToBooleanCollection);
  }

  public void setNumberCollectionToBooleanPrim(
      final NumberCollectionToBooleanPrim numberCollectionToBooleanPrim) {
    this.numberCollectionToBooleanPrim = Objects.requireNonNull(numberCollectionToBooleanPrim);
  }

  public void setNumberCollectionToBooleanPrimArray(
      final NumberCollectionToBooleanPrimArray numberCollectionToBooleanPrimArray) {
    this.numberCollectionToBooleanPrimArray =
        Objects.requireNonNull(numberCollectionToBooleanPrimArray);
  }

  public void setNumberCollectionToByte(final NumberCollectionToByte numberCollectionToByte) {
    this.numberCollectionToByte = Objects.requireNonNull(numberCollectionToByte);
  }

  public void setNumberCollectionToByteArray(
      final NumberCollectionToByteArray numberCollectionToByteArray) {
    this.numberCollectionToByteArray = Objects.requireNonNull(numberCollectionToByteArray);
  }

  public void setNumberCollectionToByteCollection(
      final NumberCollectionToByteCollection numberCollectionToByteCollection) {
    this.numberCollectionToByteCollection =
        Objects.requireNonNull(numberCollectionToByteCollection);
  }

  public void setNumberCollectionToBytePrim(
      final NumberCollectionToBytePrim numberCollectionToBytePrim) {
    this.numberCollectionToBytePrim = Objects.requireNonNull(numberCollectionToBytePrim);
  }

  public void setNumberCollectionToBytePrimArray(
      final NumberCollectionToBytePrimArray numberCollectionToBytePrimArray) {
    this.numberCollectionToBytePrimArray = Objects.requireNonNull(numberCollectionToBytePrimArray);
  }

  public void setNumberCollectionToCharacter(
      final NumberCollectionToCharacter numberCollectionToCharacter) {
    this.numberCollectionToCharacter = Objects.requireNonNull(numberCollectionToCharacter);
  }

  public void setNumberCollectionToCharacterArray(
      final NumberCollectionToCharacterArray numberCollectionToCharacterArray) {
    this.numberCollectionToCharacterArray =
        Objects.requireNonNull(numberCollectionToCharacterArray);
  }

  public void setNumberCollectionToCharacterCollection(
      final NumberCollectionToCharacterCollection numberCollectionToCharacterCollection) {
    this.numberCollectionToCharacterCollection =
        Objects.requireNonNull(numberCollectionToCharacterCollection);
  }

  public void setNumberCollectionToCharacterPrim(
      final NumberCollectionToCharacterPrim numberCollectionToCharacterPrim) {
    this.numberCollectionToCharacterPrim = Objects.requireNonNull(numberCollectionToCharacterPrim);
  }

  public void setNumberCollectionToCharacterPrimArray(
      final NumberCollectionToCharacterPrimArray numberCollectionToCharacterPrimArray) {
    this.numberCollectionToCharacterPrimArray =
        Objects.requireNonNull(numberCollectionToCharacterPrimArray);
  }

  public void setNumberCollectionToDateBased(
      final NumberCollectionToDateBased numberCollectionToDateBased) {
    this.numberCollectionToDateBased = Objects.requireNonNull(numberCollectionToDateBased);
  }

  public void setNumberCollectionToDateBasedArray(
      final NumberCollectionToDateBasedArray numberCollectionToDateBasedArray) {
    this.numberCollectionToDateBasedArray =
        Objects.requireNonNull(numberCollectionToDateBasedArray);
  }

  public void setNumberCollectionToDateBasedCollection(
      final NumberCollectionToDateBasedCollection numberCollectionToDateBasedCollection) {
    this.numberCollectionToDateBasedCollection =
        Objects.requireNonNull(numberCollectionToDateBasedCollection);
  }

  public void setNumberCollectionToDoublePrim(
      final NumberCollectionToDoublePrim numberCollectionToDoublePrim) {
    this.numberCollectionToDoublePrim = Objects.requireNonNull(numberCollectionToDoublePrim);
  }

  public void setNumberCollectionToDoublePrimArray(
      final NumberCollectionToDoublePrimArray numberCollectionToDoublePrimArray) {
    this.numberCollectionToDoublePrimArray =
        Objects.requireNonNull(numberCollectionToDoublePrimArray);
  }

  public void setNumberCollectionToEnum(final NumberCollectionToEnum numberCollectionToEnum) {
    this.numberCollectionToEnum = Objects.requireNonNull(numberCollectionToEnum);
  }

  public void setNumberCollectionToEnumArray(
      final NumberCollectionToEnumArray numberCollectionToEnumArray) {
    this.numberCollectionToEnumArray = Objects.requireNonNull(numberCollectionToEnumArray);
  }

  public void setNumberCollectionToEnumCollection(
      final NumberCollectionToEnumCollection numberCollectionToEnumCollection) {
    this.numberCollectionToEnumCollection =
        Objects.requireNonNull(numberCollectionToEnumCollection);
  }

  public void setNumberCollectionToFloatPrim(
      final NumberCollectionToFloatPrim numberCollectionToFloatPrim) {
    this.numberCollectionToFloatPrim = Objects.requireNonNull(numberCollectionToFloatPrim);
  }

  public void setNumberCollectionToFloatPrimArray(
      final NumberCollectionToFloatPrimArray numberCollectionToFloatPrimArray) {
    this.numberCollectionToFloatPrimArray =
        Objects.requireNonNull(numberCollectionToFloatPrimArray);
  }

  public void setNumberCollectionToIntegerPrim(
      final NumberCollectionToIntegerPrim numberCollectionToIntegerPrim) {
    this.numberCollectionToIntegerPrim = Objects.requireNonNull(numberCollectionToIntegerPrim);
  }

  public void setNumberCollectionToIntegerPrimArray(
      final NumberCollectionToIntegerPrimArray numberCollectionToIntegerPrimArray) {
    this.numberCollectionToIntegerPrimArray =
        Objects.requireNonNull(numberCollectionToIntegerPrimArray);
  }

  public void setNumberCollectionToLongPrim(
      final NumberCollectionToLongPrim numberCollectionToLongPrim) {
    this.numberCollectionToLongPrim = Objects.requireNonNull(numberCollectionToLongPrim);
  }

  public void setNumberCollectionToLongPrimArray(
      final NumberCollectionToLongPrimArray numberCollectionToLongPrimArray) {
    this.numberCollectionToLongPrimArray = Objects.requireNonNull(numberCollectionToLongPrimArray);
  }

  public void setNumberCollectionToNumberBased(
      final NumberCollectionToNumberBased numberCollectionToNumberBased) {
    this.numberCollectionToNumberBased = Objects.requireNonNull(numberCollectionToNumberBased);
  }

  public void setNumberCollectionToNumberBasedArray(
      final NumberCollectionToNumberBasedArray numberCollectionToNumberBasedArray) {
    this.numberCollectionToNumberBasedArray =
        Objects.requireNonNull(numberCollectionToNumberBasedArray);
  }

  public void setNumberCollectionToNumberBasedCollection(
      final NumberCollectionToNumberBasedCollection numberCollectionToNumberBasedCollection) {
    this.numberCollectionToNumberBasedCollection =
        Objects.requireNonNull(numberCollectionToNumberBasedCollection);
  }

  public void setNumberCollectionToObjectId(
      final NumberCollectionToObjectId numberCollectionToObjectId) {
    this.numberCollectionToObjectId = Objects.requireNonNull(numberCollectionToObjectId);
  }

  public void setNumberCollectionToObjectIdArray(
      final NumberCollectionToObjectIdArray numberCollectionToObjectIdArray) {
    this.numberCollectionToObjectIdArray = Objects.requireNonNull(numberCollectionToObjectIdArray);
  }

  public void setNumberCollectionToObjectIdCollection(
      final NumberCollectionToObjectIdCollection numberCollectionToObjectIdCollection) {
    this.numberCollectionToObjectIdCollection =
        Objects.requireNonNull(numberCollectionToObjectIdCollection);
  }

  public void setNumberCollectionToShortPrim(
      final NumberCollectionToShortPrim numberCollectionToShortPrim) {
    this.numberCollectionToShortPrim = Objects.requireNonNull(numberCollectionToShortPrim);
  }

  public void setNumberCollectionToShortPrimArray(
      final NumberCollectionToShortPrimArray numberCollectionToShortPrimArray) {
    this.numberCollectionToShortPrimArray =
        Objects.requireNonNull(numberCollectionToShortPrimArray);
  }

  public void setNumberCollectionToStringBased(
      final NumberCollectionToStringBased numberCollectionToStringBased) {
    this.numberCollectionToStringBased = Objects.requireNonNull(numberCollectionToStringBased);
  }

  public void setNumberCollectionToStringBasedArray(
      final NumberCollectionToStringBasedArray numberCollectionToStringBasedArray) {
    this.numberCollectionToStringBasedArray =
        Objects.requireNonNull(numberCollectionToStringBasedArray);
  }

  public void setNumberCollectionToStringBasedCollection(
      final NumberCollectionToStringBasedCollection numberCollectionToStringBasedCollection) {
    this.numberCollectionToStringBasedCollection =
        Objects.requireNonNull(numberCollectionToStringBasedCollection);
  }

  public void setNumberToBinary(final NumberToBinary numberToBinary) {
    this.numberToBinary = Objects.requireNonNull(numberToBinary);
  }

  public void setNumberToBinaryArray(final NumberToBinaryArray numberToBinaryArray) {
    this.numberToBinaryArray = Objects.requireNonNull(numberToBinaryArray);
  }

  public void setNumberToBinaryCollection(final NumberToBinaryCollection numberToBinaryCollection) {
    this.numberToBinaryCollection = Objects.requireNonNull(numberToBinaryCollection);
  }

  public void setNumberToBoolean(final NumberToBoolean numberToBoolean) {
    this.numberToBoolean = Objects.requireNonNull(numberToBoolean);
  }

  public void setNumberToBooleanArray(final NumberToBooleanArray numberToBooleanArray) {
    this.numberToBooleanArray = Objects.requireNonNull(numberToBooleanArray);
  }

  public void setNumberToBooleanCollection(
      final NumberToBooleanCollection numberToBooleanCollection) {
    this.numberToBooleanCollection = Objects.requireNonNull(numberToBooleanCollection);
  }

  public void setNumberToBooleanPrim(final NumberToBooleanPrim numberToBooleanPrim) {
    this.numberToBooleanPrim = Objects.requireNonNull(numberToBooleanPrim);
  }

  public void setNumberToBooleanPrimArray(final NumberToBooleanPrimArray numberToBooleanPrimArray) {
    this.numberToBooleanPrimArray = Objects.requireNonNull(numberToBooleanPrimArray);
  }

  public void setNumberToByte(final NumberToByte numberToByte) {
    this.numberToByte = Objects.requireNonNull(numberToByte);
  }

  public void setNumberToByteArray(final NumberToByteArray numberToByteArray) {
    this.numberToByteArray = Objects.requireNonNull(numberToByteArray);
  }

  public void setNumberToByteCollection(final NumberToByteCollection numberToByteCollection) {
    this.numberToByteCollection = Objects.requireNonNull(numberToByteCollection);
  }

  public void setNumberToBytePrim(final NumberToBytePrim numberToBytePrim) {
    this.numberToBytePrim = Objects.requireNonNull(numberToBytePrim);
  }

  public void setNumberToBytePrimArray(final NumberToBytePrimArray numberToBytePrimArray) {
    this.numberToBytePrimArray = Objects.requireNonNull(numberToBytePrimArray);
  }

  public void setNumberToCharacter(final NumberToCharacter numberToCharacter) {
    this.numberToCharacter = Objects.requireNonNull(numberToCharacter);
  }

  public void setNumberToCharacterArray(final NumberToCharacterArray numberToCharacterArray) {
    this.numberToCharacterArray = Objects.requireNonNull(numberToCharacterArray);
  }

  public void setNumberToCharacterCollection(
      final NumberToCharacterCollection numberToCharacterCollection) {
    this.numberToCharacterCollection = Objects.requireNonNull(numberToCharacterCollection);
  }

  public void setNumberToCharacterPrim(final NumberToCharacterPrim numberToCharacterPrim) {
    this.numberToCharacterPrim = Objects.requireNonNull(numberToCharacterPrim);
  }

  public void setNumberToCharacterPrimArray(
      final NumberToCharacterPrimArray numberToCharacterPrimArray) {
    this.numberToCharacterPrimArray = Objects.requireNonNull(numberToCharacterPrimArray);
  }

  public void setNumberToDateBased(final NumberToDateBased numberToDateBased) {
    this.numberToDateBased = Objects.requireNonNull(numberToDateBased);
  }

  public void setNumberToDateBasedArray(final NumberToDateBasedArray numberToDateBasedArray) {
    this.numberToDateBasedArray = Objects.requireNonNull(numberToDateBasedArray);
  }

  public void setNumberToDateBasedCollection(
      final NumberToDateBasedCollection numberToDateBasedCollection) {
    this.numberToDateBasedCollection = Objects.requireNonNull(numberToDateBasedCollection);
  }

  public void setNumberToDoublePrim(final NumberToDoublePrim numberToDoublePrim) {
    this.numberToDoublePrim = Objects.requireNonNull(numberToDoublePrim);
  }

  public void setNumberToDoublePrimArray(final NumberToDoublePrimArray numberToDoublePrimArray) {
    this.numberToDoublePrimArray = Objects.requireNonNull(numberToDoublePrimArray);
  }

  public void setNumberToEnum(final NumberToEnum numberToEnum) {
    this.numberToEnum = Objects.requireNonNull(numberToEnum);
  }

  public void setNumberToEnumArray(final NumberToEnumArray numberToEnumArray) {
    this.numberToEnumArray = Objects.requireNonNull(numberToEnumArray);
  }

  public void setNumberToEnumCollection(final NumberToEnumCollection numberToEnumCollection) {
    this.numberToEnumCollection = Objects.requireNonNull(numberToEnumCollection);
  }

  public void setNumberToFloatPrim(final NumberToFloatPrim numberToFloatPrim) {
    this.numberToFloatPrim = Objects.requireNonNull(numberToFloatPrim);
  }

  public void setNumberToFloatPrimArray(final NumberToFloatPrimArray numberToFloatPrimArray) {
    this.numberToFloatPrimArray = Objects.requireNonNull(numberToFloatPrimArray);
  }

  public void setNumberToIntegerPrim(final NumberToIntegerPrim numberToIntegerPrim) {
    this.numberToIntegerPrim = Objects.requireNonNull(numberToIntegerPrim);
  }

  public void setNumberToIntegerPrimArray(final NumberToIntegerPrimArray numberToIntegerPrimArray) {
    this.numberToIntegerPrimArray = Objects.requireNonNull(numberToIntegerPrimArray);
  }

  public void setNumberToLongPrim(final NumberToLongPrim numberToLongPrim) {
    this.numberToLongPrim = Objects.requireNonNull(numberToLongPrim);
  }

  public void setNumberToLongPrimArray(final NumberToLongPrimArray numberToLongPrimArray) {
    this.numberToLongPrimArray = Objects.requireNonNull(numberToLongPrimArray);
  }

  public void setNumberToNumberBased(final NumberToNumberBased numberToNumberBased) {
    this.numberToNumberBased = Objects.requireNonNull(numberToNumberBased);
  }

  public void setNumberToNumberBasedArray(final NumberToNumberBasedArray numberToNumberBasedArray) {
    this.numberToNumberBasedArray = Objects.requireNonNull(numberToNumberBasedArray);
  }

  public void setNumberToNumberBasedCollection(
      final NumberToNumberBasedCollection numberToNumberBasedCollection) {
    this.numberToNumberBasedCollection = Objects.requireNonNull(numberToNumberBasedCollection);
  }

  public void setNumberToObjectId(final NumberToObjectId numberToObjectId) {
    this.numberToObjectId = Objects.requireNonNull(numberToObjectId);
  }

  public void setNumberToObjectIdArray(final NumberToObjectIdArray numberToObjectIdArray) {
    this.numberToObjectIdArray = Objects.requireNonNull(numberToObjectIdArray);
  }

  public void setNumberToObjectIdCollection(
      final NumberToObjectIdCollection numberToObjectIdCollection) {
    this.numberToObjectIdCollection = Objects.requireNonNull(numberToObjectIdCollection);
  }

  public void setNumberToShortPrim(final NumberToShortPrim numberToShortPrim) {
    this.numberToShortPrim = Objects.requireNonNull(numberToShortPrim);
  }

  public void setNumberToShortPrimArray(final NumberToShortPrimArray numberToShortPrimArray) {
    this.numberToShortPrimArray = Objects.requireNonNull(numberToShortPrimArray);
  }

  public void setNumberToStringBased(final NumberToStringBased numberToStringBased) {
    this.numberToStringBased = Objects.requireNonNull(numberToStringBased);
  }

  public void setNumberToStringBasedArray(final NumberToStringBasedArray numberToStringBasedArray) {
    this.numberToStringBasedArray = Objects.requireNonNull(numberToStringBasedArray);
  }

  public void setNumberToStringBasedCollection(
      final NumberToStringBasedCollection numberToStringBasedCollection) {
    this.numberToStringBasedCollection = Objects.requireNonNull(numberToStringBasedCollection);
  }

  public void setObjectIdCollectionToBinary(
      final ObjectIdCollectionToBinary objectIdCollectionToBinary) {
    this.objectIdCollectionToBinary = Objects.requireNonNull(objectIdCollectionToBinary);
  }

  public void setObjectIdCollectionToBinaryArray(
      final ObjectIdCollectionToBinaryArray objectIdCollectionToBinaryArray) {
    this.objectIdCollectionToBinaryArray = Objects.requireNonNull(objectIdCollectionToBinaryArray);
  }

  public void setObjectIdCollectionToBinaryCollection(
      final ObjectIdCollectionToBinaryCollection objectIdCollectionToBinaryCollection) {
    this.objectIdCollectionToBinaryCollection =
        Objects.requireNonNull(objectIdCollectionToBinaryCollection);
  }

  public void setObjectIdCollectionToBoolean(
      final ObjectIdCollectionToBoolean objectIdCollectionToBoolean) {
    this.objectIdCollectionToBoolean = Objects.requireNonNull(objectIdCollectionToBoolean);
  }

  public void setObjectIdCollectionToBooleanArray(
      final ObjectIdCollectionToBooleanArray objectIdCollectionToBooleanArray) {
    this.objectIdCollectionToBooleanArray =
        Objects.requireNonNull(objectIdCollectionToBooleanArray);
  }

  public void setObjectIdCollectionToBooleanCollection(
      final ObjectIdCollectionToBooleanCollection objectIdCollectionToBooleanCollection) {
    this.objectIdCollectionToBooleanCollection =
        Objects.requireNonNull(objectIdCollectionToBooleanCollection);
  }

  public void setObjectIdCollectionToBooleanPrim(
      final ObjectIdCollectionToBooleanPrim objectIdCollectionToBooleanPrim) {
    this.objectIdCollectionToBooleanPrim = Objects.requireNonNull(objectIdCollectionToBooleanPrim);
  }

  public void setObjectIdCollectionToBooleanPrimArray(
      final ObjectIdCollectionToBooleanPrimArray objectIdCollectionToBooleanPrimArray) {
    this.objectIdCollectionToBooleanPrimArray =
        Objects.requireNonNull(objectIdCollectionToBooleanPrimArray);
  }

  public void setObjectIdCollectionToByte(final ObjectIdCollectionToByte objectIdCollectionToByte) {
    this.objectIdCollectionToByte = Objects.requireNonNull(objectIdCollectionToByte);
  }

  public void setObjectIdCollectionToByteArray(
      final ObjectIdCollectionToByteArray objectIdCollectionToByteArray) {
    this.objectIdCollectionToByteArray = Objects.requireNonNull(objectIdCollectionToByteArray);
  }

  public void setObjectIdCollectionToByteCollection(
      final ObjectIdCollectionToByteCollection objectIdCollectionToByteCollection) {
    this.objectIdCollectionToByteCollection =
        Objects.requireNonNull(objectIdCollectionToByteCollection);
  }

  public void setObjectIdCollectionToBytePrim(
      final ObjectIdCollectionToBytePrim objectIdCollectionToBytePrim) {
    this.objectIdCollectionToBytePrim = Objects.requireNonNull(objectIdCollectionToBytePrim);
  }

  public void setObjectIdCollectionToBytePrimArray(
      final ObjectIdCollectionToBytePrimArray objectIdCollectionToBytePrimArray) {
    this.objectIdCollectionToBytePrimArray =
        Objects.requireNonNull(objectIdCollectionToBytePrimArray);
  }

  public void setObjectIdCollectionToCharacter(
      final ObjectIdCollectionToCharacter objectIdCollectionToCharacter) {
    this.objectIdCollectionToCharacter = Objects.requireNonNull(objectIdCollectionToCharacter);
  }

  public void setObjectIdCollectionToCharacterArray(
      final ObjectIdCollectionToCharacterArray objectIdCollectionToCharacterArray) {
    this.objectIdCollectionToCharacterArray =
        Objects.requireNonNull(objectIdCollectionToCharacterArray);
  }

  public void setObjectIdCollectionToCharacterCollection(
      final ObjectIdCollectionToCharacterCollection objectIdCollectionToCharacterCollection) {
    this.objectIdCollectionToCharacterCollection =
        Objects.requireNonNull(objectIdCollectionToCharacterCollection);
  }

  public void setObjectIdCollectionToCharacterPrim(
      final ObjectIdCollectionToCharacterPrim objectIdCollectionToCharacterPrim) {
    this.objectIdCollectionToCharacterPrim =
        Objects.requireNonNull(objectIdCollectionToCharacterPrim);
  }

  public void setObjectIdCollectionToCharacterPrimArray(
      final ObjectIdCollectionToCharacterPrimArray objectIdCollectionToCharacterPrimArray) {
    this.objectIdCollectionToCharacterPrimArray =
        Objects.requireNonNull(objectIdCollectionToCharacterPrimArray);
  }

  public void setObjectIdCollectionToDateBased(
      final ObjectIdCollectionToDateBased objectIdCollectionToDateBased) {
    this.objectIdCollectionToDateBased = Objects.requireNonNull(objectIdCollectionToDateBased);
  }

  public void setObjectIdCollectionToDateBasedArray(
      final ObjectIdCollectionToDateBasedArray objectIdCollectionToDateBasedArray) {
    this.objectIdCollectionToDateBasedArray =
        Objects.requireNonNull(objectIdCollectionToDateBasedArray);
  }

  public void setObjectIdCollectionToDateBasedCollection(
      final ObjectIdCollectionToDateBasedCollection objectIdCollectionToDateBasedCollection) {
    this.objectIdCollectionToDateBasedCollection =
        Objects.requireNonNull(objectIdCollectionToDateBasedCollection);
  }

  public void setObjectIdCollectionToDoublePrim(
      final ObjectIdCollectionToDoublePrim objectIdCollectionToDoublePrim) {
    this.objectIdCollectionToDoublePrim = Objects.requireNonNull(objectIdCollectionToDoublePrim);
  }

  public void setObjectIdCollectionToDoublePrimArray(
      final ObjectIdCollectionToDoublePrimArray objectIdCollectionToDoublePrimArray) {
    this.objectIdCollectionToDoublePrimArray =
        Objects.requireNonNull(objectIdCollectionToDoublePrimArray);
  }

  public void setObjectIdCollectionToEnum(final ObjectIdCollectionToEnum objectIdCollectionToEnum) {
    this.objectIdCollectionToEnum = Objects.requireNonNull(objectIdCollectionToEnum);
  }

  public void setObjectIdCollectionToEnumArray(
      final ObjectIdCollectionToEnumArray objectIdCollectionToEnumArray) {
    this.objectIdCollectionToEnumArray = Objects.requireNonNull(objectIdCollectionToEnumArray);
  }

  public void setObjectIdCollectionToEnumCollection(
      final ObjectIdCollectionToEnumCollection objectIdCollectionToEnumCollection) {
    this.objectIdCollectionToEnumCollection =
        Objects.requireNonNull(objectIdCollectionToEnumCollection);
  }

  public void setObjectIdCollectionToFloatPrim(
      final ObjectIdCollectionToFloatPrim objectIdCollectionToFloatPrim) {
    this.objectIdCollectionToFloatPrim = Objects.requireNonNull(objectIdCollectionToFloatPrim);
  }

  public void setObjectIdCollectionToFloatPrimArray(
      final ObjectIdCollectionToFloatPrimArray objectIdCollectionToFloatPrimArray) {
    this.objectIdCollectionToFloatPrimArray =
        Objects.requireNonNull(objectIdCollectionToFloatPrimArray);
  }

  public void setObjectIdCollectionToIntegerPrim(
      final ObjectIdCollectionToIntegerPrim objectIdCollectionToIntegerPrim) {
    this.objectIdCollectionToIntegerPrim = Objects.requireNonNull(objectIdCollectionToIntegerPrim);
  }

  public void setObjectIdCollectionToIntegerPrimArray(
      final ObjectIdCollectionToIntegerPrimArray objectIdCollectionToIntegerPrimArray) {
    this.objectIdCollectionToIntegerPrimArray =
        Objects.requireNonNull(objectIdCollectionToIntegerPrimArray);
  }

  public void setObjectIdCollectionToLongPrim(
      final ObjectIdCollectionToLongPrim objectIdCollectionToLongPrim) {
    this.objectIdCollectionToLongPrim = Objects.requireNonNull(objectIdCollectionToLongPrim);
  }

  public void setObjectIdCollectionToLongPrimArray(
      final ObjectIdCollectionToLongPrimArray objectIdCollectionToLongPrimArray) {
    this.objectIdCollectionToLongPrimArray =
        Objects.requireNonNull(objectIdCollectionToLongPrimArray);
  }

  public void setObjectIdCollectionToNumberBased(
      final ObjectIdCollectionToNumberBased objectIdCollectionToNumberBased) {
    this.objectIdCollectionToNumberBased = Objects.requireNonNull(objectIdCollectionToNumberBased);
  }

  public void setObjectIdCollectionToNumberBasedArray(
      final ObjectIdCollectionToNumberBasedArray objectIdCollectionToNumberBasedArray) {
    this.objectIdCollectionToNumberBasedArray =
        Objects.requireNonNull(objectIdCollectionToNumberBasedArray);
  }

  public void setObjectIdCollectionToNumberBasedCollection(
      final ObjectIdCollectionToNumberBasedCollection objectIdCollectionToNumberBasedCollection) {
    this.objectIdCollectionToNumberBasedCollection =
        Objects.requireNonNull(objectIdCollectionToNumberBasedCollection);
  }

  public void setObjectIdCollectionToObjectId(
      final ObjectIdCollectionToObjectId objectIdCollectionToObjectId) {
    this.objectIdCollectionToObjectId = Objects.requireNonNull(objectIdCollectionToObjectId);
  }

  public void setObjectIdCollectionToObjectIdArray(
      final ObjectIdCollectionToObjectIdArray objectIdCollectionToObjectIdArray) {
    this.objectIdCollectionToObjectIdArray =
        Objects.requireNonNull(objectIdCollectionToObjectIdArray);
  }

  public void setObjectIdCollectionToObjectIdCollection(
      final ObjectIdCollectionToObjectIdCollection objectIdCollectionToObjectIdCollection) {
    this.objectIdCollectionToObjectIdCollection =
        Objects.requireNonNull(objectIdCollectionToObjectIdCollection);
  }

  public void setObjectIdCollectionToShortPrim(
      final ObjectIdCollectionToShortPrim objectIdCollectionToShortPrim) {
    this.objectIdCollectionToShortPrim = Objects.requireNonNull(objectIdCollectionToShortPrim);
  }

  public void setObjectIdCollectionToShortPrimArray(
      final ObjectIdCollectionToShortPrimArray objectIdCollectionToShortPrimArray) {
    this.objectIdCollectionToShortPrimArray =
        Objects.requireNonNull(objectIdCollectionToShortPrimArray);
  }

  public void setObjectIdCollectionToStringBased(
      final ObjectIdCollectionToStringBased objectIdCollectionToStringBased) {
    this.objectIdCollectionToStringBased = Objects.requireNonNull(objectIdCollectionToStringBased);
  }

  public void setObjectIdCollectionToStringBasedArray(
      final ObjectIdCollectionToStringBasedArray objectIdCollectionToStringBasedArray) {
    this.objectIdCollectionToStringBasedArray =
        Objects.requireNonNull(objectIdCollectionToStringBasedArray);
  }

  public void setObjectIdCollectionToStringBasedCollection(
      final ObjectIdCollectionToStringBasedCollection objectIdCollectionToStringBasedCollection) {
    this.objectIdCollectionToStringBasedCollection =
        Objects.requireNonNull(objectIdCollectionToStringBasedCollection);
  }

  public void setObjectIdToBinary(final ObjectIdToBinary objectIdToBinary) {
    this.objectIdToBinary = Objects.requireNonNull(objectIdToBinary);
  }

  public void setObjectIdToBinaryArray(final ObjectIdToBinaryArray objectIdToBinaryArray) {
    this.objectIdToBinaryArray = Objects.requireNonNull(objectIdToBinaryArray);
  }

  public void setObjectIdToBinaryCollection(
      final ObjectIdToBinaryCollection objectIdToBinaryCollection) {
    this.objectIdToBinaryCollection = Objects.requireNonNull(objectIdToBinaryCollection);
  }

  public void setObjectIdToBoolean(final ObjectIdToBoolean objectIdToBoolean) {
    this.objectIdToBoolean = Objects.requireNonNull(objectIdToBoolean);
  }

  public void setObjectIdToBooleanArray(final ObjectIdToBooleanArray objectIdToBooleanArray) {
    this.objectIdToBooleanArray = Objects.requireNonNull(objectIdToBooleanArray);
  }

  public void setObjectIdToBooleanCollection(
      final ObjectIdToBooleanCollection objectIdToBooleanCollection) {
    this.objectIdToBooleanCollection = Objects.requireNonNull(objectIdToBooleanCollection);
  }

  public void setObjectIdToBooleanPrim(final ObjectIdToBooleanPrim objectIdToBooleanPrim) {
    this.objectIdToBooleanPrim = Objects.requireNonNull(objectIdToBooleanPrim);
  }

  public void setObjectIdToBooleanPrimArray(
      final ObjectIdToBooleanPrimArray objectIdToBooleanPrimArray) {
    this.objectIdToBooleanPrimArray = Objects.requireNonNull(objectIdToBooleanPrimArray);
  }

  public void setObjectIdToByte(final ObjectIdToByte objectIdToByte) {
    this.objectIdToByte = Objects.requireNonNull(objectIdToByte);
  }

  public void setObjectIdToByteArray(final ObjectIdToByteArray objectIdToByteArray) {
    this.objectIdToByteArray = Objects.requireNonNull(objectIdToByteArray);
  }

  public void setObjectIdToByteCollection(final ObjectIdToByteCollection objectIdToByteCollection) {
    this.objectIdToByteCollection = Objects.requireNonNull(objectIdToByteCollection);
  }

  public void setObjectIdToBytePrim(final ObjectIdToBytePrim objectIdToBytePrim) {
    this.objectIdToBytePrim = Objects.requireNonNull(objectIdToBytePrim);
  }

  public void setObjectIdToBytePrimArray(final ObjectIdToBytePrimArray objectIdToBytePrimArray) {
    this.objectIdToBytePrimArray = Objects.requireNonNull(objectIdToBytePrimArray);
  }

  public void setObjectIdToCharacter(final ObjectIdToCharacter objectIdToCharacter) {
    this.objectIdToCharacter = Objects.requireNonNull(objectIdToCharacter);
  }

  public void setObjectIdToCharacterArray(final ObjectIdToCharacterArray objectIdToCharacterArray) {
    this.objectIdToCharacterArray = Objects.requireNonNull(objectIdToCharacterArray);
  }

  public void setObjectIdToCharacterCollection(
      final ObjectIdToCharacterCollection objectIdToCharacterCollection) {
    this.objectIdToCharacterCollection = Objects.requireNonNull(objectIdToCharacterCollection);
  }

  public void setObjectIdToCharacterPrim(final ObjectIdToCharacterPrim objectIdToCharacterPrim) {
    this.objectIdToCharacterPrim = Objects.requireNonNull(objectIdToCharacterPrim);
  }

  public void setObjectIdToCharacterPrimArray(
      final ObjectIdToCharacterPrimArray objectIdToCharacterPrimArray) {
    this.objectIdToCharacterPrimArray = Objects.requireNonNull(objectIdToCharacterPrimArray);
  }

  public void setObjectIdToDateBased(final ObjectIdToDateBased objectIdToDateBased) {
    this.objectIdToDateBased = Objects.requireNonNull(objectIdToDateBased);
  }

  public void setObjectIdToDateBasedArray(final ObjectIdToDateBasedArray objectIdToDateBasedArray) {
    this.objectIdToDateBasedArray = Objects.requireNonNull(objectIdToDateBasedArray);
  }

  public void setObjectIdToDateBasedCollection(
      final ObjectIdToDateBasedCollection objectIdToDateBasedCollection) {
    this.objectIdToDateBasedCollection = Objects.requireNonNull(objectIdToDateBasedCollection);
  }

  public void setObjectIdToDoublePrim(final ObjectIdToDoublePrim objectIdToDoublePrim) {
    this.objectIdToDoublePrim = Objects.requireNonNull(objectIdToDoublePrim);
  }

  public void setObjectIdToDoublePrimArray(
      final ObjectIdToDoublePrimArray objectIdToDoublePrimArray) {
    this.objectIdToDoublePrimArray = Objects.requireNonNull(objectIdToDoublePrimArray);
  }

  public void setObjectIdToEnum(final ObjectIdToEnum objectIdToEnum) {
    this.objectIdToEnum = Objects.requireNonNull(objectIdToEnum);
  }

  public void setObjectIdToEnumArray(final ObjectIdToEnumArray objectIdToEnumArray) {
    this.objectIdToEnumArray = Objects.requireNonNull(objectIdToEnumArray);
  }

  public void setObjectIdToEnumCollection(final ObjectIdToEnumCollection objectIdToEnumCollection) {
    this.objectIdToEnumCollection = Objects.requireNonNull(objectIdToEnumCollection);
  }

  public void setObjectIdToFloatPrim(final ObjectIdToFloatPrim objectIdToFloatPrim) {
    this.objectIdToFloatPrim = Objects.requireNonNull(objectIdToFloatPrim);
  }

  public void setObjectIdToFloatPrimArray(final ObjectIdToFloatPrimArray objectIdToFloatPrimArray) {
    this.objectIdToFloatPrimArray = Objects.requireNonNull(objectIdToFloatPrimArray);
  }

  public void setObjectIdToIntegerPrim(final ObjectIdToIntegerPrim objectIdToIntegerPrim) {
    this.objectIdToIntegerPrim = Objects.requireNonNull(objectIdToIntegerPrim);
  }

  public void setObjectIdToIntegerPrimArray(
      final ObjectIdToIntegerPrimArray objectIdToIntegerPrimArray) {
    this.objectIdToIntegerPrimArray = Objects.requireNonNull(objectIdToIntegerPrimArray);
  }

  public void setObjectIdToLongPrim(final ObjectIdToLongPrim objectIdToLongPrim) {
    this.objectIdToLongPrim = Objects.requireNonNull(objectIdToLongPrim);
  }

  public void setObjectIdToLongPrimArray(final ObjectIdToLongPrimArray objectIdToLongPrimArray) {
    this.objectIdToLongPrimArray = Objects.requireNonNull(objectIdToLongPrimArray);
  }

  public void setObjectIdToNumberBased(final ObjectIdToNumberBased objectIdToNumberBased) {
    this.objectIdToNumberBased = Objects.requireNonNull(objectIdToNumberBased);
  }

  public void setObjectIdToNumberBasedArray(
      final ObjectIdToNumberBasedArray objectIdToNumberBasedArray) {
    this.objectIdToNumberBasedArray = Objects.requireNonNull(objectIdToNumberBasedArray);
  }

  public void setObjectIdToNumberBasedCollection(
      final ObjectIdToNumberBasedCollection objectIdToNumberBasedCollection) {
    this.objectIdToNumberBasedCollection = Objects.requireNonNull(objectIdToNumberBasedCollection);
  }

  public void setObjectIdToObjectId(final ObjectIdToObjectId objectIdToObjectId) {
    this.objectIdToObjectId = Objects.requireNonNull(objectIdToObjectId);
  }

  public void setObjectIdToObjectIdArray(final ObjectIdToObjectIdArray objectIdToObjectIdArray) {
    this.objectIdToObjectIdArray = Objects.requireNonNull(objectIdToObjectIdArray);
  }

  public void setObjectIdToObjectIdCollection(
      final ObjectIdToObjectIdCollection objectIdToObjectIdCollection) {
    this.objectIdToObjectIdCollection = Objects.requireNonNull(objectIdToObjectIdCollection);
  }

  public void setObjectIdToShortPrim(final ObjectIdToShortPrim objectIdToShortPrim) {
    this.objectIdToShortPrim = Objects.requireNonNull(objectIdToShortPrim);
  }

  public void setObjectIdToShortPrimArray(final ObjectIdToShortPrimArray objectIdToShortPrimArray) {
    this.objectIdToShortPrimArray = Objects.requireNonNull(objectIdToShortPrimArray);
  }

  public void setObjectIdToStringBased(final ObjectIdToStringBased objectIdToStringBased) {
    this.objectIdToStringBased = Objects.requireNonNull(objectIdToStringBased);
  }

  public void setObjectIdToStringBasedArray(
      final ObjectIdToStringBasedArray objectIdToStringBasedArray) {
    this.objectIdToStringBasedArray = Objects.requireNonNull(objectIdToStringBasedArray);
  }

  public void setObjectIdToStringBasedCollection(
      final ObjectIdToStringBasedCollection objectIdToStringBasedCollection) {
    this.objectIdToStringBasedCollection = Objects.requireNonNull(objectIdToStringBasedCollection);
  }

  public void setStringCollectionToBinary(final StringCollectionToBinary stringCollectionToBinary) {
    this.stringCollectionToBinary = Objects.requireNonNull(stringCollectionToBinary);
  }

  public void setStringCollectionToBinaryArray(
      final StringCollectionToBinaryArray stringCollectionToBinaryArray) {
    this.stringCollectionToBinaryArray = Objects.requireNonNull(stringCollectionToBinaryArray);
  }

  public void setStringCollectionToBinaryCollection(
      final StringCollectionToBinaryCollection stringCollectionToBinaryCollection) {
    this.stringCollectionToBinaryCollection =
        Objects.requireNonNull(stringCollectionToBinaryCollection);
  }

  public void setStringCollectionToBoolean(
      final StringCollectionToBoolean stringCollectionToBoolean) {
    this.stringCollectionToBoolean = Objects.requireNonNull(stringCollectionToBoolean);
  }

  public void setStringCollectionToBooleanArray(
      final StringCollectionToBooleanArray stringCollectionToBooleanArray) {
    this.stringCollectionToBooleanArray = Objects.requireNonNull(stringCollectionToBooleanArray);
  }

  public void setStringCollectionToBooleanCollection(
      final StringCollectionToBooleanCollection stringCollectionToBooleanCollection) {
    this.stringCollectionToBooleanCollection =
        Objects.requireNonNull(stringCollectionToBooleanCollection);
  }

  public void setStringCollectionToBooleanPrim(
      final StringCollectionToBooleanPrim stringCollectionToBooleanPrim) {
    this.stringCollectionToBooleanPrim = Objects.requireNonNull(stringCollectionToBooleanPrim);
  }

  public void setStringCollectionToBooleanPrimArray(
      final StringCollectionToBooleanPrimArray stringCollectionToBooleanPrimArray) {
    this.stringCollectionToBooleanPrimArray =
        Objects.requireNonNull(stringCollectionToBooleanPrimArray);
  }

  public void setStringCollectionToByte(final StringCollectionToByte stringCollectionToByte) {
    this.stringCollectionToByte = Objects.requireNonNull(stringCollectionToByte);
  }

  public void setStringCollectionToByteArray(
      final StringCollectionToByteArray stringCollectionToByteArray) {
    this.stringCollectionToByteArray = Objects.requireNonNull(stringCollectionToByteArray);
  }

  public void setStringCollectionToByteCollection(
      final StringCollectionToByteCollection stringCollectionToByteCollection) {
    this.stringCollectionToByteCollection =
        Objects.requireNonNull(stringCollectionToByteCollection);
  }

  public void setStringCollectionToBytePrim(
      final StringCollectionToBytePrim stringCollectionToBytePrim) {
    this.stringCollectionToBytePrim = Objects.requireNonNull(stringCollectionToBytePrim);
  }

  public void setStringCollectionToBytePrimArray(
      final StringCollectionToBytePrimArray stringCollectionToBytePrimArray) {
    this.stringCollectionToBytePrimArray = Objects.requireNonNull(stringCollectionToBytePrimArray);
  }

  public void setStringCollectionToCharacter(
      final StringCollectionToCharacter stringCollectionToCharacter) {
    this.stringCollectionToCharacter = Objects.requireNonNull(stringCollectionToCharacter);
  }

  public void setStringCollectionToCharacterArray(
      final StringCollectionToCharacterArray stringCollectionToCharacterArray) {
    this.stringCollectionToCharacterArray =
        Objects.requireNonNull(stringCollectionToCharacterArray);
  }

  public void setStringCollectionToCharacterCollection(
      final StringCollectionToCharacterCollection stringCollectionToCharacterCollection) {
    this.stringCollectionToCharacterCollection =
        Objects.requireNonNull(stringCollectionToCharacterCollection);
  }

  public void setStringCollectionToCharacterPrim(
      final StringCollectionToCharacterPrim stringCollectionToCharacterPrim) {
    this.stringCollectionToCharacterPrim = Objects.requireNonNull(stringCollectionToCharacterPrim);
  }

  public void setStringCollectionToCharacterPrimArray(
      final StringCollectionToCharacterPrimArray stringCollectionToCharacterPrimArray) {
    this.stringCollectionToCharacterPrimArray =
        Objects.requireNonNull(stringCollectionToCharacterPrimArray);
  }

  public void setStringCollectionToDateBased(
      final StringCollectionToDateBased stringCollectionToDateBased) {
    this.stringCollectionToDateBased = Objects.requireNonNull(stringCollectionToDateBased);
  }

  public void setStringCollectionToDateBasedArray(
      final StringCollectionToDateBasedArray stringCollectionToDateBasedArray) {
    this.stringCollectionToDateBasedArray =
        Objects.requireNonNull(stringCollectionToDateBasedArray);
  }

  public void setStringCollectionToDateBasedCollection(
      final StringCollectionToDateBasedCollection stringCollectionToDateBasedCollection) {
    this.stringCollectionToDateBasedCollection =
        Objects.requireNonNull(stringCollectionToDateBasedCollection);
  }

  public void setStringCollectionToDoublePrim(
      final StringCollectionToDoublePrim stringCollectionToDoublePrim) {
    this.stringCollectionToDoublePrim = Objects.requireNonNull(stringCollectionToDoublePrim);
  }

  public void setStringCollectionToDoublePrimArray(
      final StringCollectionToDoublePrimArray stringCollectionToDoublePrimArray) {
    this.stringCollectionToDoublePrimArray =
        Objects.requireNonNull(stringCollectionToDoublePrimArray);
  }

  public void setStringCollectionToEnum(final StringCollectionToEnum stringCollectionToEnum) {
    this.stringCollectionToEnum = Objects.requireNonNull(stringCollectionToEnum);
  }

  public void setStringCollectionToEnumArray(
      final StringCollectionToEnumArray stringCollectionToEnumArray) {
    this.stringCollectionToEnumArray = Objects.requireNonNull(stringCollectionToEnumArray);
  }

  public void setStringCollectionToEnumCollection(
      final StringCollectionToEnumCollection stringCollectionToEnumCollection) {
    this.stringCollectionToEnumCollection =
        Objects.requireNonNull(stringCollectionToEnumCollection);
  }

  public void setStringCollectionToFloatPrim(
      final StringCollectionToFloatPrim stringCollectionToFloatPrim) {
    this.stringCollectionToFloatPrim = Objects.requireNonNull(stringCollectionToFloatPrim);
  }

  public void setStringCollectionToFloatPrimArray(
      final StringCollectionToFloatPrimArray stringCollectionToFloatPrimArray) {
    this.stringCollectionToFloatPrimArray =
        Objects.requireNonNull(stringCollectionToFloatPrimArray);
  }

  public void setStringCollectionToIntegerPrim(
      final StringCollectionToIntegerPrim stringCollectionToIntegerPrim) {
    this.stringCollectionToIntegerPrim = Objects.requireNonNull(stringCollectionToIntegerPrim);
  }

  public void setStringCollectionToIntegerPrimArray(
      final StringCollectionToIntegerPrimArray stringCollectionToIntegerPrimArray) {
    this.stringCollectionToIntegerPrimArray =
        Objects.requireNonNull(stringCollectionToIntegerPrimArray);
  }

  public void setStringCollectionToLongPrim(
      final StringCollectionToLongPrim stringCollectionToLongPrim) {
    this.stringCollectionToLongPrim = Objects.requireNonNull(stringCollectionToLongPrim);
  }

  public void setStringCollectionToLongPrimArray(
      final StringCollectionToLongPrimArray stringCollectionToLongPrimArray) {
    this.stringCollectionToLongPrimArray = Objects.requireNonNull(stringCollectionToLongPrimArray);
  }

  public void setStringCollectionToNumberBased(
      final StringCollectionToNumberBased stringCollectionToNumberBased) {
    this.stringCollectionToNumberBased = Objects.requireNonNull(stringCollectionToNumberBased);
  }

  public void setStringCollectionToNumberBasedArray(
      final StringCollectionToNumberBasedArray stringCollectionToNumberBasedArray) {
    this.stringCollectionToNumberBasedArray =
        Objects.requireNonNull(stringCollectionToNumberBasedArray);
  }

  public void setStringCollectionToNumberBasedCollection(
      final StringCollectionToNumberBasedCollection stringCollectionToNumberBasedCollection) {
    this.stringCollectionToNumberBasedCollection =
        Objects.requireNonNull(stringCollectionToNumberBasedCollection);
  }

  public void setStringCollectionToObjectId(
      final StringCollectionToObjectId stringCollectionToObjectId) {
    this.stringCollectionToObjectId = Objects.requireNonNull(stringCollectionToObjectId);
  }

  public void setStringCollectionToObjectIdArray(
      final StringCollectionToObjectIdArray stringCollectionToObjectIdArray) {
    this.stringCollectionToObjectIdArray = Objects.requireNonNull(stringCollectionToObjectIdArray);
  }

  public void setStringCollectionToObjectIdCollection(
      final StringCollectionToObjectIdCollection stringCollectionToObjectIdCollection) {
    this.stringCollectionToObjectIdCollection =
        Objects.requireNonNull(stringCollectionToObjectIdCollection);
  }

  public void setStringCollectionToShortPrim(
      final StringCollectionToShortPrim stringCollectionToShortPrim) {
    this.stringCollectionToShortPrim = Objects.requireNonNull(stringCollectionToShortPrim);
  }

  public void setStringCollectionToShortPrimArray(
      final StringCollectionToShortPrimArray stringCollectionToShortPrimArray) {
    this.stringCollectionToShortPrimArray =
        Objects.requireNonNull(stringCollectionToShortPrimArray);
  }

  public void setStringCollectionToStringBased(
      final StringCollectionToStringBased stringCollectionToStringBased) {
    this.stringCollectionToStringBased = Objects.requireNonNull(stringCollectionToStringBased);
  }

  public void setStringCollectionToStringBasedArray(
      final StringCollectionToStringBasedArray stringCollectionToStringBasedArray) {
    this.stringCollectionToStringBasedArray =
        Objects.requireNonNull(stringCollectionToStringBasedArray);
  }

  public void setStringCollectionToStringBasedCollection(
      final StringCollectionToStringBasedCollection stringCollectionToStringBasedCollection) {
    this.stringCollectionToStringBasedCollection =
        Objects.requireNonNull(stringCollectionToStringBasedCollection);
  }

  public void setStringToBinary(final StringToBinary stringToBinary) {
    this.stringToBinary = Objects.requireNonNull(stringToBinary);
  }

  public void setStringToBinaryArray(final StringToBinaryArray stringToBinaryArray) {
    this.stringToBinaryArray = Objects.requireNonNull(stringToBinaryArray);
  }

  public void setStringToBinaryCollection(final StringToBinaryCollection stringToBinaryCollection) {
    this.stringToBinaryCollection = Objects.requireNonNull(stringToBinaryCollection);
  }

  public void setStringToBoolean(final StringToBoolean stringToBoolean) {
    this.stringToBoolean = Objects.requireNonNull(stringToBoolean);
  }

  public void setStringToBooleanArray(final StringToBooleanArray stringToBooleanArray) {
    this.stringToBooleanArray = Objects.requireNonNull(stringToBooleanArray);
  }

  public void setStringToBooleanCollection(
      final StringToBooleanCollection stringToBooleanCollection) {
    this.stringToBooleanCollection = Objects.requireNonNull(stringToBooleanCollection);
  }

  public void setStringToBooleanPrim(final StringToBooleanPrim stringToBooleanPrim) {
    this.stringToBooleanPrim = Objects.requireNonNull(stringToBooleanPrim);
  }

  public void setStringToBooleanPrimArray(final StringToBooleanPrimArray stringToBooleanPrimArray) {
    this.stringToBooleanPrimArray = Objects.requireNonNull(stringToBooleanPrimArray);
  }

  public void setStringToByte(final StringToByte stringToByte) {
    this.stringToByte = Objects.requireNonNull(stringToByte);
  }

  public void setStringToByteArray(final StringToByteArray stringToByteArray) {
    this.stringToByteArray = Objects.requireNonNull(stringToByteArray);
  }

  public void setStringToByteCollection(final StringToByteCollection stringToByteCollection) {
    this.stringToByteCollection = Objects.requireNonNull(stringToByteCollection);
  }

  public void setStringToBytePrim(final StringToBytePrim stringToBytePrim) {
    this.stringToBytePrim = Objects.requireNonNull(stringToBytePrim);
  }

  public void setStringToBytePrimArray(final StringToBytePrimArray stringToBytePrimArray) {
    this.stringToBytePrimArray = Objects.requireNonNull(stringToBytePrimArray);
  }

  public void setStringToCharacter(final StringToCharacter stringToCharacter) {
    this.stringToCharacter = Objects.requireNonNull(stringToCharacter);
  }

  public void setStringToCharacterArray(final StringToCharacterArray stringToCharacterArray) {
    this.stringToCharacterArray = Objects.requireNonNull(stringToCharacterArray);
  }

  public void setStringToCharacterCollection(
      final StringToCharacterCollection stringToCharacterCollection) {
    this.stringToCharacterCollection = Objects.requireNonNull(stringToCharacterCollection);
  }

  public void setStringToCharacterPrim(final StringToCharacterPrim stringToCharacterPrim) {
    this.stringToCharacterPrim = Objects.requireNonNull(stringToCharacterPrim);
  }

  public void setStringToCharacterPrimArray(
      final StringToCharacterPrimArray stringToCharacterPrimArray) {
    this.stringToCharacterPrimArray = Objects.requireNonNull(stringToCharacterPrimArray);
  }

  public void setStringToDateBased(final StringToDateBased stringToDateBased) {
    this.stringToDateBased = Objects.requireNonNull(stringToDateBased);
  }

  public void setStringToDateBasedArray(final StringToDateBasedArray stringToDateBasedArray) {
    this.stringToDateBasedArray = Objects.requireNonNull(stringToDateBasedArray);
  }

  public void setStringToDateBasedCollection(
      final StringToDateBasedCollection stringToDateBasedCollection) {
    this.stringToDateBasedCollection = Objects.requireNonNull(stringToDateBasedCollection);
  }

  public void setStringToDoublePrim(final StringToDoublePrim stringToDoublePrim) {
    this.stringToDoublePrim = Objects.requireNonNull(stringToDoublePrim);
  }

  public void setStringToDoublePrimArray(final StringToDoublePrimArray stringToDoublePrimArray) {
    this.stringToDoublePrimArray = Objects.requireNonNull(stringToDoublePrimArray);
  }

  public void setStringToEnum(final StringToEnum stringToEnum) {
    this.stringToEnum = Objects.requireNonNull(stringToEnum);
  }

  public void setStringToEnumArray(final StringToEnumArray stringToEnumArray) {
    this.stringToEnumArray = Objects.requireNonNull(stringToEnumArray);
  }

  public void setStringToEnumCollection(final StringToEnumCollection stringToEnumCollection) {
    this.stringToEnumCollection = Objects.requireNonNull(stringToEnumCollection);
  }

  public void setStringToFloatPrim(final StringToFloatPrim stringToFloatPrim) {
    this.stringToFloatPrim = Objects.requireNonNull(stringToFloatPrim);
  }

  public void setStringToFloatPrimArray(final StringToFloatPrimArray stringToFloatPrimArray) {
    this.stringToFloatPrimArray = Objects.requireNonNull(stringToFloatPrimArray);
  }

  public void setStringToIntegerPrim(final StringToIntegerPrim stringToIntegerPrim) {
    this.stringToIntegerPrim = Objects.requireNonNull(stringToIntegerPrim);
  }

  public void setStringToIntegerPrimArray(final StringToIntegerPrimArray stringToIntegerPrimArray) {
    this.stringToIntegerPrimArray = Objects.requireNonNull(stringToIntegerPrimArray);
  }

  public void setStringToLongPrim(final StringToLongPrim stringToLongPrim) {
    this.stringToLongPrim = Objects.requireNonNull(stringToLongPrim);
  }

  public void setStringToLongPrimArray(final StringToLongPrimArray stringToLongPrimArray) {
    this.stringToLongPrimArray = Objects.requireNonNull(stringToLongPrimArray);
  }

  public void setStringToNumberBased(final StringToNumberBased stringToNumberBased) {
    this.stringToNumberBased = Objects.requireNonNull(stringToNumberBased);
  }

  public void setStringToNumberBasedArray(final StringToNumberBasedArray stringToNumberBasedArray) {
    this.stringToNumberBasedArray = Objects.requireNonNull(stringToNumberBasedArray);
  }

  public void setStringToNumberBasedCollection(
      final StringToNumberBasedCollection stringToNumberBasedCollection) {
    this.stringToNumberBasedCollection = Objects.requireNonNull(stringToNumberBasedCollection);
  }

  public void setStringToObjectId(final StringToObjectId stringToObjectId) {
    this.stringToObjectId = Objects.requireNonNull(stringToObjectId);
  }

  public void setStringToObjectIdArray(final StringToObjectIdArray stringToObjectIdArray) {
    this.stringToObjectIdArray = Objects.requireNonNull(stringToObjectIdArray);
  }

  public void setStringToObjectIdCollection(
      final StringToObjectIdCollection stringToObjectIdCollection) {
    this.stringToObjectIdCollection = Objects.requireNonNull(stringToObjectIdCollection);
  }

  public void setStringToShortPrim(final StringToShortPrim stringToShortPrim) {
    this.stringToShortPrim = Objects.requireNonNull(stringToShortPrim);
  }

  public void setStringToShortPrimArray(final StringToShortPrimArray stringToShortPrimArray) {
    this.stringToShortPrimArray = Objects.requireNonNull(stringToShortPrimArray);
  }

  public void setStringToStringBased(final StringToStringBased stringToStringBased) {
    this.stringToStringBased = Objects.requireNonNull(stringToStringBased);
  }

  public void setStringToStringBasedArray(final StringToStringBasedArray stringToStringBasedArray) {
    this.stringToStringBasedArray = Objects.requireNonNull(stringToStringBasedArray);
  }

  public void setStringToStringBasedCollection(
      final StringToStringBasedCollection stringToStringBasedCollection) {
    this.stringToStringBasedCollection = Objects.requireNonNull(stringToStringBasedCollection);
  }
}
