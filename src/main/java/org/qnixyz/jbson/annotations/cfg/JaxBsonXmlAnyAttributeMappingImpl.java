package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import java.util.Objects;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;

@JaxBsonName(name = "jaxBsonXmlAnyAttributeMapping")
@SuppressWarnings({"all"})
public class JaxBsonXmlAnyAttributeMappingImpl implements JaxBsonXmlAnyAttributeMapping {

  private String bsonPrefix;

  private String namespaceURI;

  @SuppressWarnings("unused")
  private JaxBsonXmlAnyAttributeMappingImpl() {}

  public JaxBsonXmlAnyAttributeMappingImpl(final JaxBsonXmlAnyAttributeMapping annotation) {
    bsonPrefix = Objects.requireNonNull(annotation.bsonPrefix());
    namespaceURI = Objects.requireNonNull(annotation.namespaceURI());
  }

  public JaxBsonXmlAnyAttributeMappingImpl(final String bsonPrefix, final String namespaceURI) {
    this.bsonPrefix = Objects.requireNonNull(bsonPrefix);
    this.namespaceURI = Objects.requireNonNull(namespaceURI);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonXmlAnyAttributeMapping.class;
  }

  @Override
  public String bsonPrefix() {
    return bsonPrefix;
  }

  @Override
  public String namespaceURI() {
    return namespaceURI;
  }
}
