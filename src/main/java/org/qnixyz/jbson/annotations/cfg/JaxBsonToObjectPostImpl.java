package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPost;

@JaxBsonName(name = "jaxBsonToObjectPost")
@SuppressWarnings({"all"})
public class JaxBsonToObjectPostImpl implements JaxBsonToObjectPost {

  public JaxBsonToObjectPostImpl() {}

  public JaxBsonToObjectPostImpl(final JaxBsonToObjectPost annotation) {}

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonToObjectPost.class;
  }
}
