package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;

@JaxBsonName(name = "jaxBsonToBsonPost")
@SuppressWarnings({"all"})
public class JaxBsonToBsonPostImpl implements JaxBsonToBsonPost {

  public JaxBsonToBsonPostImpl() {}

  public JaxBsonToBsonPostImpl(final JaxBsonToBsonPost annotation) {}

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonToBsonPost.class;
  }
}
