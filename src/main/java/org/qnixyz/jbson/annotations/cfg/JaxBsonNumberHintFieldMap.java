package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonNumberHintFieldMap {

  private static class JaxBsonNumberHintFieldMapEntry
      implements Comparable<JaxBsonNumberHintFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonNumberHintImpl jaxBsonNumberHint;

    private JaxBsonNumberHintFieldMapEntry() {}

    private JaxBsonNumberHintFieldMapEntry(final Field field,
        final JaxBsonNumberHint jaxBsonNumberHint) {
      fieldClass = field.getDeclaringClass().getName();
      fieldName = field.getName();
      if (jaxBsonNumberHint != null) {
        if (jaxBsonNumberHint instanceof JaxBsonNumberHintImpl) {
          this.jaxBsonNumberHint = (JaxBsonNumberHintImpl) jaxBsonNumberHint;
        } else {
          this.jaxBsonNumberHint = new JaxBsonNumberHintImpl(jaxBsonNumberHint);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonNumberHintFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonNumberHintFieldMapEntry other = (JaxBsonNumberHintFieldMapEntry) obj;
      if (fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (fieldClass == null ? 0 : fieldClass.hashCode());
      result = prime * result + (fieldName == null ? 0 : fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonNumberHintFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonNumberHintFieldMapEntry> set;

    private void add(final JaxBsonNumberHintFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonNumberHintFieldMapType, JaxBsonNumberHintFieldMap> {

    @Override
    public JaxBsonNumberHintFieldMapType marshal(final JaxBsonNumberHintFieldMap v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonNumberHintFieldMapType ret = new JaxBsonNumberHintFieldMapType();
      for (final Entry<Field, JaxBsonNumberHint> e : v.map.entrySet()) {
        ret.add(new JaxBsonNumberHintFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonNumberHintFieldMap unmarshal(final JaxBsonNumberHintFieldMapType v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonNumberHintFieldMap ret = new JaxBsonNumberHintFieldMap();
      for (final JaxBsonNumberHintFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonNumberHintMap field: "
              + e.fieldClass + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonNumberHint);
      }
      return ret;
    }
  }

  private Map<Field, JaxBsonNumberHint> map;

  public boolean containsKey(final Field key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonNumberHint get(final Field field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Field> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonNumberHintFieldMap put(final Field field,
      final JaxBsonNumberHint jaxBsonNumberHint) {
    Objects.requireNonNull(field);
    Objects.requireNonNull(jaxBsonNumberHint);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonNumberHint);
    return this;
  }

  public JaxBsonNumberHint remove(final Field field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
