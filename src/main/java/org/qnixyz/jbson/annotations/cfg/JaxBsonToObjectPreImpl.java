package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;

@JaxBsonName(name = "jaxBsonToObjectPre")
@SuppressWarnings({"all"})
public class JaxBsonToObjectPreImpl implements JaxBsonToObjectPre {

  public JaxBsonToObjectPreImpl() {}

  public JaxBsonToObjectPreImpl(final JaxBsonToObjectPre annotation) {}

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonToObjectPre.class;
  }
}
