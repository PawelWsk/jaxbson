package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredMethodByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import static org.qnixyz.jbson.impl.Utils.compareStringLists;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonToObjectPreMethodMap {

  private static class JaxBsonToObjectPreMethodMapEntry
      implements Comparable<JaxBsonToObjectPreMethodMapEntry> {

    private JaxBsonToObjectPreImpl jaxBsonToObjectPre;

    private String methodClass;

    private String methodName;

    private List<String> methodParameterTypes;

    private JaxBsonToObjectPreMethodMapEntry() {}

    private JaxBsonToObjectPreMethodMapEntry(final Method method,
        final JaxBsonToObjectPre jaxBsonToObjectPre) {
      methodClass = method.getDeclaringClass().getName();
      methodName = method.getName();
      if (method.getParameterTypes() != null) {
        for (final Class<?> pt : method.getParameterTypes()) {
          addParameterType(pt);
        }
      }
      if (jaxBsonToObjectPre != null) {
        if (jaxBsonToObjectPre instanceof JaxBsonToObjectPreImpl) {
          this.jaxBsonToObjectPre = (JaxBsonToObjectPreImpl) jaxBsonToObjectPre;
        } else {
          this.jaxBsonToObjectPre = new JaxBsonToObjectPreImpl(jaxBsonToObjectPre);
        }
      }
    }

    private void addParameterType(final Class<?> pt) {
      if (methodParameterTypes == null) {
        methodParameterTypes = new ArrayList<>();
      }
      methodParameterTypes.add(pt.getName());
    }

    @Override
    public int compareTo(final JaxBsonToObjectPreMethodMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(methodClass, o.methodClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(methodName, o.methodName);
      if (ret != 0) {
        return ret;
      }
      ret = compareStringLists(methodParameterTypes, o.methodParameterTypes);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonToObjectPreMethodMapEntry other = (JaxBsonToObjectPreMethodMapEntry) obj;
      if (methodClass == null) {
        if (other.methodClass != null) {
          return false;
        }
      } else if (!methodClass.equals(other.methodClass)) {
        return false;
      }
      if (methodName == null) {
        if (other.methodName != null) {
          return false;
        }
      } else if (!methodName.equals(other.methodName)) {
        return false;
      }
      if (methodParameterTypes == null) {
        if (other.methodParameterTypes != null) {
          return false;
        }
      } else if (!methodParameterTypes.equals(other.methodParameterTypes)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (methodClass == null ? 0 : methodClass.hashCode());
      result = prime * result + (methodName == null ? 0 : methodName.hashCode());
      return result;
    }
  }

  private static class JaxBsonToObjectPreMethodMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonToObjectPreMethodMapEntry> set;

    private void add(final JaxBsonToObjectPreMethodMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonToObjectPreMethodMapType, JaxBsonToObjectPreMethodMap> {

    @Override
    public JaxBsonToObjectPreMethodMapType marshal(final JaxBsonToObjectPreMethodMap v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonToObjectPreMethodMapType ret = new JaxBsonToObjectPreMethodMapType();
      for (final Entry<Method, JaxBsonToObjectPre> e : v.map.entrySet()) {
        ret.add(new JaxBsonToObjectPreMethodMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonToObjectPreMethodMap unmarshal(final JaxBsonToObjectPreMethodMapType v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonToObjectPreMethodMap ret = new JaxBsonToObjectPreMethodMap();
      for (final JaxBsonToObjectPreMethodMapEntry e : v.set) {
        final Method method =
            declaredMethodByName(e.methodClass, e.methodName, e.methodParameterTypes);
        if (ret.containsKey(method)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonToObjectPreMap method: "
              + e.methodClass + "." + e.methodName);
        }
        ret.put(method, e.jaxBsonToObjectPre);
      }
      return ret;
    }
  }

  private Map<Method, JaxBsonToObjectPre> map;

  public boolean containsKey(final Method key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonToObjectPre get(final Method key) {
    Objects.requireNonNull(key);
    if (map == null) {
      return null;
    }
    return map.get(key);
  }

  public Set<Method> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonToObjectPreMethodMap put(final Method method,
      final JaxBsonToObjectPre jaxBsonToObjectPre) {
    Objects.requireNonNull(method);
    Objects.requireNonNull(jaxBsonToObjectPre);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(method, jaxBsonToObjectPre);
    return this;
  }

  public JaxBsonToObjectPre remove(final Method method) {
    if (map == null) {
      return null;
    }
    return map.remove(method);
  }
}
