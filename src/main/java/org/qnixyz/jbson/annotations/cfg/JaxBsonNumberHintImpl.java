package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import java.math.RoundingMode;
import java.util.Objects;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;

@JaxBsonName(name = "jaxBsonNumberHint")
@SuppressWarnings({"all"})
public class JaxBsonNumberHintImpl implements JaxBsonNumberHint {

  private boolean asString = AS_STRING_DEFAULT;

  private int precision = PRECISION_DEFAULT;

  private RoundingMode roundingMode = RoundingMode.HALF_UP;

  public JaxBsonNumberHintImpl() {}

  public JaxBsonNumberHintImpl(final JaxBsonNumberHint annotation) {
    setAsString(annotation.asString());
    setPrecision(annotation.precision());
    setRoundingMode(annotation.roundingMode());
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonNumberHint.class;
  }

  @Override
  public boolean asString() {
    return asString;
  }

  @Override
  public int precision() {
    return precision;
  }

  @Override
  public RoundingMode roundingMode() {
    return roundingMode;
  }

  public JaxBsonNumberHintImpl setAsString(final boolean asString) {
    this.asString = asString;
    return this;
  }

  public JaxBsonNumberHintImpl setPrecision(final int precision) {
    this.precision = precision;
    return this;
  }

  public JaxBsonNumberHintImpl setRoundingMode(final RoundingMode roundingMode) {
    this.roundingMode = Objects.requireNonNull(roundingMode);
    return this;
  }
}
