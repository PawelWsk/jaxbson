package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.classByNamne;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonNameTypeMap {

  private static class JaxBsonNameTypeMapEntry implements Comparable<JaxBsonNameTypeMapEntry> {

    private JaxBsonNameImpl jaxBsonName;

    private String typeClass;

    private JaxBsonNameTypeMapEntry() {}

    private JaxBsonNameTypeMapEntry(final Class<?> type, final JaxBsonName jaxBsonName) {
      typeClass = type.getName();
      if (jaxBsonName != null) {
        if (jaxBsonName instanceof JaxBsonNameImpl) {
          this.jaxBsonName = (JaxBsonNameImpl) jaxBsonName;
        } else {
          this.jaxBsonName = new JaxBsonNameImpl(jaxBsonName);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonNameTypeMapEntry o) {
      if (o == null) {
        return 1;
      }
      final int ret = compare(typeClass, o.typeClass);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonNameTypeMapEntry other = (JaxBsonNameTypeMapEntry) obj;
      if (typeClass == null) {
        if (other.typeClass != null) {
          return false;
        }
      } else if (!typeClass.equals(other.typeClass)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (typeClass == null ? 0 : typeClass.hashCode());
      return result;
    }
  }

  private static class JaxBsonNameTypeMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonNameTypeMapEntry> set;

    private void add(final JaxBsonNameTypeMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonNameTypeMapType, JaxBsonNameTypeMap> {

    @Override
    public JaxBsonNameTypeMapType marshal(final JaxBsonNameTypeMap v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonNameTypeMapType ret = new JaxBsonNameTypeMapType();
      for (final Entry<Class<?>, JaxBsonName> e : v.map.entrySet()) {
        ret.add(new JaxBsonNameTypeMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonNameTypeMap unmarshal(final JaxBsonNameTypeMapType v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonNameTypeMap ret = new JaxBsonNameTypeMap();
      for (final JaxBsonNameTypeMapEntry e : v.set) {
        final Class<?> type = classByNamne(e.typeClass);
        if (ret.containsKey(type)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonNameMap type: " + e.typeClass);
        }
        ret.put(type, e.jaxBsonName);
      }
      return ret;
    }
  }

  private Map<Class<?>, JaxBsonName> map;

  public boolean containsKey(final Class<?> key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonName get(final Class<?> field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Class<?>> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonNameTypeMap put(final Class<?> field, final JaxBsonName jaxBsonName) {
    Objects.requireNonNull(field);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonName);
    return this;
  }

  public JaxBsonName remove(final Class<?> field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
