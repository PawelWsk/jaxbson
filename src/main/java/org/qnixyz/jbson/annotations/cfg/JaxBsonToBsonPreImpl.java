package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;

@JaxBsonName(name = "jaxBsonToBsonPre")
@SuppressWarnings({"all"})
public class JaxBsonToBsonPreImpl implements JaxBsonToBsonPre {

  public JaxBsonToBsonPreImpl() {}

  public JaxBsonToBsonPreImpl(final JaxBsonToBsonPre annotation) {}

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonToBsonPre.class;
  }
}
