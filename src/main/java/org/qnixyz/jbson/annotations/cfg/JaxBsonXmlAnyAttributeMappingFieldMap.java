package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonXmlAnyAttributeMappingFieldMap {

  private static class JaxBsonXmlAnyAttributeMappingFieldMapEntry
      implements Comparable<JaxBsonXmlAnyAttributeMappingFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonXmlAnyAttributeMappingImpl jaxBsonXmlAnyAttributeMapping;

    private JaxBsonXmlAnyAttributeMappingFieldMapEntry() {}

    private JaxBsonXmlAnyAttributeMappingFieldMapEntry(final Field field,
        final JaxBsonXmlAnyAttributeMapping jaxBsonXmlAnyAttributeMapping) {
      fieldClass = field.getDeclaringClass().getName();
      fieldName = field.getName();
      if (jaxBsonXmlAnyAttributeMapping != null) {
        if (jaxBsonXmlAnyAttributeMapping instanceof JaxBsonXmlAnyAttributeMappingImpl) {
          this.jaxBsonXmlAnyAttributeMapping =
              (JaxBsonXmlAnyAttributeMappingImpl) jaxBsonXmlAnyAttributeMapping;
        } else {
          this.jaxBsonXmlAnyAttributeMapping =
              new JaxBsonXmlAnyAttributeMappingImpl(jaxBsonXmlAnyAttributeMapping);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonXmlAnyAttributeMappingFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonXmlAnyAttributeMappingFieldMapEntry other =
          (JaxBsonXmlAnyAttributeMappingFieldMapEntry) obj;
      if (fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (fieldClass == null ? 0 : fieldClass.hashCode());
      result = prime * result + (fieldName == null ? 0 : fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonXmlAnyAttributeMappingFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonXmlAnyAttributeMappingFieldMapEntry> set;

    private void add(final JaxBsonXmlAnyAttributeMappingFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter extends
      JaxBsonAdapter<JaxBsonXmlAnyAttributeMappingFieldMapType, JaxBsonXmlAnyAttributeMappingFieldMap> {

    @Override
    public JaxBsonXmlAnyAttributeMappingFieldMapType marshal(
        final JaxBsonXmlAnyAttributeMappingFieldMap v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonXmlAnyAttributeMappingFieldMapType ret =
          new JaxBsonXmlAnyAttributeMappingFieldMapType();
      for (final Entry<Field, JaxBsonXmlAnyAttributeMapping> e : v.map.entrySet()) {
        ret.add(new JaxBsonXmlAnyAttributeMappingFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonXmlAnyAttributeMappingFieldMap unmarshal(
        final JaxBsonXmlAnyAttributeMappingFieldMapType v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonXmlAnyAttributeMappingFieldMap ret = new JaxBsonXmlAnyAttributeMappingFieldMap();
      for (final JaxBsonXmlAnyAttributeMappingFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonXmlAnyAttributeMappingMap field: " + e.fieldClass
                  + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonXmlAnyAttributeMapping);
      }
      return ret;
    }
  }

  private Map<Field, JaxBsonXmlAnyAttributeMapping> map;

  public boolean containsKey(final Field key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonXmlAnyAttributeMapping get(final Field field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Field> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonXmlAnyAttributeMappingFieldMap put(final Field field,
      final JaxBsonXmlAnyAttributeMapping jaxBsonXmlAnyAttributeMapping) {
    Objects.requireNonNull(field);
    Objects.requireNonNull(jaxBsonXmlAnyAttributeMapping);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonXmlAnyAttributeMapping);
    return this;
  }

  public JaxBsonXmlAnyAttributeMapping remove(final Field field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
