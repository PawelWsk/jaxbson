package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonIgnoreTransientFieldMap {

  private static class JaxBsonIgnoreTransientFieldMapEntry
      implements Comparable<JaxBsonIgnoreTransientFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonIgnoreTransientImpl jaxBsonIgnoreTransient;

    private JaxBsonIgnoreTransientFieldMapEntry() {}

    private JaxBsonIgnoreTransientFieldMapEntry(final Field field,
        final JaxBsonIgnoreTransient jaxBsonIgnoreTransient) {
      fieldClass = field.getDeclaringClass().getName();
      fieldName = field.getName();
      if (jaxBsonIgnoreTransient != null) {
        if (jaxBsonIgnoreTransient instanceof JaxBsonIgnoreTransientImpl) {
          this.jaxBsonIgnoreTransient = (JaxBsonIgnoreTransientImpl) jaxBsonIgnoreTransient;
        } else {
          this.jaxBsonIgnoreTransient = new JaxBsonIgnoreTransientImpl(jaxBsonIgnoreTransient);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonIgnoreTransientFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonIgnoreTransientFieldMapEntry other = (JaxBsonIgnoreTransientFieldMapEntry) obj;
      if (fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (fieldClass == null ? 0 : fieldClass.hashCode());
      result = prime * result + (fieldName == null ? 0 : fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonIgnoreTransientFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonIgnoreTransientFieldMapEntry> set;

    private void add(final JaxBsonIgnoreTransientFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter extends
      JaxBsonAdapter<JaxBsonIgnoreTransientFieldMapType, JaxBsonIgnoreTransientFieldMap> {

    @Override
    public JaxBsonIgnoreTransientFieldMapType marshal(final JaxBsonIgnoreTransientFieldMap v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonIgnoreTransientFieldMapType ret = new JaxBsonIgnoreTransientFieldMapType();
      for (final Entry<Field, JaxBsonIgnoreTransient> e : v.map.entrySet()) {
        ret.add(new JaxBsonIgnoreTransientFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonIgnoreTransientFieldMap unmarshal(final JaxBsonIgnoreTransientFieldMapType v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonIgnoreTransientFieldMap ret = new JaxBsonIgnoreTransientFieldMap();
      for (final JaxBsonIgnoreTransientFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonIgnoreTransientMap field: " + e.fieldClass + "."
                  + e.fieldName);
        }
        ret.put(field, e.jaxBsonIgnoreTransient);
      }
      return ret;
    }
  }

  private Map<Field, JaxBsonIgnoreTransient> map;

  public boolean containsKey(final Field key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonIgnoreTransient get(final Field field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Field> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonIgnoreTransientFieldMap put(final Field field,
      final JaxBsonIgnoreTransient jaxBsonIgnoreTransient) {
    Objects.requireNonNull(field);
    Objects.requireNonNull(jaxBsonIgnoreTransient);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonIgnoreTransient);
    return this;
  }

  public JaxBsonIgnoreTransient remove(final Field field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
