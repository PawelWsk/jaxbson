package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonNameFieldMap {

  private static class JaxBsonNameFieldMapEntry implements Comparable<JaxBsonNameFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonNameImpl jaxBsonName;

    private JaxBsonNameFieldMapEntry() {}

    private JaxBsonNameFieldMapEntry(final Field field, final JaxBsonName jaxBsonName) {
      fieldClass = field.getDeclaringClass().getName();
      fieldName = field.getName();
      if (jaxBsonName != null) {
        if (jaxBsonName instanceof JaxBsonNameImpl) {
          this.jaxBsonName = (JaxBsonNameImpl) jaxBsonName;
        } else {
          this.jaxBsonName = new JaxBsonNameImpl(jaxBsonName);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonNameFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonNameFieldMapEntry other = (JaxBsonNameFieldMapEntry) obj;
      if (fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (fieldClass == null ? 0 : fieldClass.hashCode());
      result = prime * result + (fieldName == null ? 0 : fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonNameFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonNameFieldMapEntry> set;

    private void add(final JaxBsonNameFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonNameFieldMapType, JaxBsonNameFieldMap> {

    @Override
    public JaxBsonNameFieldMapType marshal(final JaxBsonNameFieldMap v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonNameFieldMapType ret = new JaxBsonNameFieldMapType();
      for (final Entry<Field, JaxBsonName> e : v.map.entrySet()) {
        ret.add(new JaxBsonNameFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonNameFieldMap unmarshal(final JaxBsonNameFieldMapType v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonNameFieldMap ret = new JaxBsonNameFieldMap();
      for (final JaxBsonNameFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonNameMap field: " + e.fieldClass + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonName);
      }
      return ret;
    }
  }

  private Map<Field, JaxBsonName> map;

  public boolean containsKey(final Field field) {
    if (map == null) {
      return false;
    }
    return map.containsKey(field);
  }

  public JaxBsonName get(final Field field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Field> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonNameFieldMap put(final Field field, final JaxBsonName jaxBsonName) {
    Objects.requireNonNull(field);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonName);
    return this;
  }

  public JaxBsonName remove(final Field field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
