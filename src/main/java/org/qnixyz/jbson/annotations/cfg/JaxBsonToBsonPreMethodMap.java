package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredMethodByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import static org.qnixyz.jbson.impl.Utils.compareStringLists;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonToBsonPreMethodMap {

  private static class JaxBsonToBsonPreMethodMapEntry
      implements Comparable<JaxBsonToBsonPreMethodMapEntry> {

    private JaxBsonToBsonPreImpl jaxBsonToBsonPre;

    private String methodClass;

    private String methodName;

    private List<String> methodParameterTypes;

    private JaxBsonToBsonPreMethodMapEntry() {}

    private JaxBsonToBsonPreMethodMapEntry(final Method method,
        final JaxBsonToBsonPre jaxBsonToBsonPre) {
      methodClass = method.getDeclaringClass().getName();
      methodName = method.getName();
      if (method.getParameterTypes() != null) {
        for (final Class<?> pt : method.getParameterTypes()) {
          addParameterType(pt);
        }
      }
      if (jaxBsonToBsonPre != null) {
        if (jaxBsonToBsonPre instanceof JaxBsonToBsonPreImpl) {
          this.jaxBsonToBsonPre = (JaxBsonToBsonPreImpl) jaxBsonToBsonPre;
        } else {
          this.jaxBsonToBsonPre = new JaxBsonToBsonPreImpl(jaxBsonToBsonPre);
        }
      }
    }

    private void addParameterType(final Class<?> pt) {
      if (methodParameterTypes == null) {
        methodParameterTypes = new ArrayList<>();
      }
      methodParameterTypes.add(pt.getName());
    }

    @Override
    public int compareTo(final JaxBsonToBsonPreMethodMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(methodClass, o.methodClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(methodName, o.methodName);
      if (ret != 0) {
        return ret;
      }
      ret = compareStringLists(methodParameterTypes, o.methodParameterTypes);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonToBsonPreMethodMapEntry other = (JaxBsonToBsonPreMethodMapEntry) obj;
      if (methodClass == null) {
        if (other.methodClass != null) {
          return false;
        }
      } else if (!methodClass.equals(other.methodClass)) {
        return false;
      }
      if (methodName == null) {
        if (other.methodName != null) {
          return false;
        }
      } else if (!methodName.equals(other.methodName)) {
        return false;
      }
      if (methodParameterTypes == null) {
        if (other.methodParameterTypes != null) {
          return false;
        }
      } else if (!methodParameterTypes.equals(other.methodParameterTypes)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (methodClass == null ? 0 : methodClass.hashCode());
      result = prime * result + (methodName == null ? 0 : methodName.hashCode());
      result =
          prime * result + (methodParameterTypes == null ? 0 : methodParameterTypes.hashCode());
      return result;
    }
  }

  private static class JaxBsonToBsonPreMethodMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonToBsonPreMethodMapEntry> set;

    private void add(final JaxBsonToBsonPreMethodMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonToBsonPreMethodMapType, JaxBsonToBsonPreMethodMap> {

    @Override
    public JaxBsonToBsonPreMethodMapType marshal(final JaxBsonToBsonPreMethodMap v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonToBsonPreMethodMapType ret = new JaxBsonToBsonPreMethodMapType();
      for (final Entry<Method, JaxBsonToBsonPre> e : v.map.entrySet()) {
        ret.add(new JaxBsonToBsonPreMethodMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonToBsonPreMethodMap unmarshal(final JaxBsonToBsonPreMethodMapType v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonToBsonPreMethodMap ret = new JaxBsonToBsonPreMethodMap();
      for (final JaxBsonToBsonPreMethodMapEntry e : v.set) {
        final Method method =
            declaredMethodByName(e.methodClass, e.methodName, e.methodParameterTypes);
        if (ret.containsKey(method)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonToBsonPreMap method: "
              + e.methodClass + "." + e.methodName);
        }
        ret.put(method, e.jaxBsonToBsonPre);
      }
      return ret;
    }
  }

  private Map<Method, JaxBsonToBsonPre> map;

  public boolean containsKey(final Method key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonToBsonPre get(final Method method) {
    Objects.requireNonNull(method);
    if (map == null) {
      return null;
    }
    return map.get(method);
  }

  public Set<Method> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonToBsonPreMethodMap put(final Method method,
      final JaxBsonToBsonPre jaxBsonToBsonPre) {
    Objects.requireNonNull(method);
    Objects.requireNonNull(jaxBsonToBsonPre);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(method, jaxBsonToBsonPre);
    return this;
  }

  public JaxBsonToBsonPre remove(final Method method) {
    if (map == null) {
      return null;
    }
    return map.remove(method);
  }
}
