package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonXmlAnyAttributeMappingsFieldMap {

  private static class JaxBsonXmlAnyAttributeMappingsFieldMapEntry
      implements Comparable<JaxBsonXmlAnyAttributeMappingsFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    @JaxBsonName(name = "jaxBsonXmlAnyAttributeMappings")
    private JaxBsonXmlAnyAttributeMappingImpl[] jaxBsonXmlAnyAttributeMappings;

    private JaxBsonXmlAnyAttributeMappingsFieldMapEntry() {}

    private JaxBsonXmlAnyAttributeMappingsFieldMapEntry(final Field field,
        final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappings) {
      fieldClass = field.getDeclaringClass().getName();
      fieldName = field.getName();
      if (jaxBsonXmlAnyAttributeMappings != null) {
        final JaxBsonXmlAnyAttributeMapping[] mappings = jaxBsonXmlAnyAttributeMappings.mappings();
        if (mappings != null) {
          this.jaxBsonXmlAnyAttributeMappings =
              new JaxBsonXmlAnyAttributeMappingImpl[mappings.length];
          for (int i = 0; i < mappings.length; i++) {
            final JaxBsonXmlAnyAttributeMapping mapping = mappings[i];
            if (mapping instanceof JaxBsonXmlAnyAttributeMappingImpl) {
              this.jaxBsonXmlAnyAttributeMappings[i] = (JaxBsonXmlAnyAttributeMappingImpl) mapping;
            } else {
              this.jaxBsonXmlAnyAttributeMappings[i] =
                  new JaxBsonXmlAnyAttributeMappingImpl(mapping);
            }
          }
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonXmlAnyAttributeMappingsFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonXmlAnyAttributeMappingsFieldMapEntry other =
          (JaxBsonXmlAnyAttributeMappingsFieldMapEntry) obj;
      if (fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (fieldClass == null ? 0 : fieldClass.hashCode());
      result = prime * result + (fieldName == null ? 0 : fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonXmlAnyAttributeMappingsFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonXmlAnyAttributeMappingsFieldMapEntry> set;

    private void add(final JaxBsonXmlAnyAttributeMappingsFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter extends
      JaxBsonAdapter<JaxBsonXmlAnyAttributeMappingsFieldMapType, JaxBsonXmlAnyAttributeMappingsFieldMap> {

    @Override
    public JaxBsonXmlAnyAttributeMappingsFieldMapType marshal(
        final JaxBsonXmlAnyAttributeMappingsFieldMap v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonXmlAnyAttributeMappingsFieldMapType ret =
          new JaxBsonXmlAnyAttributeMappingsFieldMapType();
      for (final Entry<Field, JaxBsonXmlAnyAttributeMappings> e : v.map.entrySet()) {
        ret.add(new JaxBsonXmlAnyAttributeMappingsFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonXmlAnyAttributeMappingsFieldMap unmarshal(
        final JaxBsonXmlAnyAttributeMappingsFieldMapType v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonXmlAnyAttributeMappingsFieldMap ret =
          new JaxBsonXmlAnyAttributeMappingsFieldMap();
      for (final JaxBsonXmlAnyAttributeMappingsFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonXmlAnyAttributeMappingsMap field: " + e.fieldClass
                  + "." + e.fieldName);
        }
        ret.put(field, new JaxBsonXmlAnyAttributeMappingsImpl(e.jaxBsonXmlAnyAttributeMappings));
      }
      return ret;
    }
  }

  private Map<Field, JaxBsonXmlAnyAttributeMappings> map;

  public boolean containsKey(final Field key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonXmlAnyAttributeMappings get(final Field field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Field> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public JaxBsonXmlAnyAttributeMappingsFieldMap put(final Field field,
      final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappings) {
    Objects.requireNonNull(field);
    Objects.requireNonNull(jaxBsonXmlAnyAttributeMappings);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonXmlAnyAttributeMappings);
    return this;
  }

  public JaxBsonXmlAnyAttributeMappings remove(final Field field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
