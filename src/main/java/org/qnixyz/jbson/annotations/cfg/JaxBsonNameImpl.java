package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import java.util.Objects;
import org.qnixyz.jbson.annotations.JaxBsonName;

@JaxBsonName(name = "jaxBsonName")
@SuppressWarnings({"all"})
public class JaxBsonNameImpl implements JaxBsonName {

  private final String name;

  private JaxBsonNameImpl() {
    name = null;
  }

  public JaxBsonNameImpl(final JaxBsonName annotation) {
    Objects.requireNonNull(annotation);
    name = Objects.requireNonNull(annotation.name());
  }

  public JaxBsonNameImpl(final String name) {
    this.name = Objects.requireNonNull(name);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonName.class;
  }

  @Override
  public String name() {
    return name;
  }
}
