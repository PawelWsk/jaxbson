package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonTransient;

@JaxBsonName(name = "jaxBsonTransient")
@SuppressWarnings({"all"})
public class JaxBsonTransientImpl implements JaxBsonTransient {

  public JaxBsonTransientImpl() {}

  public JaxBsonTransientImpl(final JaxBsonTransient annotation) {}

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonTransient.class;
  }
}
