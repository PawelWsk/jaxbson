package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonTransientFieldMap {

  private static class JaxBsonTransientFieldMapEntry
      implements Comparable<JaxBsonTransientFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonTransientImpl jaxBsonTransient;

    private JaxBsonTransientFieldMapEntry() {}

    private JaxBsonTransientFieldMapEntry(final Field field,
        final JaxBsonTransient jaxBsonTransient) {
      fieldClass = field.getDeclaringClass().getName();
      fieldName = field.getName();
      if (jaxBsonTransient != null) {
        if (jaxBsonTransient instanceof JaxBsonTransientImpl) {
          this.jaxBsonTransient = (JaxBsonTransientImpl) jaxBsonTransient;
        } else {
          this.jaxBsonTransient = new JaxBsonTransientImpl(jaxBsonTransient);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonTransientFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonTransientFieldMapEntry other = (JaxBsonTransientFieldMapEntry) obj;
      if (fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (fieldClass == null ? 0 : fieldClass.hashCode());
      result = prime * result + (fieldName == null ? 0 : fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonTransientFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonTransientFieldMapEntry> set;

    private void add(final JaxBsonTransientFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (set == null) {
        set = new TreeSet<>();
      }
      set.add(e);
    }

    private boolean isEmpty() {
      return set == null || set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonTransientFieldMapType, JaxBsonTransientFieldMap> {

    @Override
    public JaxBsonTransientFieldMapType marshal(final JaxBsonTransientFieldMap v) throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonTransientFieldMapType ret = new JaxBsonTransientFieldMapType();
      for (final Entry<Field, JaxBsonTransient> e : v.map.entrySet()) {
        ret.add(new JaxBsonTransientFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonTransientFieldMap unmarshal(final JaxBsonTransientFieldMapType v)
        throws Exception {
      if (v == null || v.isEmpty()) {
        return null;
      }
      final JaxBsonTransientFieldMap ret = new JaxBsonTransientFieldMap();
      for (final JaxBsonTransientFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonTransientMap field: "
              + e.fieldClass + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonTransient);
      }
      return ret;
    }
  }

  private Map<Field, JaxBsonTransient> map;

  public boolean containsKey(final Field key) {
    if (map == null) {
      return false;
    }
    return map.containsKey(key);
  }

  public JaxBsonTransient get(final Field field) {
    Objects.requireNonNull(field);
    if (map == null) {
      return null;
    }
    return map.get(field);
  }

  public Set<Field> getKeySet() {
    if (map == null) {
      return Collections.emptySet();
    }
    return map.keySet();
  }

  public boolean isEmpty() {
    return map == null || map.isEmpty();
  }

  public void put(final Field field, final JaxBsonTransient jaxBsonTransient) {
    Objects.requireNonNull(field);
    Objects.requireNonNull(jaxBsonTransient);
    if (map == null) {
      map = new HashMap<>();
    }
    map.put(field, jaxBsonTransient);
  }

  public JaxBsonTransient remove(final Field field) {
    if (map == null) {
      return null;
    }
    return map.remove(field);
  }
}
