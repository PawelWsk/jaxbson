package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import java.util.Objects;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;

@JaxBsonName(name = "jaxBsonXmlAnyAttributeMappings")
@SuppressWarnings({"all"})
public class JaxBsonXmlAnyAttributeMappingsImpl implements JaxBsonXmlAnyAttributeMappings {

  @JaxBsonName(name = "jaxBsonXmlAnyAttributeMappings")
  private JaxBsonXmlAnyAttributeMappingImpl[] jaxBsonXmlAnyAttributeMappings;

  @SuppressWarnings("unused")
  private JaxBsonXmlAnyAttributeMappingsImpl() {}

  public JaxBsonXmlAnyAttributeMappingsImpl(final JaxBsonXmlAnyAttributeMapping[] mappings) {
    Objects.requireNonNull(mappings);
    setMappings(mappings);
  }

  public JaxBsonXmlAnyAttributeMappingsImpl(final JaxBsonXmlAnyAttributeMappings annotation) {
    Objects.requireNonNull(annotation);
    Objects.requireNonNull(annotation.mappings());
    setMappings(annotation.mappings());
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonXmlAnyAttributeMappings.class;
  }

  @Override
  public JaxBsonXmlAnyAttributeMapping[] mappings() {
    return jaxBsonXmlAnyAttributeMappings;
  }

  private void setMappings(final JaxBsonXmlAnyAttributeMapping[] mappings) {
    Objects.requireNonNull(mappings);
    jaxBsonXmlAnyAttributeMappings = new JaxBsonXmlAnyAttributeMappingImpl[mappings.length];
    for (int i = 0; i < mappings.length; i++) {
      if (mappings[i] instanceof JaxBsonXmlAnyAttributeMappingImpl) {
        jaxBsonXmlAnyAttributeMappings[i] = (JaxBsonXmlAnyAttributeMappingImpl) mappings[i];
      } else {
        jaxBsonXmlAnyAttributeMappings[i] = new JaxBsonXmlAnyAttributeMappingImpl(mappings[i]);
      }
    }
  }
}
