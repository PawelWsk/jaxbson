package org.qnixyz.jbson.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Instructs JaxBson to also look into other classes.
 *
 * @author Vincenzo Zocca
 */
@Target({FIELD, TYPE})
@Retention(RUNTIME)
public @interface JaxBsonSeeAlso {
  Class<?>[] value();
}
