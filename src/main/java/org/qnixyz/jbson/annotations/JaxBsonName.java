package org.qnixyz.jbson.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation to force name on field or type.
 *
 * @author Vincenzo Zocca
 */
@Retention(RUNTIME)
@Target({FIELD, TYPE})
public @interface JaxBsonName {
  String name();
}
