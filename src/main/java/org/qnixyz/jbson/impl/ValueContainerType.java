package org.qnixyz.jbson.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

enum ValueContainerType {

  /**
   * Any attribute
   */
  ANY_ATTRIBUTE(false, false, null),

  /**
   * Array
   */
  ARRAY(true, false, null),

  /**
   * List
   */
  LIST(false, true, ArrayList.class),

  /**
   * None
   */
  NONE(false, false, null),

  /**
   * Set
   */
  SET(false, true, HashSet.class),

  /**
   * Sorted set
   */
  SORTED_SET(false, true, TreeSet.class);

  protected static ValueContainerType fromField(final Field field) {
    final Class<?> type = field.getType();
    if (type.isArray()) {
      return ARRAY;
    }
    if (List.class.isAssignableFrom(type)) {
      return LIST;
    }
    if (SortedSet.class.isAssignableFrom(type)) {
      return SORTED_SET;
    }
    if (Set.class.isAssignableFrom(type)) {
      return SET;
    }
    if (Collection.class.isAssignableFrom(type)) {
      throw new IllegalStateException("Collection of type " + type.getName()
          + " not supported. Supported collections are: " + List.class.getName() + ", " + Set.class
          + " and " + SortedSet.class.getSimpleName());
    }
    if (Map.class.isAssignableFrom(type)) {
      throw new IllegalStateException(
          "Value container type " + Map.class.getName() + " not supported " + field);
    }
    return NONE;
  }

  private boolean array;

  private boolean collection;

  private final Class<?> collectionClass;

  private final Constructor<Collection<Object>> collectionConstructor;

  private ValueContainerType(final boolean array, final boolean collection,
      final Class<?> collectionClass) {
    this.array = array;
    this.collection = collection;
    this.collectionClass = collectionClass;
    collectionConstructor = createCollectionConstructor();
  }

  private Constructor<Collection<Object>> createCollectionConstructor() {
    if (collectionClass == null) {
      return null;
    }
    try {
      @SuppressWarnings("unchecked")
      final Constructor<Collection<Object>> ret =
          (Constructor<Collection<Object>>) collectionClass.getDeclaredConstructor();
      ret.setAccessible(true);
      return ret;
    } catch (NoSuchMethodException | SecurityException e) {
      throw new IllegalStateException(
          "No no-argument constructor for collection type " + collectionClass.getName());
    }
  }

  protected Collection<Object> collectionInstance() {
    try {
      return collectionConstructor.newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException(
          "Failed to instantiate object of collection class '" + collectionClass.getName() + "'.",
          e);
    }
  }

  protected boolean isArray() {
    return array;
  }

  protected boolean isCollection() {
    return collection;
  }
}
