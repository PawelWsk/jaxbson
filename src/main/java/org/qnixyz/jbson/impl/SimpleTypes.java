package org.qnixyz.jbson.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.qnixyz.jbson.DateBased;
import org.qnixyz.jbson.JaxBsonConfiguration;
import org.qnixyz.jbson.JaxBsonFieldContext;
import org.qnixyz.jbson.NumberBased;
import org.qnixyz.jbson.StringBased;

class SimpleTypes {

  private static class FuncMap {

    @FunctionalInterface
    public interface FuncToBson {
      Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Object src);
    }

    @FunctionalInterface
    public interface FuncToObject {
      Object convert(JaxBsonFieldContext fieldCtx, Class<?> mainType, Object src);
    }

    @FunctionalInterface
    public interface FuncToObjectCollection {
      Object convert(JaxBsonFieldContext fieldCtx, Class<?> mainType, Object src,
          Collection<?> dst);
    }

    private final Map<Class<?>, FuncToBson> arrayToBsonMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> binaryCollectionToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> binaryCollectionToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> binaryCollectionToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> binaryToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> binaryToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> binaryToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> booleanCollectionToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> booleanCollectionToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> booleanCollectionToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> booleanToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> booleanToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> booleanToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToBson> collectionToBsonMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> dateCollectionToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> dateCollectionToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> dateCollectionToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> dateToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> dateToObjectCollectionMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> dateToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> numberCollectionToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> numberCollectionToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> numberCollectionToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> numberToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> numberToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> numberToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> objectIdCollectionToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> objectIdCollectionToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> objectIdCollectionToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> objectIdToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> objectIdToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> objectIdToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> stringCollectionToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> stringCollectionToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> stringCollectionToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToObject> stringToObjectArrayMap = new HashMap<>();

    private final Map<Class<?>, FuncToObjectCollection> stringToObjectCollectionMap =
        new HashMap<>();

    private final Map<Class<?>, FuncToObject> stringToObjectMap = new HashMap<>();

    private final Map<Class<?>, FuncToBson> toBsonMap = new HashMap<>();

    private FuncMap() {

      putToBsonMap();
      putArrayToBsonMap();
      putCollectionToBsonMap();

      putBinaryToObjectMap();
      putBinaryToObjectArrayMap();
      putBinaryToObjectCollectionMap();
      putBinaryCollectionToObjectMap();
      putBinaryCollectionToObjectArrayMap();
      putBinaryCollectionToObjectCollectionMap();

      putBooleanToObjectMap();
      putBooleanToObjectArrayMap();
      putBooleanToObjectCollectionMap();
      putBooleanCollectionToObjectMap();
      putBooleanCollectionToObjectArrayMap();
      putBooleanCollectionToObjectCollectionMap();

      putDateToObjectMap();
      putDateToObjectArrayMap();
      putDateToObjectCollectionMap();
      putDateCollectionToObjectMap();
      putDateCollectionToObjectArrayMap();
      putDateCollectionToObjectCollectionMap();

      putNumberToObjectMap();
      putNumberToObjectArrayMap();
      putNumberToObjectCollectionMap();
      putNumberCollectionToObjectMap();
      putNumberCollectionToObjectArrayMap();
      putNumberCollectionToObjectCollectionMap();

      putObjectIdToObjectMap();
      putObjectIdToObjectArrayMap();
      putObjectIdToObjectCollectionMap();
      putObjectIdCollectionToObjectMap();
      putObjectIdCollectionToObjectArrayMap();
      putObjectIdCollectionToObjectCollectionMap();

      putStringToObjectMap();
      putStringToObjectArrayMap();
      putStringToObjectCollectionMap();
      putStringCollectionToObjectMap();
      putStringCollectionToObjectArrayMap();
      putStringCollectionToObjectCollectionMap();
    }

    private FuncToBson getArrayToBson(final Class<?> type) {
      FuncToBson ret = arrayToBsonMap.get(type);
      if (ret != null) {
        return ret;
      }
      ret = getArrayToBsonDateBased(type);
      if (ret != null) {
        return ret;
      }
      ret = getArrayToBsonStringBased(type);
      if (ret != null) {
        return ret;
      }
      return getArrayToBsonEnum(type);
    }

    private synchronized FuncToBson getArrayToBsonDateBased(final Class<?> type) {
      for (final Class<?> dateBasedType : DateBased.TYPES_SET) {
        if (dateBasedType.isAssignableFrom(type)) {
          final FuncToBson ret = arrayToBsonMap.get(dateBasedType);
          arrayToBsonMap.put(type, ret);
          return ret;
        }
      }
      return null;
    }

    private synchronized FuncToBson getArrayToBsonEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToBson ret = arrayToBsonMap.get(Enum.class);
      arrayToBsonMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToBson getArrayToBsonStringBased(final Class<?> type) {
      for (final Class<?> stringBasedType : StringBased.TYPES_SET) {
        if (stringBasedType.isAssignableFrom(type)) {
          final FuncToBson ret = arrayToBsonMap.get(stringBasedType);
          arrayToBsonMap.put(type, ret);
          return ret;
        }
      }
      return null;
    }

    private FuncToObject getBinaryCollectionToObject(final Class<?> type) {
      final FuncToObject ret = binaryCollectionToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBinaryCollectionToObjectEnum(type);
    }

    private FuncToObject getBinaryCollectionToObjectArray(final Class<?> type) {
      final FuncToObject ret = binaryCollectionToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBinaryCollectionToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getBinaryCollectionToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = binaryCollectionToObjectArrayMap.get(Enum.class);
      binaryCollectionToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getBinaryCollectionToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = binaryCollectionToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBinaryCollectionToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getBinaryCollectionToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = binaryCollectionToObjectCollectionMap.get(Enum.class);
      binaryCollectionToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getBinaryCollectionToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = binaryCollectionToObjectMap.get(Enum.class);
      binaryCollectionToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getBinaryToObject(final Class<?> type) {
      final FuncToObject ret = binaryToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBinaryToObjectEnum(type);
    }

    private FuncToObject getBinaryToObjectArray(final Class<?> type) {
      final FuncToObject ret = binaryToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBinaryToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getBinaryToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = binaryToObjectArrayMap.get(Enum.class);
      binaryToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getBinaryToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = binaryToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBinaryToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getBinaryToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = binaryToObjectCollectionMap.get(Enum.class);
      binaryToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getBinaryToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = binaryToObjectMap.get(Enum.class);
      binaryToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getBooleanCollectionToObject(final Class<?> type) {
      final FuncToObject ret = booleanCollectionToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBooleanCollectionToObjectEnum(type);
    }

    private FuncToObject getBooleanCollectionToObjectArray(final Class<?> type) {
      final FuncToObject ret = booleanCollectionToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBooleanCollectionToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getBooleanCollectionToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = booleanCollectionToObjectArrayMap.get(Enum.class);
      booleanCollectionToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getBooleanCollectionToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = booleanCollectionToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBooleanCollectionToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getBooleanCollectionToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = booleanCollectionToObjectCollectionMap.get(Enum.class);
      booleanCollectionToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getBooleanCollectionToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = booleanCollectionToObjectMap.get(Enum.class);
      booleanCollectionToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getBooleanToObject(final Class<?> type) {
      final FuncToObject ret = booleanToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBooleanToObjectEnum(type);
    }

    private FuncToObject getBooleanToObjectArray(final Class<?> type) {
      final FuncToObject ret = dateToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBooleanToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getBooleanToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = dateToObjectArrayMap.get(Enum.class);
      dateToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getBooleanToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = booleanToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getBooleanToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getBooleanToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = booleanToObjectCollectionMap.get(Enum.class);
      booleanToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getBooleanToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = booleanToObjectMap.get(Enum.class);
      booleanToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToBson getCollectionToBson(final Class<?> type) {
      FuncToBson ret = collectionToBsonMap.get(type);
      if (ret != null) {
        return ret;
      }
      ret = getCollectionToBsonDateBased(type);
      if (ret != null) {
        return ret;
      }
      ret = getCollectionToBsonStringBased(type);
      if (ret != null) {
        return ret;
      }
      return getCollectionToBsonEnum(type);
    }

    private synchronized FuncToBson getCollectionToBsonDateBased(final Class<?> type) {
      for (final Class<?> dateBasedType : DateBased.TYPES_SET) {
        if (dateBasedType.isAssignableFrom(type)) {
          final FuncToBson ret = collectionToBsonMap.get(dateBasedType);
          collectionToBsonMap.put(type, ret);
          return ret;
        }
      }
      return null;
    }

    private synchronized FuncToBson getCollectionToBsonEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToBson ret = collectionToBsonMap.get(Enum.class);
      collectionToBsonMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToBson getCollectionToBsonStringBased(final Class<?> type) {
      for (final Class<?> stringBasedType : StringBased.TYPES_SET) {
        if (stringBasedType.isAssignableFrom(type)) {
          final FuncToBson ret = collectionToBsonMap.get(stringBasedType);
          collectionToBsonMap.put(type, ret);
          return ret;
        }
      }
      return null;
    }

    private FuncToObject getDateCollectionToObject(final Class<?> type) {
      final FuncToObject ret = dateCollectionToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getDateCollectionToObjectEnum(type);
    }

    private FuncToObject getDateCollectionToObjectArray(final Class<?> type) {
      final FuncToObject ret = dateCollectionToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getDateCollectionToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getDateCollectionToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = dateCollectionToObjectArrayMap.get(Enum.class);
      dateCollectionToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getDateCollectionToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = dateCollectionToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getDateCollectionToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getDateCollectionToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = dateCollectionToObjectCollectionMap.get(Enum.class);
      dateCollectionToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getDateCollectionToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = dateCollectionToObjectMap.get(Enum.class);
      dateCollectionToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getDateToObject(final Class<?> type) {
      final FuncToObject ret = dateToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getDateToObjectEnum(type);
    }

    private FuncToObject getDateToObjectArray(final Class<?> type) {
      final FuncToObject ret = booleanToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getDateToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getDateToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = booleanToObjectArrayMap.get(Enum.class);
      booleanToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getDateToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = dateToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getDateToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getDateToObjectCollectionEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = dateToObjectCollectionMap.get(Enum.class);
      dateToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getDateToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = dateToObjectMap.get(Enum.class);
      dateToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getNumberCollectionToObject(final Class<?> type) {
      final FuncToObject ret = numberCollectionToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getNumberCollectionToObjectEnum(type);
    }

    private FuncToObject getNumberCollectionToObjectArray(final Class<?> type) {
      final FuncToObject ret = numberCollectionToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getNumberCollectionToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getNumberCollectionToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = numberCollectionToObjectArrayMap.get(Enum.class);
      numberCollectionToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getNumberCollectionToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = numberCollectionToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getNumberCollectionToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getNumberCollectionToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = numberCollectionToObjectCollectionMap.get(Enum.class);
      numberCollectionToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getNumberCollectionToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = numberCollectionToObjectMap.get(Enum.class);
      numberCollectionToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getNumberToObject(final Class<?> type) {
      final FuncToObject ret = numberToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getNumberToObjectEnum(type);
    }

    private FuncToObject getNumberToObjectArray(final Class<?> type) {
      final FuncToObject ret = numberToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getNumberToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getNumberToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = numberToObjectArrayMap.get(Enum.class);
      numberToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getNumberToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = numberToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getNumberToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getNumberToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = numberToObjectCollectionMap.get(Enum.class);
      numberToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getNumberToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = numberToObjectMap.get(Enum.class);
      numberToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getObjectIdCollectionToObject(final Class<?> type) {
      final FuncToObject ret = objectIdCollectionToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getObjectIdCollectionToObjectEnum(type);
    }

    private FuncToObject getObjectIdCollectionToObjectArray(final Class<?> type) {
      final FuncToObject ret = objectIdCollectionToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getObjectIdCollectionToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getObjectIdCollectionToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = objectIdCollectionToObjectArrayMap.get(Enum.class);
      objectIdCollectionToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getObjectIdCollectionToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = objectIdCollectionToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getObjectIdCollectionToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getObjectIdCollectionToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = objectIdCollectionToObjectCollectionMap.get(Enum.class);
      objectIdCollectionToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getObjectIdCollectionToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = objectIdCollectionToObjectMap.get(Enum.class);
      objectIdCollectionToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getObjectIdToObject(final Class<?> type) {
      final FuncToObject ret = objectIdToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getObjectIdToObjectEnum(type);
    }

    private FuncToObject getObjectIdToObjectArray(final Class<?> type) {
      final FuncToObject ret = objectIdToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getObjectIdToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getObjectIdToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = objectIdToObjectArrayMap.get(Enum.class);
      objectIdToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getObjectIdToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = objectIdToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getObjectIdToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getObjectIdToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = objectIdToObjectCollectionMap.get(Enum.class);
      objectIdToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getObjectIdToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = objectIdToObjectMap.get(Enum.class);
      objectIdToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getStringCollectionToObject(final Class<?> type) {
      final FuncToObject ret = stringCollectionToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getStringCollectionToObjectEnum(type);
    }

    private FuncToObject getStringCollectionToObjectArray(final Class<?> type) {
      final FuncToObject ret = stringCollectionToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getStringCollectionToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getStringCollectionToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = stringCollectionToObjectArrayMap.get(Enum.class);
      stringCollectionToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getStringCollectionToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = stringCollectionToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getStringCollectionToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getStringCollectionToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = stringCollectionToObjectCollectionMap.get(Enum.class);
      stringCollectionToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getStringCollectionToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = stringCollectionToObjectMap.get(Enum.class);
      stringCollectionToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToObject getStringToObject(final Class<?> type) {
      final FuncToObject ret = stringToObjectMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getStringToObjectEnum(type);
    }

    private FuncToObject getStringToObjectArray(final Class<?> type) {
      final FuncToObject ret = stringToObjectArrayMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getStringToObjectArrayEnum(type);
    }

    private synchronized FuncToObject getStringToObjectArrayEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = stringToObjectArrayMap.get(Enum.class);
      stringToObjectArrayMap.put(type, ret);
      return ret;
    }

    private FuncToObjectCollection getStringToObjectCollection(final Class<?> type) {
      final FuncToObjectCollection ret = stringToObjectCollectionMap.get(type);
      if (ret != null) {
        return ret;
      }
      return getStringToObjectCollectionEnum(type);
    }

    private synchronized FuncToObjectCollection getStringToObjectCollectionEnum(
        final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObjectCollection ret = stringToObjectCollectionMap.get(Enum.class);
      stringToObjectCollectionMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToObject getStringToObjectEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToObject ret = stringToObjectMap.get(Enum.class);
      stringToObjectMap.put(type, ret);
      return ret;
    }

    private FuncToBson getToBson(final Class<?> type) {
      FuncToBson ret = toBsonMap.get(type);
      if (ret != null) {
        return ret;
      }
      ret = getToBsonDateBased(type);
      if (ret != null) {
        return ret;
      }
      ret = getToBsonStringBased(type);
      if (ret != null) {
        return ret;
      }
      return getToBsonEnum(type);
    }

    private synchronized FuncToBson getToBsonDateBased(final Class<?> type) {
      for (final Class<?> dateBasedType : DateBased.TYPES_SET) {
        if (dateBasedType.isAssignableFrom(type)) {
          final FuncToBson ret = toBsonMap.get(dateBasedType);
          toBsonMap.put(type, ret);
          return ret;
        }
      }
      return null;
    }

    private synchronized FuncToBson getToBsonEnum(final Class<?> type) {
      if (!Enum.class.isAssignableFrom(type)) {
        return null;
      }
      final FuncToBson ret = toBsonMap.get(Enum.class);
      toBsonMap.put(type, ret);
      return ret;
    }

    private synchronized FuncToBson getToBsonStringBased(final Class<?> type) {
      for (final Class<?> stringBasedType : StringBased.TYPES_SET) {
        if (stringBasedType.isAssignableFrom(type)) {
          final FuncToBson ret = toBsonMap.get(stringBasedType);
          toBsonMap.put(type, ret);
          return ret;
        }
      }
      return null;
    }

    private void putArrayToBsonMap() {

      // Boolean
      arrayToBsonMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBooleanPrimArrayToBson().convert(fieldCtx, (boolean[]) o));
      arrayToBsonMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBooleanArrayToBson().convert(fieldCtx, (Boolean[]) o));

      // Byte
      arrayToBsonMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBytePrimArrayToBson().convert(fieldCtx, (byte[]) o));
      arrayToBsonMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getByteArrayToBson().convert(fieldCtx, (Byte[]) o));

      // Character
      arrayToBsonMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getCharacterPrimArrayToBson().convert(fieldCtx, (char[]) o));
      arrayToBsonMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getCharacterArrayToBson().convert(fieldCtx, (Character[]) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        arrayToBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getDateBasedArrayToBson().convert(fieldCtx, (Object[]) o));
      }

      // Enum
      arrayToBsonMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToBson().getEnumArrayToBson().convert(fieldCtx, mainType, (Enum<?>[]) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        arrayToBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getNumberBasedArrayToBson().convert(fieldCtx, (Object[]) o));
      }
      arrayToBsonMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getDoublePrimArrayToBson().convert(fieldCtx, (double[]) o));
      arrayToBsonMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getFloatPrimArrayToBson().convert(fieldCtx, (float[]) o));
      arrayToBsonMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getIntegerPrimArrayToBson().convert(fieldCtx, (int[]) o));
      arrayToBsonMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getLongPrimArrayToBson().convert(fieldCtx, (long[]) o));
      arrayToBsonMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getShortPrimArrayToBson().convert(fieldCtx, (short[]) o));

      // ObjectId
      arrayToBsonMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getObjectIdArrayToBson().convert(fieldCtx, (ObjectId[]) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        arrayToBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getStringBasedArrayToBson().convert(fieldCtx, (Object[]) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBinaryCollectionToObjectArrayMap() {

      // Binary
      binaryCollectionToObjectArrayMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBinaryArray().convert(fieldCtx, (Collection<Binary>) o));

      // Boolean
      binaryCollectionToObjectArrayMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBooleanArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBooleanPrimArray().convert(fieldCtx, (Collection<Binary>) o));

      // Byte
      binaryCollectionToObjectArrayMap.put(Byte.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToByteArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBytePrimArray().convert(fieldCtx, (Collection<Binary>) o));

      // Character
      binaryCollectionToObjectArrayMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToCharacterArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToCharacterPrimArray().convert(fieldCtx, (Collection<Binary>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        binaryCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToDateBasedArray()
                .convert(fieldCtx, mainType, (Collection<Binary>) o));
      }

      // Enum
      binaryCollectionToObjectArrayMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToEnumArray()
              .convert(fieldCtx, mainType, (Collection<Binary>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        binaryCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToNumberBasedArray()
                .convert(fieldCtx, mainType, (Collection<Binary>) o));
      }
      binaryCollectionToObjectArrayMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToDoublePrimArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToFloatPrimArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToIntegerPrimArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToLongPrimArray().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectArrayMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToShortPrimArray().convert(fieldCtx, (Collection<Binary>) o));

      // ObjectId
      binaryCollectionToObjectArrayMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToObjectIdArray().convert(fieldCtx, (Collection<Binary>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        binaryCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToStringBasedArray()
                .convert(fieldCtx, mainType, (Collection<Binary>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBinaryCollectionToObjectCollectionMap() {

      // Binary
      binaryCollectionToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBinaryCollection()
              .convert(fieldCtx, (Collection<Binary>) src, (Collection<Binary>) dst));

      // Boolean
      binaryCollectionToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBooleanCollection()
              .convert(fieldCtx, (Collection<Binary>) src, (Collection<Boolean>) dst));

      // Byte
      binaryCollectionToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToByteCollection()
              .convert(fieldCtx, (Collection<Binary>) src, (Collection<Byte>) dst));

      // Character
      binaryCollectionToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToCharacterCollection()
              .convert(fieldCtx, (Collection<Binary>) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        binaryCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToDateBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Binary>) src, (Collection<Object>) dst));
      }

      // Enum
      binaryCollectionToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToEnumCollection()
              .convert(fieldCtx, mainType, (Collection<Binary>) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        binaryCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Binary>) src, (Collection<Object>) dst));
      }

      // ObjectId
      binaryCollectionToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToObjectIdCollection()
              .convert(fieldCtx, (Collection<Binary>) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        binaryCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToStringBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Binary>) src, (Collection<Object>) dst));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBinaryCollectionToObjectMap() {

      // Binary
      binaryCollectionToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryCollectionToBinary().convert(fieldCtx, (Collection<Binary>) o));

      // Boolean
      binaryCollectionToObjectMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBoolean().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToBooleanPrim().convert(fieldCtx, (Collection<Binary>) o));

      // Byte
      binaryCollectionToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryCollectionToByte().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryCollectionToBytePrim().convert(fieldCtx, (Collection<Binary>) o));

      // Character
      binaryCollectionToObjectMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToCharacter().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToCharacterPrim().convert(fieldCtx, (Collection<Binary>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        binaryCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToDateBased()
                .convert(fieldCtx, mainType, (Collection<Binary>) o));
      }

      // Enum
      binaryCollectionToObjectMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToEnum().convert(fieldCtx, mainType, (Collection<Binary>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        binaryCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToNumberBased()
                .convert(fieldCtx, mainType, (Collection<Binary>) o));
      }
      binaryCollectionToObjectMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToDoublePrim().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToFloatPrim().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToIntegerPrim().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryCollectionToLongPrim().convert(fieldCtx, (Collection<Binary>) o));
      binaryCollectionToObjectMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToShortPrim().convert(fieldCtx, (Collection<Binary>) o));

      // ObjectId
      binaryCollectionToObjectMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryCollectionToObjectId().convert(fieldCtx, (Collection<Binary>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        binaryCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryCollectionToStringBased()
                .convert(fieldCtx, mainType, (Collection<Binary>) o));
      }
    }

    private void putBinaryToObjectArrayMap() {

      // Binary
      binaryToObjectArrayMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBinaryArray().convert(fieldCtx, (Binary) o));

      // Boolean
      binaryToObjectArrayMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBooleanArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBooleanPrimArray().convert(fieldCtx, (Binary) o));

      // Byte
      binaryToObjectArrayMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToByteArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBytePrimArray().convert(fieldCtx, (Binary) o));

      // Character
      binaryToObjectArrayMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToCharacterArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToCharacterPrimArray().convert(fieldCtx, (Binary) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        binaryToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBinaryToDateBasedArray().convert(fieldCtx, mainType, (Binary) o));
      }

      // Enum
      binaryToObjectArrayMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToEnumArray().convert(fieldCtx, mainType, (Binary) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        binaryToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBinaryToNumberBasedArray().convert(fieldCtx, mainType, (Binary) o));
      }
      binaryToObjectArrayMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToDoublePrimArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToFloatPrimArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToIntegerPrimArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToLongPrimArray().convert(fieldCtx, (Binary) o));
      binaryToObjectArrayMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToShortPrimArray().convert(fieldCtx, (Binary) o));

      // ObjectId
      binaryToObjectArrayMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToObjectIdArray().convert(fieldCtx, (Binary) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        binaryToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBinaryToStringBasedArray().convert(fieldCtx, mainType, (Binary) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBinaryToObjectCollectionMap() {

      // Binary
      binaryToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryToBinaryCollection()
              .convert(fieldCtx, (Binary) src, (Collection<Binary>) dst));

      // Boolean
      binaryToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryToBooleanCollection()
              .convert(fieldCtx, (Binary) src, (Collection<Boolean>) dst));

      // Byte
      binaryToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryToByteCollection().convert(fieldCtx, (Binary) src, (Collection<Byte>) dst));

      // Character
      binaryToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryToCharacterCollection()
              .convert(fieldCtx, (Binary) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        binaryToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryToDateBasedCollection()
                .convert(fieldCtx, mainType, (Binary) src, (Collection<Object>) dst));
      }

      // Enum
      binaryToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryToEnumCollection()
              .convert(fieldCtx, mainType, (Binary) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        binaryToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Binary) src, (Collection<Object>) dst));
      }

      // ObjectId
      binaryToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBinaryToObjectIdCollection()
              .convert(fieldCtx, (Binary) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        binaryToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBinaryToStringBasedCollection()
                .convert(fieldCtx, mainType, (Binary) src, (Collection<Object>) dst));
      }
    }

    private void putBinaryToObjectMap() {

      // Binary
      binaryToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBinary().convert(fieldCtx, (Binary) o));

      // Boolean
      binaryToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBoolean().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBooleanPrim().convert(fieldCtx, (Binary) o));

      // Byte
      binaryToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToByte().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToBytePrim().convert(fieldCtx, (Binary) o));

      // Character
      binaryToObjectMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToCharacter().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToCharacterPrim().convert(fieldCtx, (Binary) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        binaryToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBinaryToDateBased().convert(fieldCtx, mainType, (Binary) o));
      }

      // Enum
      binaryToObjectMap.put(Enum.class, (fieldCtx, enumType, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToEnum().convert(fieldCtx, enumType, (Binary) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        binaryToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBinaryToNumberBased().convert(fieldCtx, mainType, (Binary) o));
      }
      binaryToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToDoublePrim().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToFloatPrim().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToIntegerPrim().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToLongPrim().convert(fieldCtx, (Binary) o));
      binaryToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToShortPrim().convert(fieldCtx, (Binary) o));

      // ObjectId
      binaryToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBinaryToObjectId().convert(fieldCtx, (Binary) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        binaryToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBinaryToStringBased().convert(fieldCtx, mainType, (Binary) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBooleanCollectionToObjectArrayMap() {

      // Binary
      booleanCollectionToObjectArrayMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBinaryArray().convert(fieldCtx, (Collection<Boolean>) o));

      // Boolean
      booleanCollectionToObjectArrayMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBooleanArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBooleanPrimArray().convert(fieldCtx, (Collection<Boolean>) o));

      // Byte
      booleanCollectionToObjectArrayMap.put(Byte.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToByteArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBytePrimArray().convert(fieldCtx, (Collection<Boolean>) o));

      // Character
      booleanCollectionToObjectArrayMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToCharacterArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToCharacterPrimArray()
              .convert(fieldCtx, (Collection<Boolean>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        booleanCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToDateBasedArray()
                .convert(fieldCtx, mainType, (Collection<Boolean>) o));
      }

      // Enum
      booleanCollectionToObjectArrayMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToEnumArray()
              .convert(fieldCtx, mainType, (Collection<Boolean>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        booleanCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToNumberBasedArray()
                .convert(fieldCtx, mainType, (Collection<Boolean>) o));
      }
      booleanCollectionToObjectArrayMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToDoublePrimArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToFloatPrimArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToIntegerPrimArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToLongPrimArray().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectArrayMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToShortPrimArray().convert(fieldCtx, (Collection<Boolean>) o));

      // ObjectId
      booleanCollectionToObjectArrayMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToObjectIdArray().convert(fieldCtx, (Collection<Boolean>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToStringBasedArray()
                .convert(fieldCtx, mainType, (Collection<Boolean>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBooleanCollectionToObjectCollectionMap() {

      // Binary
      booleanCollectionToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBinaryCollection()
              .convert(fieldCtx, (Collection<Boolean>) src, (Collection<Binary>) dst));

      // Boolean
      booleanCollectionToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBooleanCollection()
              .convert(fieldCtx, (Collection<Boolean>) src, (Collection<Boolean>) dst));

      // Byte
      booleanCollectionToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToByteCollection()
              .convert(fieldCtx, (Collection<Boolean>) src, (Collection<Byte>) dst));

      // Character
      booleanCollectionToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToCharacterCollection()
              .convert(fieldCtx, (Collection<Boolean>) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        booleanCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToDateBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Boolean>) src, (Collection<Object>) dst));
      }

      // Enum
      booleanCollectionToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToEnumCollection()
              .convert(fieldCtx, mainType, (Collection<Boolean>) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        booleanCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Boolean>) src, (Collection<Object>) dst));
      }

      // ObjectId
      booleanCollectionToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToObjectIdCollection()
              .convert(fieldCtx, (Collection<Boolean>) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToStringBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Boolean>) src, (Collection<Object>) dst));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBooleanCollectionToObjectMap() {

      // Binary
      booleanCollectionToObjectMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBinary().convert(fieldCtx, (Collection<Boolean>) o));

      // Boolean
      booleanCollectionToObjectMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBoolean().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBooleanPrim().convert(fieldCtx, (Collection<Boolean>) o));

      // Byte
      booleanCollectionToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanCollectionToByte().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToBytePrim().convert(fieldCtx, (Collection<Boolean>) o));

      // Character
      booleanCollectionToObjectMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToCharacter().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToCharacterPrim().convert(fieldCtx, (Collection<Boolean>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        booleanCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToDateBased()
                .convert(fieldCtx, mainType, (Collection<Boolean>) o));
      }

      // Enum
      booleanCollectionToObjectMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToEnum().convert(fieldCtx, mainType, (Collection<Boolean>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        booleanCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToNumberBased()
                .convert(fieldCtx, mainType, (Collection<Boolean>) o));
      }
      booleanCollectionToObjectMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToDoublePrim().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToFloatPrim().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToIntegerPrim().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToLongPrim().convert(fieldCtx, (Collection<Boolean>) o));
      booleanCollectionToObjectMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToShortPrim().convert(fieldCtx, (Collection<Boolean>) o));

      // ObjectId
      booleanCollectionToObjectMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanCollectionToObjectId().convert(fieldCtx, (Collection<Boolean>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanCollectionToStringBased()
                .convert(fieldCtx, mainType, (Collection<Boolean>) o));
      }
    }

    private void putBooleanToObjectArrayMap() {

      // Binary
      dateToObjectArrayMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBinaryArray().convert(fieldCtx, (Boolean) o));

      // Boolean
      dateToObjectArrayMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBooleanArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBooleanPrimArray().convert(fieldCtx, (Boolean) o));

      // Byte
      dateToObjectArrayMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToByteArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBytePrimArray().convert(fieldCtx, (Boolean) o));

      // Character
      dateToObjectArrayMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToCharacterArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToCharacterPrimArray().convert(fieldCtx, (Boolean) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        dateToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBooleanToDateBasedArray().convert(fieldCtx, mainType, (Boolean) o));
      }

      // Enum
      dateToObjectArrayMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToEnumArray().convert(fieldCtx, mainType, (Boolean) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        dateToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBooleanToNumberBasedArray().convert(fieldCtx, mainType, (Boolean) o));
      }
      dateToObjectArrayMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToDoublePrimArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToFloatPrimArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToIntegerPrimArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToLongPrimArray().convert(fieldCtx, (Boolean) o));
      dateToObjectArrayMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToShortPrimArray().convert(fieldCtx, (Boolean) o));

      // ObjectId
      dateToObjectArrayMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToObjectIdArray().convert(fieldCtx, (Boolean) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBooleanToStringBasedArray().convert(fieldCtx, mainType, (Boolean) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putBooleanToObjectCollectionMap() {

      // Binary
      booleanToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanToBinaryCollection()
              .convert(fieldCtx, (Boolean) src, (Collection<Binary>) dst));

      // Boolean
      booleanToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanToBooleanCollection()
              .convert(fieldCtx, (Boolean) src, (Collection<Boolean>) dst));

      // Byte
      booleanToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanToByteCollection()
              .convert(fieldCtx, (Boolean) src, (Collection<Byte>) dst));

      // Character
      booleanToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanToCharacterCollection()
              .convert(fieldCtx, (Boolean) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        booleanToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanToDateBasedCollection()
                .convert(fieldCtx, mainType, (Boolean) src, (Collection<Object>) dst));
      }

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        booleanToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Boolean) src, (Collection<Object>) dst));
      }

      // Enum
      booleanToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanToEnumCollection()
              .convert(fieldCtx, mainType, (Boolean) src, (Collection<Enum<?>>) dst));

      // ObjectId
      booleanToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getBooleanToObjectIdCollection()
              .convert(fieldCtx, (Boolean) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getBooleanToStringBasedCollection()
                .convert(fieldCtx, mainType, (Boolean) src, (Collection<Object>) dst));
      }
    }

    private void putBooleanToObjectMap() {

      // Binary
      booleanToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBinary().convert(fieldCtx, (Boolean) o));

      // Boolean
      booleanToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBoolean().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBooleanPrim().convert(fieldCtx, (Boolean) o));

      // Byte
      booleanToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToByte().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToBytePrim().convert(fieldCtx, (Boolean) o));

      // Character
      booleanToObjectMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToCharacter().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToCharacterPrim().convert(fieldCtx, (Boolean) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        booleanToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBooleanToDateBased().convert(fieldCtx, mainType, (Boolean) o));
      }

      // Enum
      booleanToObjectMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToEnum().convert(fieldCtx, mainType, (Boolean) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        booleanToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBooleanToNumberBased().convert(fieldCtx, mainType, (Boolean) o));
      }
      booleanToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToDoublePrim().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToFloatPrim().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToIntegerPrim().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToLongPrim().convert(fieldCtx, (Boolean) o));
      booleanToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToShortPrim().convert(fieldCtx, (Boolean) o));

      // ObjectId
      booleanToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getBooleanToObjectId().convert(fieldCtx, (Boolean) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getBooleanToStringBased().convert(fieldCtx, mainType, (Boolean) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putCollectionToBsonMap() {

      // Binary
      collectionToBsonMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getBinaryCollectionToBson().convert(fieldCtx, (Collection<Binary>) o));

      // Boolean
      collectionToBsonMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getBooleanCollectionToBson().convert(fieldCtx, (Collection<Boolean>) o));

      // Byte
      collectionToBsonMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getByteCollectionToBson().convert(fieldCtx, (Collection<Byte>) o));

      // Character
      collectionToBsonMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getCharacterCollectionToBson().convert(fieldCtx, (Collection<Character>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        collectionToBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getDateBasedCollectionToBson().convert(fieldCtx, (Collection<Object>) o));
      }

      // Enum
      collectionToBsonMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToBson()
              .getEnumCollectionToBson().convert(fieldCtx, mainType, (Collection<Enum<?>>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        collectionToBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getNumberBasedCollectionToBson().convert(fieldCtx, (Collection<Object>) o));
      }

      // ObjectId
      collectionToBsonMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToBson().getObjectIdCollectionToBson().convert(fieldCtx, (Collection<ObjectId>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        collectionToBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getStringBasedCollectionToBson().convert(fieldCtx, (Collection<Object>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putDateCollectionToObjectArrayMap() {

      // Binary
      dateCollectionToObjectArrayMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToBinaryArray().convert(fieldCtx, (Collection<Date>) o));

      // Boolean
      dateCollectionToObjectArrayMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToBooleanArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToBooleanPrimArray().convert(fieldCtx, (Collection<Date>) o));

      // Byte
      dateCollectionToObjectArrayMap.put(Byte.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToByteArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToBytePrimArray().convert(fieldCtx, (Collection<Date>) o));

      // Character
      dateCollectionToObjectArrayMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToCharacterArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToCharacterPrimArray().convert(fieldCtx, (Collection<Date>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        dateCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToDateBasedArray()
                .convert(fieldCtx, mainType, (Collection<Date>) o));
      }

      // Enum
      dateCollectionToObjectArrayMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToEnumArray().convert(fieldCtx, mainType, (Collection<Date>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        dateCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToNumberBasedArray()
                .convert(fieldCtx, mainType, (Collection<Date>) o));
      }
      dateCollectionToObjectArrayMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToDoublePrimArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToFloatPrimArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToIntegerPrimArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToLongPrimArray().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectArrayMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToShortPrimArray().convert(fieldCtx, (Collection<Date>) o));

      // ObjectId
      dateCollectionToObjectArrayMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToObjectIdArray().convert(fieldCtx, (Collection<Date>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        dateCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToStringBasedArray()
                .convert(fieldCtx, mainType, (Collection<Date>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putDateCollectionToObjectCollectionMap() {

      // Binary
      dateCollectionToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToBinaryCollection()
              .convert(fieldCtx, (Collection<Date>) src, (Collection<Binary>) dst));

      // Boolean
      dateCollectionToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToBooleanCollection()
              .convert(fieldCtx, (Collection<Date>) src, (Collection<Boolean>) dst));

      // Byte
      dateCollectionToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToByteCollection()
              .convert(fieldCtx, (Collection<Date>) src, (Collection<Byte>) dst));

      // Character
      dateCollectionToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToCharacterCollection()
              .convert(fieldCtx, (Collection<Date>) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        dateCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToDateBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Date>) src, (Collection<Object>) dst));
      }

      // Enum
      dateCollectionToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToEnumCollection()
              .convert(fieldCtx, mainType, (Collection<Date>) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        dateCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Date>) src, (Collection<Object>) dst));
      }

      // ObjectId
      dateCollectionToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToObjectIdCollection()
              .convert(fieldCtx, (Collection<Date>) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        dateCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToStringBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Date>) src, (Collection<Object>) dst));
      }
    }

    @SuppressWarnings("unchecked")
    private void putDateCollectionToObjectMap() {

      // Binary
      dateCollectionToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToBinary().convert(fieldCtx, (Collection<Date>) o));

      // Boolean
      dateCollectionToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToBoolean().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToBooleanPrim().convert(fieldCtx, (Collection<Date>) o));

      // Byte
      dateCollectionToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToByte().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToBytePrim().convert(fieldCtx, (Collection<Date>) o));

      // Character
      dateCollectionToObjectMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToCharacter().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToCharacterPrim().convert(fieldCtx, (Collection<Date>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        dateCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToDateBased().convert(fieldCtx, mainType, (Collection<Date>) o));
      }

      // Enum
      dateCollectionToObjectMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getDateCollectionToEnum().convert(fieldCtx, mainType, (Collection<Date>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        dateCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToNumberBased()
                .convert(fieldCtx, mainType, (Collection<Date>) o));
      }
      dateCollectionToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToDoublePrim().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToFloatPrim().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToIntegerPrim().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToLongPrim().convert(fieldCtx, (Collection<Date>) o));
      dateCollectionToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToShortPrim().convert(fieldCtx, (Collection<Date>) o));

      // ObjectId
      dateCollectionToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateCollectionToObjectId().convert(fieldCtx, (Collection<Date>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        dateCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getDateCollectionToStringBased()
                .convert(fieldCtx, mainType, (Collection<Date>) o));
      }
    }

    private void putDateToObjectArrayMap() {

      // Binary
      booleanToObjectArrayMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBinaryArray().convert(fieldCtx, (Date) o));

      // Boolean
      booleanToObjectArrayMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBooleanArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBooleanPrimArray().convert(fieldCtx, (Date) o));

      // Byte
      booleanToObjectArrayMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToByteArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBytePrimArray().convert(fieldCtx, (Date) o));

      // Character
      booleanToObjectArrayMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToCharacterArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToCharacterPrimArray().convert(fieldCtx, (Date) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        booleanToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getDateToDateBasedArray().convert(fieldCtx, mainType, (Date) o));
      }

      // Enum
      booleanToObjectArrayMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToEnumArray().convert(fieldCtx, mainType, (Date) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        booleanToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getDateToNumberBasedArray().convert(fieldCtx, mainType, (Date) o));
      }
      booleanToObjectArrayMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToDoublePrimArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToFloatPrimArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToIntegerPrimArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToLongPrimArray().convert(fieldCtx, (Date) o));
      booleanToObjectArrayMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToShortPrimArray().convert(fieldCtx, (Date) o));

      // ObjectId
      booleanToObjectArrayMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToObjectIdArray().convert(fieldCtx, (Date) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        booleanToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getDateToStringBasedArray().convert(fieldCtx, mainType, (Date) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putDateToObjectCollectionMap() {

      // Binary
      dateToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateToBinaryCollection().convert(fieldCtx, (Date) src, (Collection<Binary>) dst));

      // Boolean
      dateToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateToBooleanCollection()
              .convert(fieldCtx, (Date) src, (Collection<Boolean>) dst));

      // Byte
      dateToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateToByteCollection().convert(fieldCtx, (Date) src, (Collection<Byte>) dst));

      // Character
      dateToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateToCharacterCollection()
              .convert(fieldCtx, (Date) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        dateToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getDateToDateBasedCollection()
                .convert(fieldCtx, mainType, (Date) src, (Collection<Object>) dst));
      }

      // Enum
      dateToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateToEnumCollection()
              .convert(fieldCtx, mainType, (Date) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        dateToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getDateToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Date) src, (Collection<Object>) dst));
      }

      // ObjectId
      dateToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getDateToObjectIdCollection()
              .convert(fieldCtx, (Date) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        dateToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getDateToStringBasedCollection()
                .convert(fieldCtx, mainType, (Date) src, (Collection<Object>) dst));
      }
    }

    private void putDateToObjectMap() {

      // Binary
      dateToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBinary().convert(fieldCtx, (Date) o));

      // Boolean
      dateToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBoolean().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToBooleanPrim().convert(fieldCtx, (Date) o));

      // Byte
      dateToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
          .getDateToByte().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
          .getDateToBytePrim().convert(fieldCtx, (Date) o));

      // Character
      dateToObjectMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToCharacter().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToCharacterPrim().convert(fieldCtx, (Date) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        dateToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getDateToDateBased().convert(fieldCtx, mainType, (Date) o));
      }

      // Enum
      dateToObjectMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToEnum().convert(fieldCtx, mainType, (Date) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        dateToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getDateToNumberBased().convert(fieldCtx, mainType, (Date) o));
      }
      dateToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToDoublePrim().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
          .getDateToFloatPrim().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToIntegerPrim().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
          .getDateToLongPrim().convert(fieldCtx, (Date) o));
      dateToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
          .getDateToShortPrim().convert(fieldCtx, (Date) o));

      // ObjectId
      dateToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getDateToObjectId().convert(fieldCtx, (Date) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        dateToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getDateToStringBased().convert(fieldCtx, mainType, (Date) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putNumberCollectionToObjectArrayMap() {

      // Binary
      numberCollectionToObjectArrayMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBinaryArray().convert(fieldCtx, (Collection<Number>) o));

      // Boolean
      numberCollectionToObjectArrayMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBooleanArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBooleanPrimArray().convert(fieldCtx, (Collection<Number>) o));

      // Byte
      numberCollectionToObjectArrayMap.put(Byte.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToByteArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBytePrimArray().convert(fieldCtx, (Collection<Number>) o));

      // Character
      numberCollectionToObjectArrayMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToCharacterArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToCharacterPrimArray().convert(fieldCtx, (Collection<Number>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        numberCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToDateBasedArray()
                .convert(fieldCtx, mainType, (Collection<Number>) o));
      }

      // Enum
      numberCollectionToObjectArrayMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToEnumArray()
              .convert(fieldCtx, mainType, (Collection<Number>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        numberCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToNumberBasedArray()
                .convert(fieldCtx, mainType, (Collection<Number>) o));
      }
      numberCollectionToObjectArrayMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToDoublePrimArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToFloatPrimArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToIntegerPrimArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToLongPrimArray().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectArrayMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToShortPrimArray().convert(fieldCtx, (Collection<Number>) o));

      // ObjectId
      numberCollectionToObjectArrayMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToObjectIdArray().convert(fieldCtx, (Collection<Number>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        numberCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToStringBasedArray()
                .convert(fieldCtx, mainType, (Collection<Number>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putNumberCollectionToObjectCollectionMap() {

      // Binary
      numberCollectionToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBinaryCollection()
              .convert(fieldCtx, (Collection<Number>) src, (Collection<Binary>) dst));

      // Boolean
      numberCollectionToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBooleanCollection()
              .convert(fieldCtx, (Collection<Number>) src, (Collection<Boolean>) dst));

      // Byte
      numberCollectionToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToByteCollection()
              .convert(fieldCtx, (Collection<Number>) src, (Collection<Byte>) dst));

      // Character
      numberCollectionToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToCharacterCollection()
              .convert(fieldCtx, (Collection<Number>) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        numberCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToDateBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Number>) src, (Collection<Object>) dst));
      }

      // Enum
      numberCollectionToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToEnumCollection()
              .convert(fieldCtx, mainType, (Collection<Number>) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        numberCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Number>) src, (Collection<Object>) dst));
      }

      // ObjectId
      numberCollectionToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToObjectIdCollection()
              .convert(fieldCtx, (Collection<Number>) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        numberCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToStringBasedCollection()
                .convert(fieldCtx, mainType, (Collection<Number>) src, (Collection<Object>) dst));
      }
    }

    @SuppressWarnings("unchecked")
    private void putNumberCollectionToObjectMap() {

      // Binary
      numberCollectionToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberCollectionToBinary().convert(fieldCtx, (Collection<Number>) o));

      // Boolean
      numberCollectionToObjectMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBoolean().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToBooleanPrim().convert(fieldCtx, (Collection<Number>) o));

      // Byte
      numberCollectionToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberCollectionToByte().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberCollectionToBytePrim().convert(fieldCtx, (Collection<Number>) o));

      // Character
      numberCollectionToObjectMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToCharacter().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToCharacterPrim().convert(fieldCtx, (Collection<Number>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        numberCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToDateBased()
                .convert(fieldCtx, mainType, (Collection<Number>) o));
      }

      // Enum
      numberCollectionToObjectMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToEnum().convert(fieldCtx, mainType, (Collection<Number>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        numberCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToNumberBased()
                .convert(fieldCtx, mainType, (Collection<Number>) o));
      }
      numberCollectionToObjectMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToDoublePrim().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToFloatPrim().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToIntegerPrim().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberCollectionToLongPrim().convert(fieldCtx, (Collection<Number>) o));
      numberCollectionToObjectMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToShortPrim().convert(fieldCtx, (Collection<Number>) o));

      // ObjectId
      numberCollectionToObjectMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getNumberCollectionToObjectId().convert(fieldCtx, (Collection<Number>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        numberCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getNumberCollectionToStringBased()
                .convert(fieldCtx, mainType, (Collection<Number>) o));
      }
    }

    private void putNumberToObjectArrayMap() {

      // Binary
      numberToObjectArrayMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBinaryArray().convert(fieldCtx, (Number) o));

      // Boolean
      numberToObjectArrayMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBooleanArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBooleanPrimArray().convert(fieldCtx, (Number) o));

      // Byte
      numberToObjectArrayMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToByteArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBytePrimArray().convert(fieldCtx, (Number) o));

      // Character
      numberToObjectArrayMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToCharacterArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToCharacterPrimArray().convert(fieldCtx, (Number) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        numberToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getNumberToDateBasedArray().convert(fieldCtx, mainType, (Number) o));
      }

      // Enum
      numberToObjectArrayMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToEnumArray().convert(fieldCtx, mainType, (Number) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        numberToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getNumberToNumberBasedArray().convert(fieldCtx, mainType, (Number) o));
      }
      numberToObjectArrayMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToDoublePrimArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToFloatPrimArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToIntegerPrimArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToLongPrimArray().convert(fieldCtx, (Number) o));
      numberToObjectArrayMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToShortPrimArray().convert(fieldCtx, (Number) o));

      // ObjectId
      numberToObjectArrayMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToObjectIdArray().convert(fieldCtx, (Number) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        numberToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getNumberToStringBasedArray().convert(fieldCtx, mainType, (Number) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putNumberToObjectCollectionMap() {

      // Binary
      numberToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberToBinaryCollection()
              .convert(fieldCtx, (Number) src, (Collection<Binary>) dst));

      // Boolean
      numberToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberToBooleanCollection()
              .convert(fieldCtx, (Number) src, (Collection<Boolean>) dst));

      // Byte
      numberToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberToByteCollection().convert(fieldCtx, (Number) src, (Collection<Byte>) dst));

      // Character
      numberToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberToCharacterCollection()
              .convert(fieldCtx, (Number) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        numberToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getNumberToDateBasedCollection()
                .convert(fieldCtx, mainType, (Number) src, (Collection<Object>) dst));
      }

      // Enum
      numberToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberToEnumCollection()
              .convert(fieldCtx, mainType, (Number) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        numberToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getNumberToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Number) src, (Collection<Object>) dst));
      }

      // ObjectId
      numberToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getNumberToObjectIdCollection()
              .convert(fieldCtx, (Number) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        numberToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getNumberToStringBasedCollection()
                .convert(fieldCtx, mainType, (Number) src, (Collection<Object>) dst));
      }
    }

    private void putNumberToObjectMap() {

      // Binary
      numberToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBinary().convert(fieldCtx, (Number) o));

      // Boolean
      numberToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBoolean().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBooleanPrim().convert(fieldCtx, (Number) o));

      // Byte
      numberToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToByte().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToBytePrim().convert(fieldCtx, (Number) o));

      // Character
      numberToObjectMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToCharacter().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToCharacterPrim().convert(fieldCtx, (Number) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        numberToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getNumberToDateBased().convert(fieldCtx, mainType, (Number) o));
      }

      // Enum
      numberToObjectMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToEnum().convert(fieldCtx, mainType, (Number) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        numberToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getNumberToNumberBased().convert(fieldCtx, mainType, (Number) o));
      }
      numberToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToDoublePrim().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToFloatPrim().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToIntegerPrim().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToLongPrim().convert(fieldCtx, (Number) o));
      numberToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToShortPrim().convert(fieldCtx, (Number) o));

      // ObjectId
      numberToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getNumberToObjectId().convert(fieldCtx, (Number) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        numberToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getNumberToStringBased().convert(fieldCtx, mainType, (Number) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putObjectIdCollectionToObjectArrayMap() {

      // Binary
      objectIdCollectionToObjectArrayMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBinaryArray().convert(fieldCtx, (Collection<ObjectId>) o));

      // Boolean
      objectIdCollectionToObjectArrayMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBooleanArray().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBooleanPrimArray()
              .convert(fieldCtx, (Collection<ObjectId>) o));

      // Byte
      objectIdCollectionToObjectArrayMap.put(Byte.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToByteArray().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBytePrimArray().convert(fieldCtx, (Collection<ObjectId>) o));

      // Character
      objectIdCollectionToObjectArrayMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToCharacterArray().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToCharacterPrimArray()
              .convert(fieldCtx, (Collection<ObjectId>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        objectIdCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToDateBasedArray()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) o));
      }

      // Enum
      objectIdCollectionToObjectArrayMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToEnumArray()
              .convert(fieldCtx, mainType, (Collection<ObjectId>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        objectIdCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToNumberBasedArray()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) o));
      }
      objectIdCollectionToObjectArrayMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToDoublePrimArray()
              .convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToFloatPrimArray().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToIntegerPrimArray()
              .convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToLongPrimArray().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectArrayMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToShortPrimArray().convert(fieldCtx, (Collection<ObjectId>) o));

      // ObjectId
      objectIdCollectionToObjectArrayMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToObjectIdArray().convert(fieldCtx, (Collection<ObjectId>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        objectIdCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToStringBasedArray()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putObjectIdCollectionToObjectCollectionMap() {

      // Binary
      objectIdCollectionToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBinaryCollection()
              .convert(fieldCtx, (Collection<ObjectId>) src, (Collection<Binary>) dst));

      // Boolean
      objectIdCollectionToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBooleanCollection()
              .convert(fieldCtx, (Collection<ObjectId>) src, (Collection<Boolean>) dst));

      // Byte
      objectIdCollectionToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToByteCollection()
              .convert(fieldCtx, (Collection<ObjectId>) src, (Collection<Byte>) dst));

      // Character
      objectIdCollectionToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToCharacterCollection()
              .convert(fieldCtx, (Collection<ObjectId>) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        objectIdCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToDateBasedCollection()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) src, (Collection<Object>) dst));
      }

      // Enum
      objectIdCollectionToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToEnumCollection()
              .convert(fieldCtx, mainType, (Collection<ObjectId>) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        objectIdCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) src, (Collection<Object>) dst));
      }

      // ObjectId
      objectIdCollectionToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToObjectIdCollection()
              .convert(fieldCtx, (Collection<ObjectId>) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        objectIdCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToStringBasedCollection()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) src, (Collection<Object>) dst));
      }
    }

    @SuppressWarnings("unchecked")
    private void putObjectIdCollectionToObjectMap() {

      // Binary
      objectIdCollectionToObjectMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBinary().convert(fieldCtx, (Collection<ObjectId>) o));

      // Boolean
      objectIdCollectionToObjectMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBoolean().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBooleanPrim().convert(fieldCtx, (Collection<ObjectId>) o));

      // Byte
      objectIdCollectionToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdCollectionToByte().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToBytePrim().convert(fieldCtx, (Collection<ObjectId>) o));

      // Character
      objectIdCollectionToObjectMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToCharacter().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToCharacterPrim().convert(fieldCtx, (Collection<ObjectId>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        objectIdCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToDateBased()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) o));
      }

      // Enum
      objectIdCollectionToObjectMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToEnum().convert(fieldCtx, mainType, (Collection<ObjectId>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        objectIdCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToNumberBased()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) o));
      }
      objectIdCollectionToObjectMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToDoublePrim().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToFloatPrim().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToIntegerPrim().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToLongPrim().convert(fieldCtx, (Collection<ObjectId>) o));
      objectIdCollectionToObjectMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToShortPrim().convert(fieldCtx, (Collection<ObjectId>) o));

      // ObjectId
      objectIdCollectionToObjectMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdCollectionToObjectId().convert(fieldCtx, (Collection<ObjectId>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        objectIdCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdCollectionToStringBased()
                .convert(fieldCtx, mainType, (Collection<ObjectId>) o));
      }
    }

    private void putObjectIdToObjectArrayMap() {

      // Binary
      objectIdToObjectArrayMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBinaryArray().convert(fieldCtx, (ObjectId) o));

      // Boolean
      objectIdToObjectArrayMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBooleanArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBooleanPrimArray().convert(fieldCtx, (ObjectId) o));

      // Byte
      objectIdToObjectArrayMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToByteArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBytePrimArray().convert(fieldCtx, (ObjectId) o));

      // Character
      objectIdToObjectArrayMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToCharacterArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToCharacterPrimArray().convert(fieldCtx, (ObjectId) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        objectIdToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getObjectIdToDateBasedArray().convert(fieldCtx, mainType, (ObjectId) o));
      }

      // Enum
      objectIdToObjectArrayMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToEnumArray().convert(fieldCtx, mainType, (ObjectId) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        objectIdToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdToNumberBasedArray().convert(fieldCtx, mainType, (ObjectId) o));
      }
      objectIdToObjectArrayMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToDoublePrimArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToFloatPrimArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToIntegerPrimArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToLongPrimArray().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectArrayMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToShortPrimArray().convert(fieldCtx, (ObjectId) o));

      // ObjectId
      objectIdToObjectArrayMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToObjectIdArray().convert(fieldCtx, (ObjectId) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        objectIdToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdToStringBasedArray().convert(fieldCtx, mainType, (ObjectId) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putObjectIdToObjectCollectionMap() {

      // Binary
      objectIdToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToBinaryCollection()
              .convert(fieldCtx, (ObjectId) src, (Collection<Binary>) dst));

      // Boolean
      objectIdToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToBooleanCollection()
              .convert(fieldCtx, (ObjectId) src, (Collection<Boolean>) dst));

      // Byte
      objectIdToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToByteCollection()
              .convert(fieldCtx, (ObjectId) src, (Collection<Byte>) dst));

      // Character
      objectIdToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToCharacterCollection()
              .convert(fieldCtx, (ObjectId) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        objectIdToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdToDateBasedCollection()
                .convert(fieldCtx, mainType, (ObjectId) src, (Collection<Object>) dst));
      }

      // Enum
      objectIdToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToEnumCollection()
              .convert(fieldCtx, mainType, (ObjectId) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        objectIdToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdToNumberBasedCollection()
                .convert(fieldCtx, mainType, (ObjectId) src, (Collection<Object>) dst));
      }

      // ObjectId
      objectIdToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getObjectIdToObjectIdCollection()
              .convert(fieldCtx, (ObjectId) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        objectIdToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getObjectIdToStringBasedCollection()
                .convert(fieldCtx, mainType, (ObjectId) src, (Collection<Object>) dst));
      }
    }

    private void putObjectIdToObjectMap() {

      // Binary
      objectIdToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBinary().convert(fieldCtx, (ObjectId) o));

      // Boolean
      objectIdToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBoolean().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBooleanPrim().convert(fieldCtx, (ObjectId) o));

      // Byte
      objectIdToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToByte().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToBytePrim().convert(fieldCtx, (ObjectId) o));

      // Character
      objectIdToObjectMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToCharacter().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToCharacterPrim().convert(fieldCtx, (ObjectId) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        objectIdToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getObjectIdToDateBased().convert(fieldCtx, mainType, (ObjectId) o));
      }

      // Enum
      objectIdToObjectMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToEnum().convert(fieldCtx, mainType, (ObjectId) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        objectIdToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getObjectIdToNumberBased().convert(fieldCtx, mainType, (ObjectId) o));
      }
      objectIdToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToDoublePrim().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToFloatPrim().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToIntegerPrim().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToLongPrim().convert(fieldCtx, (ObjectId) o));
      objectIdToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToShortPrim().convert(fieldCtx, (ObjectId) o));

      // ObjectId
      objectIdToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getObjectIdToObjectId().convert(fieldCtx, (ObjectId) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        objectIdToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getObjectIdToStringBased().convert(fieldCtx, mainType, (ObjectId) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putStringCollectionToObjectArrayMap() {

      // Binary
      stringCollectionToObjectArrayMap.put(Binary.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBinaryArray().convert(fieldCtx, (Collection<String>) o));

      // Boolean
      stringCollectionToObjectArrayMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBooleanArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBooleanPrimArray().convert(fieldCtx, (Collection<String>) o));

      // Byte
      stringCollectionToObjectArrayMap.put(Byte.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToByteArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Byte.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBytePrimArray().convert(fieldCtx, (Collection<String>) o));

      // Character
      stringCollectionToObjectArrayMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToCharacterArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToCharacterPrimArray().convert(fieldCtx, (Collection<String>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        stringCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToDateBasedArray()
                .convert(fieldCtx, mainType, (Collection<String>) o));
      }

      // Enum
      stringCollectionToObjectArrayMap.put(Enum.class,
          (fieldCtx, enumType, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToEnumArray()
              .convert(fieldCtx, enumType, (Collection<String>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        stringCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToNumberBasedArray()
                .convert(fieldCtx, mainType, (Collection<String>) o));
      }
      stringCollectionToObjectArrayMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToDoublePrimArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToFloatPrimArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToIntegerPrimArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Long.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToLongPrimArray().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectArrayMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToShortPrimArray().convert(fieldCtx, (Collection<String>) o));

      // ObjectId
      stringCollectionToObjectArrayMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToObjectIdArray().convert(fieldCtx, (Collection<String>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        stringCollectionToObjectArrayMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToStringBasedArray()
                .convert(fieldCtx, mainType, (Collection<String>) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putStringCollectionToObjectCollectionMap() {

      // Binary
      stringCollectionToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBinaryCollection()
              .convert(fieldCtx, (Collection<String>) src, (Collection<Binary>) dst));

      // Boolean
      stringCollectionToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBooleanCollection()
              .convert(fieldCtx, (Collection<String>) src, (Collection<Boolean>) dst));

      // Byte
      stringCollectionToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToByteCollection()
              .convert(fieldCtx, (Collection<String>) src, (Collection<Byte>) dst));

      // Character
      stringCollectionToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToCharacterCollection()
              .convert(fieldCtx, (Collection<String>) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        stringCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToDateBasedCollection()
                .convert(fieldCtx, mainType, (Collection<String>) src, (Collection<Object>) dst));
      }

      // Enum
      stringCollectionToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToEnumCollection()
              .convert(fieldCtx, mainType, (Collection<String>) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        stringCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToNumberBasedCollection()
                .convert(fieldCtx, mainType, (Collection<String>) src, (Collection<Object>) dst));
      }

      // ObjectId
      stringCollectionToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToObjectIdCollection()
              .convert(fieldCtx, (Collection<String>) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        stringCollectionToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToStringBasedCollection()
                .convert(fieldCtx, mainType, (Collection<String>) src, (Collection<Object>) dst));
      }
    }

    @SuppressWarnings("unchecked")
    private void putStringCollectionToObjectMap() {

      // Binary
      stringCollectionToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringCollectionToBinary().convert(fieldCtx, (Collection<String>) o));

      // Boolean
      stringCollectionToObjectMap.put(Boolean.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBoolean().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Boolean.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToBooleanPrim().convert(fieldCtx, (Collection<String>) o));

      // Byte
      stringCollectionToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringCollectionToByte().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringCollectionToBytePrim().convert(fieldCtx, (Collection<String>) o));

      // Character
      stringCollectionToObjectMap.put(Character.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToCharacter().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Character.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToCharacterPrim().convert(fieldCtx, (Collection<String>) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        stringCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToDateBased()
                .convert(fieldCtx, mainType, (Collection<String>) o));
      }

      // Enum
      stringCollectionToObjectMap.put(Enum.class,
          (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToEnum().convert(fieldCtx, mainType, (Collection<String>) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        stringCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToNumberBased()
                .convert(fieldCtx, mainType, (Collection<String>) o));
      }
      stringCollectionToObjectMap.put(Double.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToDoublePrim().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Float.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToFloatPrim().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Integer.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToIntegerPrim().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringCollectionToLongPrim().convert(fieldCtx, (Collection<String>) o));
      stringCollectionToObjectMap.put(Short.TYPE,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToShortPrim().convert(fieldCtx, (Collection<String>) o));

      // ObjectId
      stringCollectionToObjectMap.put(ObjectId.class,
          (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToObject()
              .getStringCollectionToObjectId().convert(fieldCtx, (Collection<String>) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        stringCollectionToObjectMap.put(type,
            (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToObject()
                .getStringCollectionToStringBased()
                .convert(fieldCtx, mainType, (Collection<String>) o));
      }
    }

    private void putStringToObjectArrayMap() {

      // Binary
      stringToObjectArrayMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBinaryArray().convert(fieldCtx, (String) o));

      // Boolean
      stringToObjectArrayMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBooleanArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBooleanPrimArray().convert(fieldCtx, (String) o));

      // Byte
      stringToObjectArrayMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToByteArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBytePrimArray().convert(fieldCtx, (String) o));

      // Character
      stringToObjectArrayMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToCharacterArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToCharacterPrimArray().convert(fieldCtx, (String) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        stringToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getStringToDateBasedArray().convert(fieldCtx, mainType, (String) o));
      }

      // Enum
      stringToObjectArrayMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToEnumArray().convert(fieldCtx, mainType, (String) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        stringToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getStringToNumberBasedArray().convert(fieldCtx, mainType, (String) o));
      }
      stringToObjectArrayMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToDoublePrimArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToFloatPrimArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToIntegerPrimArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToLongPrimArray().convert(fieldCtx, (String) o));
      stringToObjectArrayMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToShortPrimArray().convert(fieldCtx, (String) o));

      // ObjectId
      stringToObjectArrayMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToObjectIdArray().convert(fieldCtx, (String) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        stringToObjectArrayMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getStringToStringBasedArray().convert(fieldCtx, mainType, (String) o));
      }
    }

    @SuppressWarnings("unchecked")
    private void putStringToObjectCollectionMap() {

      // Binary
      stringToObjectCollectionMap.put(Binary.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringToBinaryCollection()
              .convert(fieldCtx, (String) src, (Collection<Binary>) dst));

      // Boolean
      stringToObjectCollectionMap.put(Boolean.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringToBooleanCollection()
              .convert(fieldCtx, (String) src, (Collection<Boolean>) dst));

      // Byte
      stringToObjectCollectionMap.put(Byte.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringToByteCollection().convert(fieldCtx, (String) src, (Collection<Byte>) dst));

      // Character
      stringToObjectCollectionMap.put(Character.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringToCharacterCollection()
              .convert(fieldCtx, (String) src, (Collection<Character>) dst));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        stringToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getStringToDateBasedCollection()
                .convert(fieldCtx, mainType, (String) src, (Collection<Object>) dst));
      }

      // Enum
      stringToObjectCollectionMap.put(Enum.class,
          (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringToEnumCollection()
              .convert(fieldCtx, mainType, (String) src, (Collection<Enum<?>>) dst));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        stringToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getStringToNumberBasedCollection()
                .convert(fieldCtx, mainType, (String) src, (Collection<Object>) dst));
      }

      // ObjectId
      stringToObjectCollectionMap.put(ObjectId.class,
          (fieldCtx, __, src, dst) -> fieldCtx.getConfiguration().getToObject()
              .getStringToObjectIdCollection()
              .convert(fieldCtx, (String) src, (Collection<ObjectId>) dst));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        stringToObjectCollectionMap.put(type,
            (fieldCtx, mainType, src, dst) -> fieldCtx.getConfiguration().getToObject()
                .getStringToStringBasedCollection()
                .convert(fieldCtx, mainType, (String) src, (Collection<Object>) dst));
      }
    }

    private void putStringToObjectMap() {

      // Binary
      stringToObjectMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBinary().convert(fieldCtx, (String) o));

      // Boolean
      stringToObjectMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBoolean().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBooleanPrim().convert(fieldCtx, (String) o));

      // Byte
      stringToObjectMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToByte().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToBytePrim().convert(fieldCtx, (String) o));

      // Character
      stringToObjectMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToCharacter().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToCharacterPrim().convert(fieldCtx, (String) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        stringToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getStringToDateBased().convert(fieldCtx, mainType, (String) o));
      }

      // Enum
      stringToObjectMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToEnum().convert(fieldCtx, mainType, (String) o));

      // Number based
      for (final Class<?> type : NumberBased.REFERENCE_TYPES_SET) {
        stringToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getStringToNumberBased().convert(fieldCtx, mainType, (String) o));
      }
      stringToObjectMap.put(Double.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToDoublePrim().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Float.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToFloatPrim().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Integer.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToIntegerPrim().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Long.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToLongPrim().convert(fieldCtx, (String) o));
      stringToObjectMap.put(Short.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToShortPrim().convert(fieldCtx, (String) o));

      // ObjectId
      stringToObjectMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration()
          .getToObject().getStringToObjectId().convert(fieldCtx, (String) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        stringToObjectMap.put(type, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration()
            .getToObject().getStringToStringBased().convert(fieldCtx, mainType, (String) o));
      }
    }

    private void putToBsonMap() {

      // Binary
      toBsonMap.put(Binary.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBinaryToBson().convert(fieldCtx, (Binary) o));

      // Boolean
      toBsonMap.put(Boolean.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBooleanPrimToBson().convert(fieldCtx, (boolean) o));
      toBsonMap.put(Boolean.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBooleanToBson().convert(fieldCtx, (Boolean) o));

      // Byte
      toBsonMap.put(Byte.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getBytePrimToBson().convert(fieldCtx, (byte) o));
      toBsonMap.put(Byte.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getByteToBson().convert(fieldCtx, (Byte) o));

      // Character
      toBsonMap.put(Character.TYPE, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getCharacterPrimToBson().convert(fieldCtx, (char) o));
      toBsonMap.put(Character.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getCharacterToBson().convert(fieldCtx, (Character) o));

      // Date based
      for (final Class<?> type : DateBased.TYPES_SET) {
        toBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getDateBasedToBson().convert(fieldCtx, o));
      }

      // Enum
      toBsonMap.put(Enum.class, (fieldCtx, mainType, o) -> fieldCtx.getConfiguration().getToBson()
          .getEnumToBson().convert(fieldCtx, mainType, (Enum<?>) o));

      // Number based
      for (final Class<?> type : NumberBased.TYPES_SET) {
        toBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getNumberBasedToBson().convert(fieldCtx, o));
      }

      // ObjectId
      toBsonMap.put(ObjectId.class, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
          .getObjectIdToBson().convert(fieldCtx, (ObjectId) o));

      // String based
      for (final Class<?> type : StringBased.TYPES_SET) {
        toBsonMap.put(type, (fieldCtx, __, o) -> fieldCtx.getConfiguration().getToBson()
            .getStringBasedToBson().convert(fieldCtx, o));
      }
    }
  }

  private static final FuncMap FUNC_MAP = new FuncMap();

  private static final Set<Class<?>> SIMPLE_TYPES = new HashSet<>(Arrays.asList(new Class<?>[] { //
      Boolean.class, //
      Boolean.TYPE, //
      Binary.class, //
      Byte.class, //
      Byte.TYPE, //
      Character.class, //
      Character.TYPE, //
      ObjectId.class, //
  }));

  protected static boolean isSimpleType(final JaxBsonConfiguration cfg, final Class<?> type) {
    return //
    SIMPLE_TYPES.contains(type) || //
        Enum.class.isAssignableFrom(type) || //
        cfg.getDateBased().getChecker().check(type) || //
        cfg.getNumberBased().getChecker().check(type) || //
        cfg.getStringBased().getChecker().check(type) //
    ;
  }

  protected SimpleTypes() {}

  private String classInfo(final Object o) {
    final String array = o.getClass().isArray() ? "[]" : "";
    final String collectionPre =
        Collection.class.isAssignableFrom(o.getClass()) ? "Collection<" : "";
    final String collectionPost = Collection.class.isAssignableFrom(o.getClass()) ? ">" : "";
    final Class<?> elementType = elementType(o);
    return collectionPre + elementType.getSimpleName() + collectionPost + array;
  }

  private Collection<?> createToObjectCollection(final FieldSubDescriptor fds) {
    return fds.getFd().collectionInstance();
  }

  private Class<?> elementType(final Object o) {
    final Class<?> ret = o.getClass();
    if (ret.isArray()) {
      return ret.getComponentType();
    }
    if (Collection.class.isAssignableFrom(ret)) {
      for (final Object e : (Collection<?>) o) {
        if (e != null) {
          return e.getClass();
        }
      }
      return null;
    }
    return ret;
  }

  private Class<?> getGenericType(final Collection<?> o) {
    for (final Object e : o) {
      if (e != null) {
        return e.getClass();
      }
    }
    return null;
  }

  protected Object toBson(final JaxBsonFieldContext fieldCtx, final Object o) {
    if (o == null) {
      return null;
    }
    final Class<?> cls = o.getClass();
    if (cls.isArray()) {
      final Class<?> mainType = cls.getComponentType();
      final FuncMap.FuncToBson func = FUNC_MAP.getArrayToBson(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "Failed to convert array of " + mainType.getName() + " to Bson: " + o);
    }

    if (Collection.class.isAssignableFrom(cls)) {
      final Class<?> mainType = getGenericType((Collection<?>) o);
      if (mainType == null) {
        return Collections.EMPTY_LIST;
      }
      final FuncMap.FuncToBson func = FUNC_MAP.getCollectionToBson(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "Failed to convert collection of " + mainType.getName() + " to Bson.");
    }

    {
      final Class<?> mainType = cls;
      final FuncMap.FuncToBson func = FUNC_MAP.getToBson(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "Failed to convert object of " + mainType.getName() + " to Bson.");
    }
  }

  @SuppressWarnings("unchecked")
  protected Object toObject(final FieldSubDescriptor fds, final Object o) {
    Objects.requireNonNull(fds, "Supplied parameter 'fds' is null");
    if (o == null) {
      return null;
    }
    if (!fds.isSimpleType()) {
      throw new IllegalArgumentException(
          "Supplied parameter 'fds' is not of a simple type: " + fds);
    }
    final Class<?> srcClass = o.getClass();
    final Class<?> elementType = elementType(o);
    if (elementType == null) {
      return null;
    }
    if (Binary.class.isAssignableFrom(elementType)) {
      if (Collection.class.isAssignableFrom(srcClass)) {
        return toObjectBinary(fds, (Collection<Binary>) o);
      }
      return toObjectBinary(fds, (Binary) o);
    }
    if (Boolean.class.isAssignableFrom(elementType)) {
      if (Collection.class.isAssignableFrom(srcClass)) {
        return toObjectBoolean(fds, (Collection<Boolean>) o);
      }
      return toObjectBoolean(fds, (Boolean) o);
    }
    if (Date.class.isAssignableFrom(elementType)) {
      if (Collection.class.isAssignableFrom(srcClass)) {
        return toObjectDate(fds, (Collection<Date>) o);
      }
      return toObjectDate(fds, (Date) o);
    }
    if (!Byte.class.isAssignableFrom(elementType) && Number.class.isAssignableFrom(elementType)) {
      if (Collection.class.isAssignableFrom(srcClass)) {
        return toObjectNumber(fds, (Collection<Number>) o);
      }
      return toObjectNumber(fds, (Number) o);
    }
    if (ObjectId.class.isAssignableFrom(elementType)) {
      if (Collection.class.isAssignableFrom(srcClass)) {
        return toObjectObjectId(fds, (Collection<ObjectId>) o);
      }
      return toObjectObjectId(fds, (ObjectId) o);
    }
    if (String.class.isAssignableFrom(elementType)) {
      if (Collection.class.isAssignableFrom(srcClass)) {
        return toObjectString(fds, (Collection<String>) o);
      }
      return toObjectString(fds, (String) o);
    }
    throw new IllegalStateException(
        "Unsupperted conversion from " + classInfo(o) + " to field " + fds);
  }

  private Object toObjectBinary(final FieldSubDescriptor fds, final Binary o) {
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    final Class<?> mainType = fds.getMainType();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getBinaryToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException("This is a bug.");
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func = FUNC_MAP.getBinaryToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException("This is a bug.");
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getBinaryToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException("This is a bug.");
    }
  }

  private Object toObjectBinary(final FieldSubDescriptor fds, final Collection<Binary> o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getBinaryCollectionToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of binary to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func =
          FUNC_MAP.getBinaryCollectionToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of binary to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getBinaryCollectionToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of binary to "
              + mainType.getName());
    }
  }

  private Object toObjectBoolean(final FieldSubDescriptor fds, final Boolean o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getBooleanToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert boolean to array of " + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func = FUNC_MAP.getBooleanToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert boolean to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getBooleanToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert boolean to " + mainType.getName());
    }
  }

  private Object toObjectBoolean(final FieldSubDescriptor fds, final Collection<Boolean> o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getBooleanCollectionToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of boolean to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func =
          FUNC_MAP.getBooleanCollectionToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of boolean to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getBooleanCollectionToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of boolean to "
              + mainType.getName());
    }
  }

  private Object toObjectDate(final FieldSubDescriptor fds, final Collection<Date> o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getDateCollectionToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of date to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func =
          FUNC_MAP.getDateCollectionToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of date to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getDateCollectionToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of date to "
              + mainType.getName());
    }
  }

  private Object toObjectDate(final FieldSubDescriptor fds, final Date o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getDateToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert date to array of " + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func = FUNC_MAP.getDateToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert date to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getDateToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert date to " + mainType.getName());
    }
  }

  private Object toObjectNumber(final FieldSubDescriptor fds, final Collection<Number> o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getNumberCollectionToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of number to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func =
          FUNC_MAP.getNumberCollectionToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of number to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getNumberCollectionToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of number to "
              + mainType.getName());
    }
  }

  private Object toObjectNumber(final FieldSubDescriptor fds, final Number o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getNumberToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert number to array of " + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func = FUNC_MAP.getNumberToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert number to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getNumberToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert number to " + mainType.getName());
    }
  }

  private Object toObjectObjectId(final FieldSubDescriptor fds, final Collection<ObjectId> o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getObjectIdCollectionToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of object-id to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func =
          FUNC_MAP.getObjectIdCollectionToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of object-id to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getObjectIdCollectionToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of object-id to "
              + mainType.getName());
    }
  }

  private Object toObjectObjectId(final FieldSubDescriptor fds, final ObjectId o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getObjectIdToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert object-id to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func = FUNC_MAP.getObjectIdToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert object-id to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getObjectIdToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert object-id to " + mainType.getName());
    }
  }

  private Object toObjectString(final FieldSubDescriptor fds, final Collection<String> o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getStringCollectionToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of string to array of "
              + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func =
          FUNC_MAP.getStringCollectionToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of string to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getStringCollectionToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert collection of string to "
              + mainType.getName());
    }
  }

  private Object toObjectString(final FieldSubDescriptor fds, final String o) {
    final Class<?> mainType = fds.getMainType();
    final JaxBsonFieldContextImpl fieldCtx = fds.getFd().getFieldCtx();
    if (fds.isArray()) {
      final FuncMap.FuncToObject func = FUNC_MAP.getStringToObjectArray(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert string to array of " + mainType.getName());
    }
    if (fds.isCollection()) {
      final FuncMap.FuncToObjectCollection func = FUNC_MAP.getStringToObjectCollection(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o, createToObjectCollection(fds));
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert string to collection of "
              + mainType.getName());
    }
    {
      final FuncMap.FuncToObject func = FUNC_MAP.getStringToObject(mainType);
      if (func != null) {
        return func.convert(fieldCtx, mainType, o);
      }
      throw new IllegalStateException(
          "This is a bug. No function found to convert string to " + mainType.getName());
    }
  }
}
