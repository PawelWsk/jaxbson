package org.qnixyz.jbson.impl;

import java.util.List;

public class Utils {

  private static final Boolean[] EMPTY_BOOLEAN_OBJECT_ARRAY = new Boolean[0];
  private static final Byte[] EMPTY_BYTE_OBJECT_ARRAY = new Byte[0];
  private static final byte[] EMPTY_BYTE_PRIMITIVE_ARRAY = new byte[0];
  private static final char[] EMPTY_CHAR_PRIMITIVE_ARRAY = new char[0];
  private static final Character[] EMPTY_CHARACTER_OBJECT_ARRAY = new Character[0];
  private static final Double[] EMPTY_DOUBLE_OBJECT_ARRAY = new Double[0];
  private static final Integer[] EMPTY_INTEGER_OBJECT_ARRAY = new Integer[0];
  private static final Long[] EMPTY_LONG_OBJECT_ARRAY = new Long[0];

  public static int compare(final String s1, final String s2) {
    if (s1 == s2) {
      return 0;
    }
    if (s2 == null) {
      return -1;
    }
    return s1.compareTo(s2);
  }

  public static int compareStringLists(final List<String> o1, final List<String> o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return 1;
    }
    if (o2 == null) {
      return -1;
    }
    for (int i = 0; i < o1.size(); i++) {
      final String s1 = o1.get(i);
      String s2 = null;
      if (o2.size() > i) {
        s2 = o2.get(i);
      }
      final int ret = compare(s1, s2);
      if (ret != 0) {
        return ret;
      }
    }
    return Integer.compare(o1.size(), o2.size());
  }

  public static boolean isBlank(final String cs) {
    int strLen;
    if ((cs == null) || ((strLen = cs.length()) == 0)) {
      return true;
    }
    for (int i = 0; i < strLen; i++) {
      if (!Character.isWhitespace(cs.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  public static String lcFirst(final String str) {
    if ((str == null) || (str.length() < 1)) {
      return str;
    }
    final String first = str.substring(0, 1).toLowerCase();
    return str.replaceFirst("^.", first);
  }

  public static Boolean[] toObjectArray(final boolean[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_BOOLEAN_OBJECT_ARRAY;
    }
    final Boolean[] ret = new Boolean[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = (a[i] ? Boolean.TRUE : Boolean.FALSE);
    }
    return ret;
  }

  public static Byte[] toObjectArray(final byte[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_BYTE_OBJECT_ARRAY;
    }
    final Byte[] ret = new Byte[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = Byte.valueOf(a[i]);
    }
    return ret;
  }

  public static Character[] toObjectArray(final char[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_CHARACTER_OBJECT_ARRAY;
    }
    final Character[] ret = new Character[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = Character.valueOf(a[i]);
    }
    return ret;
  }

  public static Double[] toObjectArray(final double[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_DOUBLE_OBJECT_ARRAY;
    }
    final Double[] ret = new Double[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = Double.valueOf(a[i]);
    }
    return ret;
  }

  public static Integer[] toObjectArray(final int[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_INTEGER_OBJECT_ARRAY;
    }
    final Integer[] ret = new Integer[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = Integer.valueOf(a[i]);
    }
    return ret;
  }

  public static Long[] toObjectArray(final long[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_LONG_OBJECT_ARRAY;
    }
    final Long[] ret = new Long[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = Long.valueOf(a[i]);
    }
    return ret;
  }

  public static byte[] toPrimitiveArray(final Byte[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_BYTE_PRIMITIVE_ARRAY;
    }
    final byte[] ret = new byte[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = a[i].byteValue();
    }
    return ret;
  }

  public static char[] toPrimitiveArray(final Character[] a) {
    if (a == null) {
      return null;
    }
    if (a.length == 0) {
      return EMPTY_CHAR_PRIMITIVE_ARRAY;
    }
    final char[] ret = new char[a.length];
    for (int i = 0; i < a.length; i++) {
      ret[i] = a[i].charValue();
    }
    return ret;
  }
}
