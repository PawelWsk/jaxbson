package org.qnixyz.jbson.impl;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bson.Document;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

class FieldSubDescriptor {

  private final JaxBsonContextImpl ctx;

  private final FieldDescriptor fd;

  private final Class<?> mainType;

  private final String name;

  private final Set<Class<?>> referredTypes = new HashSet<>();

  private final boolean simpleType;

  private final ValueContainerType vct;

  private final JaxBsonAdapter<Object, Object> xmlAdapter;

  protected FieldSubDescriptor(final JaxBsonContextImpl ctx, final FieldDescriptor fd,
      final String name, final ValueContainerType vct, final Class<?> mainType,
      final JaxBsonAdapter<Object, Object> xmlAdapter) {
    this.ctx = ctx;
    this.fd = fd;
    this.name = name;
    this.vct = vct;
    this.mainType = mainType;
    this.xmlAdapter = xmlAdapter;
    this.simpleType = SimpleTypes.isSimpleType(ctx.getConfiguration(), mainType);
    if (!this.simpleType) {
      this.referredTypes.add(mainType);
    }
  }

  public Document debugInfo() {
    final Document ret = new Document();
    ret.append("name", this.name);
    ret.append("mainType", this.mainType == null ? this.mainType : this.mainType.getName());
    ret.append("simpleType", this.simpleType);
    ret.append("vct", this.vct == null ? null : this.vct.name());
    ret.append("xmlAdapterClass",
        this.xmlAdapter == null ? this.xmlAdapter : this.xmlAdapter.getClass().getName());
    return ret;
  }

  private Object firstNonNull(final List<?> l) {
    for (final Object e : l) {
      if (e != null) {
        return e;
      }
    }
    return null;
  }

  protected JaxBsonContextImpl getCtx() {
    return this.ctx;
  }

  protected FieldDescriptor getFd() {
    return this.fd;
  }

  protected Field getField() {
    return this.fd.getField();
  }

  protected Class<?> getMainType() {
    return this.mainType;
  }

  protected String getName() {
    return this.name;
  }

  protected Set<Class<?>> getReferredTypes() {
    return this.referredTypes;
  }

  protected boolean isArray() {
    return this.vct.isArray();
  }

  private boolean isBsonDocument(final Object bsonValue) {
    return (bsonValue != null) && Document.class.isAssignableFrom(bsonValue.getClass());
  }

  protected boolean isCollection() {
    return this.vct.isCollection();
  }

  protected boolean isList() {
    return this.vct == ValueContainerType.LIST;
  }

  private boolean isListOfBsonDocument(final Object bsonValue) {
    if (bsonValue == null) {
      return false;
    }
    if (!List.class.isAssignableFrom(bsonValue.getClass())) {
      return false;
    }
    if (((List<?>) bsonValue).isEmpty()) {
      return true;
    }
    final Object e = firstNonNull((List<?>) bsonValue);
    if (e == null) {
      return false;
    }
    return isBsonDocument(e);
  }

  protected boolean isSet() {
    return this.vct == ValueContainerType.SET;
  }

  protected boolean isSimpleType() {
    return this.simpleType;
  }

  protected boolean isSortedSet() {
    return this.vct == ValueContainerType.SORTED_SET;
  }

  private Object marshal(final Object o) {
    if (this.xmlAdapter == null) {
      return o;
    }
    try {
      return this.xmlAdapter.marshal(o);
    } catch (final Exception e) {
      throw new IllegalStateException(
          "Unexpected exception at handling JaxBsonXmlAdapter, when attempting to call "
              + this.xmlAdapter.getClass().getName() + ".marshal(Object)",
          e);
    }
  }

  private void setField(final Object obj, final Object value) {
    try {
      getField().set(obj, value);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      throw new IllegalStateException("Unexpected exception", e);
    }
  }

  protected void toBson(final Document bson, Object value, final boolean omitType) {
    value = marshal(value);
    if (isSimpleType()) {
      value = this.ctx.getSimpleTypes().toBson(this.fd.getFieldCtx(), value);
      bson.put(this.name, value);
    } else if (value != null) {
      if (value.getClass().isArray()) {
        final List<Document> list = new ArrayList<>();
        for (final Object e : (Object[]) value) {
          list.add(this.ctx.toBson(e, omitType));
        }
        bson.put(this.name, list);
      } else if (Collection.class.isAssignableFrom(value.getClass())) {
        final List<Document> list = new ArrayList<>();
        ((Collection<?>) value).forEach(e -> list.add(this.ctx.toBson(e, omitType)));
        bson.put(this.name, list);
      } else {
        bson.put(this.name, this.ctx.toBson(value, omitType));
      }
    }
  }

  @SuppressWarnings("unchecked")
  protected void toObject(final Object o, final Object bsonValue) {

    // Document
    if (isBsonDocument(bsonValue)) {
      final String typeNameInBson =
          ((Document) bsonValue).getString(this.ctx.getConfiguration().getTypeFieldName());
      Object obj = (typeNameInBson == null) && this.fd.isOneMainType()
          ? this.ctx.toObject((Document) bsonValue, this.mainType)
          : this.ctx.toObject((Document) bsonValue);
      obj = unmarshal(obj);
      setField(o, obj);
      return;
    }

    // List of Document
    if (isListOfBsonDocument(bsonValue)) {
      if (isArray()) {
        toObjectListOfBsonDocumentToArray(o, (List<Document>) bsonValue);
      } else if (isCollection()) {
        toObjectListOfBsonDocumentToCollection(o, (List<Document>) bsonValue);
      } else {
        throw new IllegalStateException("List of Bson requires array or colletion field. Field "
            + this + ", Bson value: " + bsonValue);
      }
      return;
    }

    // Other cases
    {
      Object obj = unmarshal(bsonValue);
      if (this.xmlAdapter == null) {
        obj = this.ctx.getSimpleTypes().toObject(this, obj);
      }
      // Do not set null values on non-simple types
      if ((obj != null) || this.simpleType) {
        setField(o, obj);
      }
    }
  }

  private void toObjectListOfBsonDocumentToArray(final Object o,
      final List<Document> bsonValueList) {
    final Object[] objArr = (Object[]) Array.newInstance(this.mainType, bsonValueList.size());
    int i = 0;
    for (final Document document : bsonValueList) {
      objArr[i] = this.fd.isOneMainType() ? this.ctx.toObject(document, this.mainType)
          : this.ctx.toObject(document);
      objArr[i] = unmarshal(objArr[i]);
      i++;
    }
    setField(o, objArr);
  }

  private void toObjectListOfBsonDocumentToCollection(final Object o,
      final List<Document> bsonValueList) {
    final Collection<Object> objColl = this.fd.collectionInstance();
    for (final Document document : bsonValueList) {
      Object obj = this.fd.isOneMainType() ? this.ctx.toObject(document, this.mainType)
          : this.ctx.toObject(document);
      obj = unmarshal(obj);
      objColl.add(obj);
    }
    setField(o, objColl);
  }

  private Object unmarshal(final Object o) {
    if (this.xmlAdapter == null) {
      return o;
    }
    try {
      return this.xmlAdapter.unmarshal(o);
    } catch (final Exception e) {
      throw new IllegalStateException(
          "Unexpected exception at handling XmlAdapter, when attempting to call "
              + this.xmlAdapter.getClass().getName() + ".unmarshal(Object)",
          e);
    }
  }
}
