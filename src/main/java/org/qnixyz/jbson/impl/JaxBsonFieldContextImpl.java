package org.qnixyz.jbson.impl;

import java.lang.reflect.Field;
import java.util.Objects;
import org.qnixyz.jbson.JaxBsonConfiguration;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.JaxBsonFieldContext;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;
import org.qnixyz.jbson.helpers.JaxBsonAdapterHelper;
import org.qnixyz.jbson.helpers.XmlAnyAttributeHelper;
import org.qnixyz.jbson.helpers.XmlAnyElementHelper;
import org.qnixyz.jbson.helpers.XmlElementRefHelper;
import org.qnixyz.jbson.helpers.XmlElementRefsHelper;
import org.qnixyz.jbson.helpers.XmlElementsHelper;
import org.qnixyz.jbson.helpers.XmlTransientHelper;
import org.qnixyz.jbson.helpers.XmlValueHelper;

class JaxBsonFieldContextImpl implements JaxBsonFieldContext {

  private final JaxBsonContext ctx;

  private final Field field;

  private final JaxBsonIgnoreTransient jaxBsonIgnoreTransient;

  private final JaxBsonIgnoreTransient jaxBsonIgnoreTransientCfg;

  private final JaxBsonName jaxBsonName;

  private final JaxBsonName jaxBsonNameCfg;

  private final JaxBsonNumberHint jaxBsonNumberHint;

  private final JaxBsonNumberHint jaxBsonNumberHintCfg;

  private final JaxBsonSeeAlso jaxBsonSeeAlso;

  private final JaxBsonTransient jaxBsonTransient;

  private final JaxBsonTransient jaxBsonTransientCfg;

  private final JaxBsonAdapter<Object, Object> jaxBsonXmlAdapter;

  private final JaxBsonXmlAnyAttributeMapping jaxBsonXmlAnyAttributeMapping;

  private final JaxBsonXmlAnyAttributeMapping jaxBsonXmlAnyAttributeMappingCfg;

  private final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappings;

  private final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappingsCfg;

  private final XmlAnyAttributeHelper xmlAnyAttribute;

  private final XmlAnyElementHelper xmlAnyElement;

  private final XmlElementRefHelper xmlElementRef;

  private final XmlElementRefsHelper xmlElementRefs;

  private final XmlElementsHelper xmlElements;

  private final JaxBsonJavaTypeAdapter xmlJavaTypeAdapter;

  private final XmlTransientHelper xmlTransient;

  private final XmlValueHelper xmlValue;

  protected JaxBsonFieldContextImpl( //
      final JaxBsonContext ctx, //
      final Field field, //
      final JaxBsonIgnoreTransient jaxBsonIgnoreTransient, //
      final JaxBsonIgnoreTransient jaxBsonIgnoreTransientCfg, //
      final JaxBsonName jaxBsonName, //
      final JaxBsonName jaxBsonNameCfg, //
      final JaxBsonSeeAlso jaxBsonSeeAlso, //
      final JaxBsonNumberHint jaxBsonNumberHint, //
      final JaxBsonNumberHint jaxBsonNumberHintCfg, //
      final JaxBsonTransient jaxBsonTransient, //
      final JaxBsonTransient jaxBsonTransientCfg, //
      final XmlAnyAttributeHelper xmlAnyAttribute, //
      final JaxBsonXmlAnyAttributeMapping jaxBsonXmlAnyAttributeMapping, //
      final JaxBsonXmlAnyAttributeMapping jaxBsonXmlAnyAttributeMappingCfg, //
      final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappings, //
      final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappingsCfg, //
      final XmlAnyElementHelper xmlAnyElement, //
      final XmlElementRefHelper xmlElementRef, //
      final XmlElementRefsHelper xmlElementRefs, //
      final XmlElementsHelper xmlElements, //
      final JaxBsonJavaTypeAdapter xmlJavaTypeAdapter, //
      final XmlTransientHelper xmlTransient, //
      final XmlValueHelper xmlValue //
  ) {
    this.ctx = Objects.requireNonNull(ctx);
    this.field = Objects.requireNonNull(field);
    this.jaxBsonIgnoreTransient = jaxBsonIgnoreTransient;
    this.jaxBsonIgnoreTransientCfg = jaxBsonIgnoreTransientCfg;
    this.jaxBsonName = jaxBsonName;
    this.jaxBsonNameCfg = jaxBsonNameCfg;
    this.jaxBsonSeeAlso = jaxBsonSeeAlso;
    this.jaxBsonNumberHint = jaxBsonNumberHint;
    this.jaxBsonNumberHintCfg = jaxBsonNumberHintCfg;
    this.jaxBsonTransient = jaxBsonTransient;
    this.jaxBsonTransientCfg = jaxBsonTransientCfg;
    this.xmlAnyAttribute = xmlAnyAttribute;
    this.jaxBsonXmlAnyAttributeMapping = jaxBsonXmlAnyAttributeMapping;
    this.jaxBsonXmlAnyAttributeMappingCfg = jaxBsonXmlAnyAttributeMappingCfg;
    this.jaxBsonXmlAnyAttributeMappings = jaxBsonXmlAnyAttributeMappings;
    this.jaxBsonXmlAnyAttributeMappingsCfg = jaxBsonXmlAnyAttributeMappingsCfg;
    this.xmlAnyElement = xmlAnyElement;
    this.xmlElementRef = xmlElementRef;
    this.xmlElementRefs = xmlElementRefs;
    this.xmlElements = xmlElements;
    this.xmlJavaTypeAdapter = xmlJavaTypeAdapter;
    this.xmlTransient = xmlTransient;
    this.xmlValue = xmlValue;
    this.jaxBsonXmlAdapter = createJaxBsonXmlAdapter();
  }

  private JaxBsonAdapter<Object, Object> createJaxBsonXmlAdapter() {
    if (this.xmlJavaTypeAdapter == null) {
      return null;
    }
    Objects.requireNonNull(this.xmlJavaTypeAdapter.value(),
        "Value in JaxBsonXmlAdapter " + this.xmlJavaTypeAdapter.getClass() + " is null");
    final JaxBsonAdapter<Object, Object> ret =
        JaxBsonAdapterHelper.instance(this.xmlJavaTypeAdapter.value());
    if (ret == null) {
      Objects.requireNonNull(this.xmlJavaTypeAdapter.value(),
          "Failed to instantiate JaxBsonXmlAdapter class " + this.xmlJavaTypeAdapter.getClass());
    }
    return ret;
  }

  @Override
  public JaxBsonConfiguration getConfiguration() {
    return this.ctx.getConfiguration();
  }

  @Override
  public JaxBsonContext getContext() {
    return this.ctx;
  }

  @Override
  public Field getField() {
    return this.field;
  }

  @Override
  public JaxBsonIgnoreTransient getJaxBsonIgnoreTransient() {
    return this.jaxBsonIgnoreTransient;
  }

  @Override
  public JaxBsonIgnoreTransient getJaxBsonIgnoreTransientCfg() {
    return this.jaxBsonIgnoreTransientCfg;
  }

  @Override
  public JaxBsonName getJaxBsonName() {
    return this.jaxBsonName;
  }

  @Override
  public JaxBsonName getJaxBsonNameCfg() {
    return this.jaxBsonNameCfg;
  }

  @Override
  public JaxBsonNumberHint getJaxBsonNumberHint() {
    return this.jaxBsonNumberHint;
  }

  @Override
  public JaxBsonNumberHint getJaxBsonNumberHintCfg() {
    return this.jaxBsonNumberHintCfg;
  }

  @Override
  public JaxBsonSeeAlso getJaxBsonSeeAlso() {
    return this.jaxBsonSeeAlso;
  }

  @Override
  public JaxBsonTransient getJaxBsonTransient() {
    return this.jaxBsonTransient;
  }

  @Override
  public JaxBsonTransient getJaxBsonTransientCfg() {
    return this.jaxBsonTransientCfg;
  }

  @Override
  public JaxBsonAdapter<Object, Object> getJaxBsonXmlAdapter() {
    return this.jaxBsonXmlAdapter;
  }

  @Override
  public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMapping() {
    return this.jaxBsonXmlAnyAttributeMapping;
  }

  @Override
  public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMappingCfg() {
    return this.jaxBsonXmlAnyAttributeMappingCfg;
  }

  @Override
  public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappings() {
    return this.jaxBsonXmlAnyAttributeMappings;
  }

  @Override
  public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappingsCfg() {
    return this.jaxBsonXmlAnyAttributeMappingsCfg;
  }

  @Override
  public XmlAnyAttributeHelper getXmlAnyAttribute() {
    return this.xmlAnyAttribute;
  }

  @Override
  public XmlAnyElementHelper getXmlAnyElement() {
    return this.xmlAnyElement;
  }

  @Override
  public XmlElementRefHelper getXmlElementRef() {
    return this.xmlElementRef;
  }

  @Override
  public XmlElementRefsHelper getXmlElementRefs() {
    return this.xmlElementRefs;
  }

  public XmlElementsHelper getXmlElements() {
    return this.xmlElements;
  }

  @Override
  public XmlTransientHelper getXmlTransient() {
    return this.xmlTransient;
  }

  @Override
  public XmlValueHelper getXmlValue() {
    return this.xmlValue;
  }
}
