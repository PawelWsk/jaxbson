package org.qnixyz.jbson.impl;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import static org.qnixyz.jbson.impl.Utils.lcFirst;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.bson.Document;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.JaxBsonException;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPost;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.helpers.JaxBsonNameHelper;
import org.qnixyz.jbson.helpers.JaxBsonSeeAlsoHelper;

class ClassDescriptor {

  private final SortedMap<String, FieldDescriptor> bsonFieldNameMap = new TreeMap<>();

  private final Constructor<?> constructor;

  private final JaxBsonContextImpl ctx;

  private final FieldDescriptor fdXmlAnyAttribute;

  private final JaxBsonName jaxBsonName;

  private final JaxBsonName jaxBsonNameCfg;

  private final JaxBsonSeeAlso jaxBsonSeeAlso;

  private final Set<Class<?>> referredTypes = new HashSet<>();

  private final Set<Method> toBsonPostMethods = new HashSet<>();

  private final Set<Method> toBsonPreMethods = new HashSet<>();

  private final Set<Method> toObjectPostMethods = new HashSet<>();

  private final Set<Method> toObjectPreMethods = new HashSet<>();

  private final Class<?> type;

  private final String typeName;

  protected ClassDescriptor(final JaxBsonContextImpl ctx, final Class<?> cls) {
    this.ctx = Objects.requireNonNull(ctx, "Supplied parameter 'ctx' is null");
    this.type = Objects.requireNonNull(cls, "Supplied parameter 'cls' is null");
    this.constructor = makeConstructor();
    this.jaxBsonName = JaxBsonNameHelper.instance(this.type);
    this.jaxBsonNameCfg = ctx.getConfiguration().getJaxBsonName(this.type);
    this.jaxBsonSeeAlso = JaxBsonSeeAlsoHelper.instance(this.type);
    this.typeName = makeTypeName();
    xmlSeeAlsoToReferredTypes();
    this.fdXmlAnyAttribute = index();
  }

  private void add(final Collection<Method> collection, final Method method) {
    check(method);
    method.setAccessible(true);
    collection.add(method);
  }

  private void add(final FieldSubDescriptor fds) {
    if (fds.getName().equals(this.ctx.getConfiguration().getTypeFieldName())) {
      throw new IllegalStateException(
          "Field with Bson name '" + fds.getName() + "' in class " + this.type.getName()
              + " is reserved for type. See JaxBsonContext.getConfiguration().getTypeFieldName()");
    }
    checkDuplicateNames(fds);
    this.bsonFieldNameMap.put(fds.getName(), fds.getFd());
  }

  private void check(final Method method) {
    final StringBuilder err = new StringBuilder();
    if (method.getParameterCount() == 0) {
    } else if (method.getParameterCount() == 1) {
      final Class<?> parameterType = method.getParameterTypes()[0];
      if (!JaxBsonContext.class.isAssignableFrom(parameterType)) {
        err.append("Has 1 paramter not of type ");
        err.append(JaxBsonContext.class.getName());
        err.append(" but of type ");
        err.append(parameterType.getName());
        err.append(". ");
      }
    } else {
      err.append("Has more than 1 paramter ");
      err.append(method.getParameterCount());
      err.append(". 0 or 1 parameter of type ");
      err.append(JaxBsonContext.class.getName());
      err.append(" allowed. ");
    }
    if (err.length() > 0) {
      err.insert(0, "': ");
      err.insert(0, method.toString());
      err.insert(0, "Error in method '");
      throw new IllegalStateException(err.toString());
    }
  }

  private void checkDuplicateNames(final FieldSubDescriptor fds) {
    final FieldDescriptor dup = this.bsonFieldNameMap.get(fds.getName());
    if (dup != null) {
      throw new IllegalStateException("Multiple fields with Bson name '" + fds.getName()
          + "' in class " + this.type.getName() + ". E.g. '" + dup + "' and '" + fds + "'");
    }
  }

  public Document debugInfo() {
    final Document ret = new Document();
    ret.append("type", this.type.getName());
    ret.append("typeName", this.typeName);
    final List<Document> fds = new ArrayList<>();
    ret.append("fieldDescriptors", fds);
    for (final FieldDescriptor fd : this.bsonFieldNameMap.values()) {
      fds.add(fd.debugInfo());
    }
    return ret;
  }

  protected Set<Class<?>> getReferredTypes() {
    return this.referredTypes;
  }

  protected Class<?> getType() {
    return this.type;
  }

  protected String getTypeName() {
    return this.typeName;
  }

  private FieldDescriptor index() {
    FieldDescriptor ret = null;
    Class<?> cls = this.type;
    while (!cls.equals(Object.class)) {
      final FieldDescriptor fdXmlAnyAttribute = indexFields(cls);
      ret = ret == null ? fdXmlAnyAttribute : ret;
      indexMethods(cls);
      cls = cls.getSuperclass();
    }
    return ret;
  }

  private FieldDescriptor indexFields(final Class<?> cls) {
    FieldDescriptor fdXmlAnyAttribute = null;
    for (final Field field : cls.getDeclaredFields()) {
      final FieldDescriptor fd = new FieldDescriptor(this.ctx, field);
      if (!fd.getReferredTypes().isEmpty()) {
        this.referredTypes.addAll(fd.getReferredTypes());
      }
      if (fd.isXmlAnyAttribute()) {
        fdXmlAnyAttribute = fd;
      } else {
        fd.getFieldSubDescriptors().forEach(e -> {
          add(e);
        });
      }
    }
    return fdXmlAnyAttribute;
  }

  private void indexMethods(final Class<?> cls) {
    for (final Method method : cls.getDeclaredMethods()) {
      if (Modifier.isStatic(method.getModifiers())) {
        continue;
      }
      if (this.ctx.getConfiguration().getJaxBsonToBsonPost(method) != null) {
        add(this.toBsonPostMethods, method);
      } else if (method.getAnnotation(JaxBsonToBsonPost.class) != null) {
        add(this.toBsonPostMethods, method);
      }
      if (this.ctx.getConfiguration().getJaxBsonToBsonPre(method) != null) {
        add(this.toBsonPreMethods, method);
      } else if (method.getAnnotation(JaxBsonToBsonPre.class) != null) {
        add(this.toBsonPreMethods, method);
      }
      if (this.ctx.getConfiguration().getJaxBsonToObjectPost(method) != null) {
        add(this.toObjectPostMethods, method);
      } else if (method.getAnnotation(JaxBsonToObjectPost.class) != null) {
        add(this.toObjectPostMethods, method);
      }
      if (this.ctx.getConfiguration().getJaxBsonToObjectPre(method) != null) {
        add(this.toObjectPreMethods, method);
      } else if (method.getAnnotation(JaxBsonToObjectPre.class) != null) {
        add(this.toObjectPreMethods, method);
      }
    }
  }

  private void invoke(final Method method, final Object o) {
    try {
      if (method.getParameterCount() == 0) {
        method.invoke(o);
      } else {
        method.invoke(o, this.ctx);
      }
    } catch (final InvocationTargetException e) {
      if (e.getCause() == null) {
        throw new IllegalStateException("Unexpexted exception: " + e.getMessage(), e);
      }
      throw new IllegalStateException("Unexpexted exception: " + e.getCause().getMessage(),
          e.getCause());
    } catch (final Exception e) {
      throw new IllegalStateException("Unexpexted exception: " + e.getMessage(), e);
    }
  }

  private void invokeToBsonPostMethods(final Object o) {
    for (final Method method : this.toBsonPostMethods) {
      invoke(method, o);
    }
  }

  private void invokeToBsonPreMethods(final Object o) {
    for (final Method method : this.toBsonPreMethods) {
      invoke(method, o);
    }
  }

  private void invokeToObjectPostMethods(final Object o) {
    for (final Method method : this.toObjectPostMethods) {
      invoke(method, o);
    }
  }

  private void invokeToObjectPreMethods(final Object o) {
    for (final Method method : this.toObjectPreMethods) {
      invoke(method, o);
    }
  }

  private Constructor<?> makeConstructor() {
    try {
      final Constructor<?> ret = this.type.getDeclaredConstructor();
      ret.setAccessible(true);
      return ret;
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException("No no-argument constructor for type " + this.type.getName());
    }
  }

  private String makeTypeName() {
    if ((this.jaxBsonNameCfg != null) && !isBlank(this.jaxBsonNameCfg.name())
        && !this.jaxBsonNameCfg.name().equalsIgnoreCase("##default")) {
      return this.jaxBsonNameCfg.name();
    }
    if ((this.jaxBsonName != null) && !isBlank(this.jaxBsonName.name())
        && !this.jaxBsonName.name().equalsIgnoreCase("##default")) {
      return this.jaxBsonName.name();
    }
    return lcFirst(this.type.getSimpleName());
  }

  private Object newObjectInstance() {
    try {
      return this.constructor.newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException(
          "Failed to instantiate object of class '" + this.type.getName() + "'.", e);
    }
  }

  protected Document toBson(final Object o) {
    return toBson(o, true);
  }

  protected Document toBson(final Object o, final boolean omitType) {
    invokeToBsonPreMethods(o);
    final Document ret = new Document();
    if (!omitType || !this.ctx.getConfiguration().isAllowSmartTypeField()) {
      ret.put(this.ctx.getConfiguration().getTypeFieldName(), this.typeName);
    }
    this.bsonFieldNameMap.values().forEach(e -> {
      e.toBson(ret, o, omitType);
    });
    if (this.fdXmlAnyAttribute != null) {
      this.fdXmlAnyAttribute.addXmlAnyAttributes(ret, o);
    }
    invokeToBsonPostMethods(o);
    return ret;
  }

  protected Object toObject(final Document bson) {
    final Object ret = newObjectInstance();
    invokeToObjectPreMethods(ret);
    final SortedMap<String, String> xmlAttributeMap =
        this.fdXmlAnyAttribute == null ? null : new TreeMap<>();
    bson.forEach((bsonFieldName, bsonValue) -> {
      // Ignore type
      if (bsonFieldName.equals(this.ctx.getConfiguration().getTypeFieldName())) {
        return;
      }
      final FieldDescriptor fd = this.bsonFieldNameMap.get(bsonFieldName);
      if (fd == null) {
        if (this.fdXmlAnyAttribute == null) {
          throw new JaxBsonException.JaxBsonMappingNotFoundException(
              "Failed to map BSON field name '" + bsonFieldName + "' in BSON " + bson.toJson());
        }
        if (bsonValue instanceof String) {
          xmlAttributeMap.put(bsonFieldName, (String) bsonValue);
          return;
        }
        throw new JaxBsonException.JaxBsonMappingNotFoundException(
            "Failed to map BSON field name '" + bsonFieldName
                + "' to XmlAnyAttribute as it's value isn't a string in BSON " + bson.toJson());
      }
      fd.toObject(ret, bsonFieldName, bsonValue);
    });
    if ((xmlAttributeMap != null) && !xmlAttributeMap.isEmpty()) {
      this.fdXmlAnyAttribute.addXmlAnyAttributes(xmlAttributeMap, ret);
    }
    invokeToObjectPostMethods(ret);
    return ret;
  }

  private void xmlSeeAlsoToReferredTypes() {
    if ((this.jaxBsonSeeAlso == null) || (this.jaxBsonSeeAlso.value() == null)) {
      return;
    }
    for (final Class<?> cls : this.jaxBsonSeeAlso.value()) {
      this.referredTypes.add(cls);
    }
  }
}
