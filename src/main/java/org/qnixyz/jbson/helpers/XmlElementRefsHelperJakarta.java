package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlElementRefsHelperJakarta extends org.qnixyz.jbson.helpers.XmlElementRefsHelper {

  private final XmlElementRefHelper[] value;
  private final jakarta.xml.bind.annotation.XmlElementRefs xmlElementRefs;

  XmlElementRefsHelperJakarta(final jakarta.xml.bind.annotation.XmlElementRefs xmlElementRefs) {
    this.xmlElementRefs = Objects.requireNonNull(xmlElementRefs);
    this.value = createValue();
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlElementRefs.annotationType();
  }

  private XmlElementRefHelper[] createValue() {
    if (this.xmlElementRefs.value() == null) {
      return null;
    }
    final XmlElementRefHelper[] ret = new XmlElementRefHelper[this.xmlElementRefs.value().length];
    for (int i = 0; i < ret.length; i++) {
      if (this.xmlElementRefs.value()[i] == null) {
        ret[i] = null;
      } else {
        ret[i] = XmlElementRefHelper.instance(this.xmlElementRefs.value()[i]);
        if (ret[i] == null) {
          throw new IllegalStateException("This is a bug");
        }
      }
    }
    return ret;
  }

  @Override
  public XmlElementRefHelper[] value() {
    return this.value;
  }
}
