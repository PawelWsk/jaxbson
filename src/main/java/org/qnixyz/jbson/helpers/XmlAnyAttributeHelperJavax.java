package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlAnyAttributeHelperJavax extends org.qnixyz.jbson.helpers.XmlAnyAttributeHelper {

  private final javax.xml.bind.annotation.XmlAnyAttribute xmlAnyAttribute;

  XmlAnyAttributeHelperJavax(final javax.xml.bind.annotation.XmlAnyAttribute xmlAnyAttribute) {
    this.xmlAnyAttribute = Objects.requireNonNull(xmlAnyAttribute);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlAnyAttribute.annotationType();
  }
}
