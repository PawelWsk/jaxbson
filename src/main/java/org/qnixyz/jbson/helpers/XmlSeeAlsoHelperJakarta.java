package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlSeeAlsoHelperJakarta extends org.qnixyz.jbson.helpers.XmlSeeAlsoHelper {

  private final jakarta.xml.bind.annotation.XmlSeeAlso xmlSeeAlso;

  XmlSeeAlsoHelperJakarta(final jakarta.xml.bind.annotation.XmlSeeAlso xmlSeeAlso) {
    this.xmlSeeAlso = Objects.requireNonNull(xmlSeeAlso);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlSeeAlso.annotationType();
  }

  @Override
  public Class<?>[] value() {
    return this.xmlSeeAlso.value();
  }
}
