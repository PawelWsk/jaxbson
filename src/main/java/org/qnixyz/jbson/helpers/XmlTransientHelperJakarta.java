package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlTransientHelperJakarta extends org.qnixyz.jbson.helpers.XmlTransientHelper {

  private final jakarta.xml.bind.annotation.XmlTransient xmlTransient;

  XmlTransientHelperJakarta(final jakarta.xml.bind.annotation.XmlTransient xmlTransient) {
    this.xmlTransient = Objects.requireNonNull(xmlTransient);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlTransient.annotationType();
  }
}
