package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public abstract class XmlAnyAttributeHelper {

  private static final Class<?> JAKARTA_JAXB_CLASS = loadJaxbClass("jakarta");
  private static final Constructor<? extends XmlAnyAttributeHelper> JAKARTA_JAXBSON_CONSTRUCTOR =
      getJaxBsonJakartaConstructor(JAKARTA_JAXB_CLASS, "Jakarta");

  private static final Class<?> JAVAX_JAXB_CLASS = loadJaxbClass("javax");
  private static final Constructor<? extends XmlAnyAttributeHelper> JAVAX_JAXBSON_CONSTRUCTOR =
      getJaxBsonJakartaConstructor(JAVAX_JAXB_CLASS, "Javax");

  private static Constructor<? extends XmlAnyAttributeHelper> getJaxBsonJakartaConstructor(
      final Class<?> clsJaxb, final String suffix) {
    if (clsJaxb == null) {
      return null;
    }
    try {
      @SuppressWarnings("unchecked")
      final Class<? extends XmlAnyAttributeHelper> cls =
          (Class<? extends XmlAnyAttributeHelper>) Thread.currentThread().getContextClassLoader()
              .loadClass(XmlAnyAttributeHelper.class.getName() + suffix);
      final Constructor<? extends XmlAnyAttributeHelper> ret = cls.getDeclaredConstructor(clsJaxb);
      ret.setAccessible(true);
      return ret;
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  public static XmlAnyAttributeHelper instance(final Annotation a) {
    if (a.annotationType().equals(JAKARTA_JAXB_CLASS)) {
      return instanceJakarta(a);
    }
    if (a.annotationType().equals(JAVAX_JAXB_CLASS)) {
      return instanceJavax(a);
    }
    return null;
  }

  public static XmlAnyAttributeHelper instance(final Annotation[] as) {
    if (as == null) {
      return null;
    }
    for (final Annotation a : as) {
      final XmlAnyAttributeHelper ret = instance(a);
      if (ret != null) {
        return ret;
      }
    }
    return null;
  }

  public static XmlAnyAttributeHelper instance(final Field field) {
    return XmlAnyAttributeHelper.instance(field.getAnnotations());
  }

  private static XmlAnyAttributeHelper instanceJakarta(final Annotation o) {
    try {
      return JAKARTA_JAXBSON_CONSTRUCTOR.newInstance(o);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static XmlAnyAttributeHelper instanceJavax(final Annotation o) {
    try {
      return JAVAX_JAXBSON_CONSTRUCTOR.newInstance(o);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static Class<?> loadJaxbClass(final String prefix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(prefix + ".xml.bind.annotation.XmlAnyAttribute");
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  public abstract Class<? extends Annotation> annotationType();
}
