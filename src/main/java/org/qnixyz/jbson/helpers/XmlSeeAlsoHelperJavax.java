package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlSeeAlsoHelperJavax extends org.qnixyz.jbson.helpers.XmlSeeAlsoHelper {

  private final javax.xml.bind.annotation.XmlSeeAlso xmlSeeAlso;

  XmlSeeAlsoHelperJavax(final javax.xml.bind.annotation.XmlSeeAlso xmlSeeAlso) {
    this.xmlSeeAlso = Objects.requireNonNull(xmlSeeAlso);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlSeeAlso.annotationType();
  }

  @Override
  public Class<?>[] value() {
    return this.xmlSeeAlso.value();
  }
}
