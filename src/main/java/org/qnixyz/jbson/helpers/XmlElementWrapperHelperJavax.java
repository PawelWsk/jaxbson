package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlElementWrapperHelperJavax extends org.qnixyz.jbson.helpers.XmlElementWrapperHelper {

  private final javax.xml.bind.annotation.XmlElementWrapper xmlElementWrapper;

  XmlElementWrapperHelperJavax(
      final javax.xml.bind.annotation.XmlElementWrapper xmlElementWrapper) {
    this.xmlElementWrapper = Objects.requireNonNull(xmlElementWrapper);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlElementWrapper.annotationType();
  }

  @Override
  public String name() {
    return this.xmlElementWrapper.name();
  }

  @Override
  public String namespace() {
    return this.xmlElementWrapper.namespace();
  }

  @Override
  public boolean nillable() {
    return this.xmlElementWrapper.nillable();
  }

  @Override
  public boolean required() {
    return this.xmlElementWrapper.required();
  }
}
