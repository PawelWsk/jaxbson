package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlAnyAttributeHelperJakarta extends org.qnixyz.jbson.helpers.XmlAnyAttributeHelper {

  private final jakarta.xml.bind.annotation.XmlAnyAttribute xmlAnyAttribute;

  XmlAnyAttributeHelperJakarta(final jakarta.xml.bind.annotation.XmlAnyAttribute xmlAnyAttribute) {
    this.xmlAnyAttribute = Objects.requireNonNull(xmlAnyAttribute);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlAnyAttribute.annotationType();
  }
}
