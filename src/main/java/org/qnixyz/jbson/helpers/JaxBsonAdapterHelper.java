package org.qnixyz.jbson.helpers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonAdapterHelper {

  private static final Class<?> JAKARTA_CLASS = loadClass("Jakarta");
  private static final Class<?> JAKARTA_JAXB_CLASS = loadJaxbClass("jakarta");
  private static final Constructor<?> JAKARTA_UTIL_CONSTRUCTOR = loadUtilConstructor(JAKARTA_CLASS);

  private static final Class<?> JAVAX_CLASS = loadClass("Javax");
  private static final Class<?> JAVAX_JAXB_CLASS = loadJaxbClass("javax");
  private static final Constructor<?> JAVAX_UTIL_CONSTRUCTOR = loadUtilConstructor(JAVAX_CLASS);

  @SuppressWarnings("unchecked")
  public static JaxBsonAdapter<Object, Object> instance(final Class<?> cls) {
    if ((JAKARTA_JAXB_CLASS != null) && JAKARTA_JAXB_CLASS.isAssignableFrom(cls)) {
      return instanceJakarta(cls);
    }
    if ((JAVAX_JAXB_CLASS != null) && JAVAX_JAXB_CLASS.isAssignableFrom(cls)) {
      return instanceJavax(cls);
    }
    if (JaxBsonAdapter.class.isAssignableFrom(cls)) {
      try {
        final Constructor<?> c = cls.getDeclaredConstructor();
        c.setAccessible(true);
        return (JaxBsonAdapter<Object, Object>) c.newInstance();
      } catch (final NoSuchMethodException e) {
        throw new IllegalStateException("No empty constructor for class " + cls, e);
      } catch (final SecurityException e) {
        throw new IllegalStateException(
            "Security exception with empty constructor for class " + cls, e);
      } catch (final InstantiationException e) {
        throw new IllegalStateException("Cannot instantiate class with empty constructor " + cls,
            e);
      } catch (final IllegalAccessException e) {
        throw new IllegalStateException(
            "Illegal access exception with empty constructor for class " + cls, e);
      } catch (final IllegalArgumentException e) {
        throw new IllegalStateException("This is a bug", e);
      } catch (final InvocationTargetException e) {
        throw new IllegalStateException("This is a bug", e);
      }
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  private static JaxBsonAdapter<Object, Object> instanceJakarta(final Class<?> cls) {
    try {
      return (JaxBsonAdapter<Object, Object>) JAKARTA_UTIL_CONSTRUCTOR.newInstance(cls);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  @SuppressWarnings("unchecked")
  private static JaxBsonAdapter<Object, Object> instanceJavax(final Class<?> cls) {
    try {
      return (JaxBsonAdapter<Object, Object>) JAVAX_UTIL_CONSTRUCTOR.newInstance(cls);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static Class<?> loadClass(final String suffix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(JaxBsonAdapterHelper.class.getName() + suffix);
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  private static Class<?> loadJaxbClass(final String prefix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(prefix + ".xml.bind.annotation.adapters.XmlAdapter");
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  private static Constructor<?> loadUtilConstructor(final Class<?> cls) {
    if (cls == null) {
      return null;
    }
    try {
      final Constructor<?> ret = cls.getConstructor(Class.class);
      ret.setAccessible(true);
      return ret;
    } catch (SecurityException | NoSuchMethodException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }
}
