package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlRootElementHelperJakarta extends org.qnixyz.jbson.helpers.XmlRootElementHelper {

  private final jakarta.xml.bind.annotation.XmlRootElement xmlRootElement;

  XmlRootElementHelperJakarta(final jakarta.xml.bind.annotation.XmlRootElement xmlRootElement) {
    this.xmlRootElement = Objects.requireNonNull(xmlRootElement);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlRootElement.annotationType();
  }

  @Override
  public String name() {
    return this.xmlRootElement.name();
  }

  @Override
  public String namespace() {
    return this.xmlRootElement.namespace();
  }
}
