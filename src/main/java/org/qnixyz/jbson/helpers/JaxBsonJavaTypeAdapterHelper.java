package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;

public class JaxBsonJavaTypeAdapterHelper {

  private static final Class<?> JAKARTA_JAXB_CLASS = loadJaxbClass("jakarta");
  private static final Class<?> JAKARTA_JAXBSON_CLASS =
      loadJaxBsonClass(JAKARTA_JAXB_CLASS, "Jakarta");
  private static final Method JAKARTA_JAXBSON_METHOD =
      loadJaxBsonJakartaMethod(JAKARTA_JAXBSON_CLASS, JAKARTA_JAXB_CLASS);

  private static final Class<?> JAVAX_JAXB_CLASS = loadJaxbClass("javax");
  private static final Class<?> JAVAX_JAXBSON_CLASS = loadJaxBsonClass(JAVAX_JAXB_CLASS, "Javax");
  private static final Method JAVAX_JAXBSON_METHOD =
      loadJaxBsonJakartaMethod(JAVAX_JAXBSON_CLASS, JAVAX_JAXB_CLASS);

  private static JaxBsonJavaTypeAdapter instanceJakarta(final Annotation a) {
    try {
      return (JaxBsonJavaTypeAdapter) JAKARTA_JAXBSON_METHOD.invoke(null, a);
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static JaxBsonJavaTypeAdapter instanceJavax(final Annotation a) {
    try {
      return (JaxBsonJavaTypeAdapter) JAVAX_JAXBSON_METHOD.invoke(null, a);
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  public static JaxBsonJavaTypeAdapter jaxBsonJavaTypeAdapter(final Annotation a) {
    if (a.annotationType().equals(JaxBsonJavaTypeAdapter.class)) {
      return (JaxBsonJavaTypeAdapter) a;
    }
    if (a.annotationType().equals(JAKARTA_JAXB_CLASS)) {
      return instanceJakarta(a);
    }
    if (a.annotationType().equals(JAVAX_JAXB_CLASS)) {
      return instanceJavax(a);
    }
    return null;
  }

  public static JaxBsonJavaTypeAdapter jaxBsonJavaTypeAdapter(final Annotation[] as) {
    if (as == null) {
      return null;
    }
    JaxBsonJavaTypeAdapter other = null;
    for (final Annotation a : as) {
      if (a.annotationType().equals(JaxBsonJavaTypeAdapter.class)) {
        return (JaxBsonJavaTypeAdapter) a;
      }
      final JaxBsonJavaTypeAdapter inst = JaxBsonJavaTypeAdapterHelper.jaxBsonJavaTypeAdapter(a);
      if (inst != null) {
        other = inst;
      }
    }
    return other;
  }

  public static JaxBsonJavaTypeAdapter jaxBsonJavaTypeAdapter(final Field field) {
    return jaxBsonJavaTypeAdapter(field.getAnnotations());
  }

  private static Class<?> loadJaxbClass(final String prefix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(prefix + ".xml.bind.annotation.adapters.XmlJavaTypeAdapter");
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  private static Class<?> loadJaxBsonClass(final Class<?> clsJaxb, final String suffix) {
    try {
      if (clsJaxb == null) {
        return null;
      }
      return Thread.currentThread().getContextClassLoader()
          .loadClass(JaxBsonJavaTypeAdapterHelper.class.getName() + suffix);
    } catch (final ClassNotFoundException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static Method loadJaxBsonJakartaMethod(final Class<?> cls, final Class<?> clsJaxb) {
    if (clsJaxb == null) {
      return null;
    }
    try {
      final Method ret = cls.getDeclaredMethod("jaxBsonJavaTypeAdapter", clsJaxb);
      ret.setAccessible(true);
      return ret;
    } catch (NoSuchMethodException | SecurityException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }
}
