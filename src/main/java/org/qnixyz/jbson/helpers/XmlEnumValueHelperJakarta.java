package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlEnumValueHelperJakarta extends org.qnixyz.jbson.helpers.XmlEnumValueHelper {

  private final jakarta.xml.bind.annotation.XmlEnumValue xmlEnumValue;

  XmlEnumValueHelperJakarta(final jakarta.xml.bind.annotation.XmlEnumValue xmlEnumValue) {
    this.xmlEnumValue = Objects.requireNonNull(xmlEnumValue);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlEnumValue.annotationType();
  }

  @Override
  public String value() {
    return this.xmlEnumValue.value();
  }
}
