package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

class JaxBsonJavaTypeAdapterHelperJavax {

  static JaxBsonJavaTypeAdapter jaxBsonJavaTypeAdapter(
      final XmlJavaTypeAdapter xmlJavaTypeAdapter) {
    Objects.requireNonNull(xmlJavaTypeAdapter, XmlJavaTypeAdapter.class.getName() + " annotation");
    final Class<?> xmlAdapter = Objects.requireNonNull(xmlJavaTypeAdapter.value(),
        XmlJavaTypeAdapter.class.getName() + " annotation value");
    final JaxBsonAdapter<?, ?> jaxBsonXmlAdapter =
        JaxBsonAdapterHelper.instance(xmlJavaTypeAdapter.value());
    if (jaxBsonXmlAdapter == null) {
      throw new IllegalStateException("This is a bug.");
    }
    return new JaxBsonJavaTypeAdapter() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return JaxBsonJavaTypeAdapter.class;
      }

      @Override
      public Class<?> value() {
        return xmlAdapter;
      }
    };
  }
}
