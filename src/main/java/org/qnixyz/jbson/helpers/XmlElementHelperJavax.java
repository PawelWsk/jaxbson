package org.qnixyz.jbson.helpers;

import java.util.Objects;

class XmlElementHelperJavax extends org.qnixyz.jbson.helpers.XmlElementHelper {

  private final javax.xml.bind.annotation.XmlElement xmlElement;

  XmlElementHelperJavax(final javax.xml.bind.annotation.XmlElement xmlElement) {
    this.xmlElement = Objects.requireNonNull(xmlElement);
  }

  @Override
  public String defaultValue() {
    return this.xmlElement.defaultValue();
  }

  @Override
  public boolean isDefaultType() {
    return this.xmlElement.type().equals(javax.xml.bind.annotation.XmlElement.DEFAULT.class);
  }

  @Override
  public String name() {
    return this.xmlElement.name();
  }

  @Override
  public String namespace() {
    return this.xmlElement.namespace();
  }

  @Override
  public boolean nillable() {
    return this.xmlElement.nillable();
  }

  @Override
  public boolean required() {
    return this.xmlElement.required();
  }

  @Override
  public Class<?> type() {
    return this.xmlElement.type();
  }
}
