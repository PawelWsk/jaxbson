package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlElementsHelperJakarta extends org.qnixyz.jbson.helpers.XmlElementsHelper {

  private final XmlElementHelper[] value;
  private final jakarta.xml.bind.annotation.XmlElements xmlElements;

  XmlElementsHelperJakarta(final jakarta.xml.bind.annotation.XmlElements xmlElements) {
    this.xmlElements = Objects.requireNonNull(xmlElements);
    this.value = createValue();
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlElements.annotationType();
  }

  private XmlElementHelper[] createValue() {
    if (this.xmlElements.value() == null) {
      return null;
    }
    final XmlElementHelper[] ret = new XmlElementHelper[this.xmlElements.value().length];
    for (int i = 0; i < ret.length; i++) {
      if (this.xmlElements.value()[i] == null) {
        ret[i] = null;
      } else {
        ret[i] = XmlElementHelper.instance(this.xmlElements.value()[i]);
        if (ret[i] == null) {
          throw new IllegalStateException("This is a bug");
        }
      }
    }
    return ret;
  }

  @Override
  public XmlElementHelper[] value() {
    return this.value;
  }
}
