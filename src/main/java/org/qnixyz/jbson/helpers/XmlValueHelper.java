package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public abstract class XmlValueHelper {

  private static final Class<?> JAKARTA_JAXB_CLASS = loadJaxbClass("jakarta");
  private static final Constructor<? extends XmlValueHelper> JAKARTA_JAXBSON_CONSTRUCTOR =
      getJaxBsonConstructor(JAKARTA_JAXB_CLASS, "Jakarta");

  private static final Class<?> JAVAX_JAXB_CLASS = loadJaxbClass("javax");
  private static final Constructor<? extends XmlValueHelper> JAVAX_JAXBSON_CONSTRUCTOR =
      getJaxBsonConstructor(JAVAX_JAXB_CLASS, "Javax");

  private static Constructor<? extends XmlValueHelper> getJaxBsonConstructor(final Class<?> clsJaxb,
      final String suffix) {
    if (clsJaxb == null) {
      return null;
    }
    try {
      @SuppressWarnings("unchecked")
      final Class<? extends XmlValueHelper> cls =
          (Class<? extends XmlValueHelper>) Thread.currentThread().getContextClassLoader()
              .loadClass(XmlValueHelper.class.getName() + suffix);
      final Constructor<? extends XmlValueHelper> ret = cls.getDeclaredConstructor(clsJaxb);
      ret.setAccessible(true);
      return ret;
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  public static XmlValueHelper instance(final Annotation a) {
    if (a.annotationType().equals(JAKARTA_JAXB_CLASS)) {
      return instanceJakarta(a);
    }
    if (a.annotationType().equals(JAVAX_JAXB_CLASS)) {
      return instanceJavax(a);
    }
    return null;
  }

  public static XmlValueHelper instance(final Annotation[] as) {
    if (as == null) {
      return null;
    }
    for (final Annotation a : as) {
      final XmlValueHelper ret = XmlValueHelper.instance(a);
      if (ret != null) {
        return ret;
      }
    }
    return null;
  }

  public static XmlValueHelper instance(final Field field) {
    return instance(field.getAnnotations());
  }

  private static XmlValueHelper instanceJakarta(final Annotation a) {
    try {
      return JAKARTA_JAXBSON_CONSTRUCTOR.newInstance(a);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static XmlValueHelper instanceJavax(final Annotation a) {
    try {
      return JAVAX_JAXBSON_CONSTRUCTOR.newInstance(a);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static Class<?> loadJaxbClass(final String prefix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(prefix + ".xml.bind.annotation.XmlValue");
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  public abstract Class<? extends Annotation> annotationType();
}
