package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlElementsHelperJavax extends org.qnixyz.jbson.helpers.XmlElementsHelper {

  private final XmlElementHelper[] value;
  private final javax.xml.bind.annotation.XmlElements xmlElements;

  XmlElementsHelperJavax(final javax.xml.bind.annotation.XmlElements xmlElements) {
    this.xmlElements = Objects.requireNonNull(xmlElements);
    this.value = createValue(xmlElements);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlElements.annotationType();
  }

  private XmlElementHelper[] createValue(final javax.xml.bind.annotation.XmlElements xmlElements) {
    if (xmlElements.value() == null) {
      return null;
    }
    final XmlElementHelper[] ret = new XmlElementHelper[xmlElements.value().length];
    for (int i = 0; i < ret.length; i++) {
      if (xmlElements.value()[i] == null) {
        ret[i] = null;
      } else {
        ret[i] = XmlElementHelper.instance(xmlElements.value()[i]);
        if (ret[i] == null) {
          throw new IllegalStateException("This is a bug");
        }
      }
    }
    return ret;
  }

  @Override
  public XmlElementHelper[] value() {
    return this.value;
  }
}
