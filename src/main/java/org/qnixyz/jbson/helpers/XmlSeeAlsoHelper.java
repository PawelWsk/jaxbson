package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class XmlSeeAlsoHelper {

  private static final Class<?> JAKARTA_JAXB_CLASS = loadJaxbClass("jakarta");
  private static final Constructor<? extends XmlSeeAlsoHelper> JAKARTA_JAXBSON_CONSTRUCTOR =
      getJaxBsonJakartaConstructor(JAKARTA_JAXB_CLASS, "Jakarta");

  private static final Class<?> JAVAX_JAXB_CLASS = loadJaxbClass("javax");
  private static final Constructor<? extends XmlSeeAlsoHelper> JAVAX_JAXBSON_CONSTRUCTOR =
      getJaxBsonJakartaConstructor(JAVAX_JAXB_CLASS, "Javax");

  private static Constructor<? extends XmlSeeAlsoHelper> getJaxBsonJakartaConstructor(
      final Class<?> clsJaxb, final String suffix) {
    if (clsJaxb == null) {
      return null;
    }
    try {
      @SuppressWarnings("unchecked")
      final Class<? extends XmlSeeAlsoHelper> cls =
          (Class<? extends XmlSeeAlsoHelper>) Thread.currentThread().getContextClassLoader()
              .loadClass(XmlSeeAlsoHelper.class.getName() + suffix);
      final Constructor<? extends XmlSeeAlsoHelper> ret = cls.getDeclaredConstructor(clsJaxb);
      ret.setAccessible(true);
      return ret;
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  public static XmlSeeAlsoHelper instance(final Annotation a) {
    if (a.annotationType().equals(JAKARTA_JAXB_CLASS)) {
      return instanceJakarta(a);
    }
    if (a.annotationType().equals(JAVAX_JAXB_CLASS)) {
      return instanceJavax(a);
    }
    return null;
  }

  public static XmlSeeAlsoHelper instance(final Annotation[] as) {
    if (as == null) {
      return null;
    }
    for (final Annotation a : as) {
      final XmlSeeAlsoHelper ret = XmlSeeAlsoHelper.instance(a);
      if (ret != null) {
        return ret;
      }
    }
    return null;
  }

  public static XmlSeeAlsoHelper instance(final Class<?> type) {
    return instance(type.getAnnotations());
  }

  private static XmlSeeAlsoHelper instanceJakarta(final Annotation a) {
    try {
      return JAKARTA_JAXBSON_CONSTRUCTOR.newInstance(a);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static XmlSeeAlsoHelper instanceJavax(final Annotation a) {
    try {
      return JAVAX_JAXBSON_CONSTRUCTOR.newInstance(a);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static Class<?> loadJaxbClass(final String prefix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(prefix + ".xml.bind.annotation.XmlSeeAlso");
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  public abstract Class<? extends Annotation> annotationType();

  public abstract Class<?>[] value();
}
