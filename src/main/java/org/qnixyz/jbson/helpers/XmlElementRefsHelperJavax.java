package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlElementRefsHelperJavax extends org.qnixyz.jbson.helpers.XmlElementRefsHelper {

  private final XmlElementRefHelper[] value;
  private final javax.xml.bind.annotation.XmlElementRefs xmlElementRefs;

  XmlElementRefsHelperJavax(final javax.xml.bind.annotation.XmlElementRefs xmlElementRefs) {
    this.xmlElementRefs = Objects.requireNonNull(xmlElementRefs);
    this.value = createValue(xmlElementRefs);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlElementRefs.annotationType();
  }

  private XmlElementRefHelper[] createValue(
      final javax.xml.bind.annotation.XmlElementRefs xmlElementRefs) {
    if (xmlElementRefs.value() == null) {
      return null;
    }
    final XmlElementRefHelper[] ret = new XmlElementRefHelper[xmlElementRefs.value().length];
    for (int i = 0; i < ret.length; i++) {
      if (xmlElementRefs.value()[i] == null) {
        ret[i] = null;
      } else {
        ret[i] = XmlElementRefHelper.instance(xmlElementRefs.value()[i]);
        if (ret[i] == null) {
          throw new IllegalStateException("This is a bug");
        }
      }
    }
    return ret;
  }

  @Override
  public XmlElementRefHelper[] value() {
    return this.value;
  }
}
