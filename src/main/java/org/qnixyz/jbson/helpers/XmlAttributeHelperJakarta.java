package org.qnixyz.jbson.helpers;

import java.util.Objects;

class XmlAttributeHelperJakarta extends org.qnixyz.jbson.helpers.XmlAttributeHelper {

  private final jakarta.xml.bind.annotation.XmlAttribute xmlAttribute;

  XmlAttributeHelperJakarta(final jakarta.xml.bind.annotation.XmlAttribute xmlAttribute) {
    this.xmlAttribute = Objects.requireNonNull(xmlAttribute);
  }

  @Override
  public String name() {
    return this.xmlAttribute.name();
  }

  @Override
  public String namespace() {
    return this.xmlAttribute.namespace();
  }

  @Override
  public boolean required() {
    return this.xmlAttribute.required();
  }
}
