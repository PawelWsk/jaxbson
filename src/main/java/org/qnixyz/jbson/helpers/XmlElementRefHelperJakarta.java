package org.qnixyz.jbson.helpers;

import java.util.Objects;

class XmlElementRefHelperJakarta extends org.qnixyz.jbson.helpers.XmlElementRefHelper {

  private final jakarta.xml.bind.annotation.XmlElementRef xmlElementRef;

  XmlElementRefHelperJakarta(final jakarta.xml.bind.annotation.XmlElementRef xmlElementRef) {
    this.xmlElementRef = Objects.requireNonNull(xmlElementRef);
  }

  @Override
  public boolean isDefaultType() {
    return this.xmlElementRef.type()
        .equals(jakarta.xml.bind.annotation.XmlElementRef.DEFAULT.class);
  }

  @Override
  public String name() {
    return this.xmlElementRef.name();
  }

  @Override
  public String namespace() {
    return this.xmlElementRef.namespace();
  }

  @Override
  public boolean required() {
    return this.xmlElementRef.required();
  }

  @Override
  public Class<?> type() {
    return this.xmlElementRef.type();
  }
}
