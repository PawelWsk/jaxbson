package org.qnixyz.jbson.helpers;

import java.util.Objects;

class JaxBsonAdapterJakarta<ValueType, BoundType>
    extends org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter<ValueType, BoundType> {

  private final jakarta.xml.bind.annotation.adapters.XmlAdapter<ValueType, BoundType> xmlAdapter;

  JaxBsonAdapterJakarta(
      final jakarta.xml.bind.annotation.adapters.XmlAdapter<ValueType, BoundType> xmlAdapter) {
    this.xmlAdapter = Objects.requireNonNull(xmlAdapter);
  }

  @Override
  public ValueType marshal(final BoundType v) throws Exception {
    return this.xmlAdapter.marshal(v);
  }

  @Override
  public BoundType unmarshal(final ValueType v) throws Exception {
    return this.xmlAdapter.unmarshal(v);
  }
}
