package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlAnyElementHelperJavax extends org.qnixyz.jbson.helpers.XmlAnyElementHelper {

  private final javax.xml.bind.annotation.XmlAnyElement xmlAnyElement;

  XmlAnyElementHelperJavax(final javax.xml.bind.annotation.XmlAnyElement xmlAnyElement) {
    this.xmlAnyElement = Objects.requireNonNull(xmlAnyElement);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlAnyElement.annotationType();
  }

  @Override
  public boolean lax() {
    return this.xmlAnyElement.lax();
  }
}
