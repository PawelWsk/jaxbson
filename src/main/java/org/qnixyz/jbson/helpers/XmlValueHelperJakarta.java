package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlValueHelperJakarta extends org.qnixyz.jbson.helpers.XmlValueHelper {

  private final jakarta.xml.bind.annotation.XmlValue xmlValue;

  XmlValueHelperJakarta(final jakarta.xml.bind.annotation.XmlValue xmlValue) {
    this.xmlValue = Objects.requireNonNull(xmlValue);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlValue.annotationType();
  }
}
