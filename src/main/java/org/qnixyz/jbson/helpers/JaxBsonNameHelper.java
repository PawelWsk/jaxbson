package org.qnixyz.jbson.helpers;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import static org.qnixyz.jbson.impl.Utils.lcFirst;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import org.qnixyz.jbson.annotations.JaxBsonName;

public class JaxBsonNameHelper {

  private static JaxBsonName create(final String name) {
    return new JaxBsonName() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return JaxBsonName.class;
      }

      @Override
      public String name() {
        return name;
      }
    };
  }

  public static JaxBsonName instance(final Annotation[] as, final boolean forField) {
    if (as == null) {
      return null;
    }
    XmlAttributeHelper ah = null;
    XmlElementHelper eh = null;
    XmlElementRefHelper erh = null;
    XmlElementWrapperHelper ewh = null;
    XmlTypeHelper th = null;
    for (final Annotation a : as) {
      if (a.annotationType().equals(JaxBsonName.class)) {
        return (JaxBsonName) a;
      }
      if (forField) {
        ah = ah == null ? XmlAttributeHelper.instance(a) : ah;
        eh = eh == null ? XmlElementHelper.instance(a) : eh;
        erh = erh == null ? XmlElementRefHelper.instance(a) : erh;
        ewh = ewh == null ? XmlElementWrapperHelper.instance(a) : ewh;
      } else {
        th = th == null ? XmlTypeHelper.instance(a) : th;
      }
    }
    String name = makeName(th);
    if (name == null) {
      name = makeName(ewh);
    }
    if (name == null) {
      name = makeName(eh);
    }
    if (name == null) {
      name = makeName(ah);
    }
    if (name == null) {
      name = makeName(erh);
    }
    return name == null ? null : create(name);
  }

  public static JaxBsonName instance(final Class<?> type) {
    return instance(type.getAnnotations(), false);
  }

  public static JaxBsonName instance(final Field field) {
    return instance(field.getAnnotations(), true);
  }

  private static String makeName(final XmlAttributeHelper h) {
    if (h == null) {
      return null;
    }
    if (isBlank(h.name())) {
      return null;
    }
    if (h.name().equalsIgnoreCase("##default")) {
      return null;
    }
    return h.name();
  }

  private static String makeName(final XmlElementHelper h) {
    if (h == null) {
      return null;
    }
    if (isBlank(h.name())) {
      return null;
    }
    if (h.name().equalsIgnoreCase("##default")) {
      return null;
    }
    return h.name();
  }

  private static String makeName(final XmlElementRefHelper h) {
    if (h == null) {
      return null;
    }
    if (h.isDefaultType()) {
      return null;
    }
    final Class<?> type = h.type();
    final XmlRootElementHelper xre = XmlRootElementHelper.instance(type);
    return makeName(xre, type);
  }

  private static String makeName(final XmlElementWrapperHelper h) {
    if (h == null) {
      return null;
    }
    if (isBlank(h.name())) {
      return null;
    }
    if (h.name().equalsIgnoreCase("##default")) {
      return null;
    }
    return h.name();
  }

  private static String makeName(final XmlRootElementHelper h, final Class<?> type) {
    if ((h == null) || isBlank(h.name()) || h.name().equalsIgnoreCase("##default")) {
      return lcFirst(type.getSimpleName());
    }
    return h.name();
  }

  private static String makeName(final XmlTypeHelper h) {
    if (h == null) {
      return null;
    }
    if (isBlank(h.name())) {
      return null;
    }
    if (h.name().equalsIgnoreCase("##default")) {
      return null;
    }
    return h.name();
  }
}
