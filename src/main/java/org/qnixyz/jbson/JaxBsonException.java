package org.qnixyz.jbson;

public abstract class JaxBsonException extends RuntimeException {

  public static class JaxBsonMappingNotFoundException extends JaxBsonException {

    private static final long serialVersionUID = 1L;

    public JaxBsonMappingNotFoundException() {
      super();
    }

    public JaxBsonMappingNotFoundException(final String message) {
      super(message);
    }

    public JaxBsonMappingNotFoundException(final String message, final Throwable cause) {
      super(message, cause);
    }

    public JaxBsonMappingNotFoundException(final Throwable cause) {
      super(cause);
    }
  }

  public static class JaxBsonNameClashException extends JaxBsonException {

    private static final long serialVersionUID = 1L;

    public JaxBsonNameClashException() {
      super();
    }

    public JaxBsonNameClashException(final String message) {
      super(message);
    }

    public JaxBsonNameClashException(final String message, final Throwable cause) {
      super(message, cause);
    }

    public JaxBsonNameClashException(final Throwable cause) {
      super(cause);
    }
  }

  private static final long serialVersionUID = 1L;

  public JaxBsonException() {
    super();
  }

  public JaxBsonException(final String message) {
    super(message);
  }

  public JaxBsonException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public JaxBsonException(final Throwable cause) {
    super(cause);
  }

}
