package org.qnixyz.jbson;

import org.bson.Document;
import org.qnixyz.jbson.impl.JaxBsonContextImpl;

public interface JaxBsonContext {

  public static JaxBsonContext newInstance(final Class<?>... classes) {
    return new JaxBsonContextImpl(classes);
  }

  public static JaxBsonContext newInstance(final JaxBsonConfiguration configuration,
      final Class<?>... classes) {
    return new JaxBsonContextImpl(configuration, classes);
  }

  public Document debugInfo();

  public JaxBsonConfiguration getConfiguration();

  public Document toBson(Object o);

  public Document toBson(Object o, boolean omitType);

  public Object toObject(Document bson);

  public Object toObject(Document bson, Class<?> type);

  public Object toObject(Document bson, String typeName);
}
