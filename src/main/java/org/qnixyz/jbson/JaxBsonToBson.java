package org.qnixyz.jbson;

import static org.qnixyz.jbson.impl.Utils.toObjectArray;
import static org.qnixyz.jbson.impl.Utils.toPrimitiveArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.bson.types.Binary;
import org.bson.types.ObjectId;

public class JaxBsonToBson {

  @FunctionalInterface
  public interface BinaryCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BooleanArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Boolean[] src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, boolean[] src);
  }

  @FunctionalInterface
  public interface BooleanPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface ByteArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Byte[] src);
  }

  @FunctionalInterface
  public interface ByteCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Byte> src);
  }

  @FunctionalInterface
  public interface BytePrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, byte[] src);
  }

  @FunctionalInterface
  public interface BytePrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, byte src);
  }

  @FunctionalInterface
  public interface ByteToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Byte src);
  }

  @FunctionalInterface
  public interface CharacterArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Character[] src);
  }

  @FunctionalInterface
  public interface CharacterCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Character> src);
  }

  @FunctionalInterface
  public interface CharacterPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, char[] src);
  }

  @FunctionalInterface
  public interface CharacterPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, char src);
  }

  @FunctionalInterface
  public interface CharacterToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Character src);
  }

  @FunctionalInterface
  public interface DateBasedArrayToBson {
    List<Date> convert(JaxBsonFieldContext fieldCtx, Object[] src);
  }

  @FunctionalInterface
  public interface DateBasedCollectionToBson {
    List<Date> convert(JaxBsonFieldContext fieldCtx, Collection<Object> src);
  }

  @FunctionalInterface
  public interface DateBasedToBson {
    Date convert(JaxBsonFieldContext fieldCtx, Object src);
  }

  @FunctionalInterface
  public interface DoublePrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, double[] src);
  }

  @FunctionalInterface
  public interface EnumArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Enum<?>[] src);
  }

  @FunctionalInterface
  public interface EnumCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Enum<?>> src);
  }

  @FunctionalInterface
  public interface EnumToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Enum<?> src);
  }

  @FunctionalInterface
  public interface FloatPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, float[] src);
  }

  @FunctionalInterface
  public interface FloatPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, float src);
  }

  @FunctionalInterface
  public interface IntegerPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, int[] src);
  }

  @FunctionalInterface
  public interface LongPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, long[] src);
  }

  @FunctionalInterface
  public interface NumberBasedArrayToBson {
    List<Object> convert(JaxBsonFieldContext fieldCtx, Object[] src);
  }

  @FunctionalInterface
  public interface NumberBasedCollectionToBson {
    List<Object> convert(JaxBsonFieldContext fieldCtx, Collection<Object> src);
  }

  @FunctionalInterface
  public interface NumberBasedToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Object src);
  }

  @FunctionalInterface
  public interface ObjectIdArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, ObjectId[] src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdToBson {
    Object convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ShortPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, short[] src);
  }

  @FunctionalInterface
  public interface ShortPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, short src);
  }

  @FunctionalInterface
  public interface StringBasedArrayToBson {
    List<String> convert(JaxBsonFieldContext fieldCtx, Object[] src);
  }

  @FunctionalInterface
  public interface StringBasedCollectionToBson {
    List<String> convert(JaxBsonFieldContext fieldCtx, Collection<Object> src);
  }

  @FunctionalInterface
  public interface StringBasedToBson {
    String convert(JaxBsonFieldContext fieldCtx, Object src);
  }

  private BinaryCollectionToBson binaryCollectionToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    src.forEach(e -> ret
        .add(fieldCtx.getConfiguration().getToBson().getBinaryToBson().convert(fieldCtx, e)));
    return ret;
  };

  private BinaryToBson binaryToBson = (fieldCtx, src) -> {
    return src;
  };

  private BooleanArrayToBson booleanArrayToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    for (final Boolean e : src) {
      ret.add(fieldCtx.getConfiguration().getToBson().getBooleanToBson().convert(fieldCtx, e));
    }
    return ret;
  };

  private BooleanCollectionToBson booleanCollectionToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    src.forEach(e -> ret
        .add(fieldCtx.getConfiguration().getToBson().getBooleanToBson().convert(fieldCtx, e)));
    return ret;
  };

  private BooleanPrimArrayToBson booleanPrimArrayToBson =
      (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

  private BooleanPrimToBson booleanPrimToBson = (fieldCtx, src) -> {
    return src;
  };

  private BooleanToBson booleanToBson = (fieldCtx, src) -> {
    return src;
  };

  private ByteArrayToBson byteArrayToBson = (fieldCtx, src) -> new Binary(toPrimitiveArray(src));

  private ByteCollectionToBson byteCollectionToBson = (fieldCtx, src) -> {
    final byte[] ret = new byte[src.size()];
    int i = 0;
    for (final Byte e : src) {
      if (e != null) {
        ret[i] = e;
      }
      i++;
    }
    return new Binary(ret);
  };

  private BytePrimArrayToBson bytePrimArrayToBson = (fieldCtx, src) -> new Binary(src);

  private BytePrimToBson bytePrimToBson = (fieldCtx, src) -> new Binary(new byte[] {src});

  private ByteToBson byteToBson = (fieldCtx, src) -> new Binary(new byte[] {src});

  private CharacterArrayToBson characterArrayToBson = (fieldCtx, src) -> {
    return new String(toPrimitiveArray(src));
  };

  private CharacterCollectionToBson characterCollectionToBson = (fieldCtx, src) -> {
    final char[] ret = new char[src.size()];
    int i = 0;
    for (final Character e : src) {
      if (e != null) {
        ret[i] = e;
      }
      i++;
    }
    return new String(ret);
  };

  private CharacterPrimArrayToBson characterPrimArrayToBson = (fieldCtx, src) -> {
    return new String(src);
  };

  private CharacterPrimToBson characterPrimToBson = (fieldCtx, src) -> {
    return Character.valueOf(src).toString();
  };

  private CharacterToBson characterToBson = (fieldCtx, src) -> src.toString();

  private DateBasedArrayToBson dateBasedArrayToBson = (fieldCtx, src) -> {
    final List<Date> ret = new ArrayList<>();
    for (final Object e : src) {
      ret.add(fieldCtx.getConfiguration().getDateBased().getToBson().convert(e));
    }
    return ret;
  };

  private DateBasedCollectionToBson dateBasedCollectionToBson = (fieldCtx, src) -> {
    final List<Date> ret = new ArrayList<>();
    src.forEach(e -> ret.add(fieldCtx.getConfiguration().getDateBased().getToBson().convert(e)));
    return ret;
  };

  private DateBasedToBson dateBasedToBson =
      (fieldCtx, src) -> fieldCtx.getConfiguration().getDateBased().getToBson().convert(src);

  private DoublePrimArrayToBson doublePrimArrayToBson =
      (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

  private EnumArrayToBson enumArrayToBson = (fieldCtx, enumType, src) -> {
    final List<Object> ret = new ArrayList<>();
    for (final Enum<?> e : src) {
      ret.add(
          fieldCtx.getConfiguration().getToBson().getEnumToBson().convert(fieldCtx, enumType, e));
    }
    return ret;
  };

  private EnumCollectionToBson enumCollectionToBson = (fieldCtx, enumType, src) -> {
    final List<Object> ret = new ArrayList<>();
    for (final Enum<?> e : src) {
      ret.add(
          fieldCtx.getConfiguration().getToBson().getEnumToBson().convert(fieldCtx, enumType, e));
    }
    return ret;
  };

  private EnumToBson enumToBson = (fieldCtx, enumType, src) -> fieldCtx.getConfiguration()
      .getIndexedEnum(enumType).enumToString(src);

  private FloatPrimArrayToBson floatPrimArrayToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    for (final Float e : src) {
      ret.add(fieldCtx.getConfiguration().getToBson().getFloatPrimToBson().convert(fieldCtx, e));
    }
    return ret;
  };

  private FloatPrimToBson floatPrimToBson = (fieldCtx, src) -> {
    return Float.valueOf(src).doubleValue();
  };

  private IntegerPrimArrayToBson integerPrimArrayToBson =
      (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

  private LongPrimArrayToBson longPrimArrayToBson =
      (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

  private NumberBasedArrayToBson numberBasedArrayToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    for (final Object e : src) {
      ret.add(
          fieldCtx.getConfiguration().getNumberBased().getToBson().convert(fieldCtx, e).getValue());
    }
    return ret;
  };

  private NumberBasedCollectionToBson numberBasedCollectionToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    src.forEach(e -> ret.add(
        fieldCtx.getConfiguration().getNumberBased().getToBson().convert(fieldCtx, e).getValue()));
    return ret;
  };

  private NumberBasedToBson numberBasedToBson = (fieldCtx, src) -> fieldCtx.getConfiguration()
      .getNumberBased().getToBson().convert(fieldCtx, src).getValue();

  private ObjectIdArrayToBson objectIdArrayToBson = (fieldCtx, src) -> Arrays.asList(src);

  private ObjectIdCollectionToBson objectIdCollectionToBson = (fieldCtx, src) -> src;

  private ObjectIdToBson objectIdToBson = (fieldCtx, src) -> src;

  private ShortPrimArrayToBson shortPrimArrayToBson = (fieldCtx, src) -> {
    final List<Object> ret = new ArrayList<>();
    for (final Short e : src) {
      ret.add(fieldCtx.getConfiguration().getToBson().getShortPrimToBson().convert(fieldCtx, e));
    }
    return ret;
  };

  private ShortPrimToBson shortPrimToBson = (fieldCtx, src) -> Short.valueOf(src).intValue();

  private StringBasedArrayToBson stringBasedArrayToBson = (fieldCtx, src) -> {
    final List<String> ret = new ArrayList<>();
    for (final Object e : src) {
      ret.add(fieldCtx.getConfiguration().getStringBased().getToBson().convert(e));
    }
    return ret;
  };

  private StringBasedCollectionToBson stringBasedCollectionToBson = (fieldCtx, src) -> {
    final List<String> ret = new ArrayList<>();
    src.forEach(e -> ret.add(fieldCtx.getConfiguration().getStringBased().getToBson().convert(e)));
    return ret;
  };

  private StringBasedToBson stringBasedToBson =
      (fieldCtx, src) -> fieldCtx.getConfiguration().getStringBased().getToBson().convert(src);

  public BinaryCollectionToBson getBinaryCollectionToBson() {
    return this.binaryCollectionToBson;
  }

  public BinaryToBson getBinaryToBson() {
    return this.binaryToBson;
  }

  public BooleanArrayToBson getBooleanArrayToBson() {
    return this.booleanArrayToBson;
  }

  public BooleanCollectionToBson getBooleanCollectionToBson() {
    return this.booleanCollectionToBson;
  }

  public BooleanPrimArrayToBson getBooleanPrimArrayToBson() {
    return this.booleanPrimArrayToBson;
  }

  public BooleanPrimToBson getBooleanPrimToBson() {
    return this.booleanPrimToBson;
  }

  public BooleanToBson getBooleanToBson() {
    return this.booleanToBson;
  }

  public ByteArrayToBson getByteArrayToBson() {
    return this.byteArrayToBson;
  }

  public ByteCollectionToBson getByteCollectionToBson() {
    return this.byteCollectionToBson;
  }

  public BytePrimArrayToBson getBytePrimArrayToBson() {
    return this.bytePrimArrayToBson;
  }

  public BytePrimToBson getBytePrimToBson() {
    return this.bytePrimToBson;
  }

  public ByteToBson getByteToBson() {
    return this.byteToBson;
  }

  public CharacterArrayToBson getCharacterArrayToBson() {
    return this.characterArrayToBson;
  }

  public CharacterCollectionToBson getCharacterCollectionToBson() {
    return this.characterCollectionToBson;
  }

  public CharacterPrimArrayToBson getCharacterPrimArrayToBson() {
    return this.characterPrimArrayToBson;
  }

  public CharacterPrimToBson getCharacterPrimToBson() {
    return this.characterPrimToBson;
  }

  public CharacterToBson getCharacterToBson() {
    return this.characterToBson;
  }

  public DateBasedArrayToBson getDateBasedArrayToBson() {
    return this.dateBasedArrayToBson;
  }

  public DateBasedCollectionToBson getDateBasedCollectionToBson() {
    return this.dateBasedCollectionToBson;
  }

  public DateBasedToBson getDateBasedToBson() {
    return this.dateBasedToBson;
  }

  public DoublePrimArrayToBson getDoublePrimArrayToBson() {
    return this.doublePrimArrayToBson;
  }

  public EnumArrayToBson getEnumArrayToBson() {
    return this.enumArrayToBson;
  }

  public EnumCollectionToBson getEnumCollectionToBson() {
    return this.enumCollectionToBson;
  }

  public EnumToBson getEnumToBson() {
    return this.enumToBson;
  }

  public FloatPrimArrayToBson getFloatPrimArrayToBson() {
    return this.floatPrimArrayToBson;
  }

  public FloatPrimToBson getFloatPrimToBson() {
    return this.floatPrimToBson;
  }

  public IntegerPrimArrayToBson getIntegerPrimArrayToBson() {
    return this.integerPrimArrayToBson;
  }

  public LongPrimArrayToBson getLongPrimArrayToBson() {
    return this.longPrimArrayToBson;
  }

  public NumberBasedArrayToBson getNumberBasedArrayToBson() {
    return this.numberBasedArrayToBson;
  }

  public NumberBasedCollectionToBson getNumberBasedCollectionToBson() {
    return this.numberBasedCollectionToBson;
  }

  public NumberBasedToBson getNumberBasedToBson() {
    return this.numberBasedToBson;
  }

  public ObjectIdArrayToBson getObjectIdArrayToBson() {
    return this.objectIdArrayToBson;
  }

  public ObjectIdCollectionToBson getObjectIdCollectionToBson() {
    return this.objectIdCollectionToBson;
  }

  public ObjectIdToBson getObjectIdToBson() {
    return this.objectIdToBson;
  }

  public ShortPrimArrayToBson getShortPrimArrayToBson() {
    return this.shortPrimArrayToBson;
  }

  public ShortPrimToBson getShortPrimToBson() {
    return this.shortPrimToBson;
  }

  public StringBasedArrayToBson getStringBasedArrayToBson() {
    return this.stringBasedArrayToBson;
  }

  public StringBasedCollectionToBson getStringBasedCollectionToBson() {
    return this.stringBasedCollectionToBson;
  }

  public StringBasedToBson getStringBasedToBson() {
    return this.stringBasedToBson;
  }

  public void setBinaryCollectionToBson(final BinaryCollectionToBson binaryCollectionToBson) {
    this.binaryCollectionToBson = Objects.requireNonNull(binaryCollectionToBson);
  }

  public void setBinaryToBson(final BinaryToBson binaryToBson) {
    this.binaryToBson = Objects.requireNonNull(binaryToBson);
  }

  public void setBooleanArrayToBson(final BooleanArrayToBson booleanArrayToBson) {
    this.booleanArrayToBson = Objects.requireNonNull(booleanArrayToBson);
  }

  public void setBooleanCollectionToBson(final BooleanCollectionToBson booleanCollectionToBson) {
    this.booleanCollectionToBson = Objects.requireNonNull(booleanCollectionToBson);
  }

  public void setBooleanPrimArrayToBson(final BooleanPrimArrayToBson booleanPrimArrayToBson) {
    this.booleanPrimArrayToBson = Objects.requireNonNull(booleanPrimArrayToBson);
  }

  public void setBooleanPrimToBson(final BooleanPrimToBson booleanPrimToBson) {
    this.booleanPrimToBson = Objects.requireNonNull(booleanPrimToBson);
  }

  public void setBooleanToBson(final BooleanToBson booleanToBson) {
    this.booleanToBson = Objects.requireNonNull(booleanToBson);
  }

  public void setByteArrayToBson(final ByteArrayToBson byteArrayToBson) {
    this.byteArrayToBson = Objects.requireNonNull(byteArrayToBson);
  }

  public void setByteCollectionToBson(final ByteCollectionToBson byteCollectionToBson) {
    this.byteCollectionToBson = Objects.requireNonNull(byteCollectionToBson);
  }

  public void setBytePrimArrayToBson(final BytePrimArrayToBson bytePrimArrayToBson) {
    this.bytePrimArrayToBson = Objects.requireNonNull(bytePrimArrayToBson);
  }

  public void setBytePrimToBson(final BytePrimToBson bytePrimToBson) {
    this.bytePrimToBson = Objects.requireNonNull(bytePrimToBson);
  }

  public void setByteToBson(final ByteToBson byteToBson) {
    this.byteToBson = Objects.requireNonNull(byteToBson);
  }

  public void setCharacterArrayToBson(final CharacterArrayToBson characterArrayToBson) {
    this.characterArrayToBson = Objects.requireNonNull(characterArrayToBson);
  }

  public void setCharacterCollectionToBson(
      final CharacterCollectionToBson characterCollectionToBson) {
    this.characterCollectionToBson = Objects.requireNonNull(characterCollectionToBson);
  }

  public void setCharacterPrimArrayToBson(final CharacterPrimArrayToBson characterPrimArrayToBson) {
    this.characterPrimArrayToBson = Objects.requireNonNull(characterPrimArrayToBson);
  }

  public void setCharacterPrimToBson(final CharacterPrimToBson characterPrimToBson) {
    this.characterPrimToBson = Objects.requireNonNull(characterPrimToBson);
  }

  public void setCharacterToBson(final CharacterToBson characterToBson) {
    this.characterToBson = Objects.requireNonNull(characterToBson);
  }

  public void setDateBasedArrayToBson(final DateBasedArrayToBson dateBasedArrayToBson) {
    this.dateBasedArrayToBson = Objects.requireNonNull(dateBasedArrayToBson);
  }

  public void setDateBasedCollectionToBson(
      final DateBasedCollectionToBson dateBasedCollectionToBson) {
    this.dateBasedCollectionToBson = Objects.requireNonNull(dateBasedCollectionToBson);
  }

  public void setDateBasedToBson(final DateBasedToBson dateBasedToBson) {
    this.dateBasedToBson = Objects.requireNonNull(dateBasedToBson);
  }

  public void setDoublePrimArrayToBson(final DoublePrimArrayToBson doublePrimArrayToBson) {
    this.doublePrimArrayToBson = Objects.requireNonNull(doublePrimArrayToBson);
  }

  public void setEnumArrayToBson(final EnumArrayToBson enumArrayToBson) {
    this.enumArrayToBson = Objects.requireNonNull(enumArrayToBson);
  }

  public void setEnumCollectionToBson(final EnumCollectionToBson enumCollectionToBson) {
    this.enumCollectionToBson = Objects.requireNonNull(enumCollectionToBson);
  }

  public void setEnumToBson(final EnumToBson enumToBson) {
    this.enumToBson = Objects.requireNonNull(enumToBson);
  }

  public void setFloatPrimArrayToBson(final FloatPrimArrayToBson floatPrimArrayToBson) {
    this.floatPrimArrayToBson = Objects.requireNonNull(floatPrimArrayToBson);
  }

  public void setFloatPrimToBson(final FloatPrimToBson floatPrimToBson) {
    this.floatPrimToBson = Objects.requireNonNull(floatPrimToBson);
  }

  public void setIntegerPrimArrayToBson(final IntegerPrimArrayToBson integerPrimArrayToBson) {
    this.integerPrimArrayToBson = Objects.requireNonNull(integerPrimArrayToBson);
  }

  public void setLongPrimArrayToBson(final LongPrimArrayToBson longPrimArrayToBson) {
    this.longPrimArrayToBson = Objects.requireNonNull(longPrimArrayToBson);
  }

  public void setNumberBasedArrayToBson(final NumberBasedArrayToBson numberBasedArrayToBson) {
    this.numberBasedArrayToBson = Objects.requireNonNull(numberBasedArrayToBson);
  }

  public void setNumberBasedCollectionToBson(
      final NumberBasedCollectionToBson numberBasedCollectionToBson) {
    this.numberBasedCollectionToBson = Objects.requireNonNull(numberBasedCollectionToBson);
  }

  public void setNumberBasedToBson(final NumberBasedToBson numberBasedToBson) {
    this.numberBasedToBson = Objects.requireNonNull(numberBasedToBson);
  }

  public void setObjectIdArrayToBson(final ObjectIdArrayToBson objectIdArrayToBson) {
    this.objectIdArrayToBson = Objects.requireNonNull(objectIdArrayToBson);
  }

  public void setObjectIdCollectionToBson(final ObjectIdCollectionToBson objectIdCollectionToBson) {
    this.objectIdCollectionToBson = Objects.requireNonNull(objectIdCollectionToBson);
  }

  public void setObjectIdToBson(final ObjectIdToBson objectIdToBson) {
    this.objectIdToBson = Objects.requireNonNull(objectIdToBson);
  }

  public void setShortPrimArrayToBson(final ShortPrimArrayToBson shortPrimArrayToBson) {
    this.shortPrimArrayToBson = Objects.requireNonNull(shortPrimArrayToBson);
  }

  public void setShortPrimToBson(final ShortPrimToBson shortPrimToBson) {
    this.shortPrimToBson = Objects.requireNonNull(shortPrimToBson);
  }

  public void setStringBasedArrayToBson(final StringBasedArrayToBson stringBasedArrayToBson) {
    this.stringBasedArrayToBson = Objects.requireNonNull(stringBasedArrayToBson);
  }

  public void setStringBasedCollectionToBson(
      final StringBasedCollectionToBson stringBasedCollectionToBson) {
    this.stringBasedCollectionToBson = Objects.requireNonNull(stringBasedCollectionToBson);
  }

  public void setStringBasedToBson(final StringBasedToBson stringBasedToBson) {
    this.stringBasedToBson = Objects.requireNonNull(stringBasedToBson);
  }
}
