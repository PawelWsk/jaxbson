package org.qnixyz.jbson;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPost;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.cfg.JaxBsonIgnoreTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameTypeMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNumberHintFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPostMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPreMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPostMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPreMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingsFieldMap;
import org.qnixyz.jbson.impl.NumberXmlAdapter;

public class JaxBsonConfiguration {

  public static final boolean DEFAULT_ALLOW_SMART_TYPE_FIELD = false;

  public static final boolean DEFAULT_BOOLEAN_PRIM_BOOLEAN_NULL = false;

  public static final Byte DEFAULT_BYTE_BOOLEAN_BOOLEAN_NULL = null;

  public static final byte DEFAULT_BYTE_PRIM_BOOLEAN_FALSE = 0x0;

  public static final byte DEFAULT_BYTE_PRIM_BOOLEAN_NULL = 0x0;

  public static final byte DEFAULT_BYTE_PRIM_BOOLEAN_TRUE = 0x1;

  public static final byte DEFAULT_BYTE_PRIM_NULL = 0x0;

  public static final char DEFAULT_CHAR_PRIM_BOOLEAN_FALSE = '-';

  public static final char DEFAULT_CHAR_PRIM_BOOLEAN_TRUE = '+';

  public static final char DEFAULT_CHAR_PRIM_NULL = ' ';

  public static final Character DEFAULT_CHARACTER_BOOLEAN_NULL = null;

  public static final char DEFAULT_CHARACTER_PRIM_BOOLEAN_NULL = 0x0;

  public static final Number DEFAULT_NUMBER_BOOLEAN_FALSE = 0;

  public static final Number DEFAULT_NUMBER_BOOLEAN_NULL = null;

  public static final Number DEFAULT_NUMBER_BOOLEAN_TRUE = 1;

  public static final Number DEFAULT_NUMBER_PRIM_BOOLEAN_NULL = 0;

  public static final Number DEFAULT_NUMBER_PRIM_NULL = 0;

  public static final String DEFAULT_STRING_BOOLEAN_FALSE = Boolean.FALSE.toString();

  public static final String DEFAULT_STRING_BOOLEAN_NULL = null;

  public static final String DEFAULT_STRING_BOOLEAN_TRUE = Boolean.TRUE.toString();

  public static final String DEFAULT_TYPE_FIELD_NAME = "type";

  public static final String DEFAULT_XML_VALUE_FIELD_NAME = "_";

  private boolean allowSmartTypeField = DEFAULT_ALLOW_SMART_TYPE_FIELD;

  @JaxBsonTransient
  private DateBased dateBased = new DateBased();

  @JaxBsonName(name = "_id")
  private String id = UUID.randomUUID().toString();

  @JaxBsonTransient
  private final Map<Class<?>, IndexedEnum> indexedEnumMap = new HashMap<>();

  @JaxBsonJavaTypeAdapter(JaxBsonIgnoreTransientFieldMap.XmlAdapter.class)
  private JaxBsonIgnoreTransientFieldMap jaxBsonIgnoreTransientFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonNameFieldMap.XmlAdapter.class)
  private JaxBsonNameFieldMap jaxBsonNameFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonNameTypeMap.XmlAdapter.class)
  private JaxBsonNameTypeMap jaxBsonNameTypeMap;

  @JaxBsonJavaTypeAdapter(JaxBsonNumberHintFieldMap.XmlAdapter.class)
  private JaxBsonNumberHintFieldMap jaxBsonNumberHintFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToBsonPostMethodMap.XmlAdapter.class)
  private JaxBsonToBsonPostMethodMap jaxBsonToBsonPostMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToBsonPreMethodMap.XmlAdapter.class)
  private JaxBsonToBsonPreMethodMap jaxBsonToBsonPreMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToObjectPostMethodMap.XmlAdapter.class)
  private JaxBsonToObjectPostMethodMap jaxBsonToObjectPostMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToObjectPreMethodMap.XmlAdapter.class)
  private JaxBsonToObjectPreMethodMap jaxBsonToObjectPreMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonTransientFieldMap.XmlAdapter.class)
  private JaxBsonTransientFieldMap jaxBsonTransientFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonXmlAnyAttributeMappingFieldMap.XmlAdapter.class)
  private JaxBsonXmlAnyAttributeMappingFieldMap jaxBsonXmlAnyAttributeMappingFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonXmlAnyAttributeMappingsFieldMap.XmlAdapter.class)
  private JaxBsonXmlAnyAttributeMappingsFieldMap jaxBsonXmlAnyAttributeMappingsFieldMap;

  @JaxBsonTransient
  private NumberBased numberBased = new NumberBased();

  @JaxBsonTransient
  private StringBased stringBased = new StringBased();

  @JaxBsonTransient
  private JaxBsonToBson toBson = new JaxBsonToBson();

  @JaxBsonTransient
  private JaxBsonToObject toObject = new JaxBsonToObject();

  private String typeFieldName = DEFAULT_TYPE_FIELD_NAME;

  private boolean valueBooleanPrimBooleanNull = DEFAULT_BOOLEAN_PRIM_BOOLEAN_NULL;

  private byte valueByteBooleanFalse = DEFAULT_BYTE_PRIM_BOOLEAN_FALSE;

  private Byte valueByteBooleanNull = DEFAULT_BYTE_BOOLEAN_BOOLEAN_NULL;

  private byte valueByteBooleanTrue = DEFAULT_BYTE_PRIM_BOOLEAN_TRUE;

  private byte valueBytePrimBooleanNull = DEFAULT_BYTE_PRIM_BOOLEAN_NULL;

  private byte valueBytePrimNull = DEFAULT_BYTE_PRIM_NULL;

  private char valueCharacterBooleanFalse = DEFAULT_CHAR_PRIM_BOOLEAN_FALSE;

  private Character valueCharacterBooleanNull = DEFAULT_CHARACTER_BOOLEAN_NULL;

  private char valueCharacterBooleanTrue = DEFAULT_CHAR_PRIM_BOOLEAN_TRUE;

  private char valueCharacterPrimBooleanNull = DEFAULT_CHARACTER_PRIM_BOOLEAN_NULL;

  private char valueCharacterPrimNull = DEFAULT_CHAR_PRIM_NULL;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private Number valueNumberBooleanFalse = DEFAULT_NUMBER_BOOLEAN_FALSE;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private Number valueNumberBooleanNull = DEFAULT_NUMBER_BOOLEAN_NULL;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private Number valueNumberBooleanTrue = DEFAULT_NUMBER_BOOLEAN_TRUE;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private Number valueNumberPrimBooleanNull = DEFAULT_NUMBER_PRIM_BOOLEAN_NULL;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private Number valueNumberPrimNull = DEFAULT_NUMBER_PRIM_NULL;

  private String valueStringBooleanFalse = DEFAULT_STRING_BOOLEAN_FALSE;

  private String valueStringBooleanNull = DEFAULT_STRING_BOOLEAN_NULL;

  private String valueStringBooleanTrue = DEFAULT_STRING_BOOLEAN_TRUE;

  private String xmlValueFieldName = DEFAULT_XML_VALUE_FIELD_NAME;

  public DateBased getDateBased() {
    return this.dateBased;
  }

  public String getId() {
    return this.id;
  }

  public IndexedEnum getIndexedEnum(final Class<?> type) {
    IndexedEnum ret = this.indexedEnumMap.get(type);
    if (!this.indexedEnumMap.containsKey(type)) {
      ret = new IndexedEnum(type);
      this.indexedEnumMap.put(type, ret);
    }
    return ret;
  }

  public JaxBsonIgnoreTransient getJaxBsonIgnoreTransient(final Field field) {
    Objects.requireNonNull(field);
    if (this.jaxBsonIgnoreTransientFieldMap == null) {
      return null;
    }
    return this.jaxBsonIgnoreTransientFieldMap.get(field);
  }

  public JaxBsonIgnoreTransientFieldMap getJaxBsonIgnoreTransientFieldMap() {
    return this.jaxBsonIgnoreTransientFieldMap;
  }

  public JaxBsonName getJaxBsonName(final Class<?> type) {
    Objects.requireNonNull(type);
    if (this.jaxBsonNameTypeMap == null) {
      return null;
    }
    return this.jaxBsonNameTypeMap.get(type);
  }

  public JaxBsonName getJaxBsonName(final Field field) {
    Objects.requireNonNull(field);
    if (this.jaxBsonNameFieldMap == null) {
      return null;
    }
    return this.jaxBsonNameFieldMap.get(field);
  }

  public JaxBsonNameFieldMap getJaxBsonNameFieldMap() {
    return this.jaxBsonNameFieldMap;
  }

  public JaxBsonNameTypeMap getJaxBsonNameTypeMap() {
    return this.jaxBsonNameTypeMap;
  }

  public JaxBsonNumberHint getJaxBsonNumberHint(final Field field) {
    Objects.requireNonNull(field);
    if (this.jaxBsonNumberHintFieldMap == null) {
      return null;
    }
    return this.jaxBsonNumberHintFieldMap.get(field);
  }

  public JaxBsonNumberHintFieldMap getJaxBsonNumberHintFieldMap() {
    return this.jaxBsonNumberHintFieldMap;
  }

  public JaxBsonToBsonPost getJaxBsonToBsonPost(final Method method) {
    Objects.requireNonNull(method);
    if (this.jaxBsonToBsonPostMethodMap == null) {
      return null;
    }
    return this.jaxBsonToBsonPostMethodMap.get(method);
  }

  public JaxBsonToBsonPostMethodMap getJaxBsonToBsonPostMethodMap() {
    return this.jaxBsonToBsonPostMethodMap;
  }

  public JaxBsonToBsonPre getJaxBsonToBsonPre(final Method method) {
    Objects.requireNonNull(method);
    if (this.jaxBsonToBsonPreMethodMap == null) {
      return null;
    }
    return this.jaxBsonToBsonPreMethodMap.get(method);
  }

  public JaxBsonToBsonPreMethodMap getJaxBsonToBsonPreMethodMap() {
    return this.jaxBsonToBsonPreMethodMap;
  }

  public JaxBsonToObjectPost getJaxBsonToObjectPost(final Method method) {
    Objects.requireNonNull(method);
    if (this.jaxBsonToObjectPostMethodMap == null) {
      return null;
    }
    return this.jaxBsonToObjectPostMethodMap.get(method);
  }

  public JaxBsonToObjectPostMethodMap getJaxBsonToObjectPostMethodMap() {
    return this.jaxBsonToObjectPostMethodMap;
  }

  public JaxBsonToObjectPre getJaxBsonToObjectPre(final Method method) {
    Objects.requireNonNull(method);
    if (this.jaxBsonToObjectPreMethodMap == null) {
      return null;
    }
    return this.jaxBsonToObjectPreMethodMap.get(method);
  }

  public JaxBsonToObjectPreMethodMap getJaxBsonToObjectPreMethodMap() {
    return this.jaxBsonToObjectPreMethodMap;
  }

  public JaxBsonTransient getJaxBsonTransient(final Field field) {
    Objects.requireNonNull(field);
    if (this.jaxBsonTransientFieldMap == null) {
      return null;
    }
    return this.jaxBsonTransientFieldMap.get(field);
  }

  public JaxBsonTransientFieldMap getJaxBsonTransientFieldMap() {
    return this.jaxBsonTransientFieldMap;
  }

  public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMapping(final Field field) {
    Objects.requireNonNull(field);
    if (this.jaxBsonXmlAnyAttributeMappingFieldMap == null) {
      return null;
    }
    return this.jaxBsonXmlAnyAttributeMappingFieldMap.get(field);
  }

  public JaxBsonXmlAnyAttributeMappingFieldMap getJaxBsonXmlAnyAttributeMappingFieldMap() {
    return this.jaxBsonXmlAnyAttributeMappingFieldMap;
  }

  public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappings(final Field field) {
    Objects.requireNonNull(field);
    if (this.jaxBsonXmlAnyAttributeMappingsFieldMap == null) {
      return null;
    }
    return this.jaxBsonXmlAnyAttributeMappingsFieldMap.get(field);
  }

  public JaxBsonXmlAnyAttributeMappingsFieldMap getJaxBsonXmlAnyAttributeMappingsFieldMap() {
    return this.jaxBsonXmlAnyAttributeMappingsFieldMap;
  }

  public NumberBased getNumberBased() {
    return this.numberBased;
  }

  public StringBased getStringBased() {
    return this.stringBased;
  }

  public JaxBsonToBson getToBson() {
    return this.toBson;
  }

  public JaxBsonToObject getToObject() {
    return this.toObject;
  }

  public String getTypeFieldName() {
    return this.typeFieldName;
  }

  public byte getValueByteBooleanFalse() {
    return this.valueByteBooleanFalse;
  }

  public Byte getValueByteBooleanNull() {
    return this.valueByteBooleanNull;
  }

  public byte getValueByteBooleanTrue() {
    return this.valueByteBooleanTrue;
  }

  public byte getValueBytePrimBooleanNull() {
    return this.valueBytePrimBooleanNull;
  }

  public byte getValueBytePrimNull() {
    return this.valueBytePrimNull;
  }

  public char getValueCharacterBooleanFalse() {
    return this.valueCharacterBooleanFalse;
  }

  public Character getValueCharacterBooleanNull() {
    return this.valueCharacterBooleanNull;
  }

  public char getValueCharacterBooleanTrue() {
    return this.valueCharacterBooleanTrue;
  }

  public char getValueCharacterPrimBooleanNull() {
    return this.valueCharacterPrimBooleanNull;
  }

  public char getValueCharacterPrimNull() {
    return this.valueCharacterPrimNull;
  }

  public Number getValueNumberBooleanFalse() {
    return this.valueNumberBooleanFalse;
  }

  public Number getValueNumberBooleanNull() {
    return this.valueNumberBooleanNull;
  }

  public Number getValueNumberBooleanTrue() {
    return this.valueNumberBooleanTrue;
  }

  public Number getValueNumberPrimBooleanNull() {
    return this.valueNumberPrimBooleanNull;
  }

  public Number getValueNumberPrimNull() {
    return this.valueNumberPrimNull;
  }

  public String getValueStringBooleanFalse() {
    return this.valueStringBooleanFalse;
  }

  public String getValueStringBooleanNull() {
    return this.valueStringBooleanNull;
  }

  public String getValueStringBooleanTrue() {
    return this.valueStringBooleanTrue;
  }

  public String getXmlValueFieldName() {
    return this.xmlValueFieldName;
  }

  public boolean isAllowSmartTypeField() {
    return this.allowSmartTypeField;
  }

  public boolean isValueBooleanPrimBooleanNull() {
    return this.valueBooleanPrimBooleanNull;
  }

  public JaxBsonConfiguration setAllowSmartTypeField(final boolean allowSmartTypeField) {
    this.allowSmartTypeField = allowSmartTypeField;
    return this;
  }

  public JaxBsonConfiguration setDateBased(final DateBased dateBased) {
    this.dateBased = dateBased;
    return this;
  }

  public JaxBsonConfiguration setId(final String id) {
    this.id = Objects.requireNonNull(id);
    return this;
  }

  public JaxBsonConfiguration setJaxBsonIgnoreTransientFieldMap(
      final JaxBsonIgnoreTransientFieldMap jaxBsonIgnoreTransientFieldMap) {
    this.jaxBsonIgnoreTransientFieldMap = jaxBsonIgnoreTransientFieldMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonNameFieldMap(
      final JaxBsonNameFieldMap jaxBsonNameFieldMap) {
    this.jaxBsonNameFieldMap = jaxBsonNameFieldMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonNameTypeMap(final JaxBsonNameTypeMap jaxBsonNameTypeMap) {
    this.jaxBsonNameTypeMap = jaxBsonNameTypeMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonNumberHintFieldMap(
      final JaxBsonNumberHintFieldMap jaxBsonNumberHintFieldMap) {
    this.jaxBsonNumberHintFieldMap = jaxBsonNumberHintFieldMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonToBsonPostMethodMap(
      final JaxBsonToBsonPostMethodMap jaxBsonToBsonPostMethodMap) {
    this.jaxBsonToBsonPostMethodMap = jaxBsonToBsonPostMethodMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonToBsonPreMethodMap(
      final JaxBsonToBsonPreMethodMap jaxBsonToBsonPreMethodMap) {
    this.jaxBsonToBsonPreMethodMap = jaxBsonToBsonPreMethodMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonToObjectPostMethodMap(
      final JaxBsonToObjectPostMethodMap jaxBsonToObjectPostMethodMap) {
    this.jaxBsonToObjectPostMethodMap = jaxBsonToObjectPostMethodMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonToObjectPreMethodMap(
      final JaxBsonToObjectPreMethodMap jaxBsonToObjectPreMethodMap) {
    this.jaxBsonToObjectPreMethodMap = jaxBsonToObjectPreMethodMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonTransientFieldMap(
      final JaxBsonTransientFieldMap jaxBsonTransientFieldMap) {
    this.jaxBsonTransientFieldMap = jaxBsonTransientFieldMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonXmlAnyAttributeMappingFieldMap(
      final JaxBsonXmlAnyAttributeMappingFieldMap jaxBsonXmlAnyAttributeMappingFieldMap) {
    this.jaxBsonXmlAnyAttributeMappingFieldMap = jaxBsonXmlAnyAttributeMappingFieldMap;
    return this;
  }

  public JaxBsonConfiguration setJaxBsonXmlAnyAttributeMappingsFieldMap(
      final JaxBsonXmlAnyAttributeMappingsFieldMap jaxBsonXmlAnyAttributeMappingsFieldMap) {
    this.jaxBsonXmlAnyAttributeMappingsFieldMap = jaxBsonXmlAnyAttributeMappingsFieldMap;
    return this;
  }

  public JaxBsonConfiguration setNumberBased(final NumberBased numberBased) {
    this.numberBased = numberBased;
    return this;
  }

  public JaxBsonConfiguration setStringBased(final StringBased stringBased) {
    this.stringBased = stringBased;
    return this;
  }

  public JaxBsonConfiguration setToBson(final JaxBsonToBson toBson) {
    this.toBson = Objects.requireNonNull(toBson);
    return this;
  }

  public JaxBsonConfiguration setToObject(final JaxBsonToObject toObject) {
    this.toObject = Objects.requireNonNull(toObject);
    return this;
  }

  public JaxBsonConfiguration setTypeFieldName(final String typeFieldName) {
    this.typeFieldName = Objects.requireNonNull(typeFieldName);
    return this;
  }

  public JaxBsonConfiguration setValueBooleanPrimBooleanNull(
      final boolean valueBooleanPrimBooleanNull) {
    this.valueBooleanPrimBooleanNull = valueBooleanPrimBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueByteBooleanFalse(final byte valueByteBooleanFalse) {
    this.valueByteBooleanFalse = valueByteBooleanFalse;
    return this;
  }

  public JaxBsonConfiguration setValueByteBooleanNull(final Byte valueByteBooleanNull) {
    this.valueByteBooleanNull = valueByteBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueByteBooleanTrue(final byte valueByteBooleanTrue) {
    this.valueByteBooleanTrue = valueByteBooleanTrue;
    return this;
  }

  public JaxBsonConfiguration setValueBytePrimBooleanNull(final byte valueBytePrimBooleanNull) {
    this.valueBytePrimBooleanNull = valueBytePrimBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueBytePrimNull(final byte valueBytePrimNull) {
    this.valueBytePrimNull = valueBytePrimNull;
    return this;
  }

  public JaxBsonConfiguration setValueCharacterBooleanFalse(final char valueCharacterBooleanFalse) {
    this.valueCharacterBooleanFalse = valueCharacterBooleanFalse;
    return this;
  }

  public JaxBsonConfiguration setValueCharacterBooleanNull(
      final Character valueCharacterBooleanNull) {
    this.valueCharacterBooleanNull = valueCharacterBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueCharacterBooleanTrue(final char valueCharacterBooleanTrue) {
    this.valueCharacterBooleanTrue = valueCharacterBooleanTrue;
    return this;
  }

  public JaxBsonConfiguration setValueCharacterPrimBooleanNull(
      final char valueCharacterPrimBooleanNull) {
    this.valueCharacterPrimBooleanNull = valueCharacterPrimBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueCharacterPrimNull(final char valueCharacterPrimNull) {
    this.valueCharacterPrimNull = valueCharacterPrimNull;
    return this;
  }

  public JaxBsonConfiguration setValueNumberBooleanFalse(final Number valueNumberBooleanFalse) {
    this.valueNumberBooleanFalse = Objects.requireNonNull(valueNumberBooleanFalse,
        "Supplied parameter 'valueNumberBooleanFalse' is false");
    return this;
  }

  public JaxBsonConfiguration setValueNumberBooleanNull(final Number valueNumberBooleanNull) {
    this.valueNumberBooleanNull = valueNumberBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueNumberBooleanTrue(final Number valueNumberBooleanTrue) {
    this.valueNumberBooleanTrue = Objects.requireNonNull(valueNumberBooleanTrue,
        "Supplied parameter 'valueNumberBooleanTrue' is false");
    return this;
  }

  public JaxBsonConfiguration setValueNumberPrimBooleanNull(
      final Number valueNumberPrimBooleanNull) {
    this.valueNumberPrimBooleanNull = Objects.requireNonNull(valueNumberPrimBooleanNull);
    return this;
  }

  public JaxBsonConfiguration setValueNumberPrimNull(final Number valueNumberPrimNull) {
    this.valueNumberPrimNull = Objects.requireNonNull(valueNumberPrimNull);
    return this;
  }

  public JaxBsonConfiguration setValueStringBooleanFalse(final String valueStringBooleanFalse) {
    if (isBlank(valueStringBooleanFalse)) {
      throw new IllegalArgumentException("Supplied parameter 'valueStringBooleanFalse' is blank");
    }
    this.valueStringBooleanFalse = valueStringBooleanFalse;
    return this;
  }

  public JaxBsonConfiguration setValueStringBooleanNull(final String valueStringBooleanNull) {
    this.valueStringBooleanNull = valueStringBooleanNull;
    return this;
  }

  public JaxBsonConfiguration setValueStringBooleanTrue(final String valueStringBooleanTrue) {
    if (isBlank(valueStringBooleanTrue)) {
      throw new IllegalArgumentException("Supplied parameter 'valueStringBooleanTrue' is blank");
    }
    this.valueStringBooleanTrue = valueStringBooleanTrue;
    return this;
  }

  public JaxBsonConfiguration setXmlValueFieldName(final String xmlValueFieldName) {
    this.xmlValueFieldName = Objects.requireNonNull(xmlValueFieldName);
    return this;
  }
}
